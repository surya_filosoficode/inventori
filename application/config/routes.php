<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


#-----------------------------autentication---------------------------
	$route['login'] 		= 'login/loginv0';
	$route['logout'] 		= 'login/logout';
#-----------------------------autentication---------------------------


#-----------------------------master----------------------------------
	$route['admin/data_admin'] 		= 'admin/adminmain';
	$route['admin/data_suplier'] 	= 'admin/supliermain';
	$route['admin/data_rekanan'] 	= 'admin/rekananmain';
	$route['admin/data_sales'] 		= 'admin/salesmain';

	$route['admin/data_jenis_item'] = 'admin/itemmain/index_jenis';
	$route['admin/data_item'] 		= 'admin/itemmain/index';

	$route['admin/setting_main'] 		= 'admin/settingmain/index';
#-----------------------------master----------------------------------


#-----------------------------penjualan-------------------------------
	$route['admin/data_penjualan'] 			= 'admin/penjualanmain/index';
	$route['admin/list_penjualan'] 			= 'admin/penjualanmain/index_list_penjualan';
	$route['admin/read_penjualan/(:any)'] 	= 'admin/penjualanmain/index_read_penjualan/$1';
#-----------------------------penjualan-------------------------------


#-----------------------------pembelian-------------------------------
	$route['admin/data_pembelian'] 			= 'admin/pembelianmain/index';
	$route['admin/list_pembelian'] 			= 'admin/pembelianmain/index_list_pembelian';
	$route['admin/read_pembelian/(:any)'] 	= 'admin/pembelianmain/index_read_pembelian/$1';
#-----------------------------pembelian-------------------------------


#-----------------------------sample----------------------------------
	$route['admin/data_sample'] 	= 'admin/samplemain/index';
	$route['admin/list_sample'] 	= 'admin/samplemain/index_list';
	$route['admin/read_sample/(:any)'] 	= 'admin/samplemain/index_read/$1';
#-----------------------------sample----------------------------------

#-----------------------------retur_penjualan-------------------------
	$route['admin/data_retur_penjualan'] 	= 'admin/returpenjualanmain/index';
	$route['admin/list_retur_penjualan'] 	= 'admin/returpenjualanmain/index_list_retur_penjualan';
	$route['admin/read_retur_penjualan/(:any)'] 	= 'admin/returpenjualanmain/index_read_retur_penjualan/$1';
#-----------------------------retur_penjualan-------------------------

#-----------------------------retur_pembelian-------------------------
	$route['admin/data_retur_pembelian'] 	= 'admin/returpembelianmain/index';
	$route['admin/list_retur_pembelian'] 	= 'admin/returpembelianmain/index_list_retur_pembelian';
	$route['admin/read_retur_pembelian'] 	= 'admin/returpembelianmain/index_read_retur_pembelian';

	$route['admin/data_retur_pembelian_rusak'] 	= 'admin/returpembelianmain/index_retur_pembelian_rusak';
#-----------------------------retur_pembelian-------------------------

#-----------------------------retur_sample-------------------------
	$route['admin/data_retur_sample'] 	= 'admin/retursamplemain/index';
	$route['admin/list_retur_sample'] 	= 'admin/retursamplemain/index';
	$route['admin/read_retur_sample'] 	= 'admin/retursamplemain/index';
#-----------------------------retur_sample-------------------------




$route['admin/data_sample'] 	= 'admin/samplemain';


$route['admin/laporan_penjualan'] 		= 'report/reportpenjualanmain/index';
$route['admin/laporan_retur_penjualan'] = 'report/reportreturpenjualanmain/index';
$route['admin/laporan_piutang'] 		= 'report/reportpiutangmain/index';
$route['admin/laporan_sales'] 			= 'report/reportsalesmain/index';
// $route['admin/laporan_hasil_sales'] 	= '';

$route['admin/laporan_pembelian'] 		= 'report/reportpembelianmain/index';
$route['admin/laporan_retur_pembelian'] = 'report/reportreturpembelianmain/index';
$route['admin/laporan_hutang'] 			= 'report/reporthutangmain/index';

$route['admin/laporan_arus_barang'] 	= 'report/reportbpom/laporan_arus_barang';
$route['admin/laporan_format_bpom'] 	= 'report/reportbpom/index';
$route['admin/laporan_pendapatan_sales']= 'report/reportsaleshasil/index';


$route['admin/laporan_arus_kas'] 	= 'report/reportaruskasmain/index';
$route['admin/laporan_laba_rugi'] 	= 'report/reportlabarugimain/index';

$route['admin/laporan_penjualan_customer'] 	= 'report/reportpenjualancustomer/index';
$route['admin/laporan_retur_sample']		= 'report/reportsamplemain/index';