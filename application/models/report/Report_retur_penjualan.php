<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_retur_penjualan extends CI_Model{

    public function get_retur_penjualan_header_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);
        
        $data = $this->db->get_where("tr_rt_pj_header th", $where)->result();

        return $data;
    }

    public function get_retur_penjualan_th($th_start, $th_finish, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);
        
        $data = $this->db->get_where("tr_rt_pj_header th", $where)->result();

        return $data;
    }


    public function get_retur_penjualan_bulan($bulan, $th, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_rt_pj_header th", $where)->result();

        return $data;
    }


    public function get_retur_penjualan_triwulan($th, $where_in, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_rt_pj_header th", $where)->result();

        return $data;
    }


    public function get_retur_penjualan_detail_p($where){
        $this->db->join("item i", "i.id_item = th.id_item");
        
        $data = $this->db->get_where("tr_rt_pj_detail_p th", $where)->result();

        return $data;
    }

    public function get_retur_penjualan_detail($where){
        $this->db->join("item i", "i.id_item = th.id_item");
        
        $data = $this->db->get_where("tr_rt_pj_detail th", $where)->result();

        return $data;
    }
}
?>