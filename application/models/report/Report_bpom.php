<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_bpom extends CI_Model{

    public function distict_item_report($bulan, $th){
        $this->db->join("item it", "th.id_item = it.id_item");
        $this->db->distinct();
        $this->db->select("it.nama_item");
        $this->db->where("MONTH(periode_record_item) =", $bulan);
        $this->db->where("YEAR(periode_record_item) =", $th);
        
        $data = $this->db->get_where("record_item th", $where)->result();

        return $data;
    }

    public function get_item_report($where){
        $this->db->join("item it", "th.id_item = it.id_item");        
        $data = $this->db->get_where("record_item th", $where)->result();

        return $data;
    }
//----------table_inti-----------------------------//
#-----------pemasukan, pengeluaran, sample----------#    

    public function get_penjualan_sum_bln($where){
        $this->db->join("tr_header trh", "trh.id_tr_header = trd.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

        $data = $this->db->get_where("tr_detail trd", $where)->result();

        return $data;
    }

    public function get_sample_sum_bln($where){
        $this->db->join("tr_sm_header trh", "trh.id_tr_header = trd.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

        $data = $this->db->get_where("tr_sm_detail trd", $where)->result();

        return $data;
    }

    public function get_pembelian_sum_bln($where){
        $this->db->join("tr_pb_header trh", "trh.id_tr_header = trd.id_tr_header");
        $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

        $data = $this->db->get_where("tr_pb_detail trd", $where)->result();

        return $data;
    }

//----------table_inti-----------------------------//
#-----------pemasukan, pengeluaran, sample----------#    



//----------table_retur-----------------------------//
#-----------pemasukan, pengeluaran, sample----------#    
    public function get_retur_penjualan_in_sum_bln($where){
        $this->db->join("tr_rt_pj_header trh", "trh.id_tr_header = trd.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

        $data = $this->db->get_where("tr_rt_pj_detail trd", $where)->result();

        return $data;
    }

    public function get_retur_penjualan_out_sum_bln($where){
        $this->db->join("tr_rt_pj_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
        $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

        $data = $this->db->get_where("tr_rt_pj_detail_p trd", $where)->result();

        return $data;
    }



    public function get_retur_pembelian_in_sum_bln($where){
        $this->db->join("tr_rt_pb_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
        $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_detail_p trd", $where)->result();

        return $data;
    }

    public function get_retur_pembelian_out_sum_bln($where){
        $this->db->join("tr_rt_pb_header trh", "trh.id_tr_header = trd.id_tr_header");
        $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_detail trd", $where)->result();

        return $data;
    }

//----------table_retur-----------------------------//
#-----------pemasukan, pengeluaran, sample----------#    

//----------table_retur_sample----------------------//
#-----------sample-----------------------------------#   
    public function get_retur_sample_in_sum_bln($where){
        $this->db->join("tr_sm_rt_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
        // $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

        $data = $this->db->get_where("tr_sm_rt_detail_p trd", $where)->result();

        return $data;
    }

    public function get_disticnt_sample_out_sum_bln($where){
        // $this->db->join("tr_sm_header trh", "trh.id_tr_header_p = trd.id_tr_header_p");

        $this->db->distinct();
        $this->db->select("trh.id_tr_sm_retur_header");

        $data = $this->db->get_where("tr_sm_header trh", $where)->result();

        return $data;
    }
//----------table_retur_sample----------------------//
#-----------sample-----------------------------------#    



#--------------------------triwulan-----------------------------#
#===============================================================#
        public function distict_item_report_triwulan($th_triwulan = "0", $where_in, $where){
            $this->db->join("item it", "th.id_item = it.id_item");
            $this->db->distinct();
            $this->db->select("it.nama_item");
            $this->db->where_in("MONTH(periode_record_item)", $where_in);
            $this->db->where("YEAR(periode_record_item) =", $th_triwulan);
            
            $data = $this->db->get_where("record_item th", $where)->result();

            return $data;
        }

    //----------table_inti-----------------------------//
    #-----------pemasukan, pengeluaran, sample----------#    

        public function get_penjualan_sum_triwulan($where_in, $where){
            $this->db->join("tr_header trh", "trh.id_tr_header = trd.id_tr_header");
            $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_detail trd", $where)->result();

            return $data;
        }

        public function get_sample_sum_triwulan($where_in, $where){
            $this->db->join("tr_sm_header trh", "trh.id_tr_header = trd.id_tr_header");
            $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_sm_detail trd", $where)->result();

            return $data;
        }

        public function get_pembelian_sum_triwulan($where_in, $where){
            $this->db->join("tr_pb_header trh", "trh.id_tr_header = trd.id_tr_header");
            $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_pb_detail trd", $where)->result();

            return $data;
        }

    //----------table_inti-----------------------------//
    #-----------pemasukan, pengeluaran, sample----------#    



    //----------table_retur-----------------------------//
    #-----------pemasukan, pengeluaran, sample----------#    
        public function get_retur_penjualan_in_sum_triwulan($where_in, $where){
            $this->db->join("tr_rt_pj_header trh", "trh.id_tr_header = trd.id_tr_header");
            $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_rt_pj_detail trd", $where)->result();

            return $data;
        }

        public function get_retur_penjualan_out_sum_triwulan($where_in, $where){
            $this->db->join("tr_rt_pj_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
            $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_rt_pj_detail_p trd", $where)->result();

            return $data;
        }



        public function get_retur_pembelian_in_sum_triwulan($where_in, $where){
            $this->db->join("tr_rt_pb_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
            $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_rt_pb_detail_p trd", $where)->result();

            return $data;
        }

        public function get_retur_pembelian_out_sum_triwulan($where_in, $where){
            $this->db->join("tr_rt_pb_header trh", "trh.id_tr_header = trd.id_tr_header");
            $this->db->join("suplier sp", "sp.id_suplier = trh.id_suplier");

            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_rt_pb_detail trd", $where)->result();

            return $data;
        }

    //----------table_retur-----------------------------//
    #-----------pemasukan, pengeluaran, sample----------#    

    //----------table_retur_sample----------------------//
    #-----------sample-----------------------------------#   
        public function get_retur_sample_in_sum_triwulan($where_in, $where){
            $this->db->join("tr_sm_rt_header_p trh", "trh.id_tr_header_p = trd.id_tr_header_p");
            // $this->db->join("rekanan rk", "rk.id_rekanan = trh.id_customer");
            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_sm_rt_detail_p trd", $where)->result();

            return $data;
        }

        public function get_disticnt_sample_out_sum_triwulan($where_in, $where){
            // $this->db->join("tr_sm_header trh", "trh.id_tr_header_p = trd.id_tr_header_p");

            $this->db->distinct();
            $this->db->select("trh.id_tr_sm_retur_header");
            
            $this->db->where_in("MONTH(trh.tgl_transaksi_tr_header)", $where_in);

            $data = $this->db->get_where("tr_sm_header trh", $where)->result();

            return $data;
        }
    //----------table_retur_sample----------------------//
    #-----------sample-----------------------------------#    
#===============================================================#
#--------------------------triwulan-----------------------------#
    
}
?>