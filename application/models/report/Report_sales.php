<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_sales extends CI_Model{

    public function get_sales_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);
        
        $data = $this->db->get_where("tr_header th", $where)->result();

        return $data;
    }


    public function get_sales_th($th_start, $th_finish, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);
        
        $data = $this->db->get_where("tr_header th", $where)->result();

        return $data;
    }


    public function get_sales_bulan($bulan, $th, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_header th", $where)->result();

        return $data;
    }


    public function get_sales_triwulan($th, $where_in, $where){
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_header th", $where)->result();

        return $data;
    }
}
?>