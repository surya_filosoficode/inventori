<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_hutang extends CI_Model{

    public function get_hutang_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("suplier sp", "sp.id_suplier = th.id_suplier");

        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);
        
        $data = $this->db->get_where("tr_pb_header th", $where)->result();

        return $data;
    }


    public function get_hutang_th($th_start, $th_finish, $where){
        $this->db->join("suplier sp", "sp.id_suplier = th.id_suplier");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);
        
        $data = $this->db->get_where("tr_pb_header th", $where)->result();

        return $data;
    }


    public function get_hutang_bulan($bulan, $th, $where){
        $this->db->join("suplier sp", "sp.id_suplier = th.id_suplier");

        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_pb_header th", $where)->result();

        return $data;
    }


    public function get_hutang_triwulan($th, $where_in, $where){
        $this->db->join("suplier sp", "sp.id_suplier = th.id_suplier");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_pb_header th", $where)->result();

        return $data;
    }
}
?>