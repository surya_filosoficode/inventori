<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_penjualan_customer extends CI_Model{

    public function get_penjualan_customer_tgl($tgl_start, $tgl_finish, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('tgl_transaksi_tr_header >=', $tgl_start);
        $this->db->where('tgl_transaksi_tr_header <=', $tgl_finish);
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_customer_th($th_start, $th_finish, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");

        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) >=', $th_start);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) <=', $th_finish);
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_customer_bulan($bulan, $th, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('MONTH(th.tgl_transaksi_tr_header) = ', $bulan);
        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }


    public function get_penjualan_customer_triwulan($th, $where_in, $where){
        $this->db->join("tr_header th", "th.id_tr_header = td.id_tr_header");
        $this->db->join("rekanan rk", "rk.id_rekanan = th.id_customer");
        // $this->db->join("sales sl", "sl.id_sales = th.id_sales");
        $this->db->join("item it", "td.id_item = it.id_item");

        $this->db->where('YEAR(th.tgl_transaksi_tr_header) =', $th);
        $this->db->where_in('MONTH(th.tgl_transaksi_tr_header)', $where_in);
        
        $data = $this->db->get_where("tr_detail td", $where)->result();

        return $data;
    }
}
?>