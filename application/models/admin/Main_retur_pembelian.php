<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_retur_pembelian extends CI_Model{

    public function get_full_header($where){
        $this->db->join("suplier cs", "th.id_suplier = cs.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_header th", $where)->result();
        return $data;
    }

    public function get_full_header_p($where){
        $this->db->join("suplier cs", "th.id_suplier = cs.id_suplier");

        $data = $this->db->get_where("tr_rt_pb_header_p th", $where)->result();
        return $data;
    }



    public function get_full_detail($where){
        $this->db->join("tr_rt_pb_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("item it", "td.id_item = it.id_item");

        $data = $this->db->get_where("tr_rt_pb_detail td", $where)->result();
        return $data;
    }

    public function get_full_detail_p($where){
        $this->db->join("tr_rt_pb_header_p th", "td.id_tr_header_p = th.id_tr_header_p");
        $this->db->join("item it", "td.id_item = it.id_item");

        $data = $this->db->get_where("tr_rt_pb_detail_p td", $where)->result();
        return $data;
    }

    
    
    public function retur_insert_header($id_tr_header_p, $tipe, $selisih, $id_suplier, $tgl_transaksi_tr_header, $total_pembayaran_tr_header, $admin_create_tr_header, $time_up_tr_header){
    	$insert = $this->db->query("SELECT insert_rt_pb_header('".$id_tr_header_p."', '".$tipe."', '".$selisih."', '".$id_suplier."', '".$tgl_transaksi_tr_header."', '".$total_pembayaran_tr_header."', '".$admin_create_tr_header."', '".$time_up_tr_header."') AS id_tr_header");
    	return $insert->row_array();
    }

    public function retur_insert_header_p($id_suplier, $tgl_transaksi_tr_header, $total_pembayaran_tr_header, $admin_create_tr_header, $time_up_tr_header){
        $insert = $this->db->query("SELECT insert_rt_pb_header_p('".$id_suplier."', '".$tgl_transaksi_tr_header."', '".$total_pembayaran_tr_header."', '".$admin_create_tr_header."', '".$time_up_tr_header."') AS id_tr_header");
        return $insert->row_array();
    }

    


    public function rt_insert_detail($id_tr_header_in, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $harga_total_tr_detail, $status_rt_tr_detail, $admin_create_tr_detail, $time_up_tr_detail){
        $insert = $this->db->query("SELECT insert_rt_pb_detail('".$id_tr_header_in."', '".$id_item."', '".$kode_produksi_tr_detail."', '".$tgl_kadaluarsa_tr_detail."', '".$harga_satuan_tr_detail."', '".$jml_item_tr_detail."', '".$harga_total_tr_detail."', '".$status_rt_tr_detail."', '".$admin_create_tr_detail."', '".$time_up_tr_detail."') AS id_tr_detail");
        return $insert->row_array();
    }

    public function rt_insert_detail_p($id_tr_header_in, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $harga_total_tr_detail, $admin_create_tr_detail, $time_up_tr_detail){
    	$insert = $this->db->query("SELECT insert_rt_pb_detail_p('".$id_tr_header_in."', '".$id_item."', '".$kode_produksi_tr_detail."', '".$tgl_kadaluarsa_tr_detail."', '".$harga_satuan_tr_detail."', '".$jml_item_tr_detail."', '".$harga_total_tr_detail."', '".$admin_create_tr_detail."', '".$time_up_tr_detail."') AS id_tr_detail");
    	return $insert->row_array();
    }




    public function record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_awal_record_stok, $stok_tr_record_stok_before, $stok_tr_record_stok, $stok_akhir_record_stok, $admin_create_record_stok, $time_up_record_stok){
        $insert = $this->db->query("SELECT insert_record_stok('".$id_tr_detail."', '".$id_item."', '".$tgl_insert."', '".$keterangan_record_stok."', '".$jenis_record_stok."', '".$status_record_stok."', '".$stok_awal_record_stok."', '".$stok_tr_record_stok_before."', '".$stok_tr_record_stok."', '".$stok_akhir_record_stok."', '".$admin_create_record_stok."', '".$time_up_record_stok."') AS record_stok");
        return $insert->row_array();
    }

    public function record_stok_opnam_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_awal_record_stok, $stok_tr_record_stok_before, $stok_tr_record_stok, $stok_akhir_record_stok, $admin_create_record_stok, $time_up_record_stok){
        $insert = $this->db->query("SELECT insert_record_stok_opnam('".$id_tr_detail."', '".$id_item."', '".$tgl_insert."', '".$keterangan_record_stok."', '".$jenis_record_stok."', '".$status_record_stok."', '".$stok_awal_record_stok."', '".$stok_tr_record_stok_before."', '".$stok_tr_record_stok."', '".$stok_akhir_record_stok."', '".$admin_create_record_stok."', '".$time_up_record_stok."') AS record_stok");
        return $insert->row_array();
    }
    
}
?>