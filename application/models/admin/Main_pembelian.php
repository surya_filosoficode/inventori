<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_pembelian extends CI_Model{

    public function get_full_header_pembelian($where){
        // $this->db->join("tr_header th", "td.id_tr_header = th.id_tr_header");
        // $this->db->join("item it", "it.id_item = td.id_item");

        $this->db->join("suplier sp", "th.id_suplier = sp.id_suplier");

        $data = $this->db->get_where("tr_pb_header th", $where)->result();
        return $data;
    }
    
    public function pembelian_insert_header($id_suplier, $tgl_transaksi_tr_header, $cara_pembayaran_tr_header, $tempo_tr_header, $status_hutang, $total_pembayaran_tr_header, $disc_all_tr_header, $total_pembayaran_disc_tr_header, $ppn_tr_header, $total_pembayaran_pnn_tr_header, $admin_create_tr_header, $time_up_tr_header){
    	$insert = $this->db->query("SELECT insert_pb_tr_header('".$id_suplier."', '".$tgl_transaksi_tr_header."', '".$cara_pembayaran_tr_header."', '".$tempo_tr_header."', '".$status_hutang."', '".$total_pembayaran_tr_header."', '".$disc_all_tr_header."', '".$total_pembayaran_disc_tr_header."', '".$ppn_tr_header."', '".$total_pembayaran_pnn_tr_header."', '".$admin_create_tr_header."', '".$time_up_tr_header."') AS id_tr_header");
    	return $insert->row_array();
    }

    public function get_full_detail_pembelian($where){
        $this->db->join("tr_pb_header th", "td.id_tr_header = th.id_tr_header");
        $this->db->join("item it", "td.id_item = it.id_item");

        // $this->db->join("suplier sp", "th.id_suplier = sp.id_suplier");
        // $this->db->join("sales sl", "th.id_sales = sl.id_sales");

        $data = $this->db->get_where("tr_pb_detail td", $where)->result();
        return $data;
    }

    public function pembelian_insert_detail($id_tr_header_in, $id_item, $harga_satuan_tr_detail, $jml_item_tr_detail, $harga_total_tr_detail, $disc_item_tr_detail, $harga_total_fix_tr_detail, $admin_create_tr_detail, $time_up_tr_detail){
    	$insert = $this->db->query("SELECT insert_pb_tr_detail('".$id_tr_header_in."', '".$id_item."', '".$harga_satuan_tr_detail."', '".$jml_item_tr_detail."', '".$harga_total_tr_detail."', '".$disc_item_tr_detail."', '".$harga_total_fix_tr_detail."', '".$admin_create_tr_detail."', '".$time_up_tr_detail."') AS id_tr_detail");
    	return $insert->row_array();
    }


    public function record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_awal_record_stok, $stok_tr_record_stok_before, $stok_tr_record_stok, $stok_akhir_record_stok, $admin_create_record_stok, $time_up_record_stok){
    	$insert = $this->db->query("SELECT insert_record_stok('".$id_tr_detail."', '".$id_item."', '".$tgl_insert."', '".$keterangan_record_stok."', '".$jenis_record_stok."', '".$status_record_stok."', '".$stok_awal_record_stok."', '".$stok_tr_record_stok_before."', '".$stok_tr_record_stok."', '".$stok_akhir_record_stok."', '".$admin_create_record_stok."', '".$time_up_record_stok."') AS record_stok");
    	return $insert->row_array();
    }


    public function delete_detail_where_in($id_tr_header, $where_in){
        $this->db->where("id_tr_header", $id_tr_header);
        $this->db->where_not_in("id_item", $where_in);
        $delete = $this->db->delete("tr_pb_detail");
        return $delete;
    }

    public function get_detail_where_in($id_tr_header, $where_in){
        $this->db->where("id_tr_header", $id_tr_header);
        $this->db->where_not_in("id_item", $where_in);
        $data = $this->db->get("tr_pb_detail")->result();
        return $data;
    }

}
?>