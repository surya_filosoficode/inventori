<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Item_main extends CI_Model{

	public function get_data_produk_where($where){
        // $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
        $this->db->join("brand b", "i.id_brand = b.id_brand");
    	$data = $this->db->get_where("item i", $where);
    	return $data->result();
    }

    public function get_distinct_nama_produk($where){
        $this->db->distinct();
        $this->db->select('nama_item');
        $data = $this->db->get_where("item", $where);
        return $data->result();
    }

    public function get_data_produk_where_each($where){
        // $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
        $this->db->join("brand b", "i.id_brand = b.id_brand");
    	$data = $this->db->get_where("item i", $where);
    	return $data->row_array();
    }
    
    public function get_data_brand_where($where){
        $this->db->select("id_brand, lpad(id_brand, 3, 0) as id_brand_txt, nama_brand, keterangan_brand");
    	$data = $this->db->get_where("brand", $where);
    	return $data->result();
    }


    public function insert_record_item($periode_record_item, $id_item, $stok_record_item, $rusak_record_item){
        $insert = $this->db->query("SELECT insert_record_item('".$periode_record_item."', '".$id_item."', '".$stok_record_item."', '".$rusak_record_item."') AS id_record_item");
        return $insert->row_array();
    }

   
}
?>