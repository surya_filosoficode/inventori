 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_record_stok {
    public function __construct(){
        $this->load->library('session');
        $this->load->model("other/item_main", "im");
        $this->load->model("main/mainmodel", "mm");
        date_default_timezone_set("Asia/Bangkok");
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function insert_record(){
        date_default_timezone_set("Asia/Bangkok");
        $array_of_status = [];
        $final_status = false;
            
        if($this->check_day()){
            $data_item = $this->mm->get_data_all_where("item", array());
            foreach ($data_item as $key => $value) {
                $id_item = $value->id_item;
                $stok = $value->stok;
                $stok_opnam = $value->stok_opnam;
                $periode = date("Y-m-d");

                $status = false;
                if($this->check_periode($id_item)){
                    $insert_record = $this->im->insert_record_item($periode, $id_item, $stok, $stok_opnam);
                    if($insert_record){
                        $status = true;
                    }
                }
                array_push($array_of_status, $status);
            }

            if(!in_array(false, $array_of_status)){
                $final_status = true;
                // print_r("<script>alert(\"Update record stok berhasil. Terimakasih\");</script>");
            }else{
                // print_r("<script>alert(\"program sedang dalam masalah, hubungi admin. Terimakasih\");</script>");
            }
        }
        // else{
        //     print_r("periode exist");
        // }

        return $final_status;
    }

    public function check_periode($id_item){
        date_default_timezone_set("Asia/Bangkok");

        $where = array(
                    "id_item"=>$id_item,
                    "MONTH(periode_record_item) ="=>date("m"),
                    "YEAR(periode_record_item) ="=>date("Y")
                );

        $check_data = $this->mm->get_data_all_where("record_item", $where);
        // print_r($check_data);   
        $status = true;
        if($check_data){
            $status = false;
        }

        return $status;
    }

    public function get_day(){
        $day = date("d");
        print_r($day);
    }

    public function check_day(){
        $day = date("d");
        $array_of_day = ["01", "02", "03", "04", "05", "06", "07", "14", "21", "28", "30", "31"];

        $status = false;
        if(in_array($day, $array_of_day)){
            $status = true;
        }
        return $status;
    }
    
}