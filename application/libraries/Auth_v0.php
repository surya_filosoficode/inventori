 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_v0 {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function set_session($param = null){
    	$status = false;
        if($param != null){
    		$this->session->set_userdata("ih_mau_ngapain", $param);
            $status = true;
    	}
        return $status;
    }

    public function destroy_session($redirect = "login"){
        $this->session->sess_destroy();
        redirect($redirect);
    }

    public function get_session(){
        $data_session = array();
        if(isset($_SESSION["ih_mau_ngapain"])){
            $data_session = $_SESSION["ih_mau_ngapain"];
        }
        return $data_session;
    }

    public function auth_login(){
        if(isset($_SESSION["ih_mau_ngapain"])){
            if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                // print_r("login");
                switch ($_SESSION["ih_mau_ngapain"]["id_tipe_admin"]) {
                    case "0":
                        redirect(base_url()."admin/data_admin");
                        // print_r("admin");
                        break;
                    
                    case "1":
                        // print_r("fl");
                        redirect(base_url()."fl/home_main");
                        break;
                    

                    default:
                        // redirect(base_url()."login");
                        break;
                }
            }
        }
    }

    public function check_session_active_ad(){
        if(isset($_SESSION["ih_mau_ngapain"])){
            if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                // print_r("login");
                switch ($_SESSION["ih_mau_ngapain"]["id_tipe_admin"]) {
                    case "0":
                        // redirect(base_url()."admin/data_admin");
                        // print_r("admin");
                        break;
                    
                    case "1":
                        // print_r("fl");
                        redirect(base_url()."fl/home_main");
                        break;
                    

                    default:
                        redirect(base_url()."login");
                        break;
                }
            }else {
                redirect(base_url()."login");
            }
        }else {
            redirect(base_url()."login");
        }
    }

    public function check_session_active_fl(){
        if(isset($_SESSION["ih_mau_ngapain"])){
            if($_SESSION["ih_mau_ngapain"]["status_log"] == true){
                // print_r("login");
                switch ($_SESSION["ih_mau_ngapain"]["id_tipe_admin"]) {
                    case "0":
                        redirect(base_url()."admin/data_admin");
                        // print_r("admin");
                        break;
                    
                    case "1":
                        // print_r("fl");
                        // redirect(base_url()."fl/home_main");
                        break;
                    

                    default:
                        redirect(base_url()."login");
                        break;
                }
            }else {
                redirect(base_url()."login");
            }
        }else {
            redirect(base_url()."login");
        }
    }
}