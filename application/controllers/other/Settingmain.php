<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settingmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------setting_main--------------------------------
#===============================================================================
    
    public function create_tbl_setting(){
        $data["list_data"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        // print_r($data);
        $this->load->view("admin/setting_tbl", $data);
    }
#===============================================================================
#-----------------------------------setting_main--------------------------------
#===============================================================================

	
}
