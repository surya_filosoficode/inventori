<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/item_main', 'im');

        // $this->load->library("response_message");
        // $this->load->library("Auth_v0");
        
        // $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================
    
    public function create_tbl_item(){
        $data["list_data"] = $this->im->get_data_produk_where(array("is_del_item"=>"0"));
        $this->load->view("admin/produk_tbl", $data);
    }
#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------brand_main----------------------------------
#===============================================================================

    public function create_tbl_brand(){
        $data["list_data"] = $this->im->get_data_brand_where(array("is_del_brand"=>"0"));
        $this->load->view("admin/brand_tbl", $data);
    }

#===============================================================================
#-----------------------------------brand_main----------------------------------
#===============================================================================
	
}
