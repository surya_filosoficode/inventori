<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanmain extends CI_Controller {

	public function __construct(){
        parent::__construct();
        
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->auth_v0->check_session_active_ad();
    }

	public function laporan_penjualan(){
		$data["page"] = "laporan_penjualan";
		$this->load->view('index', $data);
	}

	public function laporan_penjualan_pelanggan(){
		$data["page"] = "laporan_penjualan_pelanggan";
		$this->load->view('index', $data);
	}

	public function laporan_retur_penjualan(){
		$data["page"] = "laporan_retur_penjualan";
		$this->load->view('index', $data);
	}

	public function laporan_piutang(){
		$data["page"] ="laporan_piutang";
		$this->load->view('index', $data);
	}


	public function laporan_pembelian(){
		$data["page"] ="laporan_pembelian";
		$this->load->view('index', $data);
	}

	public function laporan_retur_pembelian(){
		$data["page"] ="laporan_retur_pembelian";
		$this->load->view('index', $data);
	}

	public function laporan_hutang(){
		$data["page"] ="laporan_hutang";
		$this->load->view('index', $data);
	}

	public function laporan_retur_sample(){
		$data["page"] ="laporan_retur_sample";
		$this->load->view('index', $data);
	}



	public function laporan_arus_barang(){
		$data["page"] ="laporan_arus_barang";
		$this->load->view('index', $data);
	}

	public function laporan_format_bpom(){
		$data["page"] ="laporan_format_bpom";
		$this->load->view('index', $data);
	}

	public function laporan_pendapatan_sales(){
		$data["page"] ="laporan_pendapatan_sales";
		$this->load->view('index', $data);
	}



	public function laporan_arus_kas(){
		$data["page"] ="laporan_arus_kas";
		$this->load->view('index', $data);
	}

	public function laporan_laba_rugi(){
		$data["page"] ="laporan_laba_rugi";
		$this->load->view('index', $data);
	}

}
