<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportpembelianmain extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_pembelian', 'rp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

	public function index(){
		$data["page"] = "report_pembelian_main";
        $data["str_periode"] = "";
		$this->load->view('index', $data);
	}

#------------------------------show----------------------------------#

    public function get_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_pembelian_main";
        $data["str_periode"] = "";

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $data["list_data"] = $this->rp->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_pembelian_main";
        $data["str_periode"] = "";

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data["list_data"] = $this->rp->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_pembelian_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_pembelian_main";
        $data["str_periode"] = "";

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $data["list_data"] = $this->rp->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_pembelian_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_pembelian_main";
        $data["str_periode"] = "";

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["list_data"] = $this->rp->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

#------------------------------show----------------------------------#

#------------------------------main----------------------------------#
    public function main_get_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $data["list_data"] = $this->rp->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));
        }
        

        return $data;
    }

    public function main_get_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data["list_data"] = $this->rp->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));
        }
        
        return $data;
    }

    public function main_get_pembelian_th($th_start = "0", $th_finish = "0"){
        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $data["list_data"] = $this->rp->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
        }

        return $data;
    }

    public function main_get_pembelian_bulan($bulan = "0", $th = "0"){
        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["list_data"] = $this->rp->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
        }

        return $data;
    }
#------------------------------main----------------------------------#


#------------------------------print----------------------------------#
    public function print_get_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = [];
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_pembelian_tgl($tgl_start, $tgl_finish);
        }
        
        $this->load->view('print/print_pembelian_main', $data);
    }

    public function print_get_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = [];
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_pembelian_triwulan($triwulan, $th_triwulan);
        }
        
        $this->load->view('print/print_pembelian_main', $data);
    }

    public function print_get_pembelian_th($th_start = "0", $th_finish = "0"){
        $data = [];
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_pembelian_th($th_start, $th_finish);
        }

        $this->load->view('print/print_pembelian_main', $data);
    }

    public function print_get_pembelian_bulan($bulan = "0", $th = "0"){
        $data = [];
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_pembelian_bulan($bulan, $th);
        }

        $this->load->view('print/print_pembelian_main', $data);
    }
#------------------------------print----------------------------------#


#------------------------------excel----------------------------------#
    public function excel_get_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = [];
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_pembelian_tgl($tgl_start, $tgl_finish);
            $this->convert_excel($data);
        }
        
        // $this->load->view('print/print_pembelian_main', $data);
    }

    public function excel_get_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = [];
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_pembelian_triwulan($triwulan, $th_triwulan);
            $this->convert_excel($data);
        }
        
        // $this->load->view('print/print_pembelian_main', $data);
    }

    public function excel_get_pembelian_th($th_start = "0", $th_finish = "0"){
        $data = [];
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_pembelian_th($th_start, $th_finish);
            $this->convert_excel($data);
        }

        // $this->load->view('print/print_pembelian_main', $data);
    }

    public function excel_get_pembelian_bulan($bulan = "0", $th = "0"){
        $data = [];
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_pembelian_bulan($bulan, $th);
            $this->convert_excel($data);
        }

        // $this->load->view('print/print_pembelian_main', $data);
    }
#------------------------------print----------------------------------#

    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN TRANSAKSI PEMBELIAN')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Tanggal')
                    ->setCellValue('B6', 'No Faktur')
                    ->setCellValue('C6', 'Suplier')
                    ->setCellValue('D6', 'Subtotal')
                    ->setCellValue('E6', 'Diskon')
                    ->setCellValue('F6', 'Pajak')
                    ->setCellValue('G6', 'Total');

        $no_row = 7;
        $t_harga = 0;
        foreach ($data["list_data"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->tgl_transaksi_tr_header)
                    ->setCellValue('B'.$no_row, $value->id_tr_header)
                    ->setCellValue('C'.$no_row, $value->nama_suplier)
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($value->total_pembayaran_tr_header, 2, ',', '.'))
                    ->setCellValue('E'.$no_row, $value->disc_all_tr_header)
                    ->setCellValue('F'.$no_row, $value->ppn_tr_header)
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($value->total_pembayaran_pnn_tr_header, 2, ',', '.'));
            // print_r($value);
            $t_harga += $value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, "Total")
                    ->setCellValue('H'.$no_row, "Rp. ".number_format($t_harga, 2, ',', '.'));
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_pembelian');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_pembelian.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_pembelian.xlsx', NULL);
    }

}
