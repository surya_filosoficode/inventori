<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportreturpembelianmain extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_retur_pembelian', 'rp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        // $data_tr_sm_rt_header_p = $this->mm->get_data_all_where("tr_sm_rt_header_p");


        // $data["sample"] = $this->mm->get_data_all_where("sample", array("is_delete"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#
    public function get_retur_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            // $array_pengganti = array();

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_header_tgl($tgl_start, $tgl_finish, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_retur_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_triwulan($th_triwulan, $array_where_in, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_retur_pembelian_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($th_start != "0" && $th_finish != "0"){

            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_th($th_start, $th_finish, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_retur_pembelian_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_bulan($bulan, $th, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_retur_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            // $array_pengganti = array();

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_header_tgl($tgl_start, $tgl_finish, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_retur_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_triwulan($th_triwulan, $array_where_in, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_retur_pembelian_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($th_start != "0" && $th_finish != "0"){

            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_th($th_start, $th_finish, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }

        // print_r($data);
        return $data;
    }

    public function main_get_retur_pembelian_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_retur_pembelian";
        $data["str_periode"] = "";

        $data["list_pengganti"] = array();
        $data["list_sample"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;

            $data_tr_sm_rt_header = $this->rp->get_retur_pembelian_bulan($bulan, $th, array());
            // print_r($data_tr_sm_rt_header);
            $no = 0;

            $array_pengganti = array();
            $array_retur = array();

            foreach ($data_tr_sm_rt_header as $key => $value) {
                $tmp_detail = $this->rp->get_retur_pembelian_detail(array("id_tr_header"=>$value->id_tr_header));

                // print_r($tmp_detail);

                foreach ($tmp_detail as $keyd => $valued) {

                    if(!array_key_exists($valued->id_item, $array_retur)){
                        $array_retur[$valued->id_item] = array();
                        
                        $array_retur[$valued->id_item]["t_item"] = 0;
                        $array_retur[$valued->id_item]["t_item_rusak"] = 0;
                        $array_retur[$valued->id_item]["t_harga"] = 0;


                        $array_retur[$valued->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valued->id_item)); 
                    }

                    $array_retur[$valued->id_item]["t_harga"] += $valued->harga_total_tr_detail;
                    if($valued->status_rt_tr_detail == "0"){
                        $array_retur[$valued->id_item]["t_item"] += $valued->jml_item_tr_detail;
                    }else {
                        $array_retur[$valued->id_item]["t_item_rusak"] += $valued->jml_item_tr_detail;
                    }
                }


                $tmp_retur = $this->mm->get_data_all_where("tr_rt_pb_header_p", array("id_tr_header_p"=>$value->id_tr_header_p));

                foreach ($tmp_retur as $keys => $values) {
                    $tmp_retur_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$values->id_tr_header_p));

                    foreach ($tmp_retur_detail as $keysd => $valuesd) {
                        if(!array_key_exists($valuesd->id_item, $array_pengganti)){
                            $array_pengganti[$valuesd->id_item] = array();
                            
                            $array_pengganti[$valuesd->id_item]["t_item"] = 0;
                            $array_pengganti[$valuesd->id_item]["t_harga"] = 0;

                            $array_pengganti[$valuesd->id_item]["detail"] = $this->mm->get_data_each("item", array("id_item"=>$valuesd->id_item)); 
                        }

                        $array_pengganti[$valuesd->id_item]["t_item"] += $valuesd->jml_item_tr_detail;
                        $array_pengganti[$valuesd->id_item]["t_harga"] += $valuesd->harga_total_tr_detail;
                    }   
                }

                $no++;
            }
            // print_r($array_pengganti);
            // print_r($array_retur);
            $data["list_sample"] = $array_retur;
            $data["list_pengganti"] = $array_pengganti;
        }

        // print_r($data);
        return $data;
    }

#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_retur_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_retur_pembelian_tgl($tgl_start, $tgl_finish);
        }
        
        // print_r($data);
        $this->load->view('print/print_retur_pembelian', $data);
    }

    public function print_get_retur_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_retur_pembelian_triwulan($triwulan, $th_triwulan);
        }

        // print_r($data);
        $this->load->view('print/print_retur_pembelian', $data);
    }

    public function print_get_retur_pembelian_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_retur_pembelian_th($th_start, $th_finish);
        }

        // print_r($data);
        $this->load->view('print/print_retur_pembelian', $data);
    }

    public function print_get_retur_pembelian_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_retur_pembelian_bulan($bulan, $th);
        }

        // print_r($data);
        $this->load->view('print/print_retur_pembelian', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_retur_pembelian_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_retur_pembelian_tgl($tgl_start, $tgl_finish);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_retur_pembelian', $data);
    }

    public function excel_get_retur_pembelian_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_retur_pembelian_triwulan($triwulan, $th_triwulan);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_retur_pembelian', $data);
    }

    public function excel_get_retur_pembelian_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_retur_pembelian_th($th_start, $th_finish);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_retur_pembelian', $data);
    }

    public function excel_get_retur_pembelian_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_retur_pembelian_bulan($bulan, $th);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_retur_pembelian', $data);
    }
#------------------------------excel---------------------------------#


    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN TRANSAKSI RETUR PEMBELIAN')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A8', 'No')
                    ->setCellValue('B8', 'Nama Obat')
                    ->setCellValue('C8', 'Exp Date')
                    ->setCellValue('D8', 'Kode Produksi')
                    ->setCellValue('E8', 'Retur')
                    ->setCellValue('F8', 'Retur (Rusak)')
                    ->setCellValue('G8', 'Total')
                    ->setCellValue('H8', 'Harga Satuan')
                    ->setCellValue('I8', 'Jumlah Uang');


        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Daftar Produk Sample')
                    ->setCellValue('A7', '');

        $no_row = 9;
        $no = 1;
        
        $t_harga = 0;
        $t_harga_p = 0;
        $t_harga_all = 0;

        foreach ($data["list_sample"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $no)
                    ->setCellValue('B'.$no_row, $value["detail"]["nama_item"])
                    ->setCellValue('C'.$no_row, $value["detail"]["tgl_kadaluarsa_item"])
                    ->setCellValue('D'.$no_row, $value["detail"]["kode_produksi_item"])
                    ->setCellValue('E'.$no_row, $value["t_item"])
                    ->setCellValue('F'.$no_row, $value["t_item_rusak"])
                    ->setCellValue('G'.$no_row, ((int)$value["t_item"] + (int)$value["t_item_rusak"]))
                    ->setCellValue('H'.$no_row, "Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.'))
                    ->setCellValue('I'.$no_row, "Rp. ".number_format($value["t_harga"], 2, ',', '.'));
            // print_r($value);
            $t_harga += $value["t_harga"];
            $no_row++;
        }
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, "Total")
                    ->setCellValue('I'.$no_row, "Rp. ".number_format($t_harga, 2, ',', '.')
                );


        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), 'Daftar Produk Pengganti')
                    ->setCellValue('A'.($no_row+3), '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+4), 'No')
                    ->setCellValue('B'.($no_row+4), 'Nama Obat')
                    ->setCellValue('C'.($no_row+4), 'Exp Date')
                    ->setCellValue('D'.($no_row+4), 'Kode Produksi')
                    ->setCellValue('E'.($no_row+4), 'Pengembalian')
                    ->setCellValue('F'.($no_row+4), 'Total')
                    ->setCellValue('G'.($no_row+4), 'Harga Satuan')
                    ->setCellValue('H'.($no_row+4), 'Jumlah Uang');

        $no_row_next = $no_row+5;
        $no = 1;
        $t_harga_p = 0;
        foreach ($data["list_pengganti"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row_next, $no)
                    ->setCellValue('B'.$no_row_next, $value["detail"]["nama_item"])
                    ->setCellValue('C'.$no_row_next, $value["detail"]["tgl_kadaluarsa_item"])
                    ->setCellValue('D'.$no_row_next, $value["detail"]["kode_produksi_item"])
                    ->setCellValue('E'.$no_row_next, $value["t_item"])
                    ->setCellValue('F'.$no_row_next, $value["t_item"])
                    ->setCellValue('G'.$no_row_next, "Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.'))
                    ->setCellValue('H'.$no_row_next, "Rp. ".number_format($value["t_harga"], 2, ',', '.'));
            // print_r($value);
            $t_harga_p += $value["t_harga"];
            $no_row_next++;
        }

        $t_harga_all = $t_harga_p - $t_harga;

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row_next, "Total")
                    ->setCellValue('H'.$no_row_next, "Rp. ".number_format($t_harga_p, 2, ',', '.')
                );

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row_next+1), "Selisih Retur")
                    ->setCellValue('H'.($no_row_next+1), "Rp. ".number_format($t_harga_all, 2, ',', '.')
                );
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_retur');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_retur.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_retur.xlsx', NULL);
    }

}
