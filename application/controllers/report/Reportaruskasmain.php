<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportaruskasmain extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_penjualan', 'rpj');
        $this->load->model('report/report_pembelian', 'rpb');

        $this->load->model('report/report_retur_pembelian', 'rrpb');
        $this->load->model('report/report_retur_penjualan', 'rrpj');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        // $data_tr_sm_rt_header_p = $this->mm->get_data_all_where("tr_sm_rt_header_p");


        // $data["sample"] = $this->mm->get_data_all_where("sample", array("is_delete"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show----------------------------------#
    public function get_arus_kas_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();

        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $data["list_data_penjualan"] = $this->rpj->get_penjualan_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_header_tgl($tgl_start, $tgl_finish, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_header_tgl($tgl_start, $tgl_finish, array());
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_arus_kas_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();

        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();
        
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data["list_data_penjualan"] = $this->rpj->get_penjualan_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_triwulan($th_triwulan, $array_where_in, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_triwulan($th_triwulan, $array_where_in, array());
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_arus_kas_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();
        
        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($th_start != "0" && $th_finish != "0"){
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
            $data["list_data_penjualan"] = $this->rpj->get_penjualan_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_th($th_start, $th_finish, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_th($th_start, $th_finish, array());
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_arus_kas_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();
        
        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
            $data["list_data_penjualan"] = $this->rpj->get_penjualan_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_bulan($bulan, $th, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_bulan($bulan, $th, array());
        }

        // print_r($data);
        $this->load->view('index', $data);
    }
#------------------------------show----------------------------------#

#------------------------------main----------------------------------#
    public function main_get_arus_kas_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();

        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];

            $data["list_data_penjualan"] = $this->rpj->get_penjualan_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_tgl($tgl_start, $tgl_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_header_tgl($tgl_start, $tgl_finish, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_header_tgl($tgl_start, $tgl_finish, array());
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_arus_kas_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();

        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();
        
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $data["list_data_penjualan"] = $this->rpj->get_penjualan_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_triwulan($th_triwulan, $array_where_in, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_triwulan($th_triwulan, $array_where_in, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_triwulan($th_triwulan, $array_where_in, array());
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_arus_kas_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();
        
        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($th_start != "0" && $th_finish != "0"){
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
            $data["list_data_penjualan"] = $this->rpj->get_penjualan_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_th($th_start, $th_finish, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_th($th_start, $th_finish, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_th($th_start, $th_finish, array());
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_arus_kas_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_arus_kas";
        $data["str_periode"] = "";

        $data["list_data_penjualan"] = array();
        $data["list_data_pembelian"] = array();
        $data["list_data_hutang"] = array();
        $data["list_data_piutang"] = array();
        
        $data["list_data_retur_penjualan"] = array();
        $data["list_data_retur_pembelian"] = array();

        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
            $data["list_data_penjualan"] = $this->rpj->get_penjualan_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_pembelian"] = $this->rpb->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"0"));

            $data["list_data_hutang"] = $this->rpb->get_pembelian_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));

            $data["list_data_piutang"] = $this->rpj->get_penjualan_bulan($bulan, $th, array("cara_pembayaran_tr_header"=>"1", "status_hutang"=>"1"));


            $data["list_data_retur_penjualan"] = $this->rrpj->get_retur_penjualan_bulan($bulan, $th, array());

            $data["list_data_retur_pembelian"] = $this->rrpb->get_retur_pembelian_bulan($bulan, $th, array());
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }
#------------------------------main----------------------------------#

#------------------------------print---------------------------------#
    public function print_get_arus_kas_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_arus_kas_tgl($tgl_start, $tgl_finish);
        }
        
        // print_r($data);
        $this->load->view('print/print_arus_kas_main', $data);
    }

    public function print_get_arus_kas_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_arus_kas_triwulan($triwulan, $th_triwulan);
        }
        
        // print_r($data);
        $this->load->view('print/print_arus_kas_main', $data);
        // return $data;
    }

    public function print_get_arus_kas_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_arus_kas_th($th_start, $th_finish);
        }

        // print_r($data);
        $this->load->view('print/print_arus_kas_main', $data);
        // return $data;
    }

    public function print_get_arus_kas_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_arus_kas_bulan($bulan, $th);
        }

        // print_r($data);
        $this->load->view('print/print_arus_kas_main', $data);
        // return $data;
    }
#------------------------------print---------------------------------#

#------------------------------excel---------------------------------#
    public function excel_get_arus_kas_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_arus_kas_tgl($tgl_start, $tgl_finish);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_arus_kas_main', $data);
    }

    public function excel_get_arus_kas_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_arus_kas_triwulan($triwulan, $th_triwulan);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_arus_kas_main', $data);
        // return $data;
    }

    public function excel_get_arus_kas_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_arus_kas_th($th_start, $th_finish);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_arus_kas_main', $data);
        // return $data;
    }

    public function excel_get_arus_kas_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_arus_kas_bulan($bulan, $th);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_arus_kas_main', $data);
        // return $data;
    }
#------------------------------excel---------------------------------#



    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'REKAP DATA LAPORAN ARUS KAS')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', '')
                    ->setCellValue('B6', '')
                    ->setCellValue('C6', '')
                    ->setCellValue('D6', '')
                    ->setCellValue('E6', '')
                    ->setCellValue('F6', '')
                    ->setCellValue('G6', '')
                    ->setCellValue('H6', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A7', '1.')
                    ->setCellValue('B7', 'Penjualan')
                    ->setCellValue('C7', '')
                    ->setCellValue('D7', '')
                    ->setCellValue('E7', '')
                    ->setCellValue('F7', '')
                    ->setCellValue('G7', '')
                    ->setCellValue('H7', '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = 9;
        $t_list_data_penjualan = 0;
        foreach ($data["list_data_penjualan"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_rekanan)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($value->total_pembayaran_pnn_tr_header,2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_penjualan += $value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_penjualan, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_penjualan, 2, ',', '.')
                );




        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), '2.')
                    ->setCellValue('B'.($no_row+2), 'Penerimaan Piutang')
                    ->setCellValue('C'.($no_row+2), '')
                    ->setCellValue('D'.($no_row+2), '')
                    ->setCellValue('E'.($no_row+2), '')
                    ->setCellValue('F'.($no_row+2), '')
                    ->setCellValue('G'.($no_row+2), '')
                    ->setCellValue('H'.($no_row+2), '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = $no_row+3;
        $t_list_data_piutang = 0;
        foreach ($data["list_data_piutang"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_rekanan)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($value->total_pembayaran_pnn_tr_header,2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_piutang += $value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_piutang, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_piutang, 2, ',', '.')
                );




        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), '3.')
                    ->setCellValue('B'.($no_row+2), 'Pembelian')
                    ->setCellValue('C'.($no_row+2), '')
                    ->setCellValue('D'.($no_row+2), '')
                    ->setCellValue('E'.($no_row+2), '')
                    ->setCellValue('F'.($no_row+2), '')
                    ->setCellValue('G'.($no_row+2), '')
                    ->setCellValue('H'.($no_row+2), '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = $no_row+3;
        $t_list_data_pembelian = 0;
        foreach ($data["list_data_pembelian"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_suplier)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($value->total_pembayaran_pnn_tr_header,2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_pembelian += -1*$value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_pembelian, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_pembelian, 2, ',', '.')
                );
        




         $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), '4.')
                    ->setCellValue('B'.($no_row+2), 'Pembeyaran Hutang')
                    ->setCellValue('C'.($no_row+2), '')
                    ->setCellValue('D'.($no_row+2), '')
                    ->setCellValue('E'.($no_row+2), '')
                    ->setCellValue('F'.($no_row+2), '')
                    ->setCellValue('G'.($no_row+2), '')
                    ->setCellValue('H'.($no_row+2), '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = $no_row+3;
        $t_list_data_hutang = 0;
        foreach ($data["list_data_hutang"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_suplier)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format((-1*$value->total_pembayaran_pnn_tr_header),2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_hutang += $value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }
        $t_list_data_hutang = -1*$t_list_data_hutang;

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_hutang, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_hutang, 2, ',', '.')
                );




        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), '5.')
                    ->setCellValue('B'.($no_row+2), 'Retur Penjualan')
                    ->setCellValue('C'.($no_row+2), '')
                    ->setCellValue('D'.($no_row+2), '')
                    ->setCellValue('E'.($no_row+2), '')
                    ->setCellValue('F'.($no_row+2), '')
                    ->setCellValue('G'.($no_row+2), '')
                    ->setCellValue('H'.($no_row+2), '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = $no_row+3;
        $t_list_data_retur_penjualan = 0;
        foreach ($data["list_data_retur_penjualan"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_rekanan)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($value->selisih,2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_retur_penjualan += $value->selisih; 
            $no_row++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_retur_penjualan, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_retur_penjualan, 2, ',', '.')
                );





        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.($no_row+2), '6.')
                    ->setCellValue('B'.($no_row+2), 'Retur Pembelian')
                    ->setCellValue('C'.($no_row+2), '')
                    ->setCellValue('D'.($no_row+2), '')
                    ->setCellValue('E'.($no_row+2), '')
                    ->setCellValue('F'.($no_row+2), '')
                    ->setCellValue('G'.($no_row+2), '')
                    ->setCellValue('H'.($no_row+2), '');

        // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A7:B7');

        $no_row = $no_row+3;
        $t_list_data_retur_pembelian = 0;
        foreach ($data["list_data_retur_pembelian"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_tr_header)
                    ->setCellValue('B'.$no_row, $value->nama_suplier)
                    ->setCellValue('C'.$no_row, ":")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format((-1*$value->selisih),2,',','.'))
                    ->setCellValue('E'.$no_row, "")
                    ->setCellValue('F'.$no_row, "")
                    ->setCellValue('G'.$no_row, "");
            // print_r($value);
            $t_list_data_retur_pembelian += -1*$value->selisih;
            $no_row++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.$no_row, "Jumlah")
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($t_list_data_retur_pembelian, 2, ',', '.'))
                    ->setCellValue('G'.$no_row, "Rp. ".number_format($t_list_data_retur_pembelian, 2, ',', '.')
                );


        $t_all = $t_list_data_penjualan + $t_list_data_piutang + $t_list_data_pembelian + $t_list_data_hutang + $t_list_data_retur_pembelian + $t_list_data_retur_penjualan;

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.($no_row+1), "Jumlah Seluruhnya")
                    // ->setCellValue('D'.($no_row+1), "Rp. ".number_format($t_list_data_retur_pembelian, 2, ',', '.'))
                    ->setCellValue('G'.($no_row+1), "Rp. ".number_format($t_all, 2, ',', '.')
                );

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_arus_kas');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_arus_kas.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_arus_kas.xlsx', NULL);
    }
}
