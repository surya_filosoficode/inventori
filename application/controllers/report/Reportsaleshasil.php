<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportsaleshasil extends CI_Controller {

    public $keterangan_record_stok = "panjualan detail";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_sales_hasil', 'rp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        // $data["list_data"] = array();
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));
        $this->load->view('index', $data);
    }

#------------------------------show---------------------------------#


    public function get_sales_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];


            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_tgl($tgl_start, $tgl_finish, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }

            $data["list_data"] = $array_data_send;
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_sales_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_triwulan($th_triwulan, $array_where_in, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
        }


        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_sales_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_th($th_start, $th_finish, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_sales_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_bulan($bulan, $th, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
        }

        // print_r($data);
        $this->load->view('index', $data);
    }


#------------------------------show---------------------------------#


#------------------------------main---------------------------------#
    public function main_get_sales_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $array_start = explode("-", $tgl_start);
            $m_start = $this->array_of_month[(int)$array_start[1]];

            $array_finish = explode("-", $tgl_finish);
            $m_finish = $this->array_of_month[(int)$array_finish[1]];

            $data["str_periode"] = "Periode ".$array_start[2]." ".$m_start." ".$array_start[0]." - "
            .$array_finish[2]." ".$m_finish." ".$array_finish[0];


            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_tgl($tgl_start, $tgl_finish, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }

            $data["list_data"] = $array_data_send;
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_sales_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);
            $array_where_in = array();
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
            }

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_triwulan($th_triwulan, $array_where_in, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
        }


        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_sales_th($th_start = "0", $th_finish = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($th_start != "0" && $th_finish != "0"){
            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_th($th_start, $th_finish, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
            $data["str_periode"] = "Periode ".$th_start." - ". $th_finish;
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_sales_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_sales_hasil";
        $data["str_periode"] = "";
        $data["sales"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $array_data_send = array();
            foreach ($data["sales"] as $key => $value) {
                $id_sales = $value->id_sales;
                $tmp_data = $this->rp->get_sales_hasil_bulan($bulan, $th, array("id_sales"=>$id_sales));

                $array_data_send[$id_sales] = $tmp_data;
            }
            $data["list_data"] = $array_data_send;
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }
#------------------------------main---------------------------------#


#------------------------------print---------------------------------#
    public function print_get_sales_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_sales_tgl($tgl_start, $tgl_finish);
        }
        
        // print_r($data);
        $this->load->view('print/print_sales_hasil', $data);
    }

    public function print_get_sales_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_sales_triwulan($triwulan, $th_triwulan);
        }

        // print_r($data);
        $this->load->view('print/print_sales_hasil', $data);
    }

    public function print_get_sales_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_sales_th($th_start, $th_finish);
        }

        // print_r($data);
        $this->load->view('print/print_sales_hasil', $data);
    }

    public function print_get_sales_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_sales_bulan($bulan, $th);
        }

        // print_r($data);
        $this->load->view('print/print_sales_hasil', $data);
    }
#------------------------------print---------------------------------#


#------------------------------excel---------------------------------#
    public function excel_get_sales_tgl($tgl_start = "0", $tgl_finish = "0"){
        $data = array();
        if($tgl_start != "0" && $tgl_finish != "0"){
            $data = $this->main_get_sales_tgl($tgl_start, $tgl_finish);
            $this->convert_excel($data);
        }
        
        // print_r($data);
        // $this->load->view('print/print_sales_hasil', $data);
    }

    public function excel_get_sales_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_sales_triwulan($triwulan, $th_triwulan);
            $this->convert_excel($data);
        }

        // print_r($data);
        // $this->load->view('print/print_sales_hasil', $data);
    }

    public function excel_get_sales_th($th_start = "0", $th_finish = "0"){
        $data = array();
        if($th_start != "0" && $th_finish != "0"){
            $data = $this->main_get_sales_th($th_start, $th_finish);
            $this->convert_excel($data);
        }

        // print_r($data);
        // $this->load->view('print/print_sales_hasil', $data);
    }

    public function excel_get_sales_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_sales_bulan($bulan, $th);
            $this->convert_excel($data);
        }

        // print_r($data);
        // $this->load->view('print/print_sales_hasil', $data);
    }
#------------------------------excel---------------------------------#

    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'LAPORAN PENJUALAN SALES')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'Kode Sales')
                    ->setCellValue('B6', 'Nama Sales')
                    ->setCellValue('C6', 'Total Penjualan')
                    ->setCellValue('D6', 'Pendapatan');

        $no_row = 7;
        // $t_harga = 0;
        foreach ($data["sales"] as $key => $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $value->id_sales)
                    ->setCellValue('B'.$no_row, $value->nama_sales)
                    ->setCellValue('C'.$no_row, "Rp. ".number_format($data["list_data"][$value->id_sales][0]->total_pembayaran_pnn_tr_header ,2, '.', ','))
                    ->setCellValue('D'.$no_row, "Rp. ".number_format($data["list_data"][$value->id_sales][0]->total_pembayaran_pnn_tr_header ,2, '.', ','));
            // print_r($value);
            // $t_harga += $value->total_pembayaran_pnn_tr_header;
            $no_row++;
        }
        
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_penjualan_sales');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_penjualan_sales.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_penjualan_sales.xlsx', NULL);
    }

}
