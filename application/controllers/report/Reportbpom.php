<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportbpom extends CI_Controller {

    public $keterangan_record_stok = "report_bpom_main";
    public $array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('report/report_bpom', 'rp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();
    }

    public function index(){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        // print_r($data);
        $this->load->view('index', $data);
    }

#------------------------------show----------------------------------#
    public function get_bpom_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);

            $str_in = "(";
            $array_where_in = array();
            $nop = 0;
            $p =1;
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
                if($nop == 0){
                    $p = $i;
                }
                $nop++;
            }
            $str_in .= implode(",", $array_where_in);
            $str_in .= ")";

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $array_of_item = [];

            $where = array(
                    "MONTH(periode_record_item) in ".$str_in,
                    "YEAR(periode_record_item) = ".$th_triwulan
                );

            $where_in = array(
                    "MONTH(periode_record_item) ="=>$p,
                    "YEAR(periode_record_item) ="=>$th_triwulan
                );

            $check_data = $this->mm->get_data_all_where("record_item", $where_in);

            
            foreach ($check_data as $key => $value) {
                $tmp = [
                    "item"=>$this->mm->get_data_each("item", ["id_item"=>$value->id_item]),
                    "record_item"=>$value,
                    "pengeluaran"=>[
                        "rs"=>0,
                        "pbf"=>0,
                        "apotik"=>0,
                        "sp"=>0,
                        "pm"=>0,
                        "kl"=>0,
                        "to"=>0,
                        "ln"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ],
                    "pemasukan"=>[
                        "pabrik"=>0,
                        "pbf"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ]
                ];

                $where_tmp = array(
                    "trd.id_item"=> $value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );

                $data_penjualan = $this->rp->get_penjualan_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($this->db->last_query());
                // print_r($data_penjualan);

                $data_sample = $this->rp->get_sample_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_sample as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["lainnya"] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_sample);

                

                $data_pembelian = $this->rp->get_pembelian_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_pembelian as $key_dp => $value_dp) {
                    $tmp["pemasukan"][$value_dp->jenis_suplier] += $value_dp->jml_item_tr_detail;
                }


            #--------------------retur_penjualan-------------------#
                $data_retur_in_penjualan = $this->rp->get_retur_penjualan_in_sum_triwulan($array_where_in, $where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_in_penjualan as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                // print_r($data_retur_in_penjualan);


                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );
                $data_retur_out_penjualan = $this->rp->get_retur_penjualan_out_sum_triwulan($array_where_in, $where_tmp_p);


                foreach ($data_retur_out_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_retur_out_penjualan);
            #--------------------retur_penjualan-------------------#


            #--------------------retur_pembelian-------------------#
                $data_retur_out_pembelian = $this->rp->get_retur_pembelian_out_sum_triwulan($array_where_in, $where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_out_pembelian as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in"=>$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );

                $data_retur_in_pembelian_p = $this->rp->get_retur_pembelian_in_sum_triwulan($array_where_in, $where_tmp_p);

                foreach ($data_retur_in_pembelian_p as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                }

                // print_r($data_retur_in_pembelian_p);
            #--------------------retur_pembelian-------------------#

            #--------------------retur_sample----------------------#
                $where_tmp_sm = array(
                    // "MONTH(trh.tgl_transaksi_tr_header) in "=>$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );
                $distict_id_header_p = $this->rp->get_disticnt_sample_out_sum_triwulan($array_where_in, $where_tmp_sm);

                foreach ($distict_id_header_p as $key_dt => $value_dt) {
                    $where_tmp_p = array(
                        "trh.id_tr_header_p"=>$value_dt->id_tr_sm_retur_header,
                        "trd.id_item"=>$value->id_item,
                        // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                        "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                    );

                    $data_retur_in_sample_p = $this->rp->get_retur_sample_in_sum_triwulan($array_where_in, $where_tmp_p);

                    // print_r($data_retur_in_sample_p);

                    foreach ($data_retur_in_sample_p as $key_dp => $value_dp) {
                        $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    }
                }
                // print_r($distict_id_header_p);
            #--------------------retur_sample----------------------#

                array_push($array_of_item, $tmp);
            }

            $data["list_data"] = $array_of_item;
            $data["periode"] = $data["str_periode"];
        }
        
        // print_r($data);
        $this->load->view('index', $data);
    }

    public function get_bpom_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        // print_r(date("Y-m-d"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;


            $array_of_item = [];

            $where = array(
                    "MONTH(periode_record_item) ="=>$bulan,
                    "YEAR(periode_record_item) ="=>$th
                );

            $check_data = $this->mm->get_data_all_where("record_item", $where);
            foreach ($check_data as $key => $value) {
                $tmp = [
                    "item"=>$this->mm->get_data_each("item", ["id_item"=>$value->id_item]),
                    "record_item"=>$value,
                    "pengeluaran"=>[
                        "rs"=>0,
                        "pbf"=>0,
                        "apotik"=>0,
                        "sp"=>0,
                        "pm"=>0,
                        "kl"=>0,
                        "to"=>0,
                        "ln"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ],
                    "pemasukan"=>[
                        "pabrik"=>0,
                        "pbf"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ]
                ];

                $where_tmp = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );

                $data_penjualan = $this->rp->get_penjualan_sum_bln($where_tmp);
                foreach ($data_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }

                $data_sample = $this->rp->get_sample_sum_bln($where_tmp);
                foreach ($data_sample as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["lainnya"] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_sample);

                

                $data_pembelian = $this->rp->get_pembelian_sum_bln($where_tmp);
                foreach ($data_pembelian as $key_dp => $value_dp) {
                    $tmp["pemasukan"][$value_dp->jenis_suplier] += $value_dp->jml_item_tr_detail;
                }


            #--------------------retur_penjualan-------------------#
                $data_retur_in_penjualan = $this->rp->get_retur_penjualan_in_sum_bln($where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_in_penjualan as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                // print_r($data_retur_in_penjualan);


                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );
                $data_retur_out_penjualan = $this->rp->get_retur_penjualan_out_sum_bln($where_tmp_p);

                foreach ($data_retur_out_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_retur_out_penjualan);
            #--------------------retur_penjualan-------------------#


            #--------------------retur_pembelian-------------------#
                $data_retur_out_pembelian = $this->rp->get_retur_pembelian_out_sum_bln($where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_out_pembelian as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );

                $data_retur_in_pembelian_p = $this->rp->get_retur_pembelian_in_sum_bln($where_tmp_p);

                foreach ($data_retur_in_pembelian_p as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                }

                // print_r($data_retur_in_pembelian_p);
            #--------------------retur_pembelian-------------------#

            #--------------------retur_sample----------------------#
                $where_tmp_sm = array(
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );
                $distict_id_header_p = $this->rp->get_disticnt_sample_out_sum_bln($where_tmp_sm);

                foreach ($distict_id_header_p as $key_dt => $value_dt) {
                    $where_tmp_p = array(
                        "trh.id_tr_header_p"=>$value_dt->id_tr_sm_retur_header,
                        "trd.id_item"=>$value->id_item,
                        "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                        "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                    );

                    $data_retur_in_sample_p = $this->rp->get_retur_sample_in_sum_bln($where_tmp_p);

                    // print_r($data_retur_in_sample_p);

                    foreach ($data_retur_in_sample_p as $key_dp => $value_dp) {
                        $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    }
                }
                // print_r($distict_id_header_p);
            #--------------------retur_sample----------------------#

                array_push($array_of_item, $tmp);
            }

            $data["list_data"] = $array_of_item;
            $data["periode"] = $bulan."/".$th;
            // print_r($array_of_item);
        }

        // print_r($data);
        $this->load->view('index', $data);
    }
#------------------------------show----------------------------------#

#------------------------------main----------------------------------#
    public function main_get_bpom_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));

        $data["list_data"] = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $array_periode = explode("-", $triwulan);

            $str_in = "(";
            $array_where_in = array();
            $nop = 0;
            $p =1;
            for ($i=$array_periode[0]; $i <= $array_periode[1]; $i++) { 
                array_push($array_where_in, $i);
                if($nop == 0){
                    $p = $i;
                }
                $nop++;
            }
            $str_in .= implode(",", $array_where_in);
            $str_in .= ")";

            $array_triwulan = explode("-", $triwulan);

            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$array_triwulan[0]]." - ".$this->array_of_month[(int)$array_triwulan[1]]." ". $th_triwulan;

            $array_of_item = [];

            $where = array(
                    "MONTH(periode_record_item) in ".$str_in,
                    "YEAR(periode_record_item) = ".$th_triwulan
                );

            $where_in = array(
                    "MONTH(periode_record_item) ="=>$p,
                    "YEAR(periode_record_item) ="=>$th_triwulan
                );

            $check_data = $this->mm->get_data_all_where("record_item", $where_in);

            
            foreach ($check_data as $key => $value) {
                $tmp = [
                    "item"=>$this->mm->get_data_each("item", ["id_item"=>$value->id_item]),
                    "record_item"=>$value,
                    "pengeluaran"=>[
                        "rs"=>0,
                        "pbf"=>0,
                        "apotik"=>0,
                        "sp"=>0,
                        "pm"=>0,
                        "kl"=>0,
                        "to"=>0,
                        "ln"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ],
                    "pemasukan"=>[
                        "pabrik"=>0,
                        "pbf"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ]
                ];

                $where_tmp = array(
                    "trd.id_item"=> $value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );

                $data_penjualan = $this->rp->get_penjualan_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($this->db->last_query());
                // print_r($data_penjualan);

                $data_sample = $this->rp->get_sample_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_sample as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["lainnya"] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_sample);

                

                $data_pembelian = $this->rp->get_pembelian_sum_triwulan($array_where_in, $where_tmp);
                foreach ($data_pembelian as $key_dp => $value_dp) {
                    $tmp["pemasukan"][$value_dp->jenis_suplier] += $value_dp->jml_item_tr_detail;
                }


            #--------------------retur_penjualan-------------------#
                $data_retur_in_penjualan = $this->rp->get_retur_penjualan_in_sum_triwulan($array_where_in, $where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_in_penjualan as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                // print_r($data_retur_in_penjualan);


                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );
                $data_retur_out_penjualan = $this->rp->get_retur_penjualan_out_sum_triwulan($array_where_in, $where_tmp_p);


                foreach ($data_retur_out_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_retur_out_penjualan);
            #--------------------retur_penjualan-------------------#


            #--------------------retur_pembelian-------------------#
                $data_retur_out_pembelian = $this->rp->get_retur_pembelian_out_sum_triwulan($array_where_in, $where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_out_pembelian as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    // "MONTH(trh.tgl_transaksi_tr_header) in"=>$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );

                $data_retur_in_pembelian_p = $this->rp->get_retur_pembelian_in_sum_triwulan($array_where_in, $where_tmp_p);

                foreach ($data_retur_in_pembelian_p as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                }

                // print_r($data_retur_in_pembelian_p);
            #--------------------retur_pembelian-------------------#

            #--------------------retur_sample----------------------#
                $where_tmp_sm = array(
                    // "MONTH(trh.tgl_transaksi_tr_header) in "=>$str_in,
                    "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                );
                $distict_id_header_p = $this->rp->get_disticnt_sample_out_sum_triwulan($array_where_in, $where_tmp_sm);

                foreach ($distict_id_header_p as $key_dt => $value_dt) {
                    $where_tmp_p = array(
                        "trh.id_tr_header_p"=>$value_dt->id_tr_sm_retur_header,
                        "trd.id_item"=>$value->id_item,
                        // "MONTH(trh.tgl_transaksi_tr_header) in ".$str_in,
                        "YEAR(trh.tgl_transaksi_tr_header)"=>$th_triwulan
                    );

                    $data_retur_in_sample_p = $this->rp->get_retur_sample_in_sum_triwulan($array_where_in, $where_tmp_p);

                    // print_r($data_retur_in_sample_p);

                    foreach ($data_retur_in_sample_p as $key_dp => $value_dp) {
                        $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    }
                }
                // print_r($distict_id_header_p);
            #--------------------retur_sample----------------------#

                array_push($array_of_item, $tmp);
            }

            $data["list_data"] = $array_of_item;
            $data["periode"] = $data["str_periode"];
        }
        
        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

    public function main_get_bpom_bulan($bulan = "0", $th = "0"){
        $data["page"] = "report_bpom_main";
        $data["str_periode"] = "";
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        
        // print_r(date("Y-m-d"));

        $data["list_data"] = array();
        if($bulan != "0" && $th != "0"){
            $data["str_periode"] = "Periode ".$this->array_of_month[(int)$bulan]." ". $th;


            $array_of_item = [];

            $where = array(
                    "MONTH(periode_record_item) ="=>$bulan,
                    "YEAR(periode_record_item) ="=>$th
                );

            $check_data = $this->mm->get_data_all_where("record_item", $where);
            foreach ($check_data as $key => $value) {
                $tmp = [
                    "item"=>$this->mm->get_data_each("item", ["id_item"=>$value->id_item]),
                    "record_item"=>$value,
                    "pengeluaran"=>[
                        "rs"=>0,
                        "pbf"=>0,
                        "apotik"=>0,
                        "sp"=>0,
                        "pm"=>0,
                        "kl"=>0,
                        "to"=>0,
                        "ln"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ],
                    "pemasukan"=>[
                        "pabrik"=>0,
                        "pbf"=>0,
                        "retur"=>0,
                        "lainnya"=>0
                    ]
                ];

                $where_tmp = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );

                $data_penjualan = $this->rp->get_penjualan_sum_bln($where_tmp);
                foreach ($data_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }

                $data_sample = $this->rp->get_sample_sum_bln($where_tmp);
                foreach ($data_sample as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["lainnya"] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_sample);

                

                $data_pembelian = $this->rp->get_pembelian_sum_bln($where_tmp);
                foreach ($data_pembelian as $key_dp => $value_dp) {
                    $tmp["pemasukan"][$value_dp->jenis_suplier] += $value_dp->jml_item_tr_detail;
                }


            #--------------------retur_penjualan-------------------#
                $data_retur_in_penjualan = $this->rp->get_retur_penjualan_in_sum_bln($where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_in_penjualan as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                // print_r($data_retur_in_penjualan);


                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );
                $data_retur_out_penjualan = $this->rp->get_retur_penjualan_out_sum_bln($where_tmp_p);

                foreach ($data_retur_out_penjualan as $key_dp => $value_dp) {
                    $tmp["pengeluaran"][$value_dp->jenis_rekanan] += $value_dp->jml_item_tr_detail;
                }
                // print_r($data_retur_out_penjualan);
            #--------------------retur_penjualan-------------------#


            #--------------------retur_pembelian-------------------#
                $data_retur_out_pembelian = $this->rp->get_retur_pembelian_out_sum_bln($where_tmp);

                $id_tr_header_p = "0";
                foreach ($data_retur_out_pembelian as $key_dp => $value_dp) {
                    $tmp["pengeluaran"]["retur"] += $value_dp->jml_item_tr_detail;
                    $id_tr_header_p = $value_dp->id_tr_header_p;
                }

                $where_tmp_p = array(
                    "trd.id_item"=>$value->id_item,
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );

                $data_retur_in_pembelian_p = $this->rp->get_retur_pembelian_in_sum_bln($where_tmp_p);

                foreach ($data_retur_in_pembelian_p as $key_dp => $value_dp) {
                    $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                }

                // print_r($data_retur_in_pembelian_p);
            #--------------------retur_pembelian-------------------#

            #--------------------retur_sample----------------------#
                $where_tmp_sm = array(
                    "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                    "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                );
                $distict_id_header_p = $this->rp->get_disticnt_sample_out_sum_bln($where_tmp_sm);

                foreach ($distict_id_header_p as $key_dt => $value_dt) {
                    $where_tmp_p = array(
                        "trh.id_tr_header_p"=>$value_dt->id_tr_sm_retur_header,
                        "trd.id_item"=>$value->id_item,
                        "MONTH(trh.tgl_transaksi_tr_header) ="=>$bulan,
                        "YEAR(trh.tgl_transaksi_tr_header) ="=>$th
                    );

                    $data_retur_in_sample_p = $this->rp->get_retur_sample_in_sum_bln($where_tmp_p);

                    // print_r($data_retur_in_sample_p);

                    foreach ($data_retur_in_sample_p as $key_dp => $value_dp) {
                        $tmp["pemasukan"]["retur"] += $value_dp->jml_item_tr_detail;
                    }
                }
                // print_r($distict_id_header_p);
            #--------------------retur_sample----------------------#

                array_push($array_of_item, $tmp);
            }

            $data["list_data"] = $array_of_item;
            $data["periode"] = $bulan."/".$th;
            // print_r($array_of_item);
        }

        // print_r($data);
        // $this->load->view('index', $data);
        return $data;
    }

#------------------------------main----------------------------------#

#------------------------------print----------------------------------#
    public function print_get_bpom_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_bpom_triwulan($triwulan, $th_triwulan);
        }
        
        // print_r($data);
        $this->load->view('print/print_bpom_main', $data);
        // return $data;
    }

    public function print_get_bpom_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_bpom_bulan($bulan, $th);
        }

        // print_r($data);
        $this->load->view('print/print_bpom_main', $data);
        // return $data;
    }
#------------------------------print----------------------------------#

#------------------------------excel----------------------------------#
    public function excel_get_bpom_triwulan($triwulan = "0", $th_triwulan = "0"){
        $data = array();
        if($triwulan != "0" && $th_triwulan != "0"){
            $data = $this->main_get_bpom_triwulan($triwulan, $th_triwulan);
            $this->convert_excel($data);
        }
        
        // excel_r($data);
        // $this->load->view('excel/excel_bpom_main', $data);
        // return $data;
    }

    public function excel_get_bpom_bulan($bulan = "0", $th = "0"){
        $data = array();
        if($bulan != "0" && $th != "0"){
            $data = $this->main_get_bpom_bulan($bulan, $th);
            $this->convert_excel($data);
        }

        // excel_r($data);
        // $this->load->view('excel/excel_bpom_main', $data);
        // return $data;
    }
#------------------------------excel----------------------------------#


    public function convert_excel($data){
        /** Error reporting */
        error_reporting(E_ALL);
        require_once APPPATH.'third_party/PHPExcel.php';

        $objPHPExcel = new PHPExcel();


       

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Filosofi_code")
                                     ->setLastModifiedBy("Filosofi_code Application")
                                     ->setTitle("Laporan Transaksi Penjualan")
                                     ->setSubject("Office 2007 XLSX Laporan Transaksi Penjualan")
                                     ->setDescription("Laporan Transaksi Penjualan for Office 2007 XLSX")
                                     ->setKeywords("office 2007 openxml php")
                                     ->setCategory("Laporan Transaksi Penjualan");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'REKAPITULASI LAPORAN OBAT')
                    ->setCellValue('A2', $data["str_periode"])
                    ->setCellValue('A3', '')
                    ->setCellValue('A4', '')
                    ->setCellValue('A5', '');

        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A6', 'NO')
                    ->setCellValue('B6', 'PERIODE')
                    ->setCellValue('C6', 'KODE OBAT(NIE)')
                    ->setCellValue('D6', 'NAMA OBAT')
                    ->setCellValue('E6', 'STOK AWAL')
                    ->setCellValue('F6', 'PEMASUKAN')
                    ->setCellValue('J6', 'PENGELUARAN')
                    ->setCellValue('S6', 'STOK AKHIR');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:A7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:B7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C6:C7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D6:D7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:E7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('S6:S7');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F6:I7');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J6:R7');


        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('F8', 'PABRIK')
                    ->setCellValue('G8', 'PBF')
                    ->setCellValue('H8', 'RETUR')
                    ->setCellValue('I8', 'LAINNYA')
                    ->setCellValue('J8', 'RUMAH SAKIT')
                    ->setCellValue('K8', 'APOTIK')
                    ->setCellValue('L8', 'PBF')
                    ->setCellValue('M8', 'SARANA PEMERINTAH')
                    ->setCellValue('N8', 'PUSKESMAS')
                    ->setCellValue('O8', 'KLINIK')
                    ->setCellValue('P8', 'TOKO OBAT')
                    ->setCellValue('Q8', 'RETUR')
                    ->setCellValue('R8', 'LAINNYA');

        $no_row = 10;
        $no = 1;
        foreach ($data["list_data"] as $key => $value) {
            $t_stok = (float)$value["record_item"]->stok_record_item+(float)$value["record_item"]->rusak_record_item;
            $pengeluaran = +(float)$value["pengeluaran"]["rs"]
                            +(float)$value["pengeluaran"]["apotik"]
                            +(float)$value["pengeluaran"]["pbf"]
                            +(float)$value["pengeluaran"]["sp"]
                            +(float)$value["pengeluaran"]["pm"]
                            +(float)$value["pengeluaran"]["kl"]
                            +(float)$value["pengeluaran"]["to"]
                            +(float)$value["pengeluaran"]["retur"]
                            +(float)$value["pengeluaran"]["lainnya"];

            $pemasukan = (float)$value["pemasukan"]["pabrik"] +
                            (float)$value["pemasukan"]["pbf"] +
                            (float)$value["pemasukan"]["retur"] +
                            (float)$value["pemasukan"]["lainnya"];

            $t_stok_akhir = $t_stok + $pemasukan - $pengeluaran;

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$no_row, $no)
                    ->setCellValue('B'.$no_row, $data["periode"])
                    ->setCellValue('C'.$no_row, $value["item"]["id_item"])
                    ->setCellValue('D'.$no_row, $value["item"]["nama_item"].", ".$value["item"]["satuan"])
                    ->setCellValue('E'.$no_row, $t_stok)
                    ->setCellValue('F'.$no_row, $value["pemasukan"]["pabrik"])
                    ->setCellValue('G'.$no_row, $value["pemasukan"]["pbf"])
                    ->setCellValue('H'.$no_row, $value["pemasukan"]["retur"])
                    ->setCellValue('I'.$no_row, $value["pemasukan"]["lainnya"])

                    ->setCellValue('J'.$no_row, $value["pengeluaran"]["rs"])
                    ->setCellValue('K'.$no_row, $value["pengeluaran"]["apotik"])
                    ->setCellValue('L'.$no_row, $value["pengeluaran"]["pbf"])
                    ->setCellValue('M'.$no_row, $value["pengeluaran"]["sp"])
                    ->setCellValue('N'.$no_row, $value["pengeluaran"]["pm"])
                    ->setCellValue('O'.$no_row, $value["pengeluaran"]["kl"])
                    ->setCellValue('P'.$no_row, $value["pengeluaran"]["to"])
                    ->setCellValue('Q'.$no_row, $value["pengeluaran"]["retur"])
                    ->setCellValue('R'.$no_row, $value["pengeluaran"]["lainnya"])
                    ->setCellValue('S'.$no_row, $t_stok_akhir);
            // print_r($value);
            $no_row++;
        }
        
        

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Laporan_format_bpom');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $objWriter->save('./Laporan_format_bpom.xlsx'); 

        $this->load->helper('download');
        force_download('./Laporan_format_bpom.xlsx', NULL);
    }
}
