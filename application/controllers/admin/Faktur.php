<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faktur extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('admin/main_item', 'mi');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/item_main', 'im');
        $this->load->model('admin/main_penjualan', 'mp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

    public function index($id_tr_header = "0"){
        $data = array();
        $data["setting"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
        if($id_tr_header != "0"){
            $data_header = array();
            $data_detail_new = array();
            
            $data_header = $this->mp->get_full_header_penjualan(array("th.id_tr_header"=>$id_tr_header));
            $data_detail = $this->mp->get_full_detail_penjualan(array("td.id_tr_header"=>$id_tr_header));
            foreach ($data_detail as $key => $value) {
                $data_detail_new[$value->id_item] = $value;
            }
            
            $data["data_header"] = $data_header;
            $data["data_detail"] = $data_detail_new;
        }
        // print_r($data);

        $this->load->view("admin/faktur", $data);
    }

	
}
