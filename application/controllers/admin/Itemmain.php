<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Itemmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('admin/main_item', 'mi');
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/item_main', 'im');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }


#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

	public function index(){
		$data["page"] = "produk_main";
        $data["list_data"]          = $this->im->get_data_produk_where(array("is_del_item"=>"0"));
        $data["list_data_brand"]    = $this->im->get_data_brand_where(array("is_del_brand"=>"0"));

        $distinct_nama = $this->im->get_distinct_nama_produk(array());
        $distinct_nama_str = "";
        foreach ($distinct_nama as $key => $value) {
            $distinct_nama_str .= "<option value=\"".$value->nama_item."\">";
        }

        // print_r($distinct_nama);
        $data["option_nama_item"] = $distinct_nama_str;
        // print_r($data);
		$this->load->view('index', $data);
	}

    public function val_form_insert_item(){
        $config_val_input = array(
                array(
                    'field'=>'id_brand',
                    'label'=>'id_brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nama_item',
                    'label'=>'nama_item',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'satuan',
                    'label'=>'satuan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'kode_produksi',
                    'label'=>'kode_produksi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'tgl_kadaluarsa',
                    'label'=>'tgl_kadaluarsa',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'harga_bruto',
                    'label'=>'harga_bruto',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'harga_netto',
                    'label'=>'harga_netto',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'harga_jual',
                    'label'=>'harga_jual',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_brand"=>"",
                    "nama_item"=>"",
                    "satuan"=>"",
                    "kode_produksi"=>"",
                    "tgl_kadaluarsa"=>"",
                    "harga_bruto"=>"",
                    "harga_netto"=>"",
                    "harga_jual"=>""
                );

        if($this->val_form_insert_item()){
            $id_brand   = $this->input->post("id_brand");
            $nama_item  = $this->input->post("nama_item");
            $satuan     = $this->input->post("satuan");
            $kode_produksi     = $this->input->post("kode_produksi");
            $tgl_kadaluarsa     = $this->input->post("tgl_kadaluarsa");
            $harga_bruto= $this->input->post("harga_bruto");
            $harga_netto= $this->input->post("harga_netto");
            $harga_jual = $this->input->post("harga_jual");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "0";

            #check kode_produksi
            
            $cek_kode_produksi = $this->mm->get_data_each("item", array("kode_produksi_item"=>$kode_produksi));
            if(!$cek_kode_produksi){
                $insert = $this->mi->item_insert($id_brand, $nama_item, $satuan, $kode_produksi, $tgl_kadaluarsa, $harga_bruto, $harga_netto, $harga_jual, $admin_create, $date_update, $is_delete);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_brand"]     = strip_tags(form_error('id_brand'));
            $msg_detail["nama_item"]    = strip_tags(form_error('nama_item'));
            $msg_detail["satuan"]       = strip_tags(form_error('satuan'));
            $msg_detail["kode_produksi"]       = strip_tags(form_error('kode_produksi'));
            $msg_detail["tgl_kadaluarsa"]       = strip_tags(form_error('tgl_kadaluarsa'));
            $msg_detail["harga_bruto"]  = strip_tags(form_error('harga_bruto'));
            $msg_detail["harga_netto"]  = strip_tags(form_error('harga_netto'));
            $msg_detail["harga_jual"]   = strip_tags(form_error('harga_jual'));
        }

        // $msg_detail["list_data"] = $this->im->get_data_brand_where(array());
        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_item");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function get_data_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_item"])){
            $id_item = $this->input->post("id_item");

            // print_r($id_item);
            $data = $this->im->get_data_produk_where_each(array("id_item"=>$id_item));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["data"] = $data;
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "id_brand"=>"",
                    "nama_item"=>"",
                    "satuan"=>"",
                    "kode_produksi"=>"",
                    "tgl_kadaluarsa"=>"",
                    "harga_bruto"=>"",
                    "harga_netto"=>"",
                    "harga_jual"=>""
                );

        if($this->val_form_insert_item()){
            $id_item    = $this->input->post("id_item");
            $id_brand   = $this->input->post("id_brand");
            $nama_item  = $this->input->post("nama_item");
            $satuan     = $this->input->post("satuan");
            $kode_produksi     = $this->input->post("kode_produksi");
            $tgl_kadaluarsa     = $this->input->post("tgl_kadaluarsa");
            $harga_bruto= $this->input->post("harga_bruto");
            $harga_netto= $this->input->post("harga_netto");
            $harga_jual = $this->input->post("harga_jual");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "0";

            $where  = array("id_item"       =>$id_item);
            $set    = array("id_brand"      =>$id_brand,
                            "nama_item"     =>$nama_item,
                            "satuan"        =>$satuan,
                            "kode_produksi_item"        =>$kode_produksi,
                            "tgl_kadaluarsa_item"        =>$tgl_kadaluarsa, 
                            "harga_bruto"   =>$harga_bruto,
                            "harga_netto"   =>$harga_netto, 
                            "harga_jual"    =>$harga_jual,

                            "admin_create_item" =>$admin_create,
                            "time_up_item"      =>$date_update, 
                            "is_del_item"       =>$is_delete
                        );


            $cek_kode_produksi = $this->mm->get_data_each("item", array("kode_produksi_item"=>$kode_produksi, "id_item!="=>$id_item));

            if(!$cek_kode_produksi){
                $update = $this->mm->update_data("item", $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_brand"]     = strip_tags(form_error('id_brand'));
            $msg_detail["nama_item"]    = strip_tags(form_error('nama_item'));
            $msg_detail["satuan"]       = strip_tags(form_error('satuan'));
            $msg_detail["kode_produksi"]       = strip_tags(form_error('kode_produksi'));
            $msg_detail["tgl_kadaluarsa"]       = strip_tags(form_error('tgl_kadaluarsa'));
            $msg_detail["harga_bruto"]  = strip_tags(form_error('harga_bruto'));
            $msg_detail["harga_netto"]  = strip_tags(form_error('harga_netto'));
            $msg_detail["harga_jual"]   = strip_tags(form_error('harga_jual'));
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_item");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_item(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_item"])){
            $id_item = $this->input->post("id_item");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "1";
                // print_r($data_session);

            $where  = array("id_item"=>$id_item);
            $set    = array("admin_create_item"=>$admin_create, "time_up_item"=>$date_update, "is_del_item"=>$is_delete);

            $delete = $this->mm->update_data("item", $set, $where);
            if($delete){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_item");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------produk_main---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------brand_main----------------------------------
#===============================================================================

	public function index_jenis(){
		$data["page"] = "brand_main";
		$data["list_data"] = $this->im->get_data_brand_where(array("is_del_brand"=>"0"));
		$this->load->view('index', $data);
	}

	public function val_form_insert_brand(){
        $config_val_input = array(
                array(
                    'field'=>'nama_brand',
                    'label'=>'nama_brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_brand',
                    'label'=>'keterangan_brand',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function insert_brand(){
		$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_brand"=>"",
                    "keterangan_brand"=>""
                );

        if($this->val_form_insert_brand()){
        	$nama_brand 		= $this->input->post("nama_brand");
        	$keterangan_brand 	= $this->input->post("keterangan_brand");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "0";

        	$data = array(
        				"id_brand"          =>"",
        				"nama_brand"        =>$nama_brand,
        				"keterangan_brand"  =>$keterangan_brand,
                        "admin_create_brand"=>$admin_create, 
                        "time_up_brand"     =>$date_update, 
                        "is_del_brand"      =>$is_delete
        			);
        	$insert = $this->mm->insert_data("brand", $data);
        	if($insert){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
        	}
        }else{
        	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_brand"]		= strip_tags(form_error('nama_brand'));
            $msg_detail["keterangan_brand"] = strip_tags(form_error('keterangan_brand'));
        }

        // $msg_detail["list_data"] = $this->im->get_data_brand_where(array());
        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_brand");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
	}

    public function get_data_brand(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_brand"])){
            $id_brand = $this->input->post("id_brand");

            // print_r($id_brand);
            $data = $this->mm->get_data_each("brand", array("id_brand"=>$id_brand));
            if($data){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                $msg_detail["data"] = $data;
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function update_brand(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "nama_brand"=>"",
                    "keterangan_brand"=>""
                );

        if($this->val_form_insert_brand()){
            $id_brand           = $this->input->post("id_brand");
            $nama_brand         = $this->input->post("nama_brand");
            $keterangan_brand   = $this->input->post("keterangan_brand");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                // $is_delete      = "1";
                // print_r($data_session);

            $where  = array("id_brand"=>$id_brand);
            $set    = array("nama_brand"        =>$nama_brand,
                            "keterangan_brand"  =>$keterangan_brand,
                            "admin_create_brand"=>$admin_create, 
                            "time_up_brand"     =>$date_update
                        );

            $update = $this->mm->update_data("brand", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_brand"]       = strip_tags(form_error('nama_brand'));
            $msg_detail["keterangan_brand"] = strip_tags(form_error('keterangan_brand'));
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_brand");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function delete_brand(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_brand"])){
            $id_brand = $this->input->post("id_brand");

            $data_session = $this->auth_v0->get_session();
                $admin_create   = $data_session["id_admin"];
                $date_update    = date("Y-m-d H:i:s");
                $is_delete      = "1";
                // print_r($data_session);

            $where  = array("id_brand"=>$id_brand);
            $set    = array("admin_create_brand"=>$admin_create, "time_up_brand"=>$date_update, "is_del_brand"=>$is_delete);

            $delete = $this->mm->update_data("brand", $set, $where);
            if($delete){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/Itemmain/create_tbl_brand");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function create_db(){
        $data["list_data"] = $this->im->get_data_brand_where(array());
        $this->load->view("admin/brand_tbl", $data);
    }

#===============================================================================
#-----------------------------------brand_main----------------------------------
#===============================================================================
	
}
