<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporanmain extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->library("response_message");
		$this->load->library("Auth_v0");

		$this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
	}

	public function laporan_penjualan(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_sales(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_retur_penjualan(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_piutang(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}


	public function laporan_pembelian(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_retur_pembelian(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_hutang(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}



	public function laporan_arus_barang(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_format_bpom(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_pendapatan_sales(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}



	public function laporan_arus_kas(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

	public function laporan_laba_rugi(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

}
