<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekananmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/Main_rekanan', 'mr');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
        // print_r($_SESSION);
    }
#===============================================================================
#-----------------------------------LIST DATA PELANGGAN-------------------------
#===============================================================================

	public function index(){
		$data["page"] = "rekanan_main";
		$data["list_data"] = $this->mm->get_data_all_where("rekanan", array("is_delete"=>"0"));
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------END LIST DATA PELANGGAN---------------------
#===============================================================================

#===============================================================================
#-----------------------------------INSERT DATA PELANGGAN-----------------------
#===============================================================================
	public function val_form_insert_rekanan(){
        $config_val_input = array(
                array(
                    'field'=>'nama_rekanan',
                    'label'=>'Nama Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jenis_rekanan',
                    'label'=>'jenis rekanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'email_rekanan',
                    'label'=>'Email Pelanggan',
                    'rules'=>'required|valid_emails|is_unique[rekanan.email_rekanan]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'tlp_rekanan',
                    'label'=>'Tlp Pelanggan',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_ktr_rekanan',
                    'label'=>'Alamat Kantor Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'alamat_krm_rekanan',
                    'label'=>'Alamat Kirim Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'website_rekanan',
                    'label'=>'Website Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_rekanan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_rekanan"=>"",
                    "jenis_rekanan"=>"",
                    "email_rekanan"=>"",
                    "tlp_rekanan"=>"",
                    "alamat_ktr_rekanan"=>"",
                    "alamat_krm_rekanan"=>"",
                    "website_rekanan"=>""
                );

        if($this->val_form_insert_rekanan()){
            $nama_rekanan 		= $this->input->post("nama_rekanan");
            $jenis_rekanan       = $this->input->post("jenis_rekanan");
            $email_rekanan 		= $this->input->post("email_rekanan");
            $tlp_rekanan 		= $this->input->post("tlp_rekanan");
            $alamat_ktr_rekanan = $this->input->post("alamat_ktr_rekanan");
            $alamat_krm_rekanan = $this->input->post("alamat_krm_rekanan");
            $website_rekanan 	= $this->input->post("website_rekanan");
            
            $is_delete 		= "0";
            $id_admin 		= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

            $insert = $this->mr->rekanan_insert($nama_rekanan, $jenis_rekanan, $email_rekanan, $tlp_rekanan, $alamat_ktr_rekanan, $alamat_krm_rekanan, $website_rekanan, $is_delete, $time_update, $id_admin);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{

        	$msg_detail = array(
                    "nama_rekanan"=>"",
                    "jenis_rekanan"=>"",
                    "email_rekanan"=>"",
                    "tlp_rekanan"=>"",
                    "alamat_ktr_rekanan"=>"",
                    "alamat_krm_rekanan"=>"",
                    "website_rekanan"=>""
                );
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_rekanan"]			= strip_tags(form_error('nama_rekanan'));
            $msg_detail["jenis_rekanan"]         = strip_tags(form_error('jenis_rekanan'));
            $msg_detail["email_rekanan"] 		= strip_tags(form_error('email_rekanan'));
            $msg_detail["tlp_rekanan"] 			= strip_tags(form_error('tlp_rekanan'));
            $msg_detail["alamat_ktr_rekanan"] 	= strip_tags(form_error('alamat_ktr_rekanan'));
            $msg_detail["alamat_krm_rekanan"] 	= strip_tags(form_error('alamat_krm_rekanan'));
            $msg_detail["website_rekanan"] 		= strip_tags(form_error('website_rekanan'));
           
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("rekanan", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------END INSERT DATA PELANGGAN-------------------
#===============================================================================

#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_rekanan"])){
        	$id_rekanan = $this->input->post('id_rekanan');
        	$data = $this->mm->get_data_each("rekanan", array("id_rekanan"=>$id_rekanan, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update pelanggan--------------------------------
#===============================================================================

    public function val_form_update_rekanan(){
        $config_val_input = array(
                array(
                    'field'=>'nama_rekanan',
                    'label'=>'Nama Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jenis_rekanan',
                    'label'=>'jenis rekanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'email_rekanan',
                    'label'=>'Email Pelanggan',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'tlp_rekanan',
                    'label'=>'Tlp Pelanggan',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_ktr_rekanan',
                    'label'=>'Alamat Kantor Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'alamat_krm_rekanan',
                    'label'=>'Alamat Kirim Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'website_rekanan',
                    'label'=>'Website Pelanggan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_rekanan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_rekanan"=>"",
                    "jenis_rekanan"=>"",
                    "email_rekanan"=>"",
                    "tlp_rekanan"=>"",
                    "alamat_ktr_rekanan"=>"",
                    "alamat_krm_rekanan"=>"",
                    "website_rekanan"=>""
                );

        if($this->val_form_update_rekanan()){
        	$id_rekanan 		= $this->input->post("id_rekanan");

            $nama_rekanan 		= $this->input->post("nama_rekanan");
            $jenis_rekanan       = $this->input->post("jenis_rekanan");
            $email_rekanan 		= $this->input->post("email_rekanan");
            $tlp_rekanan 		= $this->input->post("tlp_rekanan");
            $alamat_ktr_rekanan = $this->input->post("alamat_ktr_rekanan");
            $alamat_krm_rekanan = $this->input->post("alamat_krm_rekanan");
            $website_rekanan 	= $this->input->post("website_rekanan");
            
            $is_delete 			= "0";
            $id_admin 			= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 		= date("Y-m-d h:i:s");

          	// check username
          	if(!$this->mm->get_data_each("rekanan", array("email_rekanan"=>$email_rekanan, "id_rekanan!="=>$id_rekanan))){
          		$set = array(
          				"nama_rekanan"=>$nama_rekanan,
                        "jenis_rekanan"=>$jenis_rekanan,
          				"email_rekanan"=>$email_rekanan,
          				"tlp_rekanan"=>$tlp_rekanan,
          				"alamat_ktr_rekanan"=>$alamat_ktr_rekanan,
          				"alamat_krm_rekanan"=>$alamat_krm_rekanan,
          				"website_rekanan"=>$website_rekanan,
          				"time_update"=>date("Y-m-d h:i:s"),
          				"id_admin"=>$this->session->userdata("admin_lv_1")["id_admin"]
          			);

          		$where = array("id_rekanan"=>$id_rekanan);

          		$update = $this->mm->update_data("rekanan", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["email_rekanan"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_rekanan"]			= strip_tags(form_error('nama_rekanan'));
            $msg_detail["jenis_rekanan"]         = strip_tags(form_error('jenis_rekanan'));
            $msg_detail["email_rekanan"] 		= strip_tags(form_error('email_rekanan'));
            $msg_detail["tlp_rekanan"] 			= strip_tags(form_error('tlp_rekanan'));
            $msg_detail["alamat_ktr_rekanan"] 	= strip_tags(form_error('alamat_ktr_rekanan'));
            $msg_detail["alamat_krm_rekanan"] 	= strip_tags(form_error('alamat_krm_rekanan'));
            $msg_detail["website_rekanan"] 		= strip_tags(form_error('website_rekanan'));
                      
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("rekanan", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------update pelanggan--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete pelanggan--------------------------------
#===============================================================================

    public function delete_rekanan(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_rekanan"=>"",
                );

        if($_POST["id_rekanan"]){
        	$id_rekanan = $this->input->post("id_rekanan");
        	$set = array(
          				"is_delete"=>"1",
          				"time_update"=>date("Y-m-d h:i:s"),
          				"id_admin"=>$this->session->userdata("admin_lv_1")["id_admin"]
          			);

      		$where = array("id_rekanan"=>$id_rekanan);

      		$update = $this->mm->update_data("rekanan", $set, $where);
        	// $delete_rekanan = $this->mm->delete_data("rekanan", array("id_rekanan"=>$id_rekanan));
        	if($update){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_rekanan"]= strip_tags(form_error('id_rekanan'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("rekanan", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================
}
