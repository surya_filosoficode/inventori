<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settingmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        // $this->load->model('admin/Main_setting', 'ma');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

#===============================================================================
#-----------------------------------home_setting----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "setting_main";
		$data["list_data"] = $this->mm->get_data_all_where("setting", array("is_del_setting"=>"0"));
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_setting----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_setting--------------------------------
#===============================================================================
	public function val_form_insert_setting(){
        $config_val_input = array(
                array(
                    'field'=>'jenis_setting',
                    'label'=>'jenis_setting',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'keterangan_setting',
                    'label'=>'keterangan_setting',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_setting(){
        // print_r($_SESSION);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "jenis_setting"=>"",
                    "keterangan_setting"=>""
                );

        if($this->val_form_insert_setting()){
            $jenis_setting 	    = $this->input->post("jenis_setting");
            $keterangan_setting = $this->input->post("keterangan_setting");

            $is_del_setting 	    = "0";
            $admin_create_setting   = $this->session->userdata("ih_mau_ngapain")["id_admin"];
            $time_update_setting 	= date("Y-m-d h:i:s");

            $data = array(
                "id_setting"=>"",
                "jenis_setting"=>$jenis_setting,
                "keterangan_setting"=>$keterangan_setting,
                "is_del_setting"=>$is_del_setting,
                "admin_create_setting"=>$admin_create_setting,
                "time_update_setting"=>$time_update_setting
            );

            $insert = $this->mm->insert_data("setting",$data);
            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["jenis_setting"]      = strip_tags(form_error('jenis_setting'));
            $msg_detail["keterangan_setting"] = strip_tags(form_error('keterangan_setting'));            
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/settingmain/create_tbl_setting");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_setting--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data_setting(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_setting"])){
        	$id_setting = $this->input->post('id_setting');
        	$data = $this->mm->get_data_each("setting", array("id_setting"=>$id_setting, "is_del_setting"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_setting--------------------------------
#===============================================================================

   
    public function update_setting(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "jenis_setting"=>"",
                    "keterangan_setting"=>""
                );

        if($this->val_form_insert_setting()){
        	$id_setting             = $this->input->post("id_setting");

            $jenis_setting          = $this->input->post("jenis_setting");
            $keterangan_setting     = $this->input->post("keterangan_setting");

            $is_del_setting         = "0";
            $admin_create_setting   = $this->session->userdata("admin_lv_1")["id_setting"];
            $time_update_setting    = date("Y-m-d h:i:s");

            $where = array("id_setting"=>$id_setting);
            $set = array("keterangan_setting"=>$keterangan_setting);

            $update = $this->mm->update_data("setting", $set, $where);
            if($update){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }
          	
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["jenis_setting"]      = strip_tags(form_error('jenis_setting'));
            $msg_detail["keterangan_setting"] = strip_tags(form_error('keterangan_setting'));              
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/settingmain/create_tbl_setting");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_setting--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_setting--------------------------------
#===============================================================================

    public function delete_setting(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_setting"=>"",
                );

        if($_POST["id_setting"]){
        	$id_setting = $this->input->post("id_setting");
        	// $where = array("id_setting"=>$id_setting);

            $set = array("is_del_setting"=>"1");
            $where = array("id_setting"=>$id_setting);

        	// $delete_setting = $this->mm->delete_data("admin", array("id_setting"=>$id_setting));
        	$delete_setting = $this->mm->update_data("setting", $set, $where);
            
            if($delete_setting){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_setting"]= strip_tags(form_error('id_setting'));        
        }

        $msg_detail["list_data"] = file_get_contents(base_url()."other/settingmain/create_tbl_setting");
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_setting--------------------------------
#===============================================================================

}
