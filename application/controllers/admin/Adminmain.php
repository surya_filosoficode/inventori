<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/Main_admin', 'ma');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "admin_main";
		$data["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
	public function val_form_insert_admin(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required|is_unique[admin.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'id_tipe_admin',
                    'label'=>'Tipe Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nama_admin',
                    'label'=>'Nama Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nip_admin',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tipe_admin"=>"",
                    "email"=>"",
                    "username"=>"",
                    "password"=>"",
                    "repassword"=>"",
                    "nama_admin"=>"",
                    "nip_admin"=>""
                );

        if($this->val_form_insert_admin()){
            $id_tipe_admin 	= $this->input->post("id_tipe_admin");
            $email 			= $this->input->post("email");
            $username 		= $this->input->post("username");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");
            $nama_admin 	= $this->input->post("nama_admin");
            $nip_admin 		= $this->input->post("nip_admin");

            $status_active 	= "0";
            $admin_del 		= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

            if ($password == $repassword) {
                $insert = $this->ma->admin_insert($id_tipe_admin, $email, $username, md5($password), $status_active, $nama_admin, $nip_admin);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
                
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                
            }
        }else{

        	$msg_detail = array(
                    "id_tipe_admin"=>"",
                    "email"=>"",
                    "username"=>"",
                    "password"=>"",
                    "repassword"=>"",
                    "nama_admin"=>"",
                    "nip_admin"=>""
                );
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_tipe_admin"]= strip_tags(form_error('id_tipe_admin'));
            $msg_detail["email"] 		= strip_tags(form_error('email'));
            $msg_detail["username"] 	= strip_tags(form_error('username'));
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));
            $msg_detail["nama_admin"] 	= strip_tags(form_error('nama_admin'));
            $msg_detail["nip_admin"] 	= strip_tags(form_error('nip_admin'));            
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_admin"])){
        	$id_admin = $this->input->post('id_admin');
        	$data = $this->mm->get_data_each("admin", array("id_admin"=>$id_admin, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update_admin(){
        $config_val_input = array(
                array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'id_tipe_admin',
                    'label'=>'Tipe Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nama_admin',
                    'label'=>'Nama Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nip_admin',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_tipe_admin"=>"",
                    "email"=>"",
                    "username"=>"",
                    "password"=>"",
                    "repassword"=>"",
                    "nama_admin"=>"",
                    "nip_admin"=>""
                );

        if($this->val_form_update_admin()){
        	$id_admin 		= $this->input->post("id_admin");

            $id_tipe_admin 	= $this->input->post("id_tipe_admin");
            $email 			= $this->input->post("email");
            $username 		= $this->input->post("username");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");
            $nama_admin 	= $this->input->post("nama_admin");
            $nip_admin 		= $this->input->post("nip_admin");

            $status_active 	= "0";
            $admin_del 		= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

          	// check username
          	if(!$this->mm->get_data_each("admin", array("username"=>$username, "id_admin!="=>$id_admin))){
          		$set = array(
          				"id_tipe_admin"=>$id_tipe_admin,
          				"email"=>$email,
          				"username"=>$username,
          				"nama_admin"=>$nama_admin,
          				"nip_admin"=>$nip_admin,
          			);

          		$where = array("id_admin"=>$id_admin);

          		$update = $this->mm->update_data("admin", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_tipe_admin"]= strip_tags(form_error('id_tipe_admin'));
            $msg_detail["email"] 		= strip_tags(form_error('email'));
            $msg_detail["username"] 	= strip_tags(form_error('username'));
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));
            $msg_detail["nama_admin"] 	= strip_tags(form_error('nama_admin'));
            $msg_detail["nip_admin"] 	= strip_tags(form_error('nip_admin'));            
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where = array("id_admin"=>$id_admin);

            $set = array("is_delete"=>"1");
            $where = array("id_admin"=>$id_admin);

        	// $delete_admin = $this->mm->delete_data("admin", array("id_admin"=>$id_admin));
        	$delete_admin = $this->mm->update_data("admin", $set, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    public function val_form_ch_pass_admin(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass_admin()){
        	$id_admin 		= $this->input->post("id_admin");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");

          	// check username
          	if($password == $repassword){
          		$set = array(
          				"password"=>md5($password)
          			);

          		$where = array("id_admin"=>$id_admin);

          		$update = $this->mm->update_data("admin", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));         
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================


#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

    public function disabled_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where 	= array("id_admin"=>$id_admin);
        	$set 	= array("status_active"=>"0");
        	
        	$update_admin = $this->mm->update_data("admin", $set, $where);
        	if($update_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================
    public function activate_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where 	= array("id_admin"=>$id_admin);
        	$set 	= array("status_active"=>"1");
        	
        	$update_admin = $this->mm->update_data("admin", $set, $where);
        	if($update_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================


}
