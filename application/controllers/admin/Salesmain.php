<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesmain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/Main_sales', 'ms');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();

        // print_r($_SESSION);
    }

#===============================================================================
#-----------------------------------LIST DATA SALES-------------------------
#===============================================================================

	public function index(){
		$data["page"] = "sales_main";
		$data["list_data"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------END LIST DATA SALES---------------------
#===============================================================================

#===============================================================================
#-----------------------------------INSERT DATA SALES-----------------------
#===============================================================================
	public function val_form_insert_sales(){
        $config_val_input = array(
                array(
                    'field'=>'nik_sales',
                    'label'=>'NIK Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_sales',
                    'label'=>'Nama Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jk_sales',
                    'label'=>'Jenis Kelamin Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tlp_sales',
                    'label'=>'Telp Sales',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_sales',
                    'label'=>'Alamat Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_sales(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nik_sales"=>"",
                    "nama_sales"=>"",
                    "jk_sales"=>"",
                    "tlp_sales"=>"",
                    "alamat_sales"=>""
                );

        if($this->val_form_insert_sales()){
            $nik_sales 		= $this->input->post("nik_sales");
            $nama_sales 	= $this->input->post("nama_sales");
            $jk_sales 		= $this->input->post("jk_sales");
            $tlp_sales 		= $this->input->post("tlp_sales");
            $alamat_sales	= $this->input->post("alamat_sales");
           
            $is_delete 		= "0";
            $id_admin 		= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

            $insert = $this->ms->sales_insert($nama_sales, $jk_sales, $alamat_sales, $tlp_sales, $nik_sales, $is_delete, $time_update, $id_admin);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }
            
        }else{

        	$msg_detail = array(
                    "nik_sales"=>"",
                    "nama_sales"=>"",
                    "jk_sales"=>"",
                    "tlp_sales"=>"",
                    "alamat_sales"=>""
                );
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nik_sales"]		= strip_tags(form_error('nik_sales'));
            $msg_detail["nama_sales"]		= strip_tags(form_error('nama_sales'));
            $msg_detail["jk_sales"] 		= strip_tags(form_error('jk_sales'));
            $msg_detail["tlp_sales"] 		= strip_tags(form_error('tlp_sales'));
            $msg_detail["alamat_sales"] 	= strip_tags(form_error('alamat_sales'));
            
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------END INSERT DATA SALES-------------------
#===============================================================================

#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_sales"])){
        	$id_sales = $this->input->post('id_sales');
        	$data = $this->mm->get_data_each("sales", array("id_sales"=>$id_sales,"is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update sales--------------------------------
#===============================================================================

    public function val_form_update_sales(){
        $config_val_input = array(
                array(
                    'field'=>'nik_sales',
                    'label'=>'NIK Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_sales',
                    'label'=>'Nama Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jk_sales',
                    'label'=>'Jenis Kelamin Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tlp_sales',
                    'label'=>'Telp Sales',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_sales',
                    'label'=>'Alamat Sales',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_sales(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nik_sales"=>"",
                    "nama_sales"=>"",
                    "jk_sales"=>"",
                    "tlp_sales"=>"",
                    "alamat_sales"=>""
                );



        if($this->val_form_update_sales()){
        	$id_sales 		= $this->input->post("id_sales");

            $nik_sales 		= $this->input->post("nik_sales");
            $nama_sales 	= $this->input->post("nama_sales");
            $jk_sales 		= $this->input->post("jk_sales");
            $tlp_sales 		= $this->input->post("tlp_sales");
            $alamat_sales 	= $this->input->post("alamat_sales");
            
            $is_delete 			= "0";
            $id_admin 			= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 		= date("Y-m-d h:i:s");

          	// check username
          	// if(!$this->mm->get_data_each("sales", array("nik_sales"=>$nik_sales, "id_sales!="=>$id_sales))){
          	if($this->mm->get_data_each("sales", array("id_sales"=>$id_sales))){
            	$set = array(
          				"nama_sales"=>$nama_sales,
          				"jk_sales"=>$jk_sales,
          				"alamat_sales"=>$alamat_sales,
          				"tlp_sales"=>$tlp_sales,
          				"nik_sales"=>$nik_sales,
          				"time_update"=>date("Y-m-d h:i:s"),
          				"id_admin"=>$this->session->userdata("admin_lv_1")["id_admin"]
          			);

          		$where = array("id_sales"=>$id_sales);

          		$update = $this->mm->update_data("sales", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}

          	// else{
          	// 	$msg_detail["email_rekanan"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	// }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nik_sales"]		= strip_tags(form_error('nik_sales'));
            $msg_detail["nama_sales"]		= strip_tags(form_error('nama_sales'));
            $msg_detail["jk_sales"] 		= strip_tags(form_error('jk_sales'));
            $msg_detail["tlp_sales"] 		= strip_tags(form_error('tlp_sales'));
            $msg_detail["alamat_sales"] 	= strip_tags(form_error('alamat_sales'));
            
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------update sales--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete sales--------------------------------
#===============================================================================

    public function delete_sales(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_sales"=>"",
                );

        if($_POST["id_sales"]){
        	$id_sales = $this->input->post("id_sales");
        	$set = array(
          				"is_delete"=>"1",
          				"time_update"=>date("Y-m-d h:i:s"),
          				"id_admin"=>$this->session->userdata("admin_lv_1")["id_admin"]
          			);

      		$where = array("id_sales"=>$id_sales);

      		$update = $this->mm->update_data("sales", $set, $where);
        	// $delete_sales = $this->mm->delete_data("sales", array("id_sales"=>$id_sales));
        	if($update){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_sales"]= strip_tags(form_error('id_sales'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("sales", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_sales--------------------------------
#===============================================================================
}
