<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Returpembelianmain extends CI_Controller {
	public $keterangan_record_stok = "retur pembelian detail";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/main_retur_pembelian', 'mrp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

	public function index_retur(){
		// $this->load->view('welcome_message');
		$this->load->view('index');
	}

//========================================================================//
//-----------------------------------index--------------------------------//
//========================================================================//
	public function index(){
		$data["page"] = "retur_pembelian_main";

		$brand = $this->mm->get_data_all_where("brand", array("is_del_brand"=>"0"));
		$suplier     = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));

		$list_suplier = array();
		foreach ($suplier as $key => $value) {
			$list_suplier[$value->id_suplier] = $value;  
		}
		

		$item = array();
		foreach ($brand as $key => $value) {
			$dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "stok!="=>"0", "is_del_item"=>"0"));
			 
			foreach ($dumrp as $keyd => $valued) {
				$item[$value->id_brand][$valued->id_item] = $valued;
			}
		}

        $list_brand = array();
                
        $list_brand_ = array();
        foreach ($brand as $key => $value) {
            $list_brand[$value->id_brand] = $value;
            
            $dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "is_del_item"=>"0"));
             
            foreach ($dumrp as $keyd => $valued) {
                $list_brand_[$value->id_brand][$valued->id_item] = $valued;
            }
        }

		$data["list_brand"]       = json_encode($list_brand);
		$data["list_item"] 	      = json_encode($item);
		$data["list_suplier"] 	  = json_encode($list_suplier);

        $data["list_item_"]     = json_encode($list_brand_);

		$this->load->view('index', $data);
	}

    public function index_list_retur_pembelian(){
        $data["page"]       = "retur_pembelian_list";
        $data["list_data"]  = $this->mrp->get_full_header(array("is_del_tr_header"=>"0"));

        $this->load->view("index", $data);
    }

    public function index_read_retur_pembelian($id_tr_header){
        $data["page"] = "retur_pembelian_read";

        $brand = $this->mm->get_data_all_where("brand", array("is_del_brand"=>"0"));
        $suplier     = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));

        $list_suplier = array();
        foreach ($suplier as $key => $value) {
            $list_suplier[$value->id_suplier] = $value;  
        }
        

        $item = array();
        foreach ($brand as $key => $value) {
            $dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "is_del_item"=>"0"));
             
            foreach ($dumrp as $keyd => $valued) {
                $item[$value->id_brand][$valued->id_item] = $valued;
            }
        }

        $list_brand = array();
                
        $list_brand_ = array();
        foreach ($brand as $key => $value) {
            $list_brand[$value->id_brand] = $value;
            
            $dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "stok!="=>"0", "is_del_item"=>"0"));
             
            foreach ($dumrp as $keyd => $valued) {
                $list_brand_[$value->id_brand][$valued->id_item] = $valued;
            }
        }

        $data["list_brand"]       = json_encode($list_brand);
        $data["list_item"]        = json_encode($item);
        $data["list_suplier"]    = json_encode($list_suplier);

        $data["list_item_"]     = json_encode($list_brand_);

        $data_header = $this->mrp->get_full_header(array("th.id_tr_header"=>$id_tr_header));
        $data_detail = $this->mrp->get_full_detail(array("th.id_tr_header"=>$id_tr_header));

        // print_r($data_header);
        $id_tr_header_p = $data_header[0]->id_tr_header_p;
        $data_header_p = $this->mrp->get_full_header_p(array("th.id_tr_header_p"=>$id_tr_header_p));
        $data_detail_p = $this->mrp->get_full_detail_p(array("th.id_tr_header_p"=>$id_tr_header_p));

        $data["data_header"]        = json_encode($data_header);
        $data["data_header_p"]      = json_encode($data_header_p);

        $data["data_detail"]        = json_encode($data_detail);
        $data["data_detail_p"]      = json_encode($data_detail_p);
        
        $this->load->view('index', $data);
    }

    public function index_retur_pembelian_rusak(){
        $data["page"] = "retur_pembelian_rusak";

        $brand = $this->mm->get_data_all_where("brand", array("is_del_brand"=>"0"));
        $suplier     = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));

        $list_suplier = array();
        foreach ($suplier as $key => $value) {
            $list_suplier[$value->id_suplier] = $value;  
        }
        

        $item = array();
        foreach ($brand as $key => $value) {
            $dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "is_del_item"=>"0"));
             
            foreach ($dumrp as $keyd => $valued) {
                $item[$value->id_brand][$valued->id_item] = $valued;
            }
        }

        $list_brand = array();
                
        $list_item_ = array();
        foreach ($brand as $key => $value) {
            $list_brand[$value->id_brand] = $value;
            
            $dumrp = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "stok!="=>"0", "is_del_item"=>"0"));
             
            foreach ($dumrp as $keyd => $valued) {
                $list_item_[$value->id_brand][$valued->id_item] = $valued;
            }
        }

        $list_item_rusak = $this->mm->get_data_all_where("item", array("stok_opnam!="=>"0", "is_del_item"=>"0"));

        $data["list_brand"]       = json_encode($list_brand);
        $data["list_item"]        = json_encode($item);
        $data["list_suplier"]    = json_encode($list_suplier);

        $data["list_item_"]     = json_encode($list_item_);

        $data["list_item_rusak"]     = json_encode($list_item_rusak);
        
        
        $this->load->view('index', $data);
    }
//========================================================================//
//-----------------------------------index--------------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------insert_retur-------------------------//
//========================================================================//
    private function val_form_insert_retur(){
        $config_val_input = array(
                array(
                    'field'=>'suplier',
                    'label'=>'suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_transaksi',
                    'label'=>'tgl_transaksi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'list_detail_retur',
                    'label'=>'list_detail_retur',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'list_detail_pengganti',
                    'label'=>'list_detail_pengganti',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_retur($tipe){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        $msg_detail = array(
                    "suplier"=>"",
                    "tgl_transaksi"=>"",
                    "list_detail_retur"=>"",
                    "list_detail_pengganti"=>""
                );
        if($this->val_form_insert_retur()){
            $suplier = $this->input->post("suplier");
            $tgl_transaksi = $this->input->post("tgl_transaksi");

            $list_detail_retur = $this->input->post("list_detail_retur");
            $list_detail_pengganti = $this->input->post("list_detail_pengganti");

            $is_del         = "0";
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $get_header = $this->get_rt_detail($list_detail_retur);
            $harga_header = 0;
            if($get_header){
                $harga_header = $get_header["msg_detail"][ "t_harga"];
            }

            $get_header_p = $this->get_rt_detail($list_detail_pengganti);
            $harga_header_p = 0;
            if($get_header_p){
                $harga_header_p = $get_header_p["msg_detail"][ "t_harga"];
            }
            
            $selisih = $harga_header_p - $harga_header;            

            // print_r("<br>");
            // print_r($list_detail_retur);
            // print_r("<br>");
            // print_r($list_detail_pengganti);
            // print_r("<br>");
            // print_r($selisih);
            // print_r("<br>");
            // print_r($harga_header);
            // print_r("<br>");
            // print_r($harga_header_p);
            
            $insert_retur_header_p = $this->insert_retur_header_p();
            if($insert_retur_header_p["status"]){
                $id_tr_header_p = $insert_retur_header_p["item"]["id_tr_header_p"];
                
                if($tipe == "0"){
                    $insert_retur_header = $this->insert_retur_header($id_tr_header_p, $selisih, $tipe);
                    if($insert_retur_header["status"]){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }else {
                    $insert_retur_header = $this->insert_retur_header_rusak($id_tr_header_p, $selisih, $tipe);
                    if($insert_retur_header["status"]){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }       
            }              
            
        }else{
            $msg_detail["suplier"]         = strip_tags(form_error('suplier'));
            $msg_detail["tgl_transaksi"]    = strip_tags(form_error('tgl_transaksi'));
            
            $msg_detail["list_detail_retur"]       = strip_tags(form_error('list_detail_retur'));
            $msg_detail["list_detail_pengganti"]  = strip_tags(form_error('list_detail_pengganti'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));

    }
//========================================================================//
//-----------------------------------insert_retur-------------------------//
//========================================================================//


#-------------------------------------#
#-insert_retur_stok_normal------------#
#-------------------------------------#


//========================================================================//
//-----------------------------------insert_header_retur------------------//
//========================================================================//
    public function insert_retur_header($id_tr_header_p, $selisih, $tipe){
        $array_return = array(
            "status"=>false,
            "item"=>[]
        );
        if($this->val_form_insert_retur()){
            $suplier = $this->input->post("suplier");
            $tgl_transaksi = $this->input->post("tgl_transaksi");

            $list_detail_retur = $this->input->post("list_detail_retur");
            // $list_detail_pengganti = $this->input->post("list_detail_pengganti");

            $is_del         = "0";
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");
            
            $get_detail = $this->get_rt_detail($list_detail_retur);

            // print_r($get_detail);
            $harga_total = 0;
            if($get_detail){
                $harga_total = $get_detail["msg_detail"][ "t_harga"];
            }
            
            $insert_header = $this->mrp->retur_insert_header($id_tr_header_p, $tipe, $selisih, $suplier,
                $tgl_transaksi, $harga_total, $admin_create, $time_update);

            if($insert_header){
                $id_tr_header   = $insert_header["id_tr_header"];
                $insert_detail  = $this->insert_rt_detail($list_detail_retur, $id_tr_header);
                if($insert_detail["msg_main"]["status"]){
                    $array_return = array(
                        "status"=>true,
                        "item"=>["selisih"=>$selisih]
                    );
                }
            }
        }

        return $array_return;
    }
//========================================================================//
//-----------------------------------insert_header_retur------------------//
//========================================================================//

//========================================================================//
//-----------------------------------insert_header_pengganti--------------//
//========================================================================//
    public function insert_retur_header_p(){
        $array_return = array(
            "status"=>false,
            "item"=>[]
        );

        if($this->val_form_insert_retur()){
            $suplier = $this->input->post("suplier");
            $tgl_transaksi = $this->input->post("tgl_transaksi");

            // $list_detail_retur = $this->input->post("list_detail_retur");
            $list_detail_pengganti = $this->input->post("list_detail_pengganti");

            $is_del         = "0";
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");
            
            $get_detail = $this->get_rt_detail($list_detail_pengganti);

            // print_r($get_detail);
            $harga_total = 0;
            if($get_detail){
                $harga_total = $get_detail["msg_detail"][ "t_harga"];
            }

            
            $insert_header = $this->mrp->retur_insert_header_p($suplier,
                $tgl_transaksi, $harga_total, $admin_create, $time_update);

            if($insert_header){
                $id_tr_header   = $insert_header["id_tr_header"];
                $insert_detail  = $this->insert_rt_p_detail($list_detail_pengganti, $id_tr_header);
                if($insert_detail["msg_main"]["status"]){
                    $array_return = array(
                        "status"=>true,
                        "item"=>[
                            "id_tr_header_p"=>$insert_header["id_tr_header"],
                            "t_harga_p"=>$harga_total
                        ]
                    );
                }
            }
        }

        return $array_return;
    }
//========================================================================//
//-----------------------------------insert_header_pengganti--------------//
//========================================================================//


//========================================================================//
//-----------------------------------insert_detail_retur------------------//
//========================================================================//
    #------------------------------------insert_detail--------------------------
    public function insert_rt_detail($data_detail = null, $id_tr_header = "xxx"){
        $jenis_record_stok = "insert_rt";


        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        // print_r($data_detail);
        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){
            $array_status = array();

            $t_harga_all_item = 0;
            foreach ($data_detail as $key => $value) {
                $status_insert = false;

                $id_item                    = $value->id_item;
                $kode_produksi_tr_detail    = $value->kode_produksi;
                $tgl_kadaluarsa_tr_detail   = $value->tgl_kadaluarsa;

                $harga_satuan_tr_detail     = $value->harga_satuan;
                $jml_item_tr_detail         = $value->jml_item;
                $harga_total_tr_detail      = $value->harga_total;

                $jenis_pengembalian         = $value->jenis_pengembalian;

                $is_del         = "0";
                $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $tmp_t_harga = $harga_satuan_tr_detail * $jml_item_tr_detail;
                
                $t_harga_all_item += $tmp_t_harga;

                
                $insert_detail = $this->mrp->rt_insert_detail($id_tr_header, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $tmp_t_harga, $jenis_pengembalian, $admin_create, $time_update);

                if($insert_detail){
                    // if($jenis_pengembalian == "0"){
                        #barang bagus
                        $insert_stok = $this->insert_record_stok("min", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                        if($insert_stok){
                            $update_stok = $this->update_stok("min", $id_item, $jml_item_tr_detail);
                            if($update_stok){
                                $status_insert = true;
                            }    
                        }
                    // }else {
                    //     #barang rusak
                    //     $insert_stok = $this->insert_record_stok_opnam("min", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                    //     if($insert_stok){
                    //         $update_stok = $this->update_stok_opnam("min", $id_item, $jml_item_tr_detail);
                    //         if($update_stok){
                    //             $status_insert = true;
                    //         }
                    //     }
                    // }
                    
                }   

                array_push($array_status, $status_insert);
            }

            if(!in_array(false, $array_status)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
    }
    #------------------------------------insert_detail--------------------------
//========================================================================//
//-----------------------------------insert_detail_retur------------------//
//========================================================================//


//========================================================================//
//-----------------------------------insert_detail_pengganti--------------//
//========================================================================//
    #------------------------------------insert_detail--------------------------
    public function insert_rt_p_detail($data_detail = null, $id_tr_header = "xxx"){
        $jenis_record_stok = "insert_rt_p";


        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        // print_r($data_detail);
        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){
            $array_status = array();

            $t_harga_all_item = 0;
            foreach ($data_detail as $key => $value) {
                $status_insert = false;

                $id_item                    = $value->id_item;
                $kode_produksi_tr_detail    = $value->kode_produksi;
                $tgl_kadaluarsa_tr_detail   = $value->tgl_kadaluarsa;

                $harga_satuan_tr_detail     = $value->harga_satuan;
                $jml_item_tr_detail         = $value->jml_item;
                $harga_total_tr_detail      = $value->harga_total;

                $jenis_pengembalian         = $value->jenis_pengembalian;

                $is_del         = "0";
                $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $tmp_t_harga = $harga_satuan_tr_detail * $jml_item_tr_detail;
                
                $t_harga_all_item += $tmp_t_harga;

                
                $insert_detail = $this->mrp->rt_insert_detail_p($id_tr_header, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $tmp_t_harga, $admin_create, $time_update);

                if($insert_detail){
                    $insert_stok = $this->insert_record_stok("plus", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                    if($insert_stok){
                        $update_stok = $this->update_stok("plus", $id_item, $jml_item_tr_detail);
                        if($update_stok){
                            $status_insert = true;
                        }    
                    }
                }   

                array_push($array_status, $status_insert);
            }

            if(!in_array(false, $array_status)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
    }
    #------------------------------------insert_detail--------------------------
//========================================================================//
//-----------------------------------insert_detail_pengganti--------------//
//========================================================================//

##-------------------------------------##
##-insert_retur_stok_normal------------##
##-------------------------------------##








##-------------------------------------##
##-insert_retur_stok_rusak-------------##
##-------------------------------------##


//========================================================================//
//-----------------------------------insert_header_retur_rusak------------//
//========================================================================//
    public function insert_retur_header_rusak($id_tr_header_p, $selisih, $tipe){
        $array_return = array(
            "status"=>false,
            "item"=>[]
        );
        if($this->val_form_insert_retur()){
            $suplier = $this->input->post("suplier");
            $tgl_transaksi = $this->input->post("tgl_transaksi");

            $list_detail_retur = $this->input->post("list_detail_retur");
            // $list_detail_pengganti = $this->input->post("list_detail_pengganti");

            $is_del         = "0";
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");
            
            $get_detail = $this->get_rt_detail($list_detail_retur);

            // print_r($get_detail);
            $harga_total = 0;
            if($get_detail){
                $harga_total = $get_detail["msg_detail"][ "t_harga"];
            }
            
            $insert_header = $this->mrp->retur_insert_header($id_tr_header_p, $tipe, $selisih, $suplier,
                $tgl_transaksi, $harga_total, $admin_create, $time_update);

            if($insert_header){
                $id_tr_header   = $insert_header["id_tr_header"];
                $insert_detail  = $this->insert_rt_detail_rusak($list_detail_retur, $id_tr_header);
                if($insert_detail["msg_main"]["status"]){
                    $array_return = array(
                        "status"=>true,
                        "item"=>["selisih"=>$selisih]
                    );
                }
            }
        }

        return $array_return;
    }
//========================================================================//
//-----------------------------------insert_header_retur_rusak------------//
//========================================================================//


//========================================================================//
//-----------------------------------insert_detail_retur_rusak------------//
//========================================================================//
    #------------------------------------insert_detail--------------------------
    public function insert_rt_detail_rusak($data_detail = null, $id_tr_header = "xxx"){
        $jenis_record_stok = "insert_rt_rusak";


        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        // print_r($data_detail);
        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){
            $array_status = array();

            $t_harga_all_item = 0;
            foreach ($data_detail as $key => $value) {
                $status_insert = false;

                $id_item                    = $value->id_item;
                $kode_produksi_tr_detail    = $value->kode_produksi;
                $tgl_kadaluarsa_tr_detail   = $value->tgl_kadaluarsa;

                $harga_satuan_tr_detail     = $value->harga_satuan;
                $jml_item_tr_detail         = $value->jml_item;
                $harga_total_tr_detail      = $value->harga_total;

                $jenis_pengembalian         = $value->jenis_pengembalian;

                $is_del         = "0";
                $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $tmp_t_harga = $harga_satuan_tr_detail * $jml_item_tr_detail;
                
                $t_harga_all_item += $tmp_t_harga;

                
                $insert_detail = $this->mrp->rt_insert_detail($id_tr_header, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $tmp_t_harga, $jenis_pengembalian, $admin_create, $time_update);

                if($insert_detail){
                    // if($jenis_pengembalian == "0"){
                        #barang bagus
                        $insert_stok = $this->insert_record_stok_opnam("min", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                        if($insert_stok){
                            $update_stok = $this->update_stok_opnam("min", $id_item, $jml_item_tr_detail);
                            if($update_stok){
                                $status_insert = true;
                            }    
                        }
                    // }else {
                    //     #barang rusak
                    //     $insert_stok = $this->insert_record_stok_opnam("min", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                    //     if($insert_stok){
                    //         $update_stok = $this->update_stok_opnam("min", $id_item, $jml_item_tr_detail);
                    //         if($update_stok){
                    //             $status_insert = true;
                    //         }
                    //     }
                    // }
                    
                }   

                array_push($array_status, $status_insert);
            }

            if(!in_array(false, $array_status)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
    }
    #------------------------------------insert_detail--------------------------
//========================================================================//
//-----------------------------------insert_detail_retur_rusak------------//
//========================================================================//


##-------------------------------------##
##-insert_retur_stok_rusak-------------##
##-------------------------------------##






##-------------------------------------##
##-delete_retur_stok_normal------------##
##-------------------------------------##



//========================================================================//
//-----------------------------------delete_retur-------------------------//
//========================================================================//
    public function delete_retur($tipe){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array();

        if($_POST["id_tr_header"]){
            $id_tr_header = $this->input->post("id_tr_header");
            if($tipe == "0"){
                print_r("enol");
                if($this->delete_retur_header_p() && $this->delete_retur_header()){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }else {

                print_r("satu");
                if($this->delete_retur_header_p() && $this->delete_retur_header_rusak()){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
            

        }else{
            $msg_detail["id_tr_header"] = strip_tags(form_error('id_tr_header'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
//========================================================================//
//-----------------------------------delete_retur-------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------delete_retur_header------------------//
//========================================================================//
    public function delete_retur_header(){
        $status = false;

        if($_POST["id_tr_header"]){
            $id_tr_header = $this->input->post("id_tr_header");
            $delete_detail = $this->delete_retur_header_detail($id_tr_header);
            if($delete_detail){
                $delete_header = $this->mm->delete_data("tr_rt_pb_header", array("id_tr_header"=>$id_tr_header));
                if($delete_header){
                    $status = true;
                }
            }
        }
        return $status;
    }
//========================================================================//
//-----------------------------------delete_retur_header------------------//
//========================================================================//

//========================================================================//
//-----------------------------------delete_retur_header_p----------------//
//========================================================================//
    public function delete_retur_header_p(){
        $status = false;

        if($_POST["id_tr_header"]){
            $id_tr_header = $this->input->post("id_tr_header");
            $data_retur = $this->mm->get_data_each("tr_rt_pb_header", ["id_tr_header"=>$id_tr_header]);

            $id_tr_header_p = $data_retur["id_tr_header_p"];
            
            $delete_detail = $this->delete_retur_header_detail_p($id_tr_header_p);
            if($delete_detail){
                $delete_header = $this->mm->delete_data("tr_rt_pb_header_p", array("id_tr_header_p"=>$id_tr_header_p));
                if($delete_header){
                    $status = true;
                }
            }
        }
        return $status;
    }
//========================================================================//
//-----------------------------------delete_retur_header_p----------------//
//========================================================================//


//========================================================================//
//-----------------------------------delete_retur_detail------------------//
//========================================================================//
    public function delete_retur_header_detail($id_tr_header){
        $jenis_record_stok = "delete_rt_detail";

        $data_detail = $this->mm->get_data_all_where("tr_rt_pb_detail", array("id_tr_header"=>$id_tr_header));

        // print_r($data_detail);
        $status_send = false;
        $array_status = array();
        foreach ($data_detail as $key => $value) {
            $status = false;
            // print_r($value);
            $id_item = $value->id_item;
            $jml_item_tr_detail = $value->jml_item_tr_detail;
            $id_tr_detail = $value->id_tr_detail;

            $jenis_pengembalian = $value->status_rt_tr_detail;

            // if($jenis_pengembalian == "0"){
            //     #barang bagus
                $insert_record_stok = $this->insert_record_stok($status = "plus", $id_tr_detail, $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                if($insert_record_stok){
                    $update_stok = $this->update_stok("plus", $id_item, $jml_item_tr_detail);
                    if($update_stok){
                        $delete_detail = $this->mm->delete_data("tr_rt_pb_detail", array("id_tr_detail"=>$id_tr_detail));
                        if($delete_detail){
                            
                            $status = true;
                        }
                    }
                }
            // }else {
            //     #barang rusak
            //     $insert_record_stok = $this->insert_record_stok_opnam($status = "min", $id_tr_detail, $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
            //     if($insert_record_stok){
            //         $update_stok = $this->update_stok_opnam("min", $id_item, $jml_item_tr_detail);
            //         if($update_stok){
            //             $delete_detail = $this->mm->delete_data("tr_rt_pb_detail", array("id_tr_detail"=>$id_tr_detail));
            //             if($delete_detail){
                            
            //                 $status = true;
            //             }
            //         }
            //     }
                
            // }

            array_push($array_status, $status);
        }

        if(!in_array(false, $array_status)){
            $status_send = true;
        }

        return $status_send;
    }
//========================================================================//
//-----------------------------------delete_retur_detail------------------//
//========================================================================//


//========================================================================//
//-----------------------------------delete_retur_detail_p----------------//
//========================================================================//
    public function delete_retur_header_detail_p($id_tr_header_p){
        $jenis_record_stok = "delete_rt_detail_p";

        $data_detail = $this->mm->get_data_all_where("tr_rt_pb_detail_p", array("id_tr_header_p"=>$id_tr_header_p));
        // print_r($id_tr_header_p);
        // print_r($data_detail);
        $status_send = false;
        $array_status = array();
        foreach ($data_detail as $key => $value) {
            $status = false;
            // print_r($value);
            $id_item = $value->id_item;
            $jml_item_tr_detail = $value->jml_item_tr_detail;
            $id_tr_detail = $value->id_tr_detail;
            
            $insert_record_stok = $this->insert_record_stok($status = "min", $id_tr_detail, $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
            if($insert_record_stok){
                $update_stok = $this->update_stok("min", $id_item, $jml_item_tr_detail);
                if($update_stok){
                    $delete_detail = $this->mm->delete_data("tr_rt_pb_detail_p", array("id_tr_detail"=>$id_tr_detail));
                    if($delete_detail){
                        
                        $status = true;
                    }
                }
            }
            
            array_push($array_status, $status);
        }

        if(!in_array(false, $array_status)){
            $status_send = true;
        }

        return $status_send;
    }
//========================================================================//
//-----------------------------------delete_retur_detail_p----------------//
//========================================================================//


##-------------------------------------##
##-delete_retur_stok_normal------------##
##-------------------------------------##





##-------------------------------------##
##-delete_retur_stok_rusak-------------##
##-------------------------------------##



//========================================================================//
//-----------------------------------delete_retur_header_rusak------------//
//========================================================================//
    public function delete_retur_header_rusak(){
        $status = false;

        if($_POST["id_tr_header"]){
            $id_tr_header = $this->input->post("id_tr_header");
            $delete_detail = $this->delete_retur_header_rusak_detail($id_tr_header);
            if($delete_detail){
                $delete_header = $this->mm->delete_data("tr_rt_pb_header", array("id_tr_header"=>$id_tr_header));
                if($delete_header){
                    $status = true;
                }
            }
        }
        return $status;
    }
//========================================================================//
//-----------------------------------delete_retur_header_rusak------------//
//========================================================================//


//========================================================================//
//-----------------------------------delete_retur_detail_rusak------------//
//========================================================================//
    public function delete_retur_header_rusak_detail($id_tr_header){
        $jenis_record_stok = "delete_rt_detail";

        $data_detail = $this->mm->get_data_all_where("tr_rt_pb_detail", array("id_tr_header"=>$id_tr_header));

        // print_r($data_detail);
        $status_send = false;
        $array_status = array();
        foreach ($data_detail as $key => $value) {
            $status = false;
            // print_r($value);
            $id_item = $value->id_item;
            $jml_item_tr_detail = $value->jml_item_tr_detail;
            $id_tr_detail = $value->id_tr_detail;

            $jenis_pengembalian = $value->status_rt_tr_detail;

            
                $insert_record_stok = $this->insert_record_stok_opnam($status = "plus", $id_tr_detail, $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                if($insert_record_stok){
                    $update_stok = $this->update_stok_opnam("plus", $id_item, $jml_item_tr_detail);
                    if($update_stok){
                        $delete_detail = $this->mm->delete_data("tr_rt_pb_detail", array("id_tr_detail"=>$id_tr_detail));
                        if($delete_detail){
                            
                            $status = true;
                        }
                    }
                }

            array_push($array_status, $status);
        }

        if(!in_array(false, $array_status)){
            $status_send = true;
        }

        return $status_send;
    }
//========================================================================//
//-----------------------------------delete_retur_detail_rusak------------//
//========================================================================//

##-------------------------------------##
##-delete_retur_stok_rusak-------------##
##-------------------------------------##




  #------------------------------------get_t_harga_detail---------------------
    public function get_rt_detail($data_detail = null){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){

            $t_harga_all_item = 0;
            foreach ($data_detail as $key => $value) {
                $harga_satuan   = $value->harga_satuan;
                $jml_item       = $value->jml_item;

                $t_harga_item   = $harga_satuan * $jml_item;
                
                $t_harga_all_item += $t_harga_item;
            }

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            $msg_detail["t_harga"] = $t_harga_all_item;
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
    }
  #------------------------------------get_t_harga_detail---------------------

  #------------------------------------update_stok----------------------------
    // if insert the status = 'min' ==> minus the stok item
    // if delete the status = 'plus' ==> minus the stok item
    public function update_stok($status, $id_item, $stok_main){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old;

            if($status == "plus"){
                $stok_item_new = (int)$stok_item_old + (int)$stok_main;
            }else if($status == "min"){
                $stok_item_new = (int)$stok_item_old - (int)$stok_main;
            }

            // print_r($stok_item_new);
            $set = array("stok"=>$stok_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

    public function update_stok_for_update($id_item, $stok_detail_before, $stok_detail_after){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_item_old = $data_item["stok"];
            $stok_item_new = (int)$stok_item_old + (int)$stok_detail_before - (int)$stok_detail_after; 
            
            $set = array("stok"=>$stok_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

  #------------------------------------update_stok----------------------------


  #------------------------------------update_stok_opnam----------------------------
    // if insert the status = 'min' ==> minus the stok_opnam item
    // if delete the status = 'plus' ==> minus the stok_opnam item
    public function update_stok_opnam($status, $id_item, $stok_opnam_main){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_opnam_item_old = $data_item["stok_opnam"];
            $stok_opnam_item_new = $stok_opnam_item_old;

            if($status == "plus"){
                $stok_opnam_item_new = (int)$stok_opnam_item_old + (int)$stok_opnam_main;
            }else if($status == "min"){
                $stok_opnam_item_new = (int)$stok_opnam_item_old - (int)$stok_opnam_main;
            }

            // print_r($stok_opnam_item_new);
            $set = array("stok_opnam"=>$stok_opnam_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

    public function update_stok_opnam_for_update($id_item, $stok_opnam_detail_before, $stok_opnam_detail_after){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_opnam_item_old = $data_item["stok_opnam"];
            $stok_opnam_item_new = (int)$stok_opnam_item_old + (int)$stok_opnam_detail_before - (int)$stok_opnam_detail_after; 
            
            $set = array("stok_opnam"=>$stok_opnam_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

  #------------------------------------update_stok_opnam----------------------------




  #------------------------------------insert_record_stok---------------------
    // if insert the status = 'min' ==> minus the stok item
    // if delete the status = 'plus' ==> plus the stok item
    public function insert_record_stok($status = "min", $id_tr_detail, $id_item, $keterangan_record_stok, $jenis_record_stok, $stok_main){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old;
            $stok_before   = 0; 

            if($status == "plus"){
                $stok_item_new = (int)$stok_item_old + (int)$stok_main;
                $status_record_stok = "plus";
            }else if($status == "min"){
                $stok_item_new = (int)$stok_item_old - (int)$stok_main;
                $status_record_stok = "min";
            }
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $insert = $this->mrp->record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_item_old, $stok_before, $stok_main, $stok_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }


    // if update the status = 'plusmin' ==> plus minus the stok item
    public function update_record_stok($id_tr_detail, $id_item, $keterangan_record_stok, $jenis_record_stok, $stok_detail_before, $stok_detail_after){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_item_old = $data_item["stok"];
            $stok_item_new = (int)$stok_item_old + (int)$stok_detail_before - (int)$stok_detail_after; 
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $status_record_stok = "plus_min";

            $insert = $this->mrp->record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_item_old, $stok_detail_before, $stok_detail_after, $stok_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }
  #------------------------------------insert_record_stok---------------------


  #------------------------------------insert_record_stok_opnam---------------------
    // if insert the status = 'min' ==> minus the stok_opnam item
    // if delete the status = 'plus' ==> plus the stok_opnam item
    public function insert_record_stok_opnam($status = "min", $id_tr_detail, $id_item, $keterangan_record_stok_opnam, $jenis_record_stok_opnam, $stok_opnam_main){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_opnam_item_old = $data_item["stok_opnam"];
            $stok_opnam_item_new = $stok_opnam_item_old;
            $stok_opnam_before   = 0; 

            if($status == "plus"){
                $stok_opnam_item_new = (int)$stok_opnam_item_old + (int)$stok_opnam_main;
                $status_record_stok_opnam = "plus_p";
            }else if($status == "min"){
                $stok_opnam_item_new = (int)$stok_opnam_item_old - (int)$stok_opnam_main;
                $status_record_stok_opnam = "min_p";
            }
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $insert = $this->mrp->record_stok_opnam_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok_opnam, $jenis_record_stok_opnam, $status_record_stok_opnam, $stok_opnam_item_old, $stok_opnam_before, $stok_opnam_main, $stok_opnam_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }


    // if update the status = 'plusmin' ==> plus minus the stok_opnam item
    public function update_record_stok_opnam($id_tr_detail, $id_item, $keterangan_record_stok_opnam, $jenis_record_stok_opnam, $stok_opnam_detail_before, $stok_opnam_detail_after){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_opnam_item_old = $data_item["stok_opnam"];
            $stok_opnam_item_new = (int)$stok_opnam_item_old + (int)$stok_opnam_detail_before - (int)$stok_opnam_detail_after; 
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $status_record_stok_opnam = "plus_min_p";

            $insert = $this->mrp->record_stok_opnam_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok_opnam, $jenis_record_stok_opnam, $status_record_stok_opnam, $stok_opnam_item_old, $stok_opnam_detail_before, $stok_opnam_detail_after, $stok_opnam_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }
  #------------------------------------insert_record_stok_opnam---------------------

    function delete_all(){
        $this->db->empty_table("tr_rt_pb_header_p");
        $this->db->empty_table("tr_rt_pb_header");
        $this->db->empty_table("tr_rt_pb_detail_p");
        $this->db->empty_table("tr_rt_pb_detail");
        $this->db->empty_table("record_stok");
        $this->db->empty_table("record_stok_opnam");
    }
}
