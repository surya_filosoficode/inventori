<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Samplemain extends CI_Controller {
	public $keterangan_record_stok = "sample detail";

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/main_sample', 'mp');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        date_default_timezone_set("Asia/Bangkok");
        // $this->auth_v0->check_session_active_ad();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

	public function index(){
		$data["page"] = "sample_main";

		$brand 		= $this->mm->get_data_all_where("brand", array("is_del_brand"=>"0"));
		$customer 	= $this->mm->get_data_all_where("rekanan", array("is_delete"=>"0"));
		// $atas_nama 		= $this->mm->get_data_all_where("atas_nama", array("is_delete"=>"0"));

		$list_customer = array();
		foreach ($customer as $key => $value) {
			$list_customer[$value->id_rekanan] = $value; 
		}
		

		$list_brand = array();
		foreach ($brand as $key => $value) {
			$list_brand[$value->id_brand] = $value; 
		}

		$item = array();
		foreach ($brand as $key => $value) {
			$dump = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "is_del_item"=>"0", "stok!="=>"0"));
			 
			foreach ($dump as $keyd => $valued) {
				$item[$value->id_brand][$valued->id_item] = $valued;
			}
		}
		$data["list_brand"] = json_encode($list_brand);
		$data["list_item"] 	= json_encode($item);
		$data["list_customer"] 	= json_encode($list_customer);
		// $data["list_atas_nama"] 	= json_encode($list_atas_nama);

		$this->load->view('index', $data);
	}

	public function index_list(){
        $data["page"]       = "sample_list";
        $data["list_data"]  = $this->mp->get_full_header_sample(array("is_del_tr_header"=>"0"));


        // print_r($data);
        $this->load->view("index", $data);
    }

    public function index_read($id_tr_header){
        $data["page"] = "sample_read";

        $brand = $this->mm->get_data_all_where("brand", array());
        $customer = $this->mm->get_data_all_where("rekanan", array());

        $list_customer = array();
        foreach ($customer as $key => $value) {
            $list_customer[$value->id_rekanan] = $value; 
        }

        $list_brand = array();
        foreach ($brand as $key => $value) {
            $list_brand[$value->id_brand] = $value; 
        }

        $item = array();
        foreach ($brand as $key => $value) {
            $dump = $this->mm->get_data_all_where("item", array("id_brand"=>$value->id_brand, "is_del_item"=>"0", "stok!="=>"0"));
             
            foreach ($dump as $keyd => $valued) {
                $item[$value->id_brand][$valued->id_item] = $valued;
            }
        }
        $data["list_brand"] = json_encode($list_brand);
        $data["list_item"]  = json_encode($item);
        $data["list_customer"]  = json_encode($list_customer);
        
        $data_header = array();
        $data_detail_new = array();
        
        $data_header = $this->mp->get_full_header_sample(array("th.id_tr_header"=>$id_tr_header));
        $data_detail = $this->mp->get_full_detail_sample(array("td.id_tr_header"=>$id_tr_header));
        foreach ($data_detail as $key => $value) {
            $data_detail_new[$value->id_item] = $value;
        }
        

        $data["data_header"] = $data_header;
        $data["data_detail"] = json_encode($data_detail_new);


        // print_r($data);
        $this->load->view("index", $data);
    }

  #------------------------------------insert_sample_header----------------
	private function val_form_insert_sample(){
		$config_val_input = array(
                array(
                    'field'=>'customer',
                    'label'=>'customer',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tgl_transaksi',
                    'label'=>'tgl_transaksi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'atas_nama',
                    'label'=>'atas_nama',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'list_detail',
                    'label'=>'list_detail',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
	}

	public function insert_sample_header(){
		$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array(
                    "customer"=>"",
                    "tgl_transaksi"=>"",
                    "atas_nama"=>"",
                    "list_detail"=>""
                );
        if($this->val_form_insert_sample()){
        	$customer = $this->input->post("customer");
        	$tgl_transaksi = $this->input->post("tgl_transaksi");
        	$atas_nama = $this->input->post("atas_nama");
        	$list_detail = $this->input->post("list_detail");

        	$is_del 		= "0";
            $admin_create 	= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");
            $status_hutang = "0";

        	$get_detail = $this->get_sample_detail($list_detail);

        	if($get_detail){
        		$harga_total = $get_detail["msg_detail"][ "t_harga"];
            }

        	
            $insert_header = $this->mp->sample_insert_header($customer, $atas_nama,
                $tgl_transaksi, $harga_total, $admin_create, $time_update);

            if($insert_header){
                $id_tr_header   = $insert_header["id_tr_header"];
                $insert_detail  = $this->insert_sample_detail($list_detail, $id_tr_header);
                if($insert_detail["msg_main"]["status"]){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }


        }else{
            $msg_detail["customer"]         = strip_tags(form_error('customer'));
            $msg_detail["tgl_transaksi"]    = strip_tags(form_error('tgl_transaksi'));
            $msg_detail["atas_nama"]            = strip_tags(form_error('atas_nama'));
            $msg_detail["list_detail"]  = strip_tags(form_error('list_detail'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
	}
  #------------------------------------insert_sample_header----------------

  #------------------------------------delete_sample_header----------------
    public function delete_sample_header(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = array();

        if($_POST["id_tr_header"]){
            $id_tr_header = $this->input->post("id_tr_header");
            $delete_detail = $this->delete_sample_detail($id_tr_header);
            if($delete_detail){
                $delete_header = $this->mm->delete_data("tr_sm_header", array("id_tr_header"=>$id_tr_header));
                if($delete_header){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
                }
            }
        }else{
            $msg_detail["id_tr_header"] = strip_tags(form_error('id_tr_header'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
  #------------------------------------delete_sample_header----------------

  #------------------------------------update_sample_header----------------
    public function update_sample_header(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array();
        
        $msg_detail = array(
                    "customer"=>"",
                    "tgl_transaksi"=>"",
                    "atas_nama"=>"",
                    "list_detail"=>""
                );
        if($this->val_form_insert_sample()){
            $id_tr_header = $this->input->post("id_tr_header");

            $customer = $this->input->post("customer");
            $tgl_transaksi = $this->input->post("tgl_transaksi");
            $atas_nama = $this->input->post("atas_nama");
            
            $list_detail = $this->input->post("list_detail");

            $is_del         = "0";
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");


            $list_detail_ar = json_decode($list_detail);

            $get_detail = $this->get_sample_detail($list_detail);

            if($get_detail){
                $harga_total = $get_detail["msg_detail"][ "t_harga"];
            }

            $update_detail = $this->update_detail($id_tr_header, $list_detail_ar);
            if($update_detail){
                // print_r("update header");
                $set_header = array(
                        "id_customer"           	=>$customer,
                        "atas_nama"              	=>$atas_nama,
                        "tgl_transaksi_tr_header"   =>$tgl_transaksi,
                        "total_pembayaran_tr_header"=>$harga_total,
                        "admin_create_tr_header"    =>$admin_create,
                        "time_up_tr_header"         =>$time_update
                    );

                $where_header = array("id_tr_header"=>$id_tr_header);

                $update_header = $this->mm->update_data("tr_sm_header", $set_header, $where_header);
                if($update_header){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }

        }else{
            $msg_detail["customer"]         = strip_tags(form_error('customer'));
            $msg_detail["tgl_transaksi"]    = strip_tags(form_error('tgl_transaksi'));
            $msg_detail["atas_nama"]            = strip_tags(form_error('atas_nama'));
            $msg_detail["list_detail"]  = strip_tags(form_error('list_detail'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function update_sample_check(){
        print_r("<pre>");
        print_r($_POST);
    }
  #------------------------------------update_sample_header---------------
  



  #------------------------------------get_t_harga_detail---------------------
	public function get_sample_detail($data_detail = null){
		$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){

        	$t_harga_all_item = 0;
        	foreach ($data_detail as $key => $value) {
                $harga_satuan   = $value->harga_satuan;
                $jml_item       = $value->jml_item;

                $t_harga_item   = $harga_satuan * $jml_item;
                
                $t_harga_all_item += $t_harga_item;
        	}

        	$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
        	$msg_detail["t_harga"] = $t_harga_all_item;
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
	}
  #------------------------------------get_t_harga_detail---------------------

  #------------------------------------insert_detail--------------------------
    public function insert_sample_detail($data_detail = null, $id_tr_header = "xxx"){
        $jenis_record_stok = "insert_sample";


        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("STOCK_READY_FAIL"));
        $msg_detail = array();

        // print_r($data_detail);
        $data_detail = json_decode($data_detail);

        if($data_detail!=null && $data_detail){
            $array_status = array();

            $t_harga_all_item = 0;
            foreach ($data_detail as $key => $value) {
                $status_insert = false;

                $id_item                    = $value->id_item;
                $kode_produksi_tr_detail    = $value->kode_produksi;
                $tgl_kadaluarsa_tr_detail   = $value->tgl_kadaluarsa;

                $harga_satuan_tr_detail     = $value->harga_satuan;
                $jml_item_tr_detail         = $value->jml_item;
                $harga_total_tr_detail      = $value->harga_total;

                $is_del         = "0";
                $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
                $time_update    = date("Y-m-d h:i:s");

                $tmp_t_harga = $harga_satuan_tr_detail * $jml_item_tr_detail;
                
                $t_harga_all_item += $tmp_t_harga;

                
                $insert_detail = $this->mp->sample_insert_detail($id_tr_header, $id_item, $kode_produksi_tr_detail, $tgl_kadaluarsa_tr_detail, $harga_satuan_tr_detail, $jml_item_tr_detail, $tmp_t_harga, $admin_create, $time_update);

                if($insert_detail){
                    $insert_stok = $this->insert_record_stok("min", $insert_detail["id_tr_detail"], $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
                    if($insert_stok){
                        $update_stok = $this->update_stok("min", $id_item, $jml_item_tr_detail);
                        if($update_stok){
                            $status_insert = true;
                        }    
                    }
                }   

                array_push($array_status, $status_insert);
            }

            if(!in_array(false, $array_status)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        return $res_msg;
    }
  #------------------------------------insert_detail--------------------------

  #------------------------------------delete_detail--------------------------
    public function delete_sample_detail($id_tr_header){
        $jenis_record_stok = "delete_sample";

        $data_detail = $this->mm->get_data_all_where("tr_sm_detail", array("id_tr_header"=>$id_tr_header));

        $status_send = false;
        $array_status = array();
        foreach ($data_detail as $key => $value) {
            $status = false;
            // print_r($value);
            $id_item = $value->id_item;
            $jml_item_tr_detail = $value->jml_item_tr_detail;
            $id_tr_detail = $value->id_tr_detail;
            // $id_item = $value->id_item;

            $insert_record_stok = $this->insert_record_stok($status = "plus", $id_tr_detail, $id_item, $this->keterangan_record_stok, $jenis_record_stok, $jml_item_tr_detail);
            if($insert_record_stok){
                $update_stok = $this->update_stok("plus", $id_item, $jml_item_tr_detail);
                if($update_stok){
                    $delete_detail = $this->mm->delete_data("tr_sm_detail", array("id_tr_detail"=>$id_tr_detail));
                    if($delete_detail){
                        
                        $status = true;
                    }
                }
            }
            array_push($array_status, $status);
        }

        if(!in_array(false, $array_status)){
            $status_send = true;
        }

        return $status_send;
    }
  #------------------------------------delete_detail--------------------------

  #------------------------------------update_detail--------------------------
    public function update_detail($id_tr_header, $list_detail){
        $jenis_record_stok = "update_sample";
        $status_send = false;
        
        $array_status_up_detail = array();
        $where_in = array();

        #foreach data detail
        foreach ($list_detail as $key => $value) {
            #setparameter

            $status = false;
            // print_r($value);
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $harga_satuan   = $value->harga_satuan;
            $jml_item       = $value->jml_item;

            #count price
            $t_harga_detail = $harga_satuan * $jml_item;


            #set update paramter
            $set_detail = array(
                    "id_item"                   =>$value->id_item,
                    "kode_produksi_tr_detail"   =>$value->kode_produksi,
                    "tgl_kadaluarsa_tr_detail"  =>$value->tgl_kadaluarsa,

                    "harga_satuan_tr_detail"    =>$harga_satuan,
                    "jml_item_tr_detail"        =>$jml_item,
                    
                    "harga_total_tr_detail"     =>$t_harga_detail,
                    
                    "admin_create_tr_detail"    =>$admin_create,
                    "time_up_tr_detail"         =>$time_update
                );

            #set where update paramter 
            $where_detail = array("id_tr_header"=>$id_tr_header, "id_item"=>$key);

            #check data detail on db
            #if avail update
            #if not insert

            $data_db_detail = $this->mm->get_data_each("tr_sm_detail", $where_detail);
            if($data_db_detail){
            
                #insert record stok
                $insert_record_stok = $this->update_record_stok(
                     $data_db_detail["id_tr_detail"],
                     $key,
                     $this->keterangan_record_stok,
                     $jenis_record_stok,
                     $data_db_detail["jml_item_tr_detail"],
                     $jml_item);
                #check insert record stok
                if($insert_record_stok){
                    #if val return true than update stok item
                    $update_stok = $this->update_stok_for_update(
                                    $key, 
                                    $data_db_detail["jml_item_tr_detail"], 
                                    $jml_item
                                );
                    #check update stok item
                    if($update_stok){
                        #if val return true than update detail tr
                        $update_detail = $this->mm->update_data("tr_sm_detail", $set_detail, $where_detail);
                        // print_r($data_db_detail);
                        #check update update detail tr
                        if($update_detail){
                            #if val return true set status process as true
                            // print_r("update<br>");
                            $status = true;
                        }
                    }
                } 
            }else{
                // print_r("insert<br>");
                #insert detail
                $insert_detail = $this->mp->sample_insert_detail(
                    $id_tr_header,
                    $key,
                    $value->kode_produksi,
                    $value->tgl_kadaluarsa,
                    $harga_satuan,
                    $jml_item,
                    $t_harga_detail,
                    $admin_create,
                    $time_update);

                #check val return insert_detail
                if($insert_detail){
                    #if val return true do insert_record_stok
                    #if not do nothing 
                    $insert_record_stok = $this->insert_record_stok(
                        "min",
                        $insert_detail["id_tr_detail"],
                        $key,
                        $this->keterangan_record_stok,
                        $jenis_record_stok,
                        $jml_item);

                    #check val return insert_record_stok
                    if($insert_record_stok){
                        #if val return true do update_stok
                        $update_stok = $this->update_stok("min", $key, $jml_item);
                        if($update_stok){
                            #if val return true set status process as true
                            $status = true;
                        }    
                    }
                }  
            }

            #push id item in array where in
            array_push($where_in, $key);

            // print_r("<pre>");
            // print_r($set_detail);
            // print_r($where_detail);

            #push status process in array array_status_up_detail
            array_push($array_status_up_detail, $status);         
        }

        // #set status default for return parameter 
        // $status_delete = false;

        // #checking param false in array_status_up_detail
        // #if not yet do delete item not selected
        // if(!in_array(false, $array_status_up_detail)){
        //     $delete_detail = $this->mp->delete_detail_where_in($id_tr_header, $where_in);
        //     if($delete_detail){
        //         $status_delete = true;
        //     }
        // }

        #get item not in $where in
        $data_detail_anomali = $this->mp->get_detail_where_in($id_tr_header, $where_in);
        // print_r($data_detail_anomali);
        $array_status_del_detail = array();
        if($data_detail_anomali){
            foreach ($data_detail_anomali as $key => $value) {
                $status_delete = false;

                $id_tr_detail_del = $value->id_tr_detail;
                $id_item_del = $value->id_item;
                $jml_item_tr_detail_del = $value->jml_item_tr_detail;
                
                $insert_record_stok = $this->insert_record_stok(
                    "plus",
                    $id_tr_detail_del, 
                    $id_item_del, 
                    $this->keterangan_record_stok, 
                    $jenis_record_stok, 
                    $jml_item_tr_detail_del);

                if($insert_record_stok){
                    $update_stok = $this->update_stok("plus", $id_item_del, $jml_item_tr_detail_del);
                    if($update_stok){
                        $delete_detail = $this->mm->delete_data("tr_sm_detail", array("id_tr_detail"=>$id_tr_detail_del));
                        if($delete_detail){
                            $status_delete = true;
                        }
                    }
                }
                array_push($array_status_del_detail, $status_delete);
            }
        }

        #checking param false in array_status_up_detail and param false in array_status_del_detail
        #if not yet do delete item not selected
        if((!in_array(false, $array_status_up_detail)) && (!in_array(false, $array_status_del_detail))){
            $status_send = true;
        }

        return $status_send;
    }
  #------------------------------------update_detail--------------------------


  #------------------------------------update_stok----------------------------
    // if insert the status = 'min' ==> minus the stok item
    // if delete the status = 'plus' ==> minus the stok item
    public function update_stok($status, $id_item, $stok_main){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old;

            if($status == "plus"){
                $stok_item_new = $stok_item_old + $stok_main;
            }else if($status == "min"){
                $stok_item_new = $stok_item_old - $stok_main;
            }

            // print_r($stok_item_new);
            $set = array("stok"=>$stok_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

    public function update_stok_for_update($id_item, $stok_detail_before, $stok_detail_after){
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old + $stok_detail_before - $stok_detail_after; 
            
            $set = array("stok"=>$stok_item_new);
            $where = array("id_item"=>$id_item);

            $update = $this->mm->update_data("item", $set, $where);
            if($update){
                $status_send = true;
            }
        }

        return $status_send;
    }

  #------------------------------------update_stok----------------------------


  #------------------------------------insert_record_stok---------------------
    // if insert the status = 'min' ==> minus the stok item
    // if delete the status = 'plus' ==> plus the stok item
    public function insert_record_stok($status = "min", $id_tr_detail, $id_item, $keterangan_record_stok, $jenis_record_stok, $stok_main){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old;
            $stok_before   = 0; 

            if($status == "plus"){
                $stok_item_new = $stok_item_old + $stok_main;
                $status_record_stok = "plus";
            }else if($status == "min"){
                $stok_item_new = $stok_item_old - $stok_main;
                $status_record_stok = "min";
            }
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $insert = $this->mp->record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_item_old, $stok_before, $stok_main, $stok_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }


    // if update the status = 'plusmin' ==> plus minus the stok item
    public function update_record_stok($id_tr_detail, $id_item, $keterangan_record_stok, $jenis_record_stok, $stok_detail_before, $stok_detail_after){
        
        $status_send = false;
        $data_item = $this->mm->get_data_each("item", array("id_item"=>$id_item));
        if($data_item){
            $tgl_insert    = date("Y-m-d");

            $stok_item_old = $data_item["stok"];
            $stok_item_new = $stok_item_old + $stok_detail_before - $stok_detail_after; 
            
            $admin_create   = $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update    = date("Y-m-d h:i:s");

            $status_record_stok = "plus_min";

            $insert = $this->mp->record_stok_insert($id_tr_detail, $id_item, $tgl_insert, $keterangan_record_stok, $jenis_record_stok, $status_record_stok, $stok_item_old, $stok_detail_before, $stok_detail_after, $stok_item_new, $admin_create, $time_update);

            if($insert){
                $status_send = true;
            }
        }

        return $status_send;
    }
  #------------------------------------insert_record_stok---------------------

}
