<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supliermain extends CI_Controller {

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('admin/Main_suplier', 'msup');
        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        
        $this->auth_v0->check_session_active_ad();

        // print_r($_SESSION);
        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
    }

	public function index(){
		$data["page"] = "suplier_main";
		$data["list_data"] = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));
		$this->load->view('index', $data);
	}

	public function val_form_insert_suplier(){
        $config_val_input = array(
                array(
                    'field'=>'nama_suplier',
                    'label'=>'Nama Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jenis_suplier',
                    'label'=>'jenis suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'email_suplier',
                    'label'=>'Email Suplier',
                    'rules'=>'required|valid_emails|is_unique[suplier.email_suplier]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'tlp_suplier',
                    'label'=>'Telp Suplier',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_ktr_suplier',
                    'label'=>'Alamat Kantor Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'alamat_krm_suplier',
                    'label'=>'Alamat Kirim Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'website',
                    'label'=>'Website',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                    )     
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_suplier(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_suplier"=>"",
                    "email_suplier"=>"",
                    "tlp_suplier"=>"",
                    "alamat_ktr_suplier"=>"",
                    "alamat_krm_suplier"=>"",
                    "website"=>""
                );

        if($this->val_form_insert_suplier()){
            $nama_suplier 		= $this->input->post("nama_suplier");
            $jenis_suplier       = $this->input->post("jenis_suplier");
            $email_suplier 		= $this->input->post("email_suplier");
            $tlp_suplier 		= $this->input->post("tlp_suplier");
            $alamat_ktr_suplier = $this->input->post("alamat_ktr_suplier");
            $alamat_krm_suplier = $this->input->post("alamat_krm_suplier");
            $website 			= $this->input->post("website");
            $is_delete 			= "0";
            $id_admin 			= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 		= date("Y-m-d h:i:s");

            $insert = $this->msup->suplier_insert($nama_suplier, $jenis_suplier, $email_suplier, $tlp_suplier, $alamat_ktr_suplier, $alamat_krm_suplier, $website, $is_delete, $time_update, $id_admin);

            if($insert){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }

        }else{

        	$msg_detail = array(
                    "nama_suplier"=>"",
                    "jenis_suplier"=>"",
                    "email_suplier"=>"",
                    "tlp_suplier"=>"",
                    "alamat_ktr_suplier"=>"",
                    "alamat_krm_suplier"=>"",
                    "website"=>""
                );
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_suplier"]			= strip_tags(form_error('nama_suplier'));
            $msg_detail["jenis_suplier"]        = strip_tags(form_error('jenis_suplier'));
            $msg_detail["email_suplier"] 		= strip_tags(form_error('email_suplier'));
            $msg_detail["tlp_suplier"] 			= strip_tags(form_error('tlp_suplier'));
            $msg_detail["alamat_ktr_suplier"] 	= strip_tags(form_error('alamat_ktr_suplier'));
            $msg_detail["alamat_krm_suplier"] 	= strip_tags(form_error('alamat_krm_suplier'));
            $msg_detail["website"] 				= strip_tags(form_error('website'));         
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    #===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_suplier"])){
        	$id_suplier = $this->input->post('id_suplier');
        	$data = $this->mm->get_data_each("suplier", array("id_suplier"=>$id_suplier, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update_suplier(){
        $config_val_input = array(
                array(
                    'field'=>'nama_suplier',
                    'label'=>'Nama Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'jenis_suplier',
                    'label'=>'jenis suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'email_suplier',
                    'label'=>'Email Suplier',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'tlp_suplier',
                    'label'=>'Telp Suplier',
                    'rules'=>'required|numeric|max_length[13]|min_length[10]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER"),
                        'max_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MAX"),
                        'min_length'=>"%s ".$this->response_message->get_error_msg("TLP_FAIL_MIN")
                    )  
                ),array(
                    'field'=>'alamat_ktr_suplier',
                    'label'=>'Alamat Kantor Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'alamat_krm_suplier',
                    'label'=>'Alamat Kirim Suplier',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'website',
                    'label'=>'Website',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )     
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_suplier(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_suplier"=>"",
                    "jenis_suplier"=>"",
                    "email_suplier"=>"",
                    "tlp_suplier"=>"",
                    "alamat_ktr_suplier"=>"",
                    "alamat_ktr_suplier"=>"",
                    "website"=>""
                );

        if($this->val_form_update_suplier()){
        	$id_suplier 		 = $this->input->post("id_suplier");
            $nama_suplier        = $this->input->post("nama_suplier");
            $jenis_suplier 	     = $this->input->post("jenis_suplier");
            $email_suplier 		 = $this->input->post("email_suplier");
            $tlp_suplier 		 = $this->input->post("tlp_suplier");
            $alamat_ktr_suplier  = $this->input->post("alamat_ktr_suplier");
            $alamat_krm_suplier  = $this->input->post("alamat_krm_suplier");
            $website 	         = $this->input->post("website");
            

            $status_active 	= "0";
            $id_admin 		= $this->session->userdata("admin_lv_1")["id_admin"];
            $time_update 	= date("Y-m-d h:i:s");

          	// check username
          	if(!$this->mm->get_data_each("suplier", array("email_suplier"=>$email_suplier, "id_suplier!="=>$id_suplier))){
          		$set = array(
          				"nama_suplier"=>$nama_suplier,
                        "jenis_suplier"=>$jenis_suplier,
          				"email_suplier"=>$email_suplier,
          				"tlp_suplier"=>$tlp_suplier,
          				"alamat_ktr_suplier"=>$alamat_ktr_suplier,
          				"alamat_krm_suplier"=>$alamat_krm_suplier,
          				"website"=>$website,
                        "time_update"=>$time_update,
                        "id_admin"=>$id_admin

          			);

          		$where = array("id_suplier"=>$id_suplier);

          		$update = $this->mm->update_data("suplier", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["email_suplier"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_suplier"]         = strip_tags(form_error('nama_suplier'));
            $msg_detail["jenis_suplier"]         = strip_tags(form_error('jenis_suplier'));
            $msg_detail["email_suplier"] 		= strip_tags(form_error('email_suplier'));
            $msg_detail["tlp_suplier"] 	        = strip_tags(form_error('tlp_suplier'));
            $msg_detail["alamat_ktr_suplier"] 	= strip_tags(form_error('alamat_ktr_suplier'));
            $msg_detail["alamat_krm_suplier"] 	= strip_tags(form_error('alamat_krm_suplier'));
            $msg_detail["website"] 	            = strip_tags(form_error('website'));
                      
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------update_suplier--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_suplier--------------------------------
#===============================================================================

    public function delete_suplier(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_suplier"=>"",
                );

        if($_POST["id_suplier"]){
        	$id_suplier = $this->input->post("id_suplier");
            $set = array(
                        "is_delete"=>"1",
                        "time_update"=>date("Y-m-d h:i:s"),
                        "id_admin"=>$this->session->userdata("admin_lv_1")["id_admin"]
                    );

            $where = array("id_suplier"=>$id_suplier);

            $update = $this->mm->update_data("suplier", $set, $where);
        	// $delete_suplier = $this->mm->delete_data("suplier", array("id_suplier"=>$id_suplier));
        	if($update){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_suplier"]= strip_tags(form_error('id_suplier'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where("suplier", array("is_delete"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------delete_suplier--------------------------------
#===============================================================================



}
