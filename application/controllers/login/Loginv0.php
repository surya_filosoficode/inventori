<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginv0 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('login/admin_main', 'am');

		$this->load->library("response_message");
		$this->load->library("Auth_v0");

        $this->auth_v0->auth_login();

        $this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
	}

	public function index(){
        // print_r($_SESSION);
        $data["page"] = "login_v0";
		$this->load->view('login/login_v0', $data);
	}

	public function logout(){
		$this->auth_v0->destroy_session();
	}

	private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function get_auth(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("username" => "",
                            "password" => "");
        if($this->val_form_log()){
        	$username = $this->input->post('username');
    		$password = $this->input->post('password');

    		$where = array(
        			'password' => md5($password),
                    'status_active' => "1",
                    "is_delete" => "0"
    			);

            $where_or = array(
                    'email' => $username,
                    'username' => $username
                );
    		
            $cek = $this->am->select_admin($where, $where_or);
    		// $cek["status_log"] = false;
            if($cek){
                $cek["status_log"] = true;
                if($this->auth_v0->set_session($cek)){
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC"));
                }
    		}

        }else {
        	$msg_detail["username"] = form_error("username");
            $msg_detail["password"] = form_error("password");

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        // print_r($cek);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
	}
}
