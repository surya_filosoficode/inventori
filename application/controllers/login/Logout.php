<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->library("response_message");
		$this->load->library("Auth_v0");

		$this->load->library('set_record_stok');
        $this->set_record_stok->insert_record();
	}

	public function index(){
        $this->auth_v0->destroy_session();
	}

}
