<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/template/assets/images/blessindo-icon-16.png">
    <title>PT. Blessindo Farma</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">
    <!--alerts CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/skins/all.css" rel="stylesheet">

    <script src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(<?php print_r(base_url());?>assets/template/assets/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <!-- <form class="form-horizontal form-material" id="loginform" action="index.html"> -->
                    <a href="javascript:void(0)" class="text-center db"><img src="<?php print_r(base_url());?>assets/template/assets/images/blessindo-icon-512.png" height="100" alt="Home" /><br/><img src="<?php print_r(base_url());?>assets/template/assets/images/logo-text-blessindo-2.png" height="50" alt="Home"/></a>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="username" id="username" required="" placeholder="Username">
                            <p id="msg_username" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" id="password" required="" placeholder="Password">
                            <p id="msg_password" style="color: red;"></p>
                        </div>
                    </div>
                    <br><br>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="login" id="login">Log In</button>
                        </div>
                    </div>
                <!-- </form> -->
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php print_r(base_url());?>assets/template/main/js/custom.min.js"></script>
    <!-- Sweet-Alert  -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.init.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php print_r(base_url());?>assets/template/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

    <script type="text/javascript">
        $(document).ready(function() {

        });

        $("#login").click(function() {
            var data_main = new FormData();
            data_main.append('username'     , $("#username").val());
            data_main.append('password'     , $("#password").val());
            
            $.ajax({
                url: "<?php echo base_url()."login/Loginv0/get_auth";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_login(res);
                    console.log(res);
                }
            });
        });

        function response_login(res) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {

                    var data_json = JSON.parse(res);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                    if (main_msg.status) {
                        swal({
                            title: "Login Success",
                            text: main_msg.msg,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            window.location.href = window.location.href;
                        });
                    } else {
                        swal({
                            title: "Login Gagal",
                            text: main_msg.msg,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        }, function() {
                            swal.close();
                            // window.location.href = window.location.href;
                        });

                        $("#msg_username").html(detail_msg.username);
                        $("#msg_password").html(detail_msg.password);
                    }
                    
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
            
        }
    </script>
</body>

</html>