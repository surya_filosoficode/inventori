<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Brand</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
     <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Data Brand</h4>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Brand<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_brand" name="nama_brand" placeholder="Nama Brand" required="">
                                <!-- <input type="search" class="form-control" id="nama_brand" name="nama_brand" placeholder="Nama Brand" required=""> -->
                                <p id="msg_nama_brand" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="keterangan_brand" name="keterangan_brand" placeholder="Keterangan Brand" required="">
                                <!-- <textarea ></textarea> -->
                                <p id="msg_keterangan_brand" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="submit" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                <button type="button" id="reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>    
                        </div>
                        <div class="col-md-12" id="out">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">No. </th>
                                            <th width="15%">Kode Brand</th>
                                            <th width="30%">Nama Brand</th>
                                            <th width="*">Keterangan</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php

                                        if($list_data){
                                            foreach ($list_data as $key => $value) {
                                                $str_btn_action = 
                                                "<center>
                                                    <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_brand."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_brand."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                </center>";
                                                print_r("<tr>
                                                            <td>".($key+1)."</td>
                                                            <td>".$value->id_brand_txt."</td>
                                                            <td>".$value->nama_brand."</td>
                                                            <td>".$value->keterangan_brand."</td>
                                                            <td>".$str_btn_action."</td>
                                                        </tr>");
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Brand</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Brand<span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_nama_brand" name="nama_brand" placeholder="Nama Brand" required="">
                            <p id="_msg_nama_brand" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Keterangan<span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_keterangan_brand" name="keterangan_brand" placeholder="Keterangan Brand" required="">
                            <!-- <textarea ></textarea> -->
                            <p id="_msg_keterangan_brand" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah</button>
                        <button type="button" id="btn_update_reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->



<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_global; 

    $(document).ready(function(){
       clear_form();
    });

    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//

        function clear_form(){
            $("#nama_brand").val("");
            $("#keterangan_brand").val("");
            $("#nama_brand").focus();

            $("#msg_nama_brand").html("");
            $("#msg_keterangan_brand").html("");
        }

        function clear_form_update(){
            $("#_nama_brand").val("");
            $("#_keterangan_brand").val("");
            $("#_nama_brand").focus();

            $("#_msg_nama_brand").html("");
            $("#_msg_keterangan_brand").html("");
        }

        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function render_data(data){
            $("#out").html(data);
        }
    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//
        $("#add_data").click(function(){
            var data_main = new FormData();
                data_main.append('nama_brand'       , $("#nama_brand").val());
                data_main.append('keterangan_brand' , $("#keterangan_brand").val());

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/insert_brand";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_insert(res.responseText);
                }
            });
        });

        $("#reset").click(function(){
            clear_form();
        });


        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form();
            } else {
                $("#msg_nama_brand").html(detail_msg.nama_brand);
                $("#msg_keterangan_brand").html(detail_msg.keterangan_brand);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//
        function update_data(id_brand){
            var data_main = new FormData();
                data_main.append('id_brand', id_brand);

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/get_data_brand";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_set_update(res.responseText);

                    id_global = id_brand;
                }
            });
        }

        function response_set_update(res){
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;

            if (main_msg.status) {
                $("#update_data").modal("show");

                $("#_nama_brand").val(detail_msg.data.nama_brand);
                $("#_keterangan_brand").val(detail_msg.data.keterangan_brand);
                $("#_nama_brand").focus();

            } else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function(){
            var data_main = new FormData();
                data_main.append('id_brand'         , id_global);
                data_main.append('nama_brand'       , $("#_nama_brand").val());
                data_main.append('keterangan_brand' , $("#_keterangan_brand").val());

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/update_brand";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_update(res.responseText);
                }
            });
        });

        $("#btn_update_reset").click(function(){
            clear_form_update();
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $("#update_data").modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_nama_brand").html(detail_msg.nama_brand);
                $("#_msg_keterangan_brand").html(detail_msg.keterangan_brand);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//        

    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//
        function delete_data(id_brand){
            var data_main = new FormData();
                data_main.append('id_brand', id_brand);

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/delete_brand";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_delete(res.responseText);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//   
    
</script>