<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Sample</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div> -->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Data Sample</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php print_r(base_url());?>admin/list_sample"  style="color: #ffff;">
                                <i class="fa fa-list"></i>&nbsp;&nbsp;List Sample
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Customer</b></label>

                                <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="customer" name="customer">
                                </select>
                                <p id="msg_customer" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Tanggal Transaksi</b></label>
                                <input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php print_r(date("Y-m-d"));?>" readonly="" />
                                <p id="msg_tgl_transaksi" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label><b>Atas Nama</b></label>

                                <input type="text" class="form-control" id="atas_nama" name="atas_nama"/>
                                </select>
                                <p id="msg_customer" style="color: red;"></p>
                            </div>
                        </div>


                        <div class="col-md-7" hidden="">
                            <div class="form-group text-right">
                                <label><b>&nbsp;</b></label><br>
                                <button type="button" id="add_main" name="add_main" class="btn waves-effect waves-light btn-rounded btn-info">Buat Detail</button>
                                <button type="button" id="reset_main" name="reset_main" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" id="tmbh_detail" name="tmbh_detail">Tambah Detail</button>
                        </div>

                        <div class="col-md-12 font_edit font_color">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No. </th>
                                            <th>Produk</th>
                                            <th class="text-right" width="25%">Jumlah Produk</th>
                                            <th class="text-right" width="15%">Kode Produksi</th>
                                            <th class="text-right" width="15%">Tgl. Kadaluarsa</th>
                                            <th class="text-right" width="25%">Harga Satuan</th>
                                            <th class="text-right" width="20%">Total Harga</th>
                                            <th class="text-right" width="10%">Proses</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_list_detail">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8"><hr></td>
                                        </tr>
                                        <tr>
                                            <td colspan="8">
                                                <div class="col-md-12">
                                                    <div class="form-group text-right">
                                                        <label><b>&nbsp;</b></label><br>
                                                        <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                                        <button type="button" id="balik" name="balik" class="btn waves-effect waves-light btn-rounded btn-danger">Batal</button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>


<!-- sample_main -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modal_insert_detail" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Form Tambah Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Brand</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="brand" name="brand">
                            </select> -->
                            <input type="text" class="form-control" id="brand" name="brand" list="dl_brand">
                            <datalist id="dl_brand">
                                
                            </datalist>
                            <p id="msg_brand" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Item</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="item" name="item">
                            </select> -->

                            <input type="text" class="form-control" name="item" id="item" list="dl_item">
                            <datalist id="dl_item">
                                
                            </datalist>

                            <p id="msg_item" style="color: red;"></p>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Harga Satuan</b></label>
                            <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" readonly="" value="0" />
                            <label><b id="val_harga_satuan">Harga Satuan</b></label>
                            <p id="msg_harga_satuan" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Nama Item</b></label>
                            <input type="text" class="form-control" id="nama_item" name="nama_item" readonly=""/>
                            <p id="msg_nama_item" style="color: red;"></p>
                        </div>
                    </div>
                    

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kode Produksi</b></label><br>
                            <!-- <input type="text" class="form-control" id="kode_produksi" name="kode_produksi" />
                            <p id="msg_kode_produksi" style="color: red;"></p> -->
                            <label><b id="val_kode_produksi"></b></label>
                            <br>
                            <p id="msg_kode_produksi" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tanggal Kadaluarsa</b></label><br>
                            <!-- <input type="date" class="form-control" id="tgl_kadaluarsa" name="tgl_kadaluarsa" />
                            <p id="msg_tgl_kadaluarsa" style="color: red;"></p> -->
                            <label><b id="val_tgl_kadaluarsa"></b></label>
                            <br>
                            <p id="msg_tgl_kadaluarsa" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Jumlah Item</b></label>
                            <input type="number" class="form-control" id="jml_item" name="jml_item" value="0"/>
                            <p id="msg_jml_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Seharga</b></label>
                            <!-- <input type="text" class="form-control" id="harga_total" name="harga_total" readonly="" value="0" /> -->
                            <br>
                            <label><b id="val_harga_total">Harga Total</b></label>
                            <p id="msg_harga_total" style="color: red;"></p>
                        </div>
                    </div>
                    <!-- <div class="col-md-5">
                        <div class="form-group">
                            <label><b>Diskon per Item</b></label>
                            <input type="number" class="form-control" id="disc_item" name="disc_item" value="0" />
                            <label>
                                <b>Harga Setelah Diskon :</b>
                                <b id="val_harga_after_disc">100</b>
                            </label>
                            <p id="msg_harga_total" style="color: red;"></p>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="check_item" name="check_item" class="btn waves-effect waves-light btn-rounded btn-info">check_item</button> -->

                <button type="button" id="add_detail" name="add_detail" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                
                <button type="button" id="reset_detail" name="reset_detail" class="btn waves-effect waves-light btn-rounded btn-danger">Batal</button>

                <!-- <button type="button" id="finis_detail" name="finis_detail" class="btn waves-effect waves-light btn-rounded btn-success">Akhiri Transaksi</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- sample_main -->

<script src="<?php print_r(base_url());?>assets/js/lib/custom_js.js"></script>
<script type="text/javascript">
    var list_customer = JSON.parse('<?php print_r($list_customer);?>');
    var list_item = JSON.parse('<?php print_r($list_item);?>');
    var list_brand = JSON.parse('<?php print_r($list_brand);?>');

    var default_brand, default_item;

    $(document).ready(function(){
        create_list_customer();
        create_list_brand();
        create_list_item();

        set_item();
    });

//========================================================================//
//-----------------------------------show_modal---------------------------//
//========================================================================//
    $("#tmbh_detail").click(function(){
        $("#modal_insert_detail").modal("show");
    });
//========================================================================//
//-----------------------------------show_modal---------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//
    $("#customer").focus(function(){
    });

    $("#customer").keyup(function(){
        var customer = $("#customer").val();
        if(jQuery.inArray(customer, list_customer) !== -1){
            $("#msg_customer").html("");
        }else {
            $("#msg_customer").html("Customer tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    });

    $("#customer").change(function(){
        // $("#id_customer").val($("#customer").val());
    });

    function create_list_customer(){
        var str_list = "";
        // console.log(list_customer);
        for (let elm in list_customer) {
            str_list += "<option value=\""+elm+"\">"+list_customer[elm].nama_rekanan+"</option>";
            // console.log(str_list);
        }

        $("#customer").html(str_list);
    }
//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//
    $("#brand").focus(function(){
    });

    $("#brand").keyup(function(){
        check_brand_in_master();

        create_list_item();
        set_item();
    });

    $("#brand").change(function(){
        check_brand_in_master();

        create_list_item();
        set_item();
    });

    function create_list_brand(){
        var str_list = "";
        // console.log(list_brand);
        var i = 0;
        for (let elm in list_brand) {
            str_list += "<option value=\""+elm+"\">"+list_brand[elm].nama_brand+"</option>";
            // console.log(str_list);
            if(i == 0){
                default_brand = elm;
            }

            i++;
        }

        $("#dl_brand").html(str_list);
    }

    function check_brand_in_master(){
        var brand = $("#brand").val();
        var status = true;
        if(!(brand in list_item)){
            status = false;

            $("#msg_brand").html("brand tidak ditemukan");
        }else {
            status = true;
            
            $("#msg_brand").html("");
        }

        return status;
    }
//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//
    $("#item").focus(function(){
    });

    $("#item").keyup(function(){
        check_item_in_master();

        set_item();
    });

    $("#item").change(function(){
        check_item_in_master();

        set_item();
    });

    $("#btn_item").click(function(){
        if(check_item_in_master()){
            console.log("item and brand good");
        }else {
            console.log("item and brand nothing");
        }
    });

    function set_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();
        // console.log(list_item[brand]);
        if(item in list_item[brand]){
            $("#msg_item").html("");
            set_value_item();
            jml_item_change();
            // jml_disc_change();
            // console.log();
        }else {
            $("#msg_item").html("Item tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    }

    function clear_item(){
        $("#nama_item").val("");
        $("#harga_satuan").val("0");
        // $("#harga_total").val("0");

        $("#val_kode_produksi").html("");
        $("#val_tgl_kadaluarsa").html("");
        $("#val_harga_satuan").html("Rp. 0");
        // $("#val_harga_total").html("Rp. 0");   
    }

    function set_value_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();

        var data = list_item[brand][item];
        var kode_produksi = list_item[brand][item].kode_produksi_item;
        var tgl_kadaluarsa = list_item[brand][item].tgl_kadaluarsa_item;
        console.log(list_item[brand][item].kode_produksi);
        var harga_bruto = data.harga_bruto;

        $("#nama_item").val(data.nama_item);
        $("#harga_satuan").val(harga_bruto);
        // $("#harga_total").val("0");

        $("#val_kode_produksi").html(kode_produksi);
        $("#val_tgl_kadaluarsa").html(tgl_kadaluarsa);

        $("#val_harga_satuan").html("Rp. "+currency(parseFloat(harga_bruto)));
        $("#val_harga_total").html("Rp. 0"); 
    }

    function create_list_item(){
        var str_list = "";
        var brand = $("#brand").val();
        var array_item_list = list_item[brand];
        // console.log(array_item_list);

        var i = 0;
        for (let elm in array_item_list) {
            str_list += "<option value=\""+elm+"\">("+
                            array_item_list[elm].kode_produksi_item+") "+
                            array_item_list[elm].nama_item+" - "+
                            array_item_list[elm].stok+" ["+
                            array_item_list[elm].satuan+"]"+
                        "</option>";

            if(i == 0){
                default_item = elm;
            }

            i++;
        }

        $("#dl_item").html(str_list);
        // $("#item").val("");
    }

    function check_item_in_master(){
        var brand = $("#brand").val();
        var item = $("#item").val();
        
        var status = false;

        if(brand in list_item){
            if(!(item in list_item[brand])){
                $("#msg_item").html("brand tidak ditemukan");
                
                status = false;
            }else {
                $("#msg_item").html("");

                status = true;
            }
        }
        
        return status;
    }
//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//
    function jml_item_change(){
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();

        var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        // $("#harga_total").val(t_harga);

        $("#val_harga_total").html("Rp. "+currency(parseFloat(t_harga))); 
    }

    $("#jml_item").keyup(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
            // jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });  

    $("#jml_item").change(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
            // jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//
    // var res_json ;
    var res_out;
    var return_sts;

    function check_stok_item(){
        var data_main = new FormData();
        data_main.append('id_item'      , $("#item").val());
        data_main.append('jml_item'     , $("#jml_item").val());

        $.ajax({
            url: "<?php echo base_url()."admin/penjualanmain/check_stok_item/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {                
            },complete: function(res){
                return_sts = response_check_stok_item(res.responseText);
                // console.log(return_sts);
                if(return_sts){

                }
            }
        });

        // console.log(res_out);
    }

    $(document).ajaxComplete(function(event, xhr, settings){
        // console.log(settings.url);
        if(settings.url == "<?php print_r(base_url());?>admin/penjualanmain/check_stok_item/"){
            // console.log(return_sts);
        }
    });

    $("#check_item").click(function(){
        // console.log(check_stok_item());
        check_stok_item();
        if(return_sts){
            console.log(return_sts+": ok");
        }
    });

    function response_check_stok_item(res){
        var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
               
        return main_msg.status;
    }
//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//

//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//
    $("#add_main").click(function(){
        // console.log(check_form_customer());
        var title = "Proses gagal";
        var msg = "Data header gagal disimpan !, Periksa kembali input anda.";
        var status = "error";
        
        if(check_form_customer()){
                
            $("#customer").attr("disabled", true);
            $("#tgl_transaksi").attr("readonly", true);
            $("#atas_nama").attr("disabled", true);

            $("#brand").focus();

            title = "Proses berhasil";
            msg = "Data header berhasil disimpan !";
            status = "success";
        
        }

        create_alert(title, msg, status)
    });

    $("#reset_main").click(function(){
        $("#customer").removeAttr("disabled", true);
        $("#atas_nama").removeAttr("disabled", true);

        $("#customer").focus();
    });

    function check_form_customer(){
        var customer = $("#customer").val();
        var tgl_transaksi = $("#tgl_transaksi").val();
        var atas_nama = $("#atas_nama").val();


        var status = false;
        if(customer && tgl_transaksi && atas_nama){
            status = true;
            // console
        }

        return status;
    }
//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//
    var detail_product = {};
    var list_item_choose = [];

    $("#add_detail").click(function(){
        var data_main = new FormData();
        data_main.append('id_item'      , $("#item").val());
        data_main.append('jml_item'     , $("#jml_item").val());

        $.ajax({
            url: "<?php echo base_url()."admin/penjualanmain/check_stok_item/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {                
            },complete: function(res){
                return_sts = response_check_stok_item(res.responseText);
                // console.log(return_sts);
                if(return_sts){
                    add_detail_penjualan();
                }else{
                    create_alert('Proses Gagal', 'stok kosong', 'error');
                    // console.log("");
                }
            }
        });
    });

    function add_detail_penjualan(){
        var customer        = $("#customer").val();
        var tgl_transaksi   = $("#tgl_transaksi").val();
        var atas_nama       = $("#atas_nama").val();

        var id_item         = $("#item").val();
        var nama_item         = $("#nama_item").val();
        var brand           = $("#brand").val();
        var item            = $("#item").val();
        var harga_satuan    = $("#harga_satuan").val();
        var jml_item        = $("#jml_item").val();
        // var harga_total     = $("#harga_total").val();
        var kode_produksi   = $("#kode_produksi").val();
        var tgl_kadaluarsa  = $("#tgl_kadaluarsa").val();
        

        var t_harga = 0;

        if(harga_satuan && jml_item){
            t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        }

        if(check_value()){
            var tmp_list = {
                "id_item"       : id_item,
                "nama_item"     : nama_item,
                
                "kode_produksi" : list_item[brand][item].kode_produksi_item,
                "tgl_kadaluarsa": list_item[brand][item].tgl_kadaluarsa_item,

                "brand"         : brand,
                "item"          : item,
                "harga_satuan"  : harga_satuan,
                "jml_item"      : jml_item,
                "harga_total"   : t_harga
                // "kode_produksi" : kode_produksi,
                // "tgl_kadaluarsa": tgl_kadaluarsa
                
            };

            if(!(item in detail_product)){
                
                detail_product[item] = tmp_list;
                // console.log(detail_product);
               
                clear_detail();
                render_tbl_detail_product();

                $("#modal_insert_detail").modal("hide");

                create_alert('Proses Berhasil', 'Data detail tersimpan', 'success');
            }else{
                create_alert('Proses Gagal', 'item sudah ada di detail penjualan', 'error');
                // console.log("");
            }
        }else{
            // console.log("check_value false");
            create_alert('Proses Gagal', 'Input data tidak lengkap periksa kembali input lagi', 'error');
                
        }
    }

    function render_tbl_detail_product(){
        var str_tbl = "";
        var no = 1;

        var t_harga = 0;
        var pajak   = 10;

        console.log(detail_product);

        for (let i in detail_product) {
            str_tbl += "<tr>"+
                            "<td align=\"right\">"+no+"</td>"+
                            "<td>("+detail_product[i].id_item+") "+
                            list_item[detail_product[i].brand][detail_product[i].id_item].nama_item+"</td>"+
                            
                            "<td align=\"right\">"+currency(parseFloat(detail_product[i].jml_item))+"</td>"+
                            "<td>"+detail_product[i].kode_produksi+"</td>"+

                            "<td>"+detail_product[i].tgl_kadaluarsa+"</td>"+

                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_satuan))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_total))+"</td>"+
                            
                            "<td>"+
                                "<center>"+
                                "<button class=\"btn btn-danger\" id=\"del_detail\" onclick=\"delete_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                                "</center>"+
                            "</td>"+
                        "</tr>";
            t_harga += parseFloat(detail_product[i].harga_total);
            console.log(i);
            no++;
        }

        str_tbl += "<tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Total</b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_t_harga\">Rp. "+currency(t_harga)+"</b></td>"+
                        "<td></td>"+
                        "<td colspan=\"1\"></td>"+
                    "</tr>";
        $("#tbl_list_detail").html(str_tbl);
    }

    function check_value(){
        
        var customer = $("#customer").val();
        var tgl_transaksi = $("#tgl_transaksi").val();
        var atas_nama = $("#atas_nama").val();

        var id_item = $("#item").val();
        var brand = $("#brand").val();
        var item = $("#item").val();
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();
        // var harga_total = $("#harga_total").val();
        // var kode_produksi = $("#kode_produksi").val();
        // var tgl_kadaluarsa = $("#tgl_kadaluarsa").val();

        var status = false;

        if(!id_item 
            || !customer || !tgl_transaksi || !atas_nama
            || !brand || !item || !harga_satuan || !jml_item
            // || !harga_total 
            // || !kode_produksi || !tgl_kadaluarsa
            || !check_item_in_master()
            ){
            console.log("kosong");
        }else {
            if(parseInt(jml_item) > 0){
                status = true;
                console.log("isi");
            }
        }

        return status;
    }
    

    $("#reset_detail").click(function(){
        $("#modal_insert_detail").modal("hide");
        clear_detail();
    });

    function clear_detail(){
        nama_item = $("#nama_item").val("");
        harga_satuan = $("#harga_satuan").val("");
        jml_item = $("#jml_item").val("0");
        // harga_total = $("#harga_total").val("");
        kode_produksi = $("#kode_produksi").html("");
        tgl_kadaluarsa = $("#tgl_kadaluarsa").html("");

        val_harga_satuan    = $("#val_harga_satuan").html("Rp. 0");
        val_harga_total     = $("#val_harga_total").html("Rp. 0");

        $("#brand").focus();

        set_item();
        set_value_item();
        jml_item_change();
    }

    function delete_detail(id_item){
        create_alert('Proses Berhasil', detail_product[id_item].nama_item+' di hapus dari detail penjualan', 'success');
        delete detail_product[id_item];
        render_tbl_detail_product();
    }

    $("#finis_detail").click(function(){

    });
//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//
    $("#lanjut").click(function(){
        if(validation_main_check()){

            var data_main = new FormData();
            // data_main.append('id_customer'  , $("#id_customer").val());
            data_main.append('customer'     , $("#customer").val());
            data_main.append('tgl_transaksi', $("#tgl_transaksi").val());
            data_main.append('atas_nama'    , $("#atas_nama").val());

            data_main.append('list_detail', JSON.stringify(detail_product));

            $.ajax({
                url: "<?php echo base_url()."admin/samplemain/insert_sample_header/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_req(res);
                }
            });
        }else{
            create_alert('Proses Gagal', 'Periksa Kembali input saudara', 'error');
        }
    });

    $("#balik").click(function(){
        $("#disc").focus();
    });

    function validation_main_check(){
        var status = false;

        var customer    = $("#customer").val();
        var atas_nama       = $("#atas_nama").val();
        var tgl_transaksi = $("#tgl_transaksi").val();


        if(!customer || !atas_nama || !tgl_transaksi || jQuery.isEmptyObject(detail_product)
            ){
            console.log("kosong");
        }else {
            status = true;
            console.log("isi");
        }

        return status;
    }

    function response_req(res){
        var res_data = JSON.parse(res);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success", "<?php print_r(base_url()."admin/list_sample")?>");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning", "<?php print_r(base_url()."admin/list_sample")?>");
        }
    }
//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//


</script>
