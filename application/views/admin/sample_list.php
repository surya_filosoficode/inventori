<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">List Sample</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">List Sample</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="http://localhost/kasir/admin/data_sample" style="color: #ffff;">
                                <i class="fa fa-plus-square"></i>&nbsp;&nbsp;Tambah Sample
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body font_edit font_color">
                    <div class="table-responsive m-t-40">
                        <table id="myTable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%">Kode Transaksi</th>
                                    <th width="15%">Customer</th>
                                    <th width="15%">Penerima Sample</th>
                                    <th width="10%">Tgl. Sample</th>
                                    <th width="15%">Seharga</th>
                                    <th width="15%">Status Ganti</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>

                            <tbody id="main_table_content">
                                <?php
                                    if($list_data){
                                        foreach ($list_data as $key => $value) {
                                            $status = "<span class=\"label label-danger\">Belum Di Ganti</span>";

                                            $str_btn_retur = "<a class=\"btn btn-success\" id=\"up_data\"  href=\"".base_url()."admin/data_retur_sample\" style=\"width: 40px;\"><i class=\"fa fa-exchange\" ></i></a>&nbsp;&nbsp;";

                                            $str_btn_main = "<a class=\"btn btn-info\" id=\"up_data\"  href=\"".base_url()."admin/read_sample/".$value->id_tr_header."\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></a>&nbsp;&nbsp;
                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_tr_header."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>&nbsp;&nbsp;";

                                            $str_btn_del_p = "";

                                            if($value->status_ganti_tr_header == "1"){
                                                $status = "<span class=\"label label-info\">Sudah Di Ganti</span>";

                                                $str_btn_del_p = "<button class=\"btn btn-primary\" id=\"del_data_pengganti\" onclick=\"delete_data_pengganti('".$value->id_tr_header."')\" style=\"width: 40px;\"><i class=\"fa fa-fast-backward\"></i></button>";    
                                                
                                                $str_btn_retur = "";
                                                $str_btn_main = "";
                                            }

                                            print_r("
                                                <tr>
                                                    <td>".$value->id_tr_header."</td>
                                                    <td>".$value->nama_rekanan."</td>
                                                    <td>".$value->atas_nama."</td>
                                                    <td>".$value->tgl_transaksi_tr_header."</td>
                                                    <td>Rp. ".number_format($value->total_pembayaran_tr_header,2,',', '.')."</td>
                                                    <td>".$status."</td>
                                                    <td>
                                                        <center>
                                                        "   .$str_btn_retur
                                                            .$str_btn_main
                                                            .$str_btn_del_p."
                                                        </center>
                                                    </td>
                                                </tr>"
                                            );
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>
                        <a class="btn btn-success" style="width: 40px;"><i class="fa fa-exchange" style="color: white;"></i></a>
                        <label class="form-label text-success">Pengembalian Sample</label>,&nbsp;
                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>,&nbsp;
                        <a class="btn btn-primary" style="width: 40px;"><i class="fa fa-fast-backward" style="color: white;"></i></a>
                        <label class="form-label text-primary">Hapus Data Produk Pengganti</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url());?>assets/js/lib/custom_js.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        
    });


    function delete_data(id_data){
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            
            SweetAlert.prototype.init = function() {
                swal({   
                    title: "Konfirmasi Hapus Data",   
                    text: "Ketika data ini di hapus maka semua yang berkaitan dengan data ini juga akan hilang, Apakah data tetap di hapus ?",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Lanjutkan",   
                    cancelButtonText: "Perbaiki",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {
                        main_delete_data(id_data);
                        // swal.close();
                    } else{
                        swal.close();
                    }
                });
            },
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                        
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }

    function main_delete_data(id_data){
        var data_main = new FormData();
        data_main.append('id_tr_header'  , id_data);

        $.ajax({
            url: "<?php echo base_url()."admin/samplemain/delete_sample_header/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_req(res);
            }
        });
    }

    function response_req(res){
        var res_data = JSON.parse(res);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success", "<?php echo base_url()."admin/list_sample";?>");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning", "<?php echo base_url()."admin/list_sample";?>");
        }
    }






    function delete_data_pengganti(id_data){
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            
            SweetAlert.prototype.init = function() {
                swal({   
                    title: "Konfirmasi Hapus Data",   
                    text: "Ketika data ini di hapus maka semua yang berkaitan dengan data ini juga akan hilang, Apakah data tetap di hapus ?",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Lanjutkan",   
                    cancelButtonText: "Perbaiki",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {
                        main_delete_data_pengganti(id_data);
                        // swal.close();
                    } else{
                        swal.close();
                    }
                });
            },
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                        
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }

    function main_delete_data_pengganti(id_data){
        var data_main = new FormData();
        data_main.append('id_tr_header'  , id_data);

        $.ajax({
            url: "<?php echo base_url()."admin/retursamplemain/delete_pengembalian/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_req(res);
            }
        });
    }
</script>


