<!DOCTYPE html>
<html>
<head>
	<title>Faktur PT. Blesindo</title>
</head>
<body>
<?php
	$array_of_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
	$array_of_day = [
					"Sunday"=>"Senin",
					"Monday"=>"Senin",
					"Tuesday"=>"Selasa",
					"Wednesday"=>"Rabu",
					"Thursday"=>"Kamis",
					"Friday"=>"Jumat",
					"Saturday"=>"Sabtu"
				];
	if(isset($data_header) && isset($data_detail)){
		$nama_rekanan 				= $data_header[0]->nama_rekanan;
		$id_customer 				= $data_header[0]->id_customer;
		$tgl_transaksi_tr_header 	= $data_header[0]->tgl_transaksi_tr_header;
		$id_tr_header 				= $data_header[0]->id_tr_header;
		$alamat_ktr_rekanan			= $data_header[0]->alamat_ktr_rekanan;


		$nama_sales 				= $data_header[0]->nama_sales;
		$tempo_tr_header			= $data_header[0]->tempo_tr_header;


		$total_pembayaran_tr_header 	 = $data_header[0]->total_pembayaran_tr_header;
		$disc_all_tr_header				 = $data_header[0]->disc_all_tr_header;
		$total_pembayaran_disc_tr_header = $data_header[0]->total_pembayaran_disc_tr_header;
		$ppn_tr_header					 = $data_header[0]->ppn_tr_header;
		$total_pembayaran_pnn_tr_header	 = $data_header[0]->total_pembayaran_pnn_tr_header;


		$t_nominal_disc = $total_pembayaran_tr_header * $disc_all_tr_header / 100;
		$t_nominal_ppn = $total_pembayaran_tr_header * $ppn_tr_header / 100;


		$timestamp_tr = strtotime($tgl_transaksi_tr_header);
		$str_timestamp_tr = date("l", $timestamp_tr);
		$hari_tr = $array_of_day[$str_timestamp_tr];
		$tgl_tr = date("d", $timestamp_tr);
		$bln_tr = $array_of_month[(int)date("m", $timestamp_tr)];
		$th_tr = date("Y", $timestamp_tr);

		$str_tr = $hari_tr.", ".$tgl_tr." ".$bln_tr." ".$th_tr;

		$datetime_tr = new DateTime($tgl_transaksi_tr_header);
		$datetime_tempo = new DateTime($tempo_tr_header);
		$diff_tempo = $datetime_tr->diff($datetime_tempo)->days;

		$no = 1;
		$str_tbl = "";
		foreach ($data_detail as $key => $value) {
			$str_tbl .= "<tr>
						<td align=\"center\">".$no."</td>
						<td>
							<table cellspacing='0' border=\"0\" style=\"width:100%;\">
								<tr>
									<td width=\"65%\">".$value->id_item." - ".ucwords($value->nama_item)."</td>
									<td style=\"text-align: right\">".$value->kode_produksi_item."</td>
								</tr>
								
							</table>
						</td>
						<td align=\"center\">".$value->satuan."</td>
						<td align=\"right\">".$value->jml_item_tr_detail."</td>
						<td align=\"right\">Rp. ".number_format($value->harga_satuan_tr_detail, 2, '.', ',')."</td>
						<td align=\"right\">".$value->disc_item_tr_detail."</td>
						<td style='text-align:right'>Rp. ".number_format($value->harga_total_fix_tr_detail, 2, '.', ',')."</td>
					</tr>";
			$no++;
		}


		$no_siup = "";
		$no_npwp = "";
		if(isset($setting)){
			$array_npwp = array();
			$array_siup = array();
			foreach ($setting as $key => $value) {
				if($value->jenis_setting == "npwp"){
					$array_npwp = $value;
					$no_npwp = $value->keterangan_setting;
				}elseif ($value->jenis_setting == "siup") {
					$array_siup = $value;
					$no_siup = $value->keterangan_setting;
				}
			}
		}

?>
	<center>
		<table style='width:550px; font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
			<td align='center' style='vertical-align:top'>	
				<span style='font-size:24pt'><b>PT. BLESSINDO FARMA</b></span>
				</br><span style='font-size:12pt'>JL. Mayjend Soengkono Kompleks Wonokriti Indah S-33</span>
				</br><span style='font-size:12pt'>Telp. (031)5673150, Fax. (031)5668523</span>
				</br><span style='font-size:12pt'>Surabaya</span>	
			</td>
		</table>
		<table style='width:60%; font-family:calibri; border-collapse: collapse;' border = '0' >
			<tr>
				<td align="left">
					<span>IZIN PBF No.<?php print_r($no_siup);?></span>
				</td>
				<td align="right">
					<span>NPWP: <?php print_r($no_npwp);?></span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<b><hr size='3px' color='black'></b>
				</td>
			</tr>
		</table>
		<table style='width:60%; font-size:8pt; font-family:calibri; border-collapse: collapse; ' border = '0'>
			<tr>
				<td style="padding-right:1px; width: 10%; font-size:12pt">
					<span>Nomor</span><br />
					<span>Hari, Tanggal</span><br />
					<span>Customer</span><br />
					<span>Alamat</span><br />
				</td>
				<td style="font-size:12pt; width: 20%;">
					<span>: <?php print_r($id_tr_header);?></span><br />
					<span>: <?php print_r($str_tr);?></span><br />
					<span>: <?php print_r($id_customer);?> - <?php print_r(ucwords($nama_rekanan));?></span><br />
					<span>: <?php print_r(ucwords($alamat_ktr_rekanan));?></span>
				</td>
				<td style="font-size: 18pt; width: 40%;text-align: center;">
					<span><u>Faktur</u></span>
				</td>
				<td style="padding-right:1px; width: 15%; font-size:12pt">
					<span>Salesman</span><br />
					<span>Tanggal Jatuh Tempo</span>
				</td>
				<td style="font-size:12pt; width: 10%;">
					<span>: <?php print_r(ucwords($nama_sales));?></span><br />
					<span>: <?php print_r($diff_tempo);?> Hari</span>
					
				</td>
			</tr>
		</table>
		<table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
			<tr align='center'>
				<td width='2%'>No</td>
				<td width='20%'>Jenis Barang</td>
				<td width='3%'>Sat</td>
				<td width='4%'>Qty</td>
				<td width='13%'>Harga</td>
				<td width='3%'>%</td>
				<td width='13%'>Jumlah</td>
			</tr>
			
			<?php print_r($str_tbl);?>
		</table>

		<table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
			<tr>
				<td width="25%" align="left" style="padding-right: 30px;">
					<table>
						<tr >
							<td style='padding-bottom: 65px;'>Penerima / Stempel</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
					</table>
				</td>
				<td width="15%" align="left" style="padding-right: 20px;">
					<table>
						<tr >
							<td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>Penjual</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
					</table>
				</td>
				<td style='border:1px solid black; padding:5px; text-align:left; width:30%;'>
					<span><center>PERHATIAN</center></span>
					<span>1. Barang harap diperiksa dengan baik</span><br />
					<span>2. Kekurangan setelah pengiriman diluar tanggung jawab kami</span><br />
					<span>3. Barang yg telah dibeli tak dapat dikembalikan</span>
				</td>
				<td>
					<table align="right">
						<tr align="right">
							<td>Sub Total:</td>
							<td> </td>
							<td><?php print_r(number_format($total_pembayaran_tr_header, 2, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>Disc 1(<?php print_r($disc_all_tr_header); ?>%):</td>
							<td></td>
							<td>Rp. <?php print_r(number_format($t_nominal_disc, 2, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>Disc 2:</td>
							<td> </td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2"><hr></td>
						</tr>
						<tr align="right">
							<td></td>
							<td colspan="2">Rp. <?php print_r(number_format($total_pembayaran_disc_tr_header, 2, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>PPN (<?php print_r($ppn_tr_header); ?>%):</td>
							<td> </td>
							<td>Rp. <?php print_r(number_format($t_nominal_ppn, 2, '.', ','));?></td>
						</tr>
						<tr align="right">
							<td>Grand Total:</td>
							<td> </td>
							<td>Rp. <?php print_r(number_format($total_pembayaran_pnn_tr_header, 2, '.', ','));?></td>
						</tr>
					</table>
				</td>	
			</tr>
			<tr>
				<!-- <td width="15%" align="left" style="padding-right: 20px;">
					<span><hr width="80px;"></span>
				</td>
				<td width="15%" align="left" style="padding-right: 20px;">
					<span><hr width="80px;"></span>
				</td> -->
			</tr>
		</table>
	</center>
<?php		
	}
?>

</body>

<script type="text/javascript">
	window.print();
</script>
</html>