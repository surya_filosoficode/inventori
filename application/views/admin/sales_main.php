<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Sales</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Sales</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_sales"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Sales</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">Kode Sales</th>
                                            <th width="15%">Nama Sales</th>
                                            <th width="15%">Tlp Sales</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r_sales => $v_sales) {
                                                    
                                                    echo "<tr>
                                                        <td>".($r_sales+1)."</td>
                                                        <td>".$v_sales->id_sales."</td>
                                                        <td>".$v_sales->nama_sales."</td>
                                                        <td>".$v_sales->tlp_sales."</td>
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_sales\" onclick=\"update_sales('".$v_sales->id_sales."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_sales\" onclick=\"delete_sales('".$v_sales->id_sales."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert sales------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_sales" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_sales";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Sales</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" hidden="">
                                <label for="message-text" class="control-label">NIK Sales <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nik_sales" name="nik_sales" value="-" readonly="" required="">
                                <p id="msg_nik_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Sales <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_sales" name="nama_sales" required="">
                                <p id="msg_nama_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Kelamin Sales<span style="color: red;">*</span></label>
                                <select class="form-control" id="jk_sales" name="jk_sales">
                                    <option value="0">Pria</option>
                                    <option value="1">Wanita</option>
                                </select>
                                <p id="msg_jk_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Sales <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="tlp_sales" name="tlp_sales" required="">
                                <p id="msg_tlp_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Sales<span style="color: red;">*</span></label>
                                <textarea class="form-control" id="alamat_sales" name="alamat_sales" required=""></textarea>
                                <p id="msg_alamat_sales" style="color: red;"></p>
                            </div>
                            
                        </div>
                    </div>
                </div>         
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_sales" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_sales------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_sales------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_sales" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Sales</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_rekanan";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" hidden="">
                                <label for="message-text" class="control-label">NIK Sales <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nik_sales" name="nik_sales" value="-" readonly="" required="">
                                <p id="_msg_nik_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Sales <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_sales" name="nama_sales" required="">
                                <p id="_msg_nama_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Kelamin Sales <span style="color: red;">*</span></label>
                                <select class="form-control" id="_jk_sales" name="jk_sales">
                                    <option value="0">Pria</option>
                                    <option value="1">Wanita</option>
                                </select>
                                <p id="_msg_jk_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Sales <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_tlp_sales" name="tlp_sales" required="">
                                <p id="_msg_tlp_sales" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Sales<span style="color: red;">*</span></label>
                                <textarea class="form-control" id="_alamat_sales" name="alamat_sales" required=""></textarea>
                                <p id="_msg_alamat_sales" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_sales" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_sales------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert sales--------------------------//
    //=========================================================================//
        $("#add_sales").click(function() {
            var data_main = new FormData();
            data_main.append('nik_sales'            , $("#nik_sales").val());
            data_main.append('nama_sales'           , $("#nama_sales").val());
            data_main.append('jk_sales'             , $("#jk_sales").val());
            data_main.append('tlp_sales'            , $("#tlp_sales").val());
            data_main.append('alamat_sales'         , $("#alamat_sales").val());
           
            $.ajax({
                url: "<?php echo base_url()."admin/salesmain/insert_sales";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_sales').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_insert();
            } else {
                $("#msg_nik_sales").html(detail_msg.nik_sales);
                $("#msg_nama_sales").html(detail_msg.nama_sales);
                $("#msg_jk_sales").html(detail_msg.jk_sales);
                $("#msg_tlp_sales").html(detail_msg.tlp_sales);
                $("#msg_alamat_sales").html(detail_msg.alamat_sales);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }

        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function clear_form_insert(){

            // $("#nik_sales").val("");
            $("#nama_sales").val("");
            $("#jk_sales").val("");
            $("#tlp_sales").val("");
            $("#alamat_sales").val("");
           
            $("#msg_nik_sales").html("");
            $("#msg_nama_sales").html("");
            $("#msg_jk_sales").html("");
            $("#msg_tlp_sales").html("");
            $("#msg_alamat_sales").html("");
           
        }

        function render_data(data){
            var str_table = "";
            var no = 1;
            for (let i in data) {
                
                str_table += "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td>"+data[i].id_sales+"</td>"+
                    // "<td>"+data[i].nik_sales+"</td>"+
                    "<td>"+data[i].nama_sales+"</td>"+
                    "<td>"+data[i].tlp_sales+"</td>"+
                    "<td>"+
                        "<center>"+
                        "<button class=\"btn btn-info\" id=\"up_sales\" onclick=\"update_sales('"+data[i].id_sales+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                        "<button class=\"btn btn-danger\" id=\"del_sales\" onclick=\"delete_sales('"+data[i].id_sales+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                        "</center>"+
                    "</td>"+
                    "</tr>";
                    no++;
                // str_table += "";
            }
            $("#main_table_content").html(str_table);
        }

        function alert_confirm(title, msg, status, txt_cencel_btn, id_sales){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: title,
                        text: msg,
                        type: status,
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: txt_cencel_btn,
                        closeOnConfirm: false
                    }, function() {
                        method_delete(id_sales);
                        swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
    //=========================================================================//
    //-----------------------------------insert sales--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_sales_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            // $("#_nik_sales").val("");
            $("#_nama_sales").val("");
            $("#_jk_sales").val("");
            $("#_tlp_sales").val("");
            $("#_alamat_sales").val("");
           

            $("#_msg_nik_sales").html("");
            $("#_msg_nama_sales").html("");
            $("#_msg_jk_sales").html("");
            $("#_msg_tlp_sales").html("");
            $("#_msg_alamat_sales").html("");
            
        }

        function update_sales(id_sales) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_sales', id_sales);

            $.ajax({
                url: "<?php echo base_url()."admin/salesmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_sales);
                    $("#update_sales").modal('show');
                }
            });
        }

        function set_val_update(res, id_sales) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_sales;
                
                $("#_nik_sales").val(list_data.nik_sales);
                $("#_nama_sales").val(list_data.nama_sales);
                $("#_jk_sales").val(list_data.jk_sales);
                $("#_tlp_sales").val(list_data.tlp_sales);
                $("#_alamat_sales").val(list_data.alamat_sales);
                
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_sales_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_sales--------------------------//
    //=========================================================================//
        $("#btn_update_sales").click(function() {
            var data_main = new FormData();
            data_main.append('id_sales', id_cache);

            data_main.append('nik_sales'         , $("#_nik_sales").val());
            data_main.append('nama_sales'        , $("#_nama_sales").val());
            data_main.append('jk_sales'          , $("#_jk_sales").val());
            data_main.append('tlp_sales'         , $("#_tlp_sales").val());
            data_main.append('alamat_sales'      , $("#_alamat_sales").val());
            
            $.ajax({
                url: "<?php echo base_url()."admin/salesmain/update_sales";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_sales').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_nik_sales").html(detail_msg.nik_sales);
                $("#_msg_nama_sales").html(detail_msg.nama_sales);
                $("#_msg_jk_sales").html(detail_msg.jk_sales);
                $("#_msg_tlp_sales").html(detail_msg.tlp_sales);
                $("#_msg_alamat_sales").html(detail_msg.alamat_sales);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------update_sales--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------sales_delete--------------------------//
    //=========================================================================//

        function method_delete(id_sales){
            var data_main = new FormData();
            data_main.append('id_sales', id_sales);

            $.ajax({
                url: "<?php echo base_url()."admin/salesmain/delete_sales";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_sales(id_sales) {
            alert_confirm("Pesan Konfirmasi.!!", "Hapus ?", "warning", "Hapus", id_sales);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------sales_delete--------------------------//
    //=========================================================================//

   
</script>