<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Pembelian</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div> -->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php
    $id_suplier = "";
    $suplier = "";
    $tgl_transaksi = "";

    $disc = "";
    $cara_pembayaran = "";
    $tempo = "";

    $id_tr_header = $this->uri->segment(3);

    if($data_header){
        $id_suplier = $data_header[0]->id_suplier;
        $suplier = $data_header[0]->id_suplier;//
        $tgl_transaksi = $data_header[0]->tgl_transaksi_tr_header;
        $disc = $data_header[0]->disc_all_tr_header;
        $cara_pembayaran = $data_header[0]->cara_pembayaran_tr_header;//
        $tempo = $data_header[0]->tempo_tr_header;
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Data Pembelian</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php print_r(base_url());?>admin/list_pembelian"  style="color: #ffff;">
                                <i class="fa fa-list"></i>&nbsp;&nbsp;List Pembelian
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Kode Suplier</b></label>
                                <input type="text" class="form-control" id="id_suplier" name="id_suplier" value="90" readonly=""/>
                                <p id="msg_id_suplier" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Tanggal Transaksi</b></label>
                                <input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php print_r(date("Y-m-d"));?>" readonly="" />
                                <p id="msg_tgl_transaksi" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><b>Suplier</b></label>

                                <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="suplier" name="suplier">
                                </select>
                                <p id="msg_suplier" style="color: red;"></p>
                            </div>
                        </div>


                        <div class="col-md-12" hidden="">
                            <div class="form-group text-right">
                                <button type="button" id="add_main" name="add_main" class="btn waves-effect waves-light btn-rounded btn-info">Buat Detail</button>
                                <button type="button" id="reset_main" name="reset_main" class="btn waves-effect waves-light btn-rounded btn-danger">Batal</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr></div>

                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" id="tmbh_detail" name="tmbh_detail">Tambah Detail</button>       
                        </div>

                        <div class="col-md-12 font_edit font_color">
                            <div class="table-responsive m-t-40">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No. </th>
                                            <th>Produk</th>
                                            <th class="text-right" width="10%">Kode Produksi</th>
                                            <th class="text-right" width="10%">Tgl. Kadaluarsa</th>
                                            <th class="text-right" width="5%">Jumlah Produk</th>
                                            <th class="text-right" width="10%">Harga Satuan Pembelian</th>
                                            <th class="text-right" width="15%">Total Harga</th>
                                            <th class="text-right" width="5%">Diskon</th>
                                            <th class="text-right" width="15%">Harga Setelah Diskon</th>
                                            <th class="text-right" width="5%">Proses</th>
                                        </tr>
                                        <!-- <tr>
                                            <td>#</td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="gf" name="gf" style="font-size: 12px;">
                                            </td>

                                        </tr> -->
                                    </thead>
                                    <tbody id="tbl_list_detail">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="12"><hr></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Diskon</b></label>
                                    <input type="number" class="form-control" id="disc" name="disc" step='0.01' />
                                    <p id="msg_disc" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Cara Pembayaran</b></label>
                                    <select class="form-control" id="cara_pembayaran" name="cara_pembayaran">
                                        <option value="0">Tunai</option>
                                        <option value="1">Kredit</option>
                                    </select>
                                    <p id="msg_cara_pembayaran" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Tanggal Jatuh Tempo</b></label>
                                    <input type="date" class="form-control" id="tempo" name="tempo"/>
                                    <p id="msg_tempo" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="5">
                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <label><b>&nbsp;</b></label><br>
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                    <button type="button" id="balik" name="balik" class="btn waves-effect waves-light btn-rounded btn-danger">Kembali</button>
                                </div>
                            </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>


<!-- pembelian_main -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modal_insert_detail" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Form Ubah Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Brand</b></label>

                            <input type="text" class="form-control" id="brand" name="brand" list="dl_brand">
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="brand" name="brand">
                            </select> -->

                            <datalist id="dl_brand">
                                
                            </datalist>

                            <p id="msg_brand" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Item</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="item" name="item"> -->
                            <!-- </select> -->
                            <input type="text" class="form-control" name="item" id="item" list="dl_item">

                            <!-- <button type="button" id="btn_item" name="btn_item">cek item</button> -->
                            <datalist id="dl_item">
                                
                            </datalist>

                            <p id="msg_item" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Nama Item</b></label>
                            <input type="text" class="form-control" id="nama_item" name="nama_item" readonly=""/>
                            <p id="msg_nama_item" style="color: red;"></p>
                        </div>
                    </div>
                    

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kode Produksi</b></label><br>
                            <!-- <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" value="0" /> -->
                            <label><b id="val_kode_produksi"></b></label>
                            <br>
                            <p id="msg_kode_produksi" style="color: red;"></p>
                            <!-- <p id="msg_harga_satuan" style="color: red;"></p> -->
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Keterangan Harga Jual</b></label><br>
                            <!-- <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" value="0" /> -->
                            <label><b id="val_harga_jual">RP. 0</b></label>
                            <br>
                            <p id="msg_val_harga_jual" style="color: red;"></p>
                            <!-- <p id="msg_harga_satuan" style="color: red;"></p> -->
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tgl. Kadaluarsa</b></label><br>
                            <!-- <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" value="0" /> -->
                            <label><b id="val_tgl_kadaluarsa"></b></label>
                            <br>
                            <p id="msg_tgl_kadaluarsa" style="color: red;"></p>
                            <!-- <p id="msg_harga_satuan" style="color: red;"></p> -->
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Harga Satuan Pembelian</b></label>
                            <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" value="0" />
                            <label><b id="val_harga_satuan">RP. 0</b></label>
                            <p id="msg_harga_satuan" style="color: red;"></p>
                        </div>
                    </div>

                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Jumlah Item</b></label>
                            <input type="number" class="form-control" id="jml_item" name="jml_item" value="0"/>
                            <p id="msg_jml_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Harga Total</b></label>
                            <input type="text" class="form-control" id="harga_total" name="harga_total" readonly="" value="0" />
                            <label><b id="val_harga_total">Harga Total</b></label>
                            <p id="msg_harga_total" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Diskon per Item</b></label>
                            <input type="number" class="form-control" id="disc_item" name="disc_item" value="0" />
                            <label>
                                <b>Harga Setelah Diskon :</b>
                                <b id="val_harga_after_disc">100</b>
                            </label>
                            <p id="msg_harga_total" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" id="check_item" name="check_item" class="btn waves-effect waves-light btn-rounded btn-info">check_item</button> -->

                <button type="button" id="add_detail" name="add_detail" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                
                <button type="button" id="reset_detail" name="reset_detail" class="btn waves-effect waves-light btn-rounded btn-danger">Batal</button>

                <!-- <button type="button" id="finis_detail" name="finis_detail" class="btn waves-effect waves-light btn-rounded btn-success">Akhiri Transaksi</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- pembelian_main -->


<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var list_suplier = JSON.parse('<?php print_r($list_suplier);?>');
    var list_item = JSON.parse('<?php print_r($list_item);?>');
    var list_brand = JSON.parse('<?php print_r($list_brand);?>');

    console.log(list_item);
    console.log(list_item);
    
    var list_detail = JSON.parse('<?php print_r($data_detail);?>');

    var detail_product = {};

    var default_brand, default_item, harga_jual_global;

    $(document).ready(function(){
        create_list_suplier();
            $("#id_suplier").val($("#suplier").val());
            $("#suplier").focus();

        // select_cara_bayar();

        // $("#tempo").val("2019-10-22");
        // set_tgl_tempo();
        select_cara_bayar();

        set_param_update();
        set_detail_update();

        create_list_brand();
        create_list_item();
        
        set_item();
        jml_disc_change();
    });

    function set_param_update(){
        $("#id_suplier").val("<?php print_r($id_suplier); ?>");
        $("#suplier").val("<?php print_r($suplier); ?>");
        $("#tgl_transaksi").val("<?php print_r($tgl_transaksi); ?>");
        $("#disc").val("<?php print_r($disc); ?>");
        $("#cara_pembayaran").val("<?php print_r($cara_pembayaran); ?>");
        $("#tempo").val("<?php print_r($tempo); ?>");
    }


    function set_detail_update() {
        // console.log(list_detail);
        for (let i in list_detail) {
            var tmp_list = {
                "id_item"       : list_detail[i].id_item,
                "nama_item"     : list_detail[i].nama_item,
                "kode_produksi" : list_detail[i].kode_produksi_item,
                "tgl_kadaluarsa": list_detail[i].tgl_kadaluarsa_item,
                "brand"         : list_detail[i].id_brand,
                "item"          : list_detail[i].id_item,
                "harga_satuan"  : parseFloat(list_detail[i].harga_satuan_tr_detail),
                "jml_item"      : parseInt(list_detail[i].jml_item_tr_detail),
                "disc_item"     : parseFloat(list_detail[i].disc_item_tr_detail),
                "harga_total"   : parseFloat(list_detail[i].harga_total_tr_detail),
                "harga_after_disc"   : parseFloat(list_detail[i].harga_total_fix_tr_detail)
                
            };

            detail_product[i] = tmp_list;
        }
        render_tbl_detail_product();
    }

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function create_alert(title, msg, status){
        $(function() {
            "use strict";                      
           $.toast({
            heading: title,
            text: msg,
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: status,
            hideAfter: 3500, 
            stack: 6
          });
        });
    }


//========================================================================//
//-----------------------------------show_modal---------------------------//
//========================================================================//
    $("#tmbh_detail").click(function(){
        $("#modal_insert_detail").modal("show");
    });
//========================================================================//
//-----------------------------------show_modal---------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------suplier-----------------------------//
//========================================================================//
    $("#suplier").focus(function(){
    });

    $("#suplier").keyup(function(){
        var suplier = $("#suplier").val();
        if(jQuery.inArray(suplier, list_suplier) !== -1){
            $("#msg_suplier").html("");
        }else {
            $("#msg_suplier").html("suplier tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    });

    $("#suplier").change(function(){
        $("#id_suplier").val($("#suplier").val());
    });

    function create_list_suplier(){
        var str_list = "";
        // console.log(list_suplier);
        for (let elm in list_suplier) {
            str_list += "<option value=\""+elm+"\">"+list_suplier[elm].nama_suplier+"</option>";
            // console.log(str_list);
        }

        $("#suplier").html(str_list);
    }
//========================================================================//
//-----------------------------------suplier-----------------------------//
//========================================================================//



//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//
    $("#brand").focus(function(){
    });

    $("#brand").keyup(function(){
        check_brand_in_master();

        set_item();
        create_list_item();
    });

    $("#brand").change(function(){
        check_brand_in_master();

        set_item();
        create_list_item();
    });

    function create_list_brand(){
        var str_list = "";
        // console.log(list_brand);
        var i = 0;
        for (let elm in list_brand) {
            str_list += "<option value=\""+elm+"\">"+list_brand[elm].nama_brand+"</option>";
            // console.log(str_list);
            if(i == 0){
                default_brand = elm;
            }

            i++;
        }

        $("#dl_brand").html(str_list);
    }

    function check_brand_in_master(){
        var brand = $("#brand").val();
        var status = true;
        if(!(brand in list_item)){
            status = false;

            $("#msg_brand").html("brand tidak ditemukan");
        }else {
            status = true;
            
            $("#msg_brand").html("");
        }

        return status;
    }
//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//
    $("#item").focus(function(){
    });

    $("#item").keyup(function(){
        check_item_in_master();

        set_item();
    });

    $("#item").change(function(){
        check_item_in_master();

        set_item();
    });


    $("#btn_item").click(function(){
        if(check_item_in_master()){
            console.log("item and brand good");
        }else {
            console.log("item and brand nothing");
        }
    });


    function set_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();
        // console.log(list_item[brand]);
        if(item in list_item[brand]){
            $("#msg_item").html("");
            set_value_item();
            jml_item_change();
            jml_disc_change();
            // console.log();
        }else {
            $("#msg_item").html("Item tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    }

    function clear_item(){
        $("#nama_item").val("");
        $("#harga_satuan").val("0");
        $("#harga_total").val("0");

        $("#val_harga_satuan").html("Rp. 0");
        $("#val_harga_total").html("Rp. 0");   
    }

    function set_value_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();

        var data = list_item[brand][item];
        // console.log(data);
        var harga_bruto = data.harga_bruto;
        var harga_jual = data.harga_jual;

        harga_jual_global = harga_jual;

        $("#nama_item").val(data.nama_item);
        $("#val_tgl_kadaluarsa").html(data.tgl_kadaluarsa_item);
        $("#val_kode_produksi").html(data.kode_produksi_item);

        $("#harga_satuan").val(harga_bruto);
        $("#harga_total").val("0");

        $("#val_harga_satuan").html("Rp. "+currency(parseFloat(harga_bruto)));
        $("#val_harga_total").html("Rp. 0");

        $("#val_harga_jual").html("Rp. "+currency(parseFloat(harga_jual)));    
    }

    function create_list_item(){
        var str_list = "";
        var brand = $("#brand").val();
        var array_item_list = list_item[brand];
        // console.log(array_item_list);

        var i = 0;
        for (let elm in array_item_list) {
            str_list += "<option value=\""+elm+"\">("+
                            array_item_list[elm].kode_produksi_item+") "+
                            array_item_list[elm].nama_item+" - "+
                            array_item_list[elm].stok+" ["+
                            array_item_list[elm].satuan+"]"+
                        "</option>";

            if(i == 0){
                default_item = elm;
            }

            i++;
        }

        $("#dl_item").html(str_list);
        // $("#item").val("");
    }


    function check_item_in_master(){
        var brand = $("#brand").val();
        var item = $("#item").val();
        
        var status = false;

        if(brand in list_item){
            if(!(item in list_item[brand])){
                $("#msg_item").html("brand tidak ditemukan");
                
                status = false;
            }else {
                $("#msg_item").html("");

                status = true;
            }
        }
        
        return status;
    }
//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//



//========================================================================//
//-----------------------------------harga_beli_satuan--------------------//
//========================================================================//
    function harga_satuan_change(){
        var harga_satuan = parseFloat($("#harga_satuan").val());
        $("#val_harga_satuan").html("Rp. "+currency(harga_satuan));
    }

    function check_range_harga_jual(){
        var harga_satuan = parseFloat($("#harga_satuan").val());
        
        if(harga_satuan >= harga_jual_global){
            $("#msg_val_harga_jual").html("harga beli melebihi harga jual \n <a href=\"<?php print_r(base_url());?>admin/data_item\">silahkan ubah disini</a>");
            return false;
        }else {            
            $("#msg_val_harga_jual").html("");
            return true;
        }
    }

    $("#harga_satuan").change(function(){
        harga_satuan_change();
        check_range_harga_jual();
        jml_disc_change();
    });

    $("#harga_satuan").keyup(function(){
        harga_satuan_change();
        check_range_harga_jual();
        jml_disc_change();
    });
//========================================================================//
//-----------------------------------harga_beli_satuan--------------------//
//========================================================================//



//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//
    function jml_item_change(){
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();

        var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        $("#harga_total").val(t_harga);

        $("#val_harga_total").html("Rp. "+currency(parseFloat(t_harga))); 
    }

    $("#jml_item").keyup(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });  

    $("#jml_item").change(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------disc_item----------------------------//
//========================================================================//
    function jml_disc_change(){
        var harga_satuan= $("#harga_satuan").val();
        var jml_item    = $("#jml_item").val();
        var disc_item   = $("#disc_item").val();

        if(harga_satuan && jml_item && disc_item){
            var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);

            var harga_after_disc = t_harga - (t_harga * disc_item / 100);

            $("#val_harga_after_disc").html("Rp. "+currency(parseFloat(harga_after_disc))); 
        }else {
            $("#val_harga_after_disc").html("Rp. 0");
        }
    }

    $("#disc_item").keyup(function(){
        var disc_item = $("#disc_item").val();
        if(disc_item){
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });  

    $("#disc_item").change(function(){
        var disc_item = $("#disc_item").val();
        if(disc_item){
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------disc_item----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------disc---------------------------------//
//========================================================================//
    function disc_all_change() {
        var disc_all = $("#disc").val();
        var t_harga = 0;
        var pajak   = 10;

        for (let i in detail_product) {
            t_harga += parseFloat(detail_product[i].harga_after_disc);
        }

        var t_after_disc = t_harga - (t_harga * disc_all / 100);
        var t_after_pajak = t_after_disc + (t_after_disc * pajak / 100);

        $("#out_tr_disc_title").html("Harga Setelah Diskon "+disc_all+"%");
        $("#out_tr_disc").html("Rp. "+currency(t_after_disc));
        $("#out_tr_after_pajak").html("Rp. "+currency(t_after_pajak));
        
    }

    $("#disc").keyup(function(){
        disc_all_change();
    });  

    $("#disc").change(function(){
        disc_all_change();
    });
//========================================================================//
//-----------------------------------disc---------------------------------//
//========================================================================//



//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//
    $("#add_main").click(function(){
        // console.log(check_form_suplier());
        // if(check_form_suplier()){
            $("#id_suplier").attr("readonly", true);
            $("#suplier").attr("disabled", true);
            $("#tgl_transaksi").attr("readonly", true);
            // $("#disc").attr("readonly", true);
            

            $("#brand").focus();
        // }
    });

    $("#reset_main").click(function(){
        $("#suplier").removeAttr("disabled", true);
        // $("#disc").removeAttr("readonly", true);
        

        $("#suplier").focus();


        $("#modal_insert_detail").modal("hide");
    });
//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------cara_bayar---------------------------//
//========================================================================//
    $("#cara_pembayaran").change(function(){
        select_cara_bayar();
        // set_tgl_tempo();
        // set_tgl_tempo();
    });

    function set_tgl_tempo(){
        var tgl_transaksi = $("#tgl_transaksi").val().split("-");
        var date_transaksi = new Date(tgl_transaksi);
        var new_date_transaksi = new Date(date_transaksi);

        new_date_transaksi.setDate(new_date_transaksi.getDate() + 40);

        var d = new_date_transaksi.getDate() + 0;
        var m = new_date_transaksi.getMonth() + 1;
        var y = new_date_transaksi.getFullYear();

        var fix_d = "-";
        if(d < 10){
            fix_d += "0"+d;
        }else {
            fix_d += d;
        }

        var fix_m = "-";
        if(m < 10){
            fix_m += "0"+m;
        }else {
            fix_m += m;
        }

        console.log(fix_d);
        console.log(fix_m);
        console.log(y);

        var tgl_tempo = y+fix_m+fix_d;

        // console.log(tgl_tempo);

        // console.log(tgl_tempo);
        
        $("#tempo").val(tgl_tempo);
    }

    function select_cara_bayar(){
        var cara_pembayaran = $("#cara_pembayaran").val();
        if(cara_pembayaran == "0"){
            // $("#tempo").val("");
            $("#tempo").attr("readonly", true);
        }else if(cara_pembayaran == "1"){
            $("#tempo").removeAttr("readonly", true);
        }
    }
//========================================================================//
//-----------------------------------cara_bayar---------------------------//
//========================================================================//



//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//
    
    $("#add_detail").click(function(){
        add_detail_penjualan();
    });

    function add_detail_penjualan(){
        var id_suplier      = $("#id_suplier").val();
        var suplier         = $("#suplier").val();
        var tgl_transaksi   = $("#tgl_transaksi").val();
        // var disc            = $("#disc").val();

        var nama_item       = $("#nama_item").val();
        var brand           = $("#brand").val();
        var item            = $("#item").val();
        var harga_satuan    = $("#harga_satuan").val();
        var jml_item        = $("#jml_item").val();
        var disc_item       = $("#disc_item").val();
        var harga_total     = $("#harga_total").val();
        // var kode_produksi   = $("#kode_produksi").val();
        // var tgl_kadaluarsa  = $("#tgl_kadaluarsa").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();
        
        var t_harga = 0;
        var harga_after_disc = 0;

        if(harga_satuan && jml_item && disc_item){
            t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
            harga_after_disc = t_harga - (t_harga * disc_item / 100);
        }


        if(check_range_harga_jual()){
            if(check_value()){
                var tmp_list = {  
                    "id_item"       : item,
                    "nama_item"     : nama_item,
                    "kode_produksi" : list_item[brand][item].kode_produksi_item,
                    "tgl_kadaluarsa": list_item[brand][item].tgl_kadaluarsa_item,
                    "brand"         : brand,
                    "item"          : item,
                    "harga_satuan"  : harga_satuan,
                    "jml_item"      : jml_item,
                    "disc_item"     : disc_item,
                    "harga_total"   : t_harga,
                    "harga_after_disc"   : harga_after_disc,
                    // "kode_produksi" : kode_produksi,
                    // "tgl_kadaluarsa": tgl_kadaluarsa
                    
                };

                if(!(item in detail_product)){
                    
                    detail_product[item] = tmp_list;
                    // console.log(detail_product);
                   
                    clear_detail();
                    render_tbl_detail_product();

                    $("#modal_insert_detail").modal("hide");

                    create_alert('Proses Berhasil', 'Data detail tersimpan', 'success');
                }else{
                    create_alert('Proses Gagal', 'item sudah ada di detail penjualan', 'error');
                    // console.log("");
                }
            }else{
                // console.log("check_value false");
                create_alert('Proses Gagal', 'Input data tidak lengkap periksa kembali input lagi', 'error');
                    
            }
        }else {
            create_alert('Proses Gagal', 'Harga beli lebih besar dari harga jual', 'error');
        }
        
    }

    function render_tbl_detail_product(){
        var str_tbl = "";
        var no = 1;

        var disc    = $("#disc").val();
        var t_harga = 0;
        var pajak   = 10;

        console.log(detail_product);

        for (let i in detail_product) {
            str_tbl += "<tr>"+
                            "<td align=\"right\">"+no+"</td>"+
                            "<td>("+detail_product[i].item+") "+
                            list_item[detail_product[i].brand][detail_product[i].item].nama_item+"</td>"+

                            "<td>"+detail_product[i].kode_produksi+"</td>"+
                            "<td>"+detail_product[i].tgl_kadaluarsa+"</td>"+

                            "<td align=\"right\">"+currency(parseFloat(detail_product[i].jml_item))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_satuan))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_total))+"</td>"+
                            "<td align=\"right\">"+currency(detail_product[i].disc_item)+"%</td>"+
                            "<td align=\"right\">"+currency(detail_product[i].harga_after_disc)+"</td>"+
                            "<td>"+
                                "<center>"+
                                // "<button class=\"btn btn-info\" id=\"up_detail\" onclick=\"update_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                                "<button class=\"btn btn-danger\" id=\"del_detail\" onclick=\"delete_detail('"+detail_product[i].item+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                                "</center>"+
                            "</td>"+
                        "</tr>";
            t_harga += parseFloat(detail_product[i].harga_after_disc);
            console.log(i);
            no++;
        }

        var t_after_disc = t_harga - (t_harga * disc / 100);
        var t_after_pajak = t_after_disc + (t_after_disc * pajak / 100);
        console.log(t_after_disc);
        console.log(t_after_pajak);
        str_tbl += "<tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Total</b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_t_harga\">Rp. "+currency(t_harga)+"</b></td>"+
                        "<td></td>"+
                    "</tr><tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\" id=\"out_tr_disc_title\">Harga Setelah Diskon "+disc+"% </b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_disc\">Rp. "+currency(t_after_disc)+"</b></td>"+
                        "<td></td>"+
                    "</tr><tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Setelah Pajak "+pajak+"% </b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_after_pajak\">Rp. "+currency(t_after_pajak)+"</b></td>"+
                        "<td></td>"+
                    "</tr>";
        $("#tbl_list_detail").html(str_tbl);
    }

    function check_value(){
        var id_suplier = $("#id_suplier").val();
        var suplier = $("#suplier").val();
        var tgl_transaksi = $("#tgl_transaksi").val();
        var disc = $("#disc").val();

        var nama_item = $("#nama_item").val();
        var brand = $("#brand").val();
        var item = $("#item").val();
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();
        var disc_item  = $("#disc_item").val();
        var harga_total = $("#harga_total").val();
        // var kode_produksi = $("#kode_produksi").val();
        // var tgl_kadaluarsa = $("#tgl_kadaluarsa").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();

        var disc_item = $("#disc_item").val();

        var status = false;

        if(!nama_item 
            || !suplier || !tgl_transaksi
            || !brand 
            || !item || !harga_satuan || !jml_item
            || !harga_total
            // || !kode_produksi 
            // || !tgl_kadaluarsa 
            || !disc_item || !check_item_in_master()
            // || !cara_pembayaran || !tempo
            ){
            console.log("kosong");
        }else {
            if(parseInt(jml_item) > 0){
                status = true;
                console.log("isi");
            }
        }

        return status;
    }
    

    $("#reset_detail").click(function(){
        $("#modal_insert_detail").modal("hide");
        clear_detail();
    });

    function clear_detail(){
        nama_item = $("#nama_item").val("");
        item = $("#item").val(default_item);
        brand = $("#brand").val(default_brand);

        harga_satuan = $("#harga_satuan").val("");
        jml_item = $("#jml_item").val("0");
        harga_total = $("#harga_total").val("");
        // kode_produksi = $("#kode_produksi").val("");
        // tgl_kadaluarsa = $("#tgl_kadaluarsa").val("");
        disc_item = $("#disc_item").val("0");
        // var cara_pembayaran = $("#cara_pembayaran").val("0");
        // var tempo = $("#tempo").val("");

        $("#val_kode_produksi").html("");
        $("#val_tgl_kadaluarsa").html("");

        val_harga_satuan    = $("#val_harga_satuan").html("Rp. 0");
        val_harga_total     = $("#val_harga_total").html("Rp. 0");
        val_harga_after_disc= $("#val_harga_after_disc").html("Rp. 0");

        $("#brand").focus();

        set_item();
        set_value_item();
        jml_item_change();
    }

    function delete_detail(id_item){
        create_alert('Proses Berhasil', detail_product[id_item].nama_item+' di hapus dari detail penjualan', 'success');
        delete detail_product[id_item];
        render_tbl_detail_product();
    }

    $("#finis_detail").click(function(){
        $("#disc").focus();
    });
//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//
    $("#lanjut").click(function(){
        if(validation_main_check()){

            var data_main = new FormData();
            data_main.append('id_tr_header'  , "<?php print_r($id_tr_header);?>");
            data_main.append('id_suplier'  , $("#id_suplier").val());
            data_main.append('suplier'     , $("#suplier").val());
            data_main.append('tgl_transaksi', $("#tgl_transaksi").val());


            data_main.append('disc'         , $("#disc").val());
            data_main.append('cara_pembayaran'  , $("#cara_pembayaran").val());
            data_main.append('tempo'        , $("#tempo").val());

            data_main.append('list_detail_pembelian', JSON.stringify(detail_product));

            $.ajax({
                url: "<?php echo base_url()."admin/pembelianmain/update_pembelian_header/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_req(res);
                }
            });
        }else{
            create_alert('Proses Gagal', 'Periksa Kembali input saudara', 'error');
        }
    });

    $("#balik").click(function(){
        $("#disc").focus();
    });

    function validation_main_check(){
        var status = false;

        var suplier    = $("#suplier").val();
        var tgl_transaksi = $("#tgl_transaksi").val();

        var disc        = $("#disc").val();
        var cara_pembayaran = $("#cara_pembayaran").val();
        var tempo       = $("#tempo").val();

        if(!suplier || !tgl_transaksi 
            || !disc || !cara_pembayaran || !tempo || jQuery.isEmptyObject(detail_product)
            ){
            console.log("kosong");
        }else {
            status = true;
            console.log("isi");
        }

        return status;
    }


    function response_req(res){
        var res_data = JSON.parse(res);
        console.log(res_data);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning");
        }
    }

    function create_sweet_alert(title, msg, status) {
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            if(status == "success"){
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Lanjutkan",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            window.location.href = "<?php print_r(base_url());?>admin/list_pembelian";  
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }else {
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Perbaiki",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            swal.close();
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }
            
            
            //init
            
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
        
    }
//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//

</script>