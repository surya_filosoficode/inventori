<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Setting Main</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
     <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Data Setting Main</h4>

                </div>
                <div class="card-body">
                    <div class="row">
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Setting<span style="color: red;">*</span></label>
                                <select class="form-control" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="jenis_setting" name="jenis_setting">
                                    <option value="npwp" selected="">Nomor NPWP</option>
                                    <option value="siup">Nomor IJIN SIUP</option>
                                </select>
                                <p id="msg_jenis_setting" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Keterangan Setting<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="keterangan_setting" name="keterangan_setting" placeholder="Keterangan Setting" required="">
                                <p id="msg_keterangan_setting" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="submit" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                <button type="button" id="reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div> -->
                        <div class="col-md-12">
                            <hr>    
                        </div>
                        <div class="col-md-12" id="out">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">No</th>
                                            <th width="30%">Jenis Setting</th>
                                            <th width="30%">Nilai Setting</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id=\"main_table_content\">
                                        <?php
                                            if($list_data){
                                                $no = 1;
                                                foreach ($list_data as $key => $value) {
                                                    $jenis_setting = "Nomor NPWP";
                                                    if($value->jenis_setting == "siup"){
                                                        $jenis_setting = "Nomor SIUP";
                                                    }

                                                    $str_btn_action = 
                                                    "<center>".
                                                        "<button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_setting."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;".
                                                        // "<button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_setting."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>".
                                                    "</center>";
                                                    print_r("<tr>
                                                                <td>".$no."</td>
                                                                <td>".$jenis_setting."</td>
                                                                <td>".$value->keterangan_setting."</td>
                                                                <td>".$str_btn_action."</td>
                                                            </tr>");
                                                    $no++;
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <!-- <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label> -->
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Setting Main</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Jenis Setting<span style="color: red;">*</span></label>
                            <select class="form-control" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="_jenis_setting" name="_jenis_setting">
                                <option value="npwp" selected="">Nomor NPWP</option>
                                <option value="siup">Nomor IJIN SIUP</option>
                            </select>
                            <p id="_msg_jenis_setting" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Keterangan Setting<span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_keterangan_setting" name="_keterangan_setting" placeholder="Keterangan Setting" required="">
                            <p id="_msg_keterangan_setting" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah</button>
                        <button type="button" id="btn_update_reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->



<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_global; 

    $(document).ready(function(){
       clear_form();
    });

    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//
        function currency(x){
            return x.toLocaleString('us-EG');
        }

        function clear_form(){
            $("#jenis_setting").val("npwp");
            $("#keterangan_setting").val("");
            
            $("#msg_jenis_setting").html("");
            $("#msg_keterangan_setting").html("");
            
            $("#jenis_setting").focus();
        }

        function clear_form_update(){
            $("#_jenis_setting").val("npwp");
            $("#_keterangan_setting").val("");
            
            $("#_msg_jenis_setting").html("");
            $("#_msg_keterangan_setting").html("");
            
            $("#_jenis_setting").focus();
        }

        function change_currency(val_number){
            var str_return;
            if(val_number != ""){
                str_return = "Rp. "+currency(parseFloat(val_number));
            }else{
                str_return = "Rp. 0";
            }

            return str_return;
        }


        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function render_data(data){
            $("#out").html(data);
        }
    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//
        $("#add_data").click(function(){
            var data_main = new FormData();
                data_main.append('jenis_setting'        , $("#jenis_setting").val());
                data_main.append('keterangan_setting'   , $("#keterangan_setting").val());
                
            $.ajax({
                url: "<?php echo base_url()."admin/settingmain/insert_setting";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_insert(res.responseText);
                }
            });
        });

        $("#reset").click(function(){
            clear_form();
        });


        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form();
            } else {
                
                $("#msg_jenis_setting").html(detail_msg.jenis_setting);
                $("#msg_keterangan_setting").html(detail_msg.keterangan_setting);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//
        function update_data(id_data){
            var data_main = new FormData();
                data_main.append('id_setting', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/settingmain/get_data_setting";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_set_update(res.responseText);

                    id_global = id_data;
                }
            });
        }

        function response_set_update(res){
            var data_json = JSON.parse(res);
            // console.log(data_json);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;

            if (main_msg.status) {
                $("#update_data").modal("show");

                $("#_jenis_setting").val(detail_msg.list_data.jenis_setting);
                $("#_keterangan_setting").val(detail_msg.list_data.keterangan_setting);
                
                $("#_jenis_setting").focus();

            } else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function(){
            var data_main = new FormData();
                data_main.append('id_setting'         , id_global);

                data_main.append('jenis_setting'     , $("#_jenis_setting").val());
                data_main.append('keterangan_setting'    , $("#_keterangan_setting").val());
                

            $.ajax({
                url: "<?php echo base_url()."admin/settingmain/update_setting";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_update(res.responseText);
                }
            });
        });

        $("#btn_update_reset").click(function(){
            clear_form_update();
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $("#update_data").modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_jenis_setting").html(detail_msg.jenis_setting);
                $("#_msg_keterangan_setting").html(detail_msg.keterangan_setting);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//        

    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//
        function delete_data(id_data){
            var data_main = new FormData();
                data_main.append('id_setting', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/settingmain/delete_setting";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_delete(res.responseText);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//   
    
</script>