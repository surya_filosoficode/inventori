<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- <div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Penjualan</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div> -->
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Data Penjualan</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="btn btn-default" style="height: 35px;">List Penjualan</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Kode Customer</b></label>
                                <input type="text" class="form-control" id="id_customer" name="id_customer" value="90" readonly=""/>
                                <p id="msg_id_customer" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-8">
                            <div class="form-group">
                                <label><b>Customer</b></label>
                                <input type="text" class="form-control" id="customer" name="customer" placeholder="Cari customer.." list="list_customer"/>
                                <p id="msg_customer" style="color: red;"></p>
                                <datalist id="list_customer" class="search-listing">
                                    <option id="0912" value="dimas">
                                </datalist>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Tanggal Transaksi</b></label>
                                <input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php print_r(date("Y-m-d"));?>" readonly="" />
                                <p id="msg_tgl_transaksi" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label><b>Sales</b></label>
                                <select class="form-control" id="sales" name="sales">
                                    <option value="50">50%</option>
                                </select>
                                <p id="msg_sales" style="color: red;"></p>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group text-right">
                                <label><b>&nbsp;</b></label><br>
                                <button type="button" id="add_main" name="add_main" class="btn waves-effect waves-light btn-rounded btn-info">Buat Detail</button>
                                <button type="button" id="reset_main" name="reset_main" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <!-- <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Kode Item</b></label>
                                <input type="text" class="form-control" id="id_item" name="id_item" readonly=""/>
                                <p id="msg_id_item" style="color: red;"></p>
                            </div>
                        </div> -->
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Brand</b></label>
                                <!-- <input type="text" class="form-control" id="brand" name="brand" placeholder="Cari brand.." list="list_brand"/> -->
                                <input type="text" class="form-control" id="brand" name="brand" list="dl_brand">
                                <!-- <select class="form-control" id="brand" name="brand">
                                    
                                </select> -->
                                <datalist id="dl_brand">
                                    
                                </datalist>
                                <p id="msg_brand" style="color: red;"></p>
                                <!-- <datalist id="list_brand" class="search-listing">
                                </datalist> -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Item</b></label>
                                <!-- <input type="text" class="form-control" id="item" name="item" placeholder="Cari item.." list="list_item"/> -->
                                <input type="text" class="form-control" name="item" id="item" placeholder="Cari item.." list="dl_item">

                                <button type="button" id="btn_item" name="btn_item">cek item</button>
                                <datalist id="dl_item">
                                    
                                </datalist>

                                <p id="msg_item" style="color: red;"></p>
                            </div>
                        </div>


                         <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Harga Satuan</b></label>
                                <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" readonly="" value="0" />
                                <label><b id="val_harga_satuan">Harga Satuan</b></label>
                                <p id="msg_harga_satuan" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Kode Produksi</b></label>
                                <input type="text" class="form-control" id="kode_produksi" name="kode_produksi" />
                                <p id="msg_kode_produksi" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Tanggal Kadaluarsa</b></label>
                                <input type="date" class="form-control" id="tgl_kadaluarsa" name="tgl_kadaluarsa" />
                                <p id="msg_tgl_kadaluarsa" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Jumlah Item</b></label>
                                <input type="number" class="form-control" id="jml_item" name="jml_item" value="0"/>
                                <p id="msg_jml_item" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Harga Total</b></label>
                                <input type="text" class="form-control" id="harga_total" name="harga_total" readonly="" value="0" />
                                <label><b id="val_harga_total">Harga Total</b></label>
                                <p id="msg_harga_total" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label><b>Diskon per Item</b></label>
                                <input type="number" class="form-control" id="disc_item" name="disc_item" />
                                <label>
                                    <b>Harga Setelah Diskon :</b>
                                    <b id="val_harga_after_disc">100</b>
                                </label>
                                <p id="msg_harga_total" style="color: red;"></p>
                            </div>
                        </div>
                        
                        


                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="button" id="check_item" name="check_item" class="btn waves-effect waves-light btn-rounded btn-info">check_item</button>

                                <button type="button" id="add_detail" name="add_detail" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                
                                <button type="button" id="reset_detail" name="reset_detail" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>

                                <button type="button" id="finis_detail" name="finis_detail" class="btn waves-effect waves-light btn-rounded btn-success">Akhiri Transaksi</button>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No. </th>
                                            <th>Produk</th>
                                            <th class="text-right" width="20%">Jumlah Produk</th>
                                            <th class="text-right" width="15%">Harga Satuan</th>
                                            <th class="text-right" width="20%">Total Harga</th>
                                            <th class="text-right" width="10%">Diskon</th>
                                            <th class="text-right" width="20%">Harga Setelah Diskon</th>
                                            <th class="text-right" width="5%">Proses</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_list_detail">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8"><hr></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Diskon</b></label>
                                    <input type="number" class="form-control" id="disc" name="disc" step='0.01' />
                                    <p id="msg_disc" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Cara Pembayaran</b></label>
                                    <select class="form-control" id="cara_pembayaran" name="cara_pembayaran">
                                        <option value="0">Tunai</option>
                                        <option value="1">Kredit</option>
                                    </select>
                                    <p id="msg_cara_pembayaran" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><b>Tanggal Jatuh Tempo</b></label>
                                    <input type="date" class="form-control" id="tempo" name="tempo"/>
                                    <p id="msg_tempo" style="color: red;"></p>
                                </div>
                            </div>
                                            </td>
                                            <td colspan="2">
                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <label><b>&nbsp;</b></label><br>
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Lanjutkan</button>
                                    <button type="button" id="balik" name="balik" class="btn waves-effect waves-light btn-rounded btn-danger">Kembali</button>
                                </div>
                            </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>

<!-- ============================================================== -->
<!-- --------------------------update_detail----------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_detail" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Detail Penjualan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Kode Item</b></label>
                            <input type="text" class="form-control" id="_id_item" name="id_item" readonly=""/>
                            <p id="_msg_id_item" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Brand</b></label>
                            <select class="form-control" id="_brand" name="brand">
                                
                            </select>
                            <p id="_msg_brand" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Item</b></label>
                            <input type="text" class="form-control" id="_item" name="item" placeholder="Cari item.." list="_list_item"/>

                            <p id="_msg_item" style="color: red;"></p>
                            <datalist id="_list_item" class="search-listing">
                            </datalist>
                        </div>
                    </div>
                    

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Harga Satuan</b></label>
                            <input type="text" class="form-control" id="_harga_satuan" name="harga_satuan" readonly="" value="0" />
                            <label><b id="_val_harga_satuan">Harga Satuan</b></label>
                            <p id="_msg_harga_satuan" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Jumlah Item</b></label>
                            <input type="number" class="form-control" id="_jml_item" name="jml_item" value="0"/>
                            <p id="_msg_jml_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label><b>Harga Total</b></label>
                            <input type="text" class="form-control" id="_harga_total" name="harga_total" readonly="" value="0" />
                            <label><b id="_val_harga_total">Harga Total</b></label>
                            <p id="_msg_harga_total" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kode Produksi</b></label>
                            <input type="text" class="form-control" id="_kode_produksi" name="kode_produksi" />
                            <p id="_msg_kode_produksi" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tanggal Kadaluarsa</b></label>
                            <input type="date" class="form-control" id="_tgl_kadaluarsa" name="tgl_kadaluarsa" />
                            <p id="_msg_tgl_kadaluarsa" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Cara Pembayaran</b></label>
                            <select class="form-control" id="_cara_pembayaran" name="cara_pembayaran">
                                <option value="0">Tunai</option>
                                <option value="1">Kredit</option>
                            </select>
                            <p id="_msg_cara_pembayaran" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tanggal Jatuh Tempo</b></label>
                            <input type="date" class="form-control" id="_tempo" name="tempo"/>
                            <p id="_msg_tempo" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_detail" class="btn btn-success">Ubah Data</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_detail----------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var list_customer = JSON.parse('<?php print_r($list_customer);?>');
    var list_item = JSON.parse('<?php print_r($list_item);?>');
    var list_brand = JSON.parse('<?php print_r($list_brand);?>');

    $(document).ready(function(){
        create_list_customer();
        
        create_list_brand();
        create_list_item();
        // set_item();
        $("#customer").focus();

        select_cara_bayar();

        $("#tempo").val("2019-10-22");
    });

    

    function currency(x){
        return x.toLocaleString('us-EG');
    }


//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//
    $("#customer").focus(function(){
    });

    $("#customer").keyup(function(){
        var customer = $("#customer").val();
        if(jQuery.inArray(customer, list_customer) !== -1){
            $("#msg_customer").html("");
        }else {
            $("#msg_customer").html("Customer tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    });

    function create_list_customer(){
        var str_list = "";
        for (let elm in list_customer) {
            str_list += "<option value=\""+list_customer[elm]+"\">";
        }

        $("#list_customer").html(str_list);
    }
//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//
    $("#brand").focus(function(){
    });

    
    $("#brand").change(function(){
        clear_item();
        create_list_item();
    });

    function create_list_brand(){
        var str_list = "";
        for (let elm in list_brand) {
            // console.log(elm);
            str_list += "<option value=\""+list_brand[elm].id_brand+"\">"+list_brand[elm].nama_brand+"</option>";
        }

        $("#brand").html(str_list);
    }
//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//
    $("#item").focus(function(){
    });

    $("#item").keyup(function(){
        set_item();
    });

    function set_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();
        // console.log(list_item[brand]);
        // console.log(brand);
        // console.log(item);

        // console.log(list_item[brand][item]);
        if(item in list_item[brand]){
            $("#msg_item").html("");
            set_value_item(list_item[brand][item])
            // console.log();
        }else {
            $("#msg_item").html("Item tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    }

    function clear_item(){
        $("#id_item").val("");
        $("#harga_satuan").val("0");
        $("#harga_total").val("0");

        $("#val_harga_satuan").html("Rp. 0");
        $("#val_harga_total").html("Rp. 0");   
    }

    function set_value_item(data){
        console.log(data);
        $("#id_item").val(data.id_item);
        $("#harga_satuan").val(data.harga_jual);
        $("#harga_total").val("0");

        $("#val_harga_satuan").html("Rp. "+currency(parseFloat(data.harga_jual)));
        $("#val_harga_total").html("Rp. 0"); 
    }

    function create_list_item(){
        var str_list = "";
        var brand = $("#brand").val();
        var array_item_list = list_item[brand];
        console.log(array_item_list);
        for (let elm in array_item_list) {
            str_list += "<option value=\""+elm+"\">";
        }

        $("#list_item").html(str_list);
        $("#item").val("");
    }
//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//
    function jml_item_change(){
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();

        var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        $("#harga_total").val(t_harga);

        $("#val_harga_total").html("Rp. "+currency(parseFloat(t_harga))); 
    }

    $("#jml_item").keyup(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
        }else {
            console.log("tidak boleh null");
        }
    });  

    $("#jml_item").change(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------disc_item----------------------------//
//========================================================================//
    function jml_disc_change(){
        var harga_satuan= $("#harga_satuan").val();
        var jml_item    = $("#jml_item").val();
        var disc_item   = $("#disc_item").val();

        if(harga_satuan && jml_item && disc_item){
            var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);

            var harga_after_disc = t_harga - (t_harga * disc_item / 100);

            $("#val_harga_after_disc").html("Rp. "+currency(parseFloat(harga_after_disc))); 
        }else {
            $("#val_harga_after_disc").html("Rp. 0");
        }
    }

    $("#disc_item").keyup(function(){
        var disc_item = $("#disc_item").val();
        if(disc_item){
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });  

    $("#disc_item").change(function(){
        var disc_item = $("#disc_item").val();
        if(disc_item){
            jml_disc_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------disc_item----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------disc---------------------------------//
//========================================================================//
    function disc_all_change() {
        var disc_all = $("#disc").val();
        var t_harga = 0;
        var pajak   = 10;

        for (let i in detail_product) {
            t_harga += parseFloat(detail_product[i].harga_total);
        }

        var t_after_disc = t_harga - (t_harga * disc_all / 100);
        var t_after_pajak = t_after_disc + (t_after_disc * pajak / 100);

        $("#out_tr_disc_title").html("Harga Setelah Diskon "+disc_all+"%");
        $("#out_tr_disc").html("Rp. "+currency(t_after_disc));
        $("#out_tr_after_pajak").html("Rp. "+currency(t_after_pajak));
        
    }

    $("#disc").keyup(function(){
        disc_all_change();
    });  

    $("#disc").change(function(){
        disc_all_change();
    });
//========================================================================//
//-----------------------------------disc---------------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//
    // var res_json ;
    function check_stok_item(){
        var data_main = new FormData();
        data_main.append('id_item'      , $("#id_item").val());
        data_main.append('jml_item'     , $("#jml_item").val());

        $.ajax({
            url: "<?php echo base_url()."admin/penjualanmain/check_stok_item/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                
            }
            // ,complete: function(res){
            //     // console.log(res.responseText);
            //     var return_sts = response_check_stok_item(res.responseText);
            //     // return return_sts;
            //     console.log(return_sts);
            // }
        });
    }

    $(document).ajaxComplete(function(event, xhr, settings){
        console.log("ok");
    });

    $("#check_item").click(function(){
        // console.log(check_stok_item());
        check_stok_item();
    });

    function response_check_stok_item(res){
        var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
               
        return main_msg.status;
    }
//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//


//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//
    $("#add_main").click(function(){
        console.log(check_form_customer());
        if(check_form_customer()){
            $("#id_customer").attr("readonly", true);
            $("#customer").attr("readonly", true);
            $("#tgl_transaksi").attr("readonly", true);
            // $("#disc").attr("readonly", true);
            $("#sales").attr("readonly", true);

            $("#brand").focus();
        }
    });

    function check_form_customer(){
        var sts_return = false;
        var customer = $("#customer").val();

        if(jQuery.inArray(customer, list_customer) !== -1){
            sts_return = true;
        }
        return sts_return;
    }

    $("#reset_main").click(function(){
        $("#customer").removeAttr("readonly", true);
        // $("#disc").removeAttr("readonly", true);
        $("#sales").removeAttr("readonly", true);

        $("#customer").focus();
    });
//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    

//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//
    var detail_product = {};
    var list_item_choose = [];

    function check_form_brand(){
        var sts_return = false;
        var brand = $("#brand").val();

        if(brand in list_brand){
            sts_return = true;
        }
        return sts_return;
    }

    function check_form_item(){
        var sts_return = false;
        var brand = $("#brand").val();
        var item = $("#item").val();
        if(item in list_item[brand]){
            sts_return = true;
        }
        return sts_return;
    }

    $("#add_detail").click(function(){
        
    });

    function add_detail_penjualan(){
        var id_customer     = $("#id_customer").val();
        var customer        = $("#customer").val();
        var tgl_transaksi   = $("#tgl_transaksi").val();
        // var disc            = $("#disc").val();
        var sales           = $("#sales").val();

        var id_item         = $("#id_item").val();
        var brand           = $("#brand").val();
        var item            = $("#item").val();
        var harga_satuan    = $("#harga_satuan").val();
        var jml_item        = $("#jml_item").val();
        var disc_item       = $("#disc_item").val();
        var harga_total     = $("#harga_total").val();
        var kode_produksi   = $("#kode_produksi").val();
        var tgl_kadaluarsa  = $("#tgl_kadaluarsa").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();
        
        var t_harga = 0;
        var harga_after_disc = 0;

        if(harga_satuan && jml_item && disc_item){
            t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
            harga_after_disc = t_harga - (t_harga * disc_item / 100);
        }

        var tmp_list = {
            // "id_customer"   : id_customer,
            // "customer"      : customer,
            // "tgl_transaksi" : tgl_transaksi,
            // "disc"          : disc,
            // "sales"         : sales,

            "id_item"       : id_item,
            "brand"         : brand,
            "item"          : item,
            "harga_satuan"  : harga_satuan,
            "jml_item"      : jml_item,
            "disc_item"     : disc_item,
            "harga_total"   : t_harga,
            "harga_after_disc"   : harga_after_disc,
            "kode_produksi" : kode_produksi,
            "tgl_kadaluarsa": tgl_kadaluarsa
            // "cara_pembayaran": cara_pembayaran,
            // "tempo"         : tempo
        };

        if(check_value()){
            if(!(id_item in detail_product)){
                
                detail_product[id_item] = tmp_list;
                console.log(detail_product);
               
                clear_detail();

                render_tbl_detail_product();
            }else{
                console.log("item sudah ada");
            }
        }
    }

    function render_tbl_detail_product(){
        var str_tbl = "";
        var no = 1;

        var disc    = $("#disc").val();
        var t_harga = 0;
        var pajak   = 10;

        for (let i in detail_product) {
            str_tbl += "<tr>"+
                            "<td align=\"right\">"+no+"</td>"+
                            "<td>("+detail_product[i].id_item+") "+detail_product[i].item+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(detail_product[i].jml_item))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_satuan))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_total))+"</td>"+
                            "<td align=\"right\">"+currency(detail_product[i].disc_item)+"%</td>"+
                            "<td align=\"right\">"+currency(detail_product[i].harga_after_disc)+"</td>"+
                            "<td>"+
                                "<center>"+
                                // "<button class=\"btn btn-info\" id=\"up_detail\" onclick=\"update_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                                "<button class=\"btn btn-danger\" id=\"del_detail\" onclick=\"delete_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                                "</center>"+
                            "</td>"+
                        "</tr>";
            t_harga += parseFloat(detail_product[i].harga_total);
            console.log(i);
            no++;
        }

        var t_after_disc = t_harga - (t_harga * disc / 100);
        var t_after_pajak = t_after_disc + (t_after_disc * pajak / 100);
        console.log(t_after_disc);
        console.log(t_after_pajak);
        str_tbl += "<tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Total</b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_t_harga\">Rp. "+currency(t_harga)+"</b></td>"+
                        "<td></td>"+
                    "</tr><tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\" id=\"out_tr_disc_title\">Harga Setelah Diskon "+disc+"% </b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_disc\">Rp. "+currency(t_after_disc)+"</b></td>"+
                        "<td></td>"+
                    "</tr><tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Setelah Pajak "+pajak+"% </b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_after_pajak\">Rp. "+currency(t_after_pajak)+"</b></td>"+
                        "<td></td>"+
                    "</tr>";
        $("#tbl_list_detail").html(str_tbl);
    }

    function check_value(){
        var id_customer = $("#id_customer").val();
        var customer = $("#customer").val();
        var tgl_transaksi = $("#tgl_transaksi").val();
        var disc = $("#disc").val();
        var sales = $("#sales").val();

        var id_item = $("#id_item").val();
        var brand = $("#brand").val();
        var item = $("#item").val();
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();
        var disc_item  = $("#disc_item").val();
        var harga_total = $("#harga_total").val();
        var kode_produksi = $("#kode_produksi").val();
        var tgl_kadaluarsa = $("#tgl_kadaluarsa").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();

        var disc_item = $("#disc_item").val();

        var status = false;

        if(!id_item 
            || !customer || !customer || !tgl_transaksi || !sales
            || !id_item || !brand || !item || !harga_satuan || !jml_item
            || !harga_total || !kode_produksi || !tgl_kadaluarsa || !disc_item
            // || !cara_pembayaran || !tempo
            ){
            console.log("kosong");
        }else {
            status = true;
            console.log("isi");
        }

        return status;
    }
    

    $("#reset_detail").click(function(){
        clear_detail();
    });

    function clear_detail(){
        var id_item = $("#id_item").val("");
        var item = $("#item").val("");
        var brand = $("#brand").val("1");
        var harga_satuan = $("#harga_satuan").val("");
        var jml_item = $("#jml_item").val("");
        var harga_total = $("#harga_total").val("");
        var kode_produksi = $("#kode_produksi").val("");
        var tgl_kadaluarsa = $("#tgl_kadaluarsa").val("");
        var disc_item = $("#disc_item").val("0");
        // var cara_pembayaran = $("#cara_pembayaran").val("0");
        // var tempo = $("#tempo").val("");

        var val_harga_satuan    = $("#val_harga_satuan").html("Rp. 0");
        var val_harga_total     = $("#val_harga_total").html("Rp. 0");
        var val_harga_after_disc= $("#val_harga_after_disc").html("Rp. 0");

        $("#brand").focus();
    }

    function delete_detail(id_item){
        delete detail_product[id_item];
        render_tbl_detail_product();
    }

    function update_detail(id_item){
        $("#update_detail").modal("show");
    }

    $("#finis_detail").click(function(){
        $("#disc").focus();
    });
//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//
    $("#lanjut").click(function(){
        if(check_form_customer() &&
            detail_product){

            var data_main = new FormData();
            data_main.append('id_customer'  , $("#id_customer").val());
            data_main.append('customer'     , $("#customer").val());
            data_main.append('tgl_transaksi', $("#tgl_transaksi").val());
            data_main.append('sales'        , $("#sales").val());


            data_main.append('disc'         , $("#disc").val());
            data_main.append('cara_pembayaran'  , $("#cara_pembayaran").val());
            data_main.append('tempo'        , $("#tempo").val());

            data_main.append('list_detail_penjualan', JSON.stringify(detail_product));

            $.ajax({
                url: "<?php echo base_url()."admin/penjualanmain/insert_penjualan/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    // response_disabled(res);
                }
            });


        }
    });

    $("#balik").click(function(){
        $("#disc").focus();
    });
//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//

</script>