
<?php
    $id_customer = "";
    $customer = "";
    $tgl_transaksi = "";

    
    $id_tr_header = $this->uri->segment(3);

    if($data_header){
        $main_data = json_decode($data_header);
        
        $id_customer = $main_data[0]->id_customer;
        $customer = $main_data[0]->id_customer;//
        $tgl_transaksi = $main_data[0]->tgl_transaksi_tr_header;
    }
?>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">Update Retur Penjualan</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="<?php print_r(base_url());?>admin/list_retur_penjualan"  style="color: #ffff;">
                                <i class="fa fa-list"></i>&nbsp;&nbsp;List Retur Penjualan
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Kode Customer</b></label>
                                <input type="text" class="form-control" id="id_customer" name="id_customer" value="90" readonly=""/>
                                <p id="msg_id_customer" style="color: red;"></p>
                            </div>
                        </div>
                        
                        <div class="col-md-5">
                            <div class="form-group">
                                <label><b>Customer</b></label>

                                <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="customer" name="customer">
                                </select>
                                <p id="msg_customer" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Tanggal Transaksi</b></label>
                                <input type="text" class="form-control" id="tgl_transaksi" name="tgl_transaksi" value="<?php print_r(date("Y-m-d"));?>" readonly="" />
                                <p id="msg_tgl_transaksi" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="button" id="add_main" name="add_main" class="btn waves-effect waves-light btn-rounded btn-info">Buat Detail</button>
                                <button type="button" id="reset_main" name="reset_main" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div>
                    </div>


                    <div class="row font_edit font_color">
                        <div class="col-md-12">
                            <h4 class="modal-title">Produk Retur</h4>       
                        </div>
                        
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" id="tmbh_detail" name="tmbh_detail">Tambah Detail</button>       
                        </div>
                        
                        <div class="col-md-12">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No. </th>
                                            <th>Produk</th>
                                            <th class="text-center" width="10%">Kondisi Produk</th>
                                            <th class="text-center" width="10%">Kode Produksi</th>
                                            <th class="text-center" width="10%">Tgl. Kadaluarsa</th>
                                            <th class="text-center" width="10%">Jumlah Produk</th>
                                            <th class="text-center" width="15%">Harga Satuan</th>
                                            <th class="text-center" width="15%">Seharga</th>
                                            <th class="text-center" width="5%">Proses</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_list_detail">

                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <br>
                        </div>
                    </div>

                    <div class="row font_edit font_color">
                        <div class="col-md-6">
                            <h4 class="modal-title">Ditukar Dengan</h4>       
                        </div>
                        <div class="col-md-6 text-right">
                            <h4 class="modal-title">Selisih Total Harga: <b id="selisih">Rp. 0</b></h4>
                        </div>
                        
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" id="tmbh_detail_" name="tmbh_detail_">Tambah Detail Tuakar</button>       
                        </div>
                        
                        <div class="col-md-12">
                            <div class="table-responsive m-t-40" style="clear: both;">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No. </th>
                                            <th>Produk</th>
                                            <th class="text-center" width="10%">Kode Produksi</th>
                                            <th class="text-center" width="10%">Tgl. Kadaluarsa</th>
                                            <th class="text-center" width="10%">Jumlah Produk</th>
                                            <th class="text-center" width="15%">Harga Satuan</th>
                                            <th class="text-center" width="15%">Seharga</th>
                                            <th class="text-center" width="5%">Proses</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_list_detail_">

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="8"><hr></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            </td>
                                            <td colspan="2">
                                            </td>
                                            <!-- <td colspan="2">
                                            </td> -->
                                            <td colspan="4">
                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <label><b>&nbsp;</b></label><br>
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Lanjutkan</button>
                                    <button type="button" id="balik" name="balik" class="btn waves-effect waves-light btn-rounded btn-danger">Kembali</button>
                                </div>
                            </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>


<!-- main_retur -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modal_insert_detail" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Form Tambah Detail</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Brand</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="brand" name="brand"> -->
                            <!-- </select> -->
                            <input type="text" class="form-control" id="brand" name="brand" list="dl_brand">

                            <datalist id="dl_brand">
                                
                            </datalist>
                            <p id="msg_brand" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Item</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="item" name="item">
                            </select> -->
                            <input type="text" class="form-control" name="item" id="item" placeholder="Cari item.." list="dl_item">

                            <!-- <button type="button" id="btn_item" name="btn_item">cek item</button> -->
                            <datalist id="dl_item">
                                
                            </datalist>

                            <p id="msg_item" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Nama Item</b></label>
                            <input type="text" class="form-control" id="nama_item" name="nama_item" readonly=""/>
                            <p id="msg_nama_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tgl. Kadaluarsa</b></label>
                            <br>
                            <label><b id="val_tgl_kadaluarsa">Tgl. Kadaluarsa</b></label>
                            <p id="msg_tgl_kadaluarsa" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Harga Satuan</b></label>
                            <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" readonly="" value="0" />
                            <label><b id="val_harga_satuan">Harga Satuan</b></label>
                            <p id="msg_harga_satuan" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kode Produksi</b></label>
                            <br>
                            <label><b id="val_kode_produksi">Kode Produksi</b></label>
                            <p id="msg_kode_produksi" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <hr>
                    </div>
                   

                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Jumlah Item</b></label>
                            <input type="number" class="form-control" id="jml_item" name="jml_item" value="0"/>
                            <p id="msg_jml_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Seharga</b></label>
                            <br>
                            <label><b id="val_harga_total">Seharga</b></label>
                            <p id="msg_harga_total" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Jenis Pengembalian</b></label>
                            <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="jenis_pengembalian" name="jenis_pengembalian">
                                <option value="0">Produk Berlebih/Tidak Rusak/Kadaluarsa</option>
                                <option value="1">Produk Rusak/Kadaluarsa</option>
                            </select>

                            <p id="msg_jenis_pengembalian" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="add_detail" name="add_detail" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                            
                <button type="button" id="reset_detail" name="reset_detail" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- main_retur -->



<!-- pengganti_retur -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="modal_insert_detail_" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myLargeModalLabel">Form Tambah Detail Pengganti</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Brand</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="brand_" name="brand_"> -->
                            <!-- </select> -->
                            <input type="text" class="form-control" id="brand_" name="brand_" list="dl_brand_">

                            <datalist id="dl_brand_">
                                
                            </datalist>
                            <p id="msg_brand_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Item</b></label>
                            <!-- <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="item_" name="item_">
                            </select> -->
                            <input type="text" class="form-control" name="item_" id="item_" placeholder="Cari item_.." list="dl_item_">

                            <!-- <button type="button" id="btn_item_" name="btn_item_">cek item_</button> -->
                            <datalist id="dl_item_">
                                
                            </datalist>

                            <p id="msg_item_" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Nama Item_</b></label>
                            <input type="text" class="form-control" id="nama_item_" name="nama_item_" readonly=""/>
                            <p id="msg_nama_item_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Tgl. Kadaluarsa</b></label>
                            <br>
                            <label><b id="val_tgl_kadaluarsa_">Tgl. Kadaluarsa</b></label>
                            <p id="msg_tgl_kadaluarsa_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Harga Satuan</b></label>
                            <input type="text" class="form-control" id="harga_satuan_" name="harga_satuan_" readonly="" value="0" />
                            <label><b id="val_harga_satuan_">Harga Satuan</b></label>
                            <p id="msg_harga_satuan_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Kode Produksi</b></label>
                            <br>
                            <label><b id="val_kode_produksi_">Kode Produksi</b></label>
                            <p id="msg_kode_produksi_" style="color: red;"></p>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <hr>
                    </div>
                   

                    
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Jumlah Item_</b></label>
                            <input type="number" class="form-control" id="jml_item_" name="jml_item_" value="0"/>
                            <p id="msg_jml_item_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label><b>Seharga</b></label>
                            <br>
                            <label><b id="val_harga_total_">Seharga</b></label>
                            <p id="msg_harga_total_" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-6" hidden="">
                        <div class="form-group">
                            <label><b>Jenis Pengembalian</b></label>
                            <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="jenis_pengembalian_" name="jenis_pengembalian_">
                                <option value="0">Produk Berlebih/Tidak Rusak/Kadaluarsa</option>
                                <option value="1">Produk Rusak/Kadaluarsa</option>
                            </select>

                            <p id="msg_jenis_pengembalian_" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="add_detail_" name="add_detail_" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                            
                <button type="button" id="reset_detail_" name="reset_detail_" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- pengganti_retur -->


<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var list_customer = JSON.parse('<?php print_r($list_customer);?>');
    var list_item = JSON.parse('<?php print_r($list_item);?>');
    var list_brand = JSON.parse('<?php print_r($list_brand);?>');

    var list_item_ = JSON.parse('<?php print_r($list_item_);?>');

    var data_header = JSON.parse('<?php print_r($data_header);?>');
    var data_detail = JSON.parse('<?php print_r($data_detail);?>');

    var data_header_p = JSON.parse('<?php print_r($data_header_p);?>');
    var data_detail_p = JSON.parse('<?php print_r($data_detail_p);?>');

    // console.log(list_item);

    var default_brand, default_item;
    var t_retur = 0, t_pengganti = 0;

    $(document).ready(function(){
        create_list_customer();
        
        // $("#id_customer").val($("#customer").val());

        // $("#customer").focus();

        // $("#tempo").val("2019-10-22");
        set_param_header();

        set_detail_update_h();
        set_detail_update_h_p();


        check_tambahan_uang();

        create_list_brand();
        create_list_item();
        set_item();


    });

    function set_param_header(){
        $("#id_customer").val("<?php print_r($id_customer); ?>");
        $("#customer").val("<?php print_r($customer); ?>");
        $("#tgl_transaksi").val("<?php print_r($tgl_transaksi); ?>");
        
    }


    function set_detail_update_h() {
        // console.log(list_detail);
        for (let i in data_detail) {

            var tmp_list = {
                "id_item"       : data_detail[i].id_item,
                "nama_item"     : data_detail[i].nama_item,
                "jenis_pengembalian"     : data_detail[i].status_rt_tr_detail,
                "brand"         : data_detail[i].brand,
                "item"          : data_detail[i].item,
                "harga_satuan"  : parseFloat(data_detail[i].harga_satuan_tr_detail),
                "jml_item"     : parseFloat(data_detail[i].jml_item_tr_detail),
                "harga_total"   : parseFloat(data_detail[i].harga_total_tr_detail),
                "kode_produksi" : data_detail[i].kode_produksi_item,
                "tgl_kadaluarsa": data_detail[i].tgl_kadaluarsa_item
            };

            detail_product[data_detail[i].id_item] = tmp_list;
        }
        render_tbl_detail_product();
    }

    function set_detail_update_h_p() {
        // console.log(list_detail);
        for (let i in data_detail_p) {

            var tmp_list = {
                "id_item"       : data_detail_p[i].id_item,
                "nama_item"     : data_detail_p[i].nama_item,
                "jenis_pengembalian"     : "0",
                "brand"         : data_detail_p[i].brand,
                "item"          : data_detail_p[i].item,
                "harga_satuan"  : parseFloat(data_detail_p[i].harga_satuan_tr_detail),
                "jml_item"     : parseFloat(data_detail_p[i].jml_item_tr_detail),
                "harga_total"   : parseFloat(data_detail_p[i].harga_total_tr_detail),
                "kode_produksi" : data_detail_p[i].kode_produksi_item,
                "tgl_kadaluarsa": data_detail_p[i].tgl_kadaluarsa_item
            };

            detail_product_pengganti[data_detail_p[i].id_item] = tmp_list;
        }
        render_tbl_detail_product_pengganti();
    }

    function currency(x){
        return x.toLocaleString('us-EG');
    }

    function create_alert(title, msg, status){
        $(function() {
            "use strict";                      
           $.toast({
            heading: title,
            text: msg,
            position: 'top-right',
            loaderBg:'#ff6849',
            icon: status,
            hideAfter: 3500, 
            stack: 6
          });
        });
    }


//========================================================================//
//-----------------------------------btn_insert_detail--------------------//
//========================================================================//
    $("#tmbh_detail").click(function(){
        $("#modal_insert_detail").modal("show");
    });


    $("#tmbh_detail_").click(function(){
        $("#modal_insert_detail_").modal("show");


        create_list_brand_pengganti();
        create_list_item_pengganti();
        set_item_pengganti();
    });
//========================================================================//
//-----------------------------------btn_insert_detail--------------------//
//========================================================================//

//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//
    $("#customer").focus(function(){
    });

    $("#customer").keyup(function(){
        var customer = $("#customer").val();
        if(jQuery.inArray(customer, list_customer) !== -1){
            $("#msg_customer").html("");
        }else {
            $("#msg_customer").html("Customer tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    });

    $("#customer").change(function(){
        $("#id_customer").val($("#customer").val());
    });

    function create_list_customer(){
        var str_list = "";
        for (let elm in list_customer) {
            str_list += "<option value=\""+elm+"\">"+list_customer[elm].nama_rekanan+"</option>";
        }

        $("#customer").html(str_list);
    }
//========================================================================//
//-----------------------------------customer-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//
    $("#brand").focus(function(){
    });

    $("#brand").keyup(function(){
        check_brand_in_master();

        set_item();
        create_list_item();
    });

    $("#brand").change(function(){
        check_brand_in_master();

        set_item();
        create_list_item();
    });

    function create_list_brand(){
        var str_list = "";
        // console.log(list_brand);
        var i = 0;
        for (let elm in list_brand) {
            str_list += "<option value=\""+elm+"\">"+list_brand[elm].nama_brand+"</option>";
            // console.log(str_list);
            if(i == 0){
                default_brand = elm;
            }

            i++;
        }

        $("#dl_brand").html(str_list);
    }

    function check_brand_in_master(){
        var brand = $("#brand").val();
        var status = true;
        if(!(brand in list_item)){
            status = false;

            $("#msg_brand").html("brand tidak ditemukan");
        }else {
            status = true;
            
            $("#msg_brand").html("");
        }

        return status;
    }
//========================================================================//
//-----------------------------------brand--------------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//
    $("#item").focus(function(){
    });

    $("#item").keyup(function(){
        check_item_in_master();

        set_item();
    });

    $("#item").change(function(){
        check_item_in_master();

        set_item();
    });

    $("#btn_item").click(function(){
        if(check_item_in_master()){
            console.log("item and brand good");
        }else {
            console.log("item and brand nothing");
        }
    });

    function set_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();
        // console.log(list_item[brand]);
        if(item in list_item[brand]){
            $("#msg_item").html("");
            set_value_item();
            jml_item_change();
            // console.log();
        }else {
            $("#msg_item").html("Item tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    }

    function clear_item(){
        $("#nama_item").val("");
        $("#harga_satuan").val("0");
        $("#harga_total").val("0");

        $("#val_kode_produksi").html("");
        $("#val_tgl_kadaluarsa").html("");

        $("#val_harga_satuan").html("Rp. 0");
        $("#val_harga_total").html("Rp. 0");   
    }

    function set_value_item(){
        var brand = $("#brand").val();  
        var item = $("#item").val();

        var data = list_item[brand][item];
        // console.log(data);
        var harga_jual = data.harga_bruto;

        $("#nama_item").val(data.nama_item);
        $("#harga_satuan").val(harga_jual);
        $("#harga_total").val("0");

        $("#val_kode_produksi").html(data.kode_produksi_item);
        $("#val_tgl_kadaluarsa").html(data.tgl_kadaluarsa_item);

        $("#val_harga_satuan").html("Rp. "+currency(parseFloat(harga_jual)));
        $("#val_harga_total").html("Rp. 0"); 
    }

    function create_list_item(){
        var str_list = "";
        var brand = $("#brand").val();
        var array_item_list = list_item[brand];
        // console.log(array_item_list);

        var i = 0;
        for (let elm in array_item_list) {
            str_list += "<option value=\""+elm+"\">("+
                            array_item_list[elm].kode_produksi_item+") "+
                            array_item_list[elm].nama_item+" - "+
                            array_item_list[elm].stok+" ["+
                            array_item_list[elm].satuan+"]"+
                        "</option>";

            if(i == 0){
                default_item = elm;
            }

            i++;
        }

        $("#dl_item").html(str_list);
        // $("#item").val("");
    }

    function check_item_in_master(){
        var brand = $("#brand").val();
        var item = $("#item").val();
        
        var status = false;

        if(brand in list_item){
            if(!(item in list_item[brand])){
                $("#msg_item").html("brand tidak ditemukan");
                
                status = false;
            }else {
                $("#msg_item").html("");

                status = true;
            }
        }
        
        return status;
    }
//========================================================================//
//-----------------------------------item---------------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//
    function jml_item_change(){
        var harga_satuan = $("#harga_satuan").val();
        var jml_item = $("#jml_item").val();

        var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        $("#harga_total").val(t_harga);

        $("#val_harga_total").html("Rp. "+currency(parseFloat(t_harga))); 
    }

    $("#jml_item").keyup(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
        }else {
            $("#val_harga_total").html("Rp. 0");

            console.log("tidak boleh null");
        }
    });  

    $("#jml_item").change(function(){
        var jml_item = $("#jml_item").val();
        if(jml_item){
            jml_item_change();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------jml_item-----------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//
    // var res_json ;
    var res_out;
    var return_sts;

    function check_stok_item(){
        var data_main = new FormData();
        data_main.append('id_item'      , $("#item").val());
        data_main.append('jml_item'     , $("#jml_item").val());

        $.ajax({
            url: "<?php echo base_url()."admin/penjualanmain/check_stok_item/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {                
            },complete: function(res){
                return_sts = response_check_stok_item(res.responseText);
                // console.log(return_sts);
                if(return_sts){

                }
            }
        });

        // console.log(res_out);
    }

    $(document).ajaxComplete(function(event, xhr, settings){
        // console.log(settings.url);
        if(settings.url == "<?php print_r(base_url());?>admin/penjualanmain/check_stok_item/"){
            // console.log(return_sts);
        }
    });

    $("#check_item").click(function(){
        // console.log(check_stok_item());
        check_stok_item();
        if(return_sts){
            console.log(return_sts+": ok");
        }
    });

    function response_check_stok_item(res){
        var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
               
        return main_msg.status;
    }
//========================================================================//
//-----------------------------------check_stok_item----------------------//
//========================================================================//

//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//
    $("#add_main").click(function(){
        // console.log(check_form_customer());
        // if(check_form_customer()){
            $("#id_customer").attr("readonly", true);
            $("#customer").attr("disabled", true);
            $("#tgl_transaksi").attr("readonly", true);
            // $("#disc").attr("readonly", true);
            $("#sales").attr("disabled", true);

            $("#brand").focus();
        // }
    });

    $("#reset_main").click(function(){
        $("#customer").removeAttr("disabled", true);
        // $("#disc").removeAttr("readonly", true);
        $("#sales").removeAttr("disabled", true);

        $("#customer").focus();
    });
//========================================================================//
//-----------------------------------btn_main-----------------------------//
//========================================================================//


//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//
    var detail_product = {};
    var list_item_choose = [];

    $("#add_detail").click(function(){
        add_detail_penjualan();
    });

    function add_detail_penjualan(){
        var id_item         = $("#item").val();
        var nama_item       = $("#nama_item").val();
        
        var brand           = $("#brand").val();
        var item            = $("#item").val();
        var jml_item        = $("#jml_item").val();

        var harga_satuan    = list_item[brand][item].harga_bruto;
        var harga_total     = parseFloat(harga_satuan)*parseFloat(jml_item);

        var kode_produksi   = list_item[brand][item].kode_produksi_item;
        var tgl_kadaluarsa  = list_item[brand][item].tgl_kadaluarsa_item;

        var jenis_pengembalian = $("#jenis_pengembalian").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();
        
        var t_harga = 0;
        if(harga_satuan && jml_item){
            t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        }

        if(check_value()){
            var tmp_list = {
                "id_item"       : item,
                "nama_item"     : nama_item,
                "jenis_pengembalian"     : jenis_pengembalian,
                "brand"         : brand,
                "item"          : item,
                "harga_satuan"  : harga_satuan,
                "jml_item"      : jml_item,
                "harga_total"   : t_harga,
                "kode_produksi" : kode_produksi,
                "tgl_kadaluarsa": tgl_kadaluarsa
            };

            if(!(item in detail_product)){
                
                detail_product[item] = tmp_list;
                // console.log(detail_product);
               
                clear_detail();
                render_tbl_detail_product();

                create_alert('Proses Berhasil', 'Data detail tersimpan', 'success');

                $("#modal_insert_detail").modal('hide');
            }else{
                create_alert('Proses Gagal', 'item sudah ada di detail penjualan', 'error');
                // console.log("");
            }
        }else{
            // console.log("check_value false");
            create_alert('Proses Gagal', 'Input data tidak lengkap periksa kembali input lagi', 'error');
                
        }
    }

    function render_tbl_detail_product(){
        var str_tbl = "";
        var no = 1;

        var disc    = $("#disc").val();
        var t_harga = 0;
        var pajak   = 10;

        console.log(detail_product);

        for (let i in detail_product) {

            var jenis_pengembalian_str = "Rusak";
            if(detail_product[i].jenis_pengembalian == "0"){
                jenis_pengembalian_str = "Tidak Rusak";
            }

            str_tbl += "<tr>"+
                            "<td align=\"right\">"+no+"</td>"+
                            "<td>("+detail_product[i].id_item+") "+detail_product[i].nama_item+"</td>"+
                            "<td align=\"right\">"+jenis_pengembalian_str+"</td>"+
                            "<td align=\"right\">"+detail_product[i].kode_produksi+"</td>"+
                            "<td align=\"right\">"+detail_product[i].tgl_kadaluarsa+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(detail_product[i].jml_item))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_satuan))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product[i].harga_total))+"</td>"+
                            "<td>"+
                                "<center>"+
                                // "<button class=\"btn btn-info\" id=\"up_detail\" onclick=\"update_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                                "<button class=\"btn btn-danger\" id=\"del_detail\" onclick=\"delete_detail('"+detail_product[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                                "</center>"+
                            "</td>"+
                        "</tr>";
            t_harga += parseFloat(detail_product[i].harga_total);
            console.log(i);
            no++;
        }

        str_tbl += "<tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"3\"><b style=\"color: #262626;\">Harga Total</b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_t_harga\">Rp. "+currency(t_harga)+"</b></td>"+
                        "<td></td>"+
                    "</tr>";
        $("#tbl_list_detail").html(str_tbl);

        t_retur = t_harga;
        check_tambahan_uang();
    }

    function check_value(){
        var id_customer     = $("#id_customer").val();
        var customer        = $("#customer").val();
        var tgl_transaksi   = $("#tgl_transaksi").val();

        var id_item         = $("#item").val();
        var nama_item       = $("#nama_item").val();

        var brand           = $("#brand").val();
        var item            = $("#item").val();
        var jml_item        = $("#jml_item").val();

        var harga_satuan    = list_item[brand][item].harga_bruto;
        var harga_total     = parseFloat(harga_satuan)*parseFloat(jml_item);

        var kode_produksi   = list_item[brand][item].kode_produksi_item;
        var tgl_kadaluarsa  = list_item[brand][item].tgl_kadaluarsa_item;

        var jenis_pengembalian = $("#jenis_pengembalian").val();

        var status = false;

        if(!id_customer 
            || !customer || !tgl_transaksi

            || !id_item || !brand 
            || !item || !jml_item
            || !harga_satuan || !harga_total || !kode_produksi || !tgl_kadaluarsa
            // || !cara_pembayaran || !tempo
            || !jenis_pengembalian
            || !check_item_in_master()
            ){
            console.log("kosong");
        }else {
            if(parseInt(jml_item) > 0){
                status = true;
                console.log("isi");
            }
        }

        return status;
    }
    

    $("#reset_detail").click(function(){
        clear_detail();
    });

    function clear_detail(){
        id_item = $("#nama_item").val("");
        // item = $("#item").val(default_item);
        // brand = $("#brand").val(default_brand);
        harga_satuan = $("#harga_satuan").val("");
        jml_item = $("#jml_item").val("0");
        harga_total = $("#harga_total").val("");
        kode_produksi = $("#kode_produksi").val("");
        tgl_kadaluarsa = $("#tgl_kadaluarsa").val("");
        disc_item = $("#disc_item").val("0");
        // var cara_pembayaran = $("#cara_pembayaran").val("0");
        // var tempo = $("#tempo").val("");

        val_harga_satuan    = $("#val_harga_satuan").html("Rp. 0");
        val_harga_total     = $("#val_harga_total").html("Rp. 0");
        val_harga_after_disc= $("#val_harga_after_disc").html("Rp. 0");

        $("#brand").focus();

        set_item();
        set_value_item();
        jml_item_change();
    }

    function delete_detail(id_item){
        create_alert('Proses Berhasil', detail_product[id_item].nama_item+' di hapus dari detail penjualan', 'success');
        delete detail_product[id_item];
        render_tbl_detail_product();
    }

    $("#finis_detail").click(function(){
        $("#disc").focus();
    });
//========================================================================//
//-----------------------------------btn_detail---------------------------//
//========================================================================//

//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//
    $("#lanjut").click(function(){
        if(validation_main_check() && check_tambahan_uang()){

            var data_main = new FormData();
            data_main.append('id_tr_header'  , "<?php print_r($id_tr_header);?>");

            data_main.append('id_customer'  , $("#id_customer").val());
            data_main.append('customer'     , $("#customer").val());
            data_main.append('tgl_transaksi', $("#tgl_transaksi").val());

            data_main.append('list_detail_retur', JSON.stringify(detail_product));
            data_main.append('list_detail_pengganti', JSON.stringify(detail_product_pengganti));

            $.ajax({
                url: "<?php echo base_url()."admin/returpenjualanmain/insert_retur/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_req(res);
                }
            });
        }else{
            create_alert('Proses Gagal', 'Periksa Kembali input saudara', 'error');
        }
    });

    $("#balik").click(function(){
        $("#disc").focus();
    });

    function validation_main_check(){
        var status = false;
        var id_customer    = $("#id_customer").val();
        var customer    = $("#customer").val();
        var tgl_transaksi = $("#tgl_transaksi").val();

        if(!id_customer || !customer || !tgl_transaksi 
            || jQuery.isEmptyObject(detail_product) || jQuery.isEmptyObject(detail_product_pengganti)
            ){
            console.log("kosong");
        }else {
            status = true;
            console.log("isi");
        }

        return status;
    }

    function response_req(res){
        var res_data = JSON.parse(res);
        // console.log(res_data);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning");
        }
    }

    function create_sweet_alert(title, msg, status) {
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            if(status == "success"){
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Lanjutkan",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            window.location.href = "<?php print_r(base_url());?>admin/list_retur_penjualan";  
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }else {
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Perbaiki",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            swal.close();
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }
            
            
            //init
            
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }
//========================================================================//
//-----------------------------------btn_finish---------------------------//
//========================================================================//





// -------------------------------------------Pengganti------------------------------------------//


//========================================================================//
//-----------------------------------brand_pengganti----------------------//
//========================================================================//
    $("#brand_").focus(function(){
    });

    $("#brand_").keyup(function(){
        check_brand_in_master_pengganti();

        set_item_pengganti();
        create_list_item_pengganti();
    });

    $("#brand_").change(function(){
        check_brand_in_master_pengganti();

        set_item_pengganti();
        create_list_item_pengganti();
    });

    function create_list_brand_pengganti(){
        var str_list = "";
        // console.log(list_brand);
        var i = 0;
        for (let elm in list_brand) {
            str_list += "<option value=\""+elm+"\">"+list_brand[elm].nama_brand+"</option>";
            // console.log(str_list);
            if(i == 0){
                default_brand = elm;
            }

            i++;
        }

        $("#dl_brand_").html(str_list);
    }

    function check_brand_in_master_pengganti(){
        var brand = $("#brand_").val();
        var status = true;
        if(!(brand in list_item_)){
            status = false;

            $("#msg_brand_").html("brand tidak ditemukan");
        }else {
            status = true;
            
            $("#msg_brand_").html("");
        }

        return status;
    }
//========================================================================//
//-----------------------------------brand_pengganti----------------------//
//========================================================================//

//========================================================================//
//-----------------------------------item_pengganti-----------------------//
//========================================================================//
    $("#item_").focus(function(){
    });

    $("#item_").keyup(function(){
        check_item_in_master_pengganti();

        set_item_pengganti();
    });

    $("#item_").change(function(){
        check_item_in_master_pengganti();

        set_item_pengganti();
    });

    $("#btn_item_").click(function(){
        if(check_item_in_master_pengganti()){
            console.log("item and brand good");
        }else {
            console.log("item and brand nothing");
        }
    });

    function set_item_pengganti(){
        var brand = $("#brand_").val();  
        var item = $("#item_").val();
        // console.log(list_item_[brand]);
        if(item in list_item_[brand]){
            $("#msg_item_").html("");
            set_value_item_pengganti();
            jml_item_change_pengganti();
            // console.log();
        }else {
            $("#msg_item_").html("Item tidak di temukan, Silahkan cari data yang sesuai yang telah di tetapkan");
        }
    }

    function clear_item_pengganti(){
        $("#nama_item_").val("");
        $("#harga_satuan_").val("0");
        $("#harga_total_").val("0");

        $("#val_kode_produksi_").html("");
        $("#val_tgl_kadaluarsa_").html("");

        $("#val_harga_satuan_").html("Rp. 0");
        $("#val_harga_total_").html("Rp. 0");   
    }

    function set_value_item_pengganti(){
        var brand = $("#brand_").val();  
        var item = $("#item_").val();

        var data = list_item_[brand][item];
        // console.log(data);
        var harga_jual = data.harga_bruto;

        $("#nama_item_").val(data.nama_item);
        $("#harga_satuan_").val(harga_jual);
        $("#harga_total_").val("0");

        $("#val_kode_produksi_").html(data.kode_produksi_item);
        $("#val_tgl_kadaluarsa_").html(data.tgl_kadaluarsa_item);

        $("#val_harga_satuan_").html("Rp. "+currency(parseFloat(harga_jual)));
        $("#val_harga_total_").html("Rp. 0"); 
    }

    function create_list_item_pengganti(){
        var str_list = "";
        var brand = $("#brand_").val();
        var array_item_list = list_item_[brand];
        // console.log(array_item_list);

        var i = 0;
        for (let elm in array_item_list) {
            str_list += "<option value=\""+elm+"\">("+
                            array_item_list[elm].kode_produksi_item+") "+
                            array_item_list[elm].nama_item+" - "+
                            array_item_list[elm].stok+" ["+
                            array_item_list[elm].satuan+"]"+
                        "</option>";

            if(i == 0){
                default_item = elm;
            }

            i++;
        }

        $("#dl_item_").html(str_list);
        // $("#item_").val("");
    }

    function check_item_in_master_pengganti(){
        var brand = $("#brand_").val();
        var item = $("#item_").val();
        
        var status = false;

        if(brand in list_item_){
            if(!(item in list_item_[brand])){
                $("#msg_item_").html("brand tidak ditemukan");
                
                status = false;
            }else {
                $("#msg_item_").html("");

                status = true;
            }
        }
        
        return status;
    }
//========================================================================//
//-----------------------------------item_pengganti-----------------------//
//========================================================================//


//========================================================================//
//-----------------------------------jml_item_pengganti-------------------//
//========================================================================//
    function jml_item_change_pengganti(){
        var harga_satuan = $("#harga_satuan_").val();
        var jml_item = $("#jml_item_").val();

        var t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        $("#harga_total_").val(t_harga);

        $("#val_harga_total_").html("Rp. "+currency(parseFloat(t_harga))); 
    }

    $("#jml_item_").keyup(function(){
        var jml_item = $("#jml_item_").val();
        if(jml_item){
            jml_item_change_pengganti();
        }else {
            $("#val_harga_total_").html("Rp. 0");

            console.log("tidak boleh null");
        }
    });  

    $("#jml_item_").change(function(){
        var jml_item = $("#jml_item_").val();
        if(jml_item){
            jml_item_change_pengganti();
        }else {
            console.log("tidak boleh null");
        }
    });
//========================================================================//
//-----------------------------------jml_item_pengganti-------------------//
//========================================================================//

//========================================================================//
//-----------------------------------check_stok_item_pengganti------------//
//========================================================================//
    function response_check_stok_item_pengganti(res){
        var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
               
        return main_msg.status;
    }
//========================================================================//
//-----------------------------------check_stok_item_pengganti------------//
//========================================================================//


//========================================================================//
//-----------------------------------btn_detail_pengganti-----------------//
//========================================================================//
    var detail_product_pengganti = {};
    var list_item__choose_pengganti = [];

    $("#add_detail_").click(function(){
        var data_main = new FormData();
        data_main.append('id_item'      , $("#item_").val());
        data_main.append('jml_item'     , $("#jml_item_").val());

        $.ajax({
            url: "<?php echo base_url()."admin/penjualanmain/check_stok_item/";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {                
            },complete: function(res){
                return_sts = response_check_stok_item_pengganti(res.responseText);
                // console.log(return_sts);
                if(return_sts){
                    add_detail_penjualan_pengganti();
                }else{
                    create_alert('Proses Gagal', 'stok kosong', 'error');
                    // console.log("");
                }
            }
        });
    });

    function add_detail_penjualan_pengganti(){
        var id_item         = $("#item_").val();
        var nama_item       = $("#nama_item_").val();
        
        var brand           = $("#brand_").val();
        var item            = $("#item_").val();
        var jml_item        = $("#jml_item_").val();

        var harga_satuan    = list_item_[brand][item].harga_bruto;
        var harga_total     = parseFloat(harga_satuan)*parseFloat(jml_item);

        var kode_produksi   = list_item_[brand][item].kode_produksi_item;
        var tgl_kadaluarsa  = list_item_[brand][item].tgl_kadaluarsa_item;

        var jenis_pengembalian = $("#jenis_pengembalian_").val();
        // var cara_pembayaran = $("#cara_pembayaran").val();
        // var tempo = $("#tempo").val();
        
        var t_harga = 0;
        if(harga_satuan && jml_item){
            t_harga = parseFloat(harga_satuan) * parseFloat(jml_item);
        }

        if(check_value_pengganti()){
            var tmp_list = {
                "id_item"       : item,
                "nama_item"     : nama_item,
                "jenis_pengembalian"     : jenis_pengembalian,
                "brand"         : brand,
                "item"          : item,
                "harga_satuan"  : harga_satuan,
                "jml_item"      : jml_item,
                "harga_total"   : t_harga,
                "kode_produksi" : kode_produksi,
                "tgl_kadaluarsa": tgl_kadaluarsa
            };

            if(!(item in detail_product_pengganti)){
                
                detail_product_pengganti[item] = tmp_list;
                // console.log(detail_product_pengganti);
               
                clear_detail_pengganti();
                render_tbl_detail_product_pengganti();

                create_alert('Proses Berhasil', 'Data detail tersimpan', 'success');

                $("#modal_insert_detail_").modal('hide');
            }else{
                create_alert('Proses Gagal', 'item sudah ada di detail penjualan', 'error');
                // console.log("");
            }
        }else{
            // console.log("check_value_pengganti false");
            create_alert('Proses Gagal', 'Input data tidak lengkap periksa kembali input lagi', 'error');
                
        }
    }

    function render_tbl_detail_product_pengganti(){
        var str_tbl = "";
        var no = 1;

        var t_harga = 0;
        var pajak   = 10;

        console.log(detail_product_pengganti);

        for (let i in detail_product_pengganti) {

            var jenis_pengembalian_str = "Rusak";
            if(detail_product_pengganti[i].jenis_pengembalian == "0"){
                jenis_pengembalian_str = "Tidak Rusak";
            }

            str_tbl += "<tr>"+
                            "<td align=\"right\">"+no+"</td>"+
                            "<td>("+detail_product_pengganti[i].id_item+") "+detail_product_pengganti[i].nama_item+"</td>"+
                            // "<td align=\"right\">"+jenis_pengembalian_str+"</td>"+
                            "<td align=\"right\">"+detail_product_pengganti[i].kode_produksi+"</td>"+
                            "<td align=\"right\">"+detail_product_pengganti[i].tgl_kadaluarsa+"</td>"+
                            "<td align=\"right\">"+currency(parseFloat(detail_product_pengganti[i].jml_item))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product_pengganti[i].harga_satuan))+"</td>"+
                            "<td align=\"right\">Rp. "+currency(parseFloat(detail_product_pengganti[i].harga_total))+"</td>"+
                            "<td>"+
                                "<center>"+
                                // "<button class=\"btn btn-info\" id=\"up_detail\" onclick=\"update_detail('"+detail_product_pengganti[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                                "<button class=\"btn btn-danger\" id=\"del_detail_\" onclick=\"delete_detail_pengganti('"+detail_product_pengganti[i].id_item+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                                "</center>"+
                            "</td>"+
                        "</tr>";
            t_harga += parseFloat(detail_product_pengganti[i].harga_total);
            console.log(i);
            no++;
        }

        str_tbl += "<tr>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"></td>"+
                        "<td colspan=\"2\"><b style=\"color: #262626;\">Harga Total</b></td>"+
                        "<td align=\"right\"><b style=\"color: #262626;\" id=\"out_tr_t_harga\">Rp. "+currency(t_harga)+"</b></td>"+
                        "<td></td>"+
                    "</tr>";
        $("#tbl_list_detail_").html(str_tbl);

        t_pengganti = t_harga;

        check_tambahan_uang();
    }

    function check_value_pengganti(){
        var id_customer     = $("#id_customer").val();
        var customer        = $("#customer").val();
        var tgl_transaksi   = $("#tgl_transaksi").val();

        var id_item         = $("#item_").val();
        var nama_item       = $("#nama_item_").val();

        var brand           = $("#brand_").val();
        var item            = $("#item_").val();
        var jml_item        = $("#jml_item_").val();

        var harga_satuan    = list_item_[brand][item].harga_bruto;
        var harga_total     = parseFloat(harga_satuan)*parseFloat(jml_item);

        var kode_produksi   = list_item_[brand][item].kode_produksi_item;
        var tgl_kadaluarsa  = list_item_[brand][item].tgl_kadaluarsa_item;

        var jenis_pengembalian = $("#jenis_pengembalian_").val();

        var status = false;

        if(!id_customer 
            || !customer || !tgl_transaksi

            || !id_item || !brand 
            || !item || !jml_item
            || !harga_satuan || !harga_total || !kode_produksi || !tgl_kadaluarsa
            // || !cara_pembayaran || !tempo
            || !jenis_pengembalian
            || !check_item_in_master_pengganti()
            ){
            console.log("kosong");
        }else {
            if(parseInt(jml_item) > 0){
                status = true;
                console.log("isi");
            }
        }

        return status;
    }
    

    $("#reset_detail").click(function(){
        clear_detail_pengganti();
    });

    function clear_detail_pengganti(){
        id_item = $("#nama_item_").val("");
        // item = $("#item_").val(default_item);
        // brand = $("#brand_").val(default_brand);
        harga_satuan    = $("#harga_satuan_").val("");
        jml_item        = $("#jml_item_").val("0");
        harga_total     = $("#harga_total_").val("");
        kode_produksi   = $("#kode_produksi_").val("");
        tgl_kadaluarsa  = $("#tgl_kadaluarsa_").val("");
        disc_item       = $("#disc_item_").val("0");
        // var cara_pembayaran = $("#cara_pembayaran").val("0");
        // var tempo = $("#tempo").val("");

        val_harga_satuan    = $("#val_harga_satuan_").html("Rp. 0");
        val_harga_total     = $("#val_harga_total_").html("Rp. 0");
        val_harga_after_disc= $("#val_harga_after_disc_").html("Rp. 0");

        $("#brand_").focus();

        set_item_pengganti();
        set_value_item_pengganti();
        jml_item_change_pengganti();
    }

    function delete_detail_pengganti(id_item){
        create_alert('Proses Berhasil', detail_product_pengganti[id_item].nama_item+' di hapus dari detail penjualan', 'success');
        delete detail_product_pengganti[id_item];
        render_tbl_detail_product_pengganti();
    }
//========================================================================//
//-----------------------------------btn_detail_pengganti-----------------//
//========================================================================//



//========================================================================//
//-----------------------------------check_tambahan_uang------------------//
//========================================================================//
    function check_tambahan_uang(){
        var selisih = 0;
        // if(t_retur != 0 && t_pengganti != 0){
        selisih = parseFloat(t_pengganti) - parseFloat(t_retur);
        var status = true;
        if(selisih < 0){
            status = false;
            create_alert('Proses Gagal', 'Jumlah selisih harus minimal 0', 'error');
        }

        $("#selisih").html("Rp. "+currency(selisih));
        return status;
    }
//========================================================================//
//-----------------------------------check_tambahan_uang------------------//
//========================================================================//


</script>