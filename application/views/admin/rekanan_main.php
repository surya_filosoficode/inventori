<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Pelanggan</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Pelanggan</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_rekanan"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Pelanggan</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">Kode Pelanggan</th>
                                            <th width="15%">Nama Pelanggan</th>
                                            <th width="15%">Jenis Pelanggan</th>
                                            <th width="15%">Email Pelanggan</th>
                                            <th width="15%">Tlp Pelanggan</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r_rekanan => $v_rekanan) {
                                                    $str_rekanan = "Pedagang Besar Farmasi";
                                                    switch ($v_rekanan->jenis_rekanan) {
                                                        case 'pbf':
                                                            $str_rekanan = "Pedagang Besar Farmasi";
                                                            break;

                                                        case 'rs':
                                                            $str_rekanan = "Rumah Sakit";
                                                            break;

                                                        case 'apotik':
                                                            $str_rekanan = "Apotik";
                                                            break;

                                                        case 'sp':
                                                            $str_rekanan = "Sarana Pemerintahan";
                                                            break;

                                                        case 'pm':
                                                            $str_rekanan = "Puskesmas";
                                                            break;

                                                        case 'kl':
                                                            $str_rekanan = "Klinik";
                                                            break;

                                                        case 'to':
                                                            $str_rekanan = "Toko Obat";
                                                            break;

                                                        case 'ln':
                                                            $str_rekanan = "Lain-nya";
                                                            break;
                                                        
                                                        default:
                                                            $str_rekanan = "";
                                                            break;
                                                    }
                                                    echo "<tr>
                                                        <td>".($r_rekanan+1)."</td>
                                                        <td>".$v_rekanan->id_rekanan."</td>
                                                        <td>".$v_rekanan->nama_rekanan."</td>
                                                        <td>".$str_rekanan."</td>
                                                        <td>".$v_rekanan->email_rekanan."</td>
                                                        <td>".$v_rekanan->tlp_rekanan."</td>
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_rekanan\" onclick=\"update_rekanan('".$v_rekanan->id_rekanan."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_rekanan\" onclick=\"delete_rekanan('".$v_rekanan->id_rekanan."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert pelanggan------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_rekanan" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_rekanan";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Pelanggan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Pelanggan <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_rekanan" name="nama_rekanan" required="">
                                <p id="msg_nama_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Pelanggan<span style="color: red;">*</span></label>
                                <select class="form-control" id="jenis_rekanan" name="jenis_rekanan" required="">
                                    <!-- <option value="pabrik">Pabrik</option> -->
                                    <option value="pbf">Pedagang Besar Farmasi</option>
                                    <option value="rs">Rumah Sakit</option>
                                    <option value="apotik">Apotik</option>
                                    <option value="sp">Sarana Pemerintahan</option>
                                    <option value="pm">Puskesmas</option>
                                    <option value="kl">Klinik</option>
                                    <option value="to">Toko Obat</option>
                                    <option value="ln">Lain-nya</option>
                                </select>
                                <p id="msg_jenis_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Email Pelanggan <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" id="email_rekanan" name="email_rekanan" required="">
                                <p id="msg_email_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Pelanggan <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="tlp_rekanan" name="tlp_rekanan" required="">
                                <p id="msg_tlp_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kantor Pelanggan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_ktr_rekanan" name="alamat_ktr_rekanan" required="">
                                <p id="msg_alamat_ktr_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kirim Pelanggan <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_krm_rekanan" name="alamat_krm_rekanan" required="">
                                <p id="msg_alamat_krm_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Website Pelanggan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="website_rekanan" name="website_rekanan" required="">
                                <p id="msg_website_rekanan" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_rekanan" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_pelanggan------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_pelanggan------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_rekanan" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Pelanggan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_rekanan";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Pelanggan <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_rekanan" name="nama_rekanan" required="">
                                <p id="_msg_nama_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Pelanggan<span style="color: red;">*</span></label>
                                <!-- <input type="text" class="form-control" id="_jenis_rekanan" name="_jenis_rekanan" required=""> -->
                                <select class="form-control" id="_jenis_rekanan" name="_jenis_rekanan" required="">
                                    <!-- <option value="pabrik">Pabrik</option> -->
                                    <option value="pbf">Pedagang Besar Farmasi</option>
                                    <option value="rs">Rumah Sakit</option>
                                    <option value="apotik">Apotik</option>
                                    <option value="sp">Sarana Pemerintahan</option>
                                    <option value="pm">Puskesmas</option>
                                    <option value="kl">Klinik</option>
                                    <option value="to">Toko Obat</option>
                                    <option value="ln">Lain-nya</option>
                                </select>
                                <p id="_msg_jenis_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Email Pelanggan <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" id="_email_rekanan" name="email_rekanan" required="">
                                <p id="_msg_email_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Pelanggan <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_tlp_rekanan" name="tlp_rekanan" required="">
                                <p id="_msg_tlp_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kantor Pelanggan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_ktr_rekanan" name="alamat_ktr_rekanan" required="">
                                <p id="_msg_alamat_ktr_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kirim Pelanggan <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_krm_rekanan" name="alamat_krm_rekanan" required="">
                                <p id="_msg_alamat_krm_rekanan" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Website Pelanggan<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_website_rekanan" name="website_rekanan" required="">
                                <p id="_msg_website_rekanan" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_rekanan" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_pelanggan------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert pelanggan--------------------------//
    //=========================================================================//
        $("#add_rekanan").click(function() {
            var data_main = new FormData();
            data_main.append('nama_rekanan'         , $("#nama_rekanan").val());
            data_main.append('jenis_rekanan'         , $("#jenis_rekanan").val());
            data_main.append('email_rekanan'        , $("#email_rekanan").val());
            data_main.append('tlp_rekanan'          , $("#tlp_rekanan").val());
            data_main.append('alamat_ktr_rekanan'   , $("#alamat_ktr_rekanan").val());
            data_main.append('alamat_krm_rekanan'   , $("#alamat_krm_rekanan").val());

            data_main.append('website_rekanan'      , $("#website_rekanan").val());

            $.ajax({
                url: "<?php echo base_url()."admin/rekananmain/insert_rekanan";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_rekanan').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_insert();
            } else {
                $("#msg_nama_rekanan").html(detail_msg.nama_rekanan);
                $("#msg_jenis_rekanan").html(detail_msg.jenis_rekanan);
                $("#msg_email_rekanan").html(detail_msg.email_rekanan);
                $("#msg_tlp_rekanan").html(detail_msg.tlp_rekanan);
                $("#msg_alamat_ktr_rekanan").html(detail_msg.alamat_ktr_rekanan);
                $("#msg_alamat_krm_rekanan").html(detail_msg.alamat_krm_rekanan);
                $("#msg_website_rekanan").html(detail_msg.website_rekanan);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }

        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function clear_form_insert(){

            $("#nama_rekanan").val("");
            $("#jenis_rekanan").val("");
            $("#email_rekanan").val("");
            $("#tlp_rekanan").val("");
            $("#alamat_ktr_rekanan").val("");
            $("#alamat_krm_rekanan").val("");
            $("#website_rekanan").val("");
            

            $("#msg_nama_rekanan").html("");
            $("#msg_jenis_rekanan").html("");
            $("#msg_email_rekanan").html("");
            $("#msg_tlp_rekanan").html("");
            $("#msg_alamat_ktr_rekanan").html("");
            $("#msg_alamat_krm_rekanan").html("");
            $("#msg_website_rekanan").html("");
           
        }

        function render_data(data){
            var str_table = "";
            var no = 1;
            for (let i in data) {
                var str_rekanan = "Pedagang Besar Farmasi";
                switch (data[i].jenis_rekanan) {
                    case 'pbf':
                        str_rekanan = "Pedagang Besar Farmasi";
                        break;

                    case 'rs':
                        str_rekanan = "Rumah Sakit";
                        break;

                    case 'apotik':
                        str_rekanan = "Apotik";
                        break;

                    case 'sp':
                        str_rekanan = "Sarana Pemerintahan";
                        break;

                    case 'pm':
                        str_rekanan = "Puskesmas";
                        break;

                    case 'kl':
                        str_rekanan = "Klinik";
                        break;

                    case 'to':
                        str_rekanan = "Toko Obat";
                        break;

                    case 'ln':
                        str_rekanan = "Lain-nya";
                        break;
                    
                    default:
                        str_rekanan = "";
                        break;
                }
                
                str_table += "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td>"+data[i].id_rekanan+"</td>"+
                    "<td>"+data[i].nama_rekanan+"</td>"+
                    "<td>"+str_rekanan+"</td>"+
                    "<td>"+data[i].email_rekanan+"</td>"+
                    "<td>"+data[i].tlp_rekanan+"</td>"+
                    "<td>"+
                        "<center>"+
                        "<button class=\"btn btn-info\" id=\"up_rekanan\" onclick=\"update_rekanan('"+data[i].id_rekanan+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                        "<button class=\"btn btn-danger\" id=\"del_rekanan\" onclick=\"delete_rekanan('"+data[i].id_rekanan+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                        "</center>"+
                    "</td>"+
                    "</tr>";
                    no++;
                // str_table += "";
            }
            $("#main_table_content").html(str_table);
        }

        function alert_confirm(title, msg, status, txt_cencel_btn, id_rekanan){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: title,
                        text: msg,
                        type: status,
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: txt_cencel_btn,
                        closeOnConfirm: false
                    }, function() {
                        method_delete(id_rekanan);
                        swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
    //=========================================================================//
    //-----------------------------------insert pelanggan--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_pelanggan_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nama_rekanan").val("");
            $("#_jenis_rekanan").val("");
            $("#_email_rekanan").val("");
            $("#_tlp_rekanan").val("");
            $("#_alamat_ktr_rekanan").val("");
            $("#_alamat_krm_rekanan").val("");
            $("#_website_rekanan").val("");
           

            $("#_msg_nama_rekanan").html("");
            $("#_msg_jenis_rekanan").html("");
            $("#_msg_email_rekanan").html("");
            $("#_msg_tlp_rekanan").html("");
            $("#_msg_alamat_ktr_rekanan").html("");
            $("#_msg_alamat_krm_rekanan").html("");
            $("#_msg_website_rekanan").html("");
            
        }

        function update_rekanan(id_rekanan) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_rekanan', id_rekanan);

            $.ajax({
                url: "<?php echo base_url()."admin/rekananmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_rekanan);
                    $("#update_rekanan").modal('show');
                }
            });
        }

        function set_val_update(res, id_rekanan) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_rekanan;

                $("#_nama_rekanan").val(list_data.nama_rekanan);
                $("#_jenis_rekanan").val(list_data.jenis_rekanan);
                $("#_email_rekanan").val(list_data.email_rekanan);
                $("#_tlp_rekanan").val(list_data.tlp_rekanan);
                $("#_alamat_ktr_rekanan").val(list_data.alamat_ktr_rekanan);
                $("#_alamat_krm_rekanan").val(list_data.alamat_krm_rekanan);
                $("#_website_rekanan").val(list_data.website_rekanan);
                
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_pelanggan_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_pelanggan--------------------------//
    //=========================================================================//
        $("#btn_update_rekanan").click(function() {
            var data_main = new FormData();
            data_main.append('id_rekanan', id_cache);

            data_main.append('nama_rekanan'         , $("#_nama_rekanan").val());
            data_main.append('jenis_rekanan'         , $("#_jenis_rekanan").val());
            data_main.append('email_rekanan'        , $("#_email_rekanan").val());
            data_main.append('tlp_rekanan'          , $("#_tlp_rekanan").val());
            data_main.append('alamat_ktr_rekanan'   , $("#_alamat_ktr_rekanan").val());
            data_main.append('alamat_krm_rekanan'   , $("#_alamat_krm_rekanan").val());

            data_main.append('website_rekanan'      , $("#_website_rekanan").val());
            
            $.ajax({
                url: "<?php echo base_url()."admin/rekananmain/update_rekanan";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_rekanan').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_nama_rekanan").html(detail_msg.nama_rekanan);
                $("#_msg_jenis_rekanan").html(detail_msg.jenis_rekanan);
                $("#_msg_email_rekanan").html(detail_msg.email_rekanan);
                $("#_msg_tlp_rekanan").html(detail_msg.tlp_rekanan);
                $("#_msg_alamat_ktr_rekanan").html(detail_msg.alamat_ktr_rekanan);
                $("#_msg_alamat_krm_rekanan").html(detail_msg.alamat_krm_rekanan);
                $("#_msg_website_rekanan").html(detail_msg.website_rekanan);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------update_pelanggan--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------pelanggan_delete--------------------------//
    //=========================================================================//

        function method_delete(id_rekanan){
            var data_main = new FormData();
            data_main.append('id_rekanan', id_rekanan);

            $.ajax({
                url: "<?php echo base_url()."admin/rekananmain/delete_rekanan";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_rekanan(id_rekanan) {
            alert_confirm("Pesan Konfirmasi.!!", "Hapus ?", "warning", "Hapus", id_rekanan);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------pelanggan_update--------------------------//
    //=========================================================================//

   
</script>