<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php print_r(base_url());?>assets/template/assets/images/favicon.png">
    <title>Admin Press Admin Template - The Ultimate Bootstrap 4 Admin Template</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Custom CSS -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php print_r(base_url());?>assets/template/main/css/colors/blue.css" id="theme" rel="stylesheet">
</head>

<div class="table-responsive m-t-40">
    <table id="myTable" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="15%">(Kode Produk) Nama Produk</th>
                <th width="10%">Kode Produksi</th>
                <th width="10%">Tgl. Kadaluarsa</th>
                <th width="10%">Stok (Satuan)</th>
                <th width="10%">Stok Rusak(Satuan)</th>

                <th width="10%">Harga Beli</th>
                <!-- <th width="11%">Harga Netto</th> -->
                <th width="11%">Harga Jual</th>
                <th width="13%">Aksi</th>
            </tr>
        </thead>
        <tbody id=\"main_table_content\">
            <?php
                if($list_data){
                    foreach ($list_data as $key => $value) {
                        $str_btn_action = 
                        "<center>
                            <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                            <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                        </center>";
                        print_r("<tr>
                                    <td>(".$value->id_item.") ".$value->nama_item."</td>
                                    <td>".$value->kode_produksi_item."</td>
                                    <td>".$value->tgl_kadaluarsa_item."</td>
                                    <td align=\"right\">".$value->stok." (".$value->satuan.")</td>
                                    <td align=\"right\">".$value->stok_opnam." (".$value->satuan.")</td>
                                    <td align=\"right\">Rp. ".number_format($value->harga_bruto, 2, ",", ".")."</td>
                                    <td align=\"right\">Rp. ".number_format($value->harga_jual, 2, ",", ".")."</td>
                                    <td>".$str_btn_action."</td>
                                </tr>");
                    }
                }
            ?>
        </tbody>
    </table>
</div>
    
    <!-- This is data table -->
    <!-- <script src="<?=base_url()?>assets/template/assets/plugins/datatables/jquery.dataTables.min.js"></script> -->
    <!-- start - This is for export functionality only -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script> -->

    <!-- <script src="<?php print_r(base_url());?>assets/template/datatable/dataTables.buttons.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.flash.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/jszip.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/pdfmake.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/vfs_fonts.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.html5.min.js"></script>
    <script src="<?php print_r(base_url());?>assets/template/datatable/buttons.print.min.js"></script> -->
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>