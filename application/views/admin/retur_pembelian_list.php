<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">List Retur Pembeian</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">List Retur Pembeian</h4> -->
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="m-b-0 text-white">List Retur Pembeian</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="http://localhost/kasir/admin/data_retur_pembelian" style="color: #ffff;">
                                <i class="fa fa-plus-square"></i>&nbsp;&nbsp;Tambah Retur Pembeian
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body font_edit font_color">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="15%">Kode Transaksi</th>
                                            <th width="20%">Customer</th>
                                            <th width="15%">Tgl. Retur</th>
                                            <th width="15%">Harga Barang Retur</th>
                                            <th width="15%">Nominal Tambahan</th>
                                            <th width="10%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody id="main_table_content">
                                        <?php
                                            if(!empty($list_data)){
                                                foreach ($list_data as $key => $value) {
                                                    $status_retur = $value->status_retur;
                                                    echo "<tr>
                                                        <td>".$value->id_tr_header."</td>
                                                        <td>".$value->nama_suplier."</td>
                                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                                        <td align=\"right\">Rp. ".number_format(ceil($value->total_pembayaran_tr_header), 2, ",", ".")."</td>
                                                        <td align=\"right\">Rp. ".number_format(ceil($value->selisih), 2, ",", ".")."</td>
                                                        <td>
                                                            <center>".
                                                            
                                                            // "<a class=\"btn btn-info\" id=\"up_retur_penjualan\"  href=\"".base_url()."admin/read_retur_penjualan/".$value->id_tr_header."\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></a>&nbsp;&nbsp;".
                                                            
                                                            "<button class=\"btn btn-danger\" id=\"del_retur_penjualan\" onclick=\"delete_penjualan('".$value->id_tr_header."', '".$status_retur."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>".

                                                            "</center>
                                                        </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <!-- <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp; -->
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        
    });

    function delete_penjualan(id_data, status){
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            
            SweetAlert.prototype.init = function() {
                swal({   
                    title: "Konfirmasi Hapus Data",   
                    text: "Ketika data ini di hapus maka semua yang berkaitan dengan data ini juga akan hilang, Apakah data tetap di hapus ?",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Lanjutkan",   
                    cancelButtonText: "Perbaiki",   
                    closeOnConfirm: false,   
                    closeOnCancel: false 
                }, function(isConfirm){   
                    if (isConfirm) {
                        main_delete_data(id_data, status);
                        // swal.close();
                    } else{
                        swal.close();
                    }
                });
            },
            $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                        
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
    }

    function main_delete_data(id_data, status){
        var data_main = new FormData();
        data_main.append('id_tr_header'  , id_data);

        $.ajax({
            url: "<?php echo base_url()."admin/returpembelianmain/delete_retur/";?>"+status,
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                // console.log(res);
                response_req(res);
            }
        });
    }

    function response_req(res){
        var res_data = JSON.parse(res);
            var msg_main = res_data.msg_main;
            var msg_detail = res_data.msg_detail;

        if(msg_main.status){
            create_sweet_alert("Proses Berhasil", msg_main.msg, "success");
        }else {
            create_sweet_alert("Proses Gagal", msg_main.msg, "warning");
        }
    }

    function create_sweet_alert(title, msg, status) {
        ! function($) {
            "use strict";
            // var next = swal.close();
            var SweetAlert = function() {};
            if(status == "success"){
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Lanjutkan",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            window.location.href = "<?php print_r(base_url());?>admin/list_retur_pembelian";  
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }else {
                SweetAlert.prototype.init = function() {
                    swal({   
                        title: title,   
                        text: msg,   
                        type: status,   
                        showCancelButton: false,   
                        confirmButtonColor: "#DD6B55",   
                        confirmButtonText: "Perbaiki",   
                        cancelButtonText: "Perbaiki",   
                        closeOnConfirm: false,   
                        closeOnCancel: false 
                    }, function(isConfirm){   
                        if (isConfirm) {
                            swal.close();
                        } 
                    });
                },
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }
            
            
            //init
            
        }(window.jQuery),

        function($) {
            "use strict";
            $.SweetAlert.init()
        }(window.jQuery);
        
    }
</script>