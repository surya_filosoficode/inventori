<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Produk</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
     <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Data Produk</h4>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Brand<span style="color: red;">*</span></label>
                                <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="id_brand" name="id_brand">
                                    <?php
                                        if($list_data_brand){
                                            foreach ($list_data_brand as $key => $value) {
                                                // print_r($value);
                                                print_r("<option value=\"".$value->id_brand."\">".$value->nama_brand."</option>");             
                                            }
                                        }
                                    ?>
                                </select>
                                <p id="msg_id_brand" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Produk<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_item" name="nama_item" placeholder="Nama Produk" required="">
                                <p id="msg_nama_item" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Satuan Produk<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="satuan" name="satuan" placeholder="Satuan Produk" required="">
                                <p id="msg_satuan" style="color: red;"></p>
                            </div>
                        </div>
                       
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Harga Beli
                                    <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="harga_bruto" name="harga_bruto" placeholder="12.000.xxx" required="">
                                <label><b id="val_harga_bruto">Rp. 0</b></label>
                                <p id="msg_harga_bruto" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4" hidden="">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Harga Netto
                                    <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="harga_netto" name="harga_netto" placeholder="12.000.xxx" value="1" readonly="" required="">
                                <label><b id="val_harga_netto">Rp. 0</b></label>
                                <p id="msg_harga_netto" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Harga Jual
                                    <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="harga_jual" name="harga_jual" placeholder="12.000.xxx" required="">
                                <label><b id="val_harga_jual">Rp. 0</b></label>
                                <p id="msg_harga_jual" style="color: red;"></p>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group text-right">
                                <button type="submit" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                                <button type="button" id="reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>    
                        </div>
                        <div class="col-md-12" id="out">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="15%">Brand</th>
                                            <th width="20%">(Kode Produk) Nama Produk</th>
                                            <th width="10%">Stok (Satuan)</th>
                                            <th width="14%">Harga Beli</th>
                                            <!-- <th width="14%">Harga Netto</th> -->
                                            <th width="14%">Harga Jual</th>
                                            <th width="13%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                        if($list_data){
                                            foreach ($list_data as $key => $value) {
                                                $str_btn_action = 
                                                "<center>
                                                    <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                    <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$value->id_item."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                </center>";
                                                print_r("<tr>
                                                            <td>".$value->nama_brand."</td>
                                                            <td>(".$value->id_item.") ".$value->nama_item."</td>
                                                            <td align=\"right\">".$value->stok." (".$value->satuan.")</td>
                                                            <td align=\"right\">Rp. ".number_format($value->harga_bruto, 2, ",", ".")."</td>
                                                            <td align=\"right\">Rp. ".number_format($value->harga_jual, 2, ",", ".")."</td>
                                                            <td>".$str_btn_action."</td>
                                                        </tr>");
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Produk</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Brand<span style="color: red;">*</span></label>
                            <select class="select2 form-control custom-select select2-hidden-accessible" style="width: 100%; height:36px;" tabindex="-1" aria-hidden="true" id="_id_brand" name="id_brand">
                                <?php
                                    if($list_data_brand){
                                        foreach ($list_data_brand as $key => $value) {
                                            // print_r($value);
                                            print_r("<option value=\"".$value->id_brand."\">".$value->nama_brand."</option>");             
                                        }
                                    }
                                ?>
                            </select>
                            <p id="_msg_id_brand" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Produk<span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_nama_item" name="nama_item" placeholder="Nama Produk" required="">
                            <p id="_msg_nama_item" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Satuan Produk<span style="color: red;">*</span></label>
                            <input type="text" class="form-control" id="_satuan" name="satuan" placeholder="Satuan Produk" required="">
                            <p id="_msg_satuan" style="color: red;"></p>
                        </div>
                    </div>
                   
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Harga Beli
                                <span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="_harga_bruto" name="harga_bruto" placeholder="12.000.xxx" required="">
                            <label><b id="_val_harga_bruto">Rp. 0</b></label>
                            <p id="_msg_harga_bruto" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4" hidden="">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Harga Netto
                                <span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="_harga_netto" name="harga_netto" placeholder="12.000.xxx" value="1" readonly="" required="">
                            <label><b id="_val_harga_netto">Rp. 0</b></label>
                            <p id="_msg_harga_netto" style="color: red;"></p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Harga Jual
                                <span style="color: red;">*</span></label>
                            <input type="number" class="form-control" id="_harga_jual" name="harga_jual" placeholder="12.000.xxx" required="">
                            <label><b id="_val_harga_jual">Rp. 0</b></label>
                            <p id="_msg_harga_jual" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah</button>
                        <button type="button" id="btn_update_reset" class="btn waves-effect waves-light btn-rounded btn-danger">Reset</button>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->



<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_global; 

    $(document).ready(function(){
       clear_form();
    });

    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//
        function currency(x){
            return x.toLocaleString('us-EG');
        }

        function clear_form(){
            // $("#id_brand").val("");
            $("#nama_item").val("");
            $("#satuan").val("");
            $("#harga_bruto").val("");
            $("#harga_netto").val("");
            $("#harga_jual").val("");
            
            $("#msg_id_brand").html("");
            $("#msg_nama_item").html("");
            $("#msg_satuan").html("");
            $("#msg_harga_bruto").html("");
            $("#msg_harga_netto").html("");
            $("#msg_harga_jual").html("");

            $("#val_harga_bruto").html("");
            $("#val_harga_netto").html("");
            $("#val_harga_jual").html("");

            $("#id_brand").focus();
        }

        function clear_form_update(){
            // $("#_id_brand").val("");
            $("#_nama_item").val("");
            $("#_satuan").val("");
            $("#_harga_bruto").val("");
            $("#_harga_netto").val("");
            $("#_harga_jual").val("");
            
            $("#_msg_id_brand").html("");
            $("#_msg_nama_item").html("");
            $("#_msg_satuan").html("");
            $("#_msg_harga_bruto").html("");
            $("#_msg_harga_netto").html("");
            $("#_msg_harga_jual").html("");

            $("#_val_harga_bruto").html("");
            $("#_val_harga_netto").html("");
            $("#_val_harga_jual").html("");

            $("#_id_brand").focus();
        }

        function change_currency(val_number){
            var str_return;
            if(val_number != ""){
                str_return = "Rp. "+currency(parseFloat(val_number));
            }else{
                str_return = "Rp. 0";
            }

            return str_return;
        }


        $("#harga_bruto").change(function(){
            var harga_bruto = $("#harga_bruto").val();
            $("#val_harga_bruto").html(change_currency(harga_bruto));
        });

        $("#harga_bruto").keyup(function(){
            var harga_bruto = $("#harga_bruto").val();
            $("#val_harga_bruto").html(change_currency(harga_bruto));
        });

        $("#harga_netto").change(function(){
            var harga_netto = $("#harga_netto").val();
            $("#val_harga_netto").html(change_currency(harga_netto));
        });

        $("#harga_netto").keyup(function(){
            var harga_netto = $("#harga_netto").val();
            $("#val_harga_netto").html(change_currency(harga_netto));
        });

        $("#harga_jual").change(function(){
            var harga_jual = $("#harga_jual").val();
            $("#val_harga_jual").html(change_currency(harga_jual));
        });

        $("#harga_jual").keyup(function(){
            var harga_jual = $("#harga_jual").val();
            $("#val_harga_jual").html(change_currency(harga_jual));
        });


        $("#_harga_bruto").change(function(){
            var harga_bruto = $("#_harga_bruto").val();
            $("#_val_harga_bruto").html(change_currency(harga_bruto));
        });

        $("#_harga_bruto").keyup(function(){
            var harga_bruto = $("#_harga_bruto").val();
            $("#_val_harga_bruto").html(change_currency(harga_bruto));
        });

        $("#_harga_netto").change(function(){
            var harga_netto = $("#_harga_netto").val();
            $("#_val_harga_netto").html(change_currency(harga_netto));
        });

        $("#_harga_netto").keyup(function(){
            var harga_netto = $("#_harga_netto").val();
            $("#_val_harga_netto").html(change_currency(harga_netto));
        });

        $("#_harga_jual").change(function(){
            var harga_jual = $("#_harga_jual").val();
            $("#_val_harga_jual").html(change_currency(harga_jual));
        });

        $("#_harga_jual").keyup(function(){
            var harga_jual = $("#_harga_jual").val();
            $("#_val_harga_jual").html(change_currency(harga_jual));
        });


        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function render_data(data){
            $("#out").html(data);
        }
    //=========================================================================//
    //-----------------------------------master_function-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//
        $("#add_data").click(function(){
            var data_main = new FormData();
                data_main.append('id_brand'     , $("#id_brand").val());
                data_main.append('nama_item'    , $("#nama_item").val());
                data_main.append('satuan'       , $("#satuan").val());
                data_main.append('harga_bruto'  , $("#harga_bruto").val());
                data_main.append('harga_netto'  , $("#harga_netto").val());
                data_main.append('harga_jual'   , $("#harga_jual").val());

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/insert_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_insert(res.responseText);
                }
            });
        });

        $("#reset").click(function(){
            clear_form();
        });


        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form();
            } else {
                
                $("#msg_id_brand").html(detail_msg.id_brand);
                $("#msg_nama_item").html(detail_msg.nama_item);
                $("#msg_satuan").html(detail_msg.satuan);
                $("#msg_harga_bruto").html(detail_msg.harga_bruto);
                $("#msg_harga_netto").html(detail_msg.harga_netto);
                $("#msg_harga_jual").html(detail_msg.harga_jual);
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_insert---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//
        function update_data(id_data){
            var data_main = new FormData();
                data_main.append('id_item', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/get_data_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_set_update(res.responseText);

                    id_global = id_data;
                }
            });
        }

        function response_set_update(res){
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;

            if (main_msg.status) {
                $("#update_data").modal("show");

                $("#_id_brand").val(detail_msg.data.id_brand);
                $("#_nama_item").val(detail_msg.data.nama_item);
                $("#_satuan").val(detail_msg.data.satuan);
                $("#_harga_bruto").val(detail_msg.data.harga_bruto);
                $("#_harga_netto").val(detail_msg.data.harga_netto);
                $("#_harga_jual").val(detail_msg.data.harga_jual);

                $("#_val_harga_bruto").html(change_currency(detail_msg.data.harga_bruto));
                $("#_val_harga_netto").html(change_currency(detail_msg.data.harga_netto));
                $("#_val_harga_jual").html(change_currency(detail_msg.data.harga_jual));


                $("#_id_brand").focus();

            } else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_update----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function(){
            var data_main = new FormData();
                data_main.append('id_item'         , id_global);

                data_main.append('id_brand'     , $("#_id_brand").val());
                data_main.append('nama_item'    , $("#_nama_item").val());
                data_main.append('satuan'       , $("#_satuan").val());
                data_main.append('harga_bruto'  , $("#_harga_bruto").val());
                data_main.append('harga_netto'  , $("#_harga_netto").val());
                data_main.append('harga_jual'   , $("#_harga_jual").val());

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/update_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_update(res.responseText);
                }
            });
        });

        $("#btn_update_reset").click(function(){
            clear_form_update();
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $("#update_data").modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_id_brand").html(detail_msg.id_brand);
                $("#_msg_nama_item").html(detail_msg.nama_item);
                $("#_msg_satuan").html(detail_msg.satuan);
                $("#_msg_harga_bruto").html(detail_msg.harga_bruto);
                $("#_msg_harga_netto").html(detail_msg.harga_netto);
                $("#_msg_harga_jual").html(detail_msg.harga_jual);

                // change_harga_bruto();
                // change_harga_netto();
                // change_harga_jual();
                $("#_val_harga_bruto").html(change_currency($("#_harga_bruto").val()));
                $("#_val_harga_netto").html(change_currency($("#_harga_netto").val()));
                $("#_val_harga_jual").html(change_currency($("#_harga_jual").val()));
                
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------form_update---------------------------//
    //=========================================================================//        

    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//
        function delete_data(id_data){
            var data_main = new FormData();
                data_main.append('id_item', id_data);

            $.ajax({
                url: "<?php echo base_url()."admin/itemmain/delete_item";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    
                    
                },
                complete: function(res){
                    console.log(res.responseText);
                    response_delete(res.responseText);
                }
            });
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------delete_data---------------------------//
    //=========================================================================//   
    
</script>