<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Suplier</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Suplier</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_suplier"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Suplier</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">Kode Suplier</th>
                                            <th width="15%">Nama Suplier</th>
                                            <th width="15%">Jenis Suplier</th>
                                            <th width="15%">Email Suplier</th>
                                            <th width="15%">Tlp Suplier</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r_suplier => $v_suplier) {
                                                    $str_jn_sup = "Pedagang Besar Farmasi";
                                                    switch ($v_suplier->jenis_suplier) {
                                                        case 'pbf':
                                                            $str_jn_sup = "Pedagang Besar Farmasi";
                                                            break;

                                                        case 'pabrik':
                                                            $str_jn_sup = "Pabrik";
                                                            break;

                                                        default:
                                                            $str_jn_sup = "";
                                                            break;
                                                    }
                                                    echo "<tr>
                                                        <td>".($r_suplier+1)."</td>
                                                        <td>".$v_suplier->id_suplier."</td>
                                                        <td>".$v_suplier->nama_suplier."</td>
                                                        <td>".$str_jn_sup."</td>
                                                        <td>".$v_suplier->email_suplier."</td>
                                                        <td>".$v_suplier->tlp_suplier."</td>
                                                        <td>
                                                            <center>
                                                            <button class=\"btn btn-info\" id=\"up_suplier\" onclick=\"update_suplier('".$v_suplier->id_suplier."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_suplier\" onclick=\"delete_suplier('".$v_suplier->id_suplier."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_suplier------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_suplier" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_suplier";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Suplier</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Suplier <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_suplier" name="nama_suplier" required="">
                                <p id="msg_nama_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Suplier<span style="color: red;">*</span></label>
                                <select class="form-control" id="jenis_suplier" name="jenis_suplier" required="">
                                    <option value="pabrik">Pabrik</option>
                                    <option value="pbf">Pedagang Besar Farmasi</option>
                                </select>
                                <p id="msg_jenis_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Email Suplier <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" id="email_suplier" name="email_suplier" required="">
                                <p id="msg_email_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Suplier <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="tlp_suplier" name="tlp_suplier" required="">
                                <p id="msg_tlp_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kantor Suplier<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_ktr_suplier" name="alamat_ktr_suplier" required="">
                                <p id="msg_alamat_ktr_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kirim Suplier <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="alamat_krm_suplier" name="alamat_krm_suplier" required="">
                                <p id="msg_alamat_krm_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Website<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="website" name="website" required="">
                                <p id="msg_website" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_suplier" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_suplier------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_suplier------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_suplier" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Suplier</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_suplier";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Suplier <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_suplier" name="nama_suplier" required="">
                                <p id="_msg_nama_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Jenis Suplier<span style="color: red;">*</span></label>
                                <select class="form-control" id="_jenis_suplier" name="_jenis_suplier" required="">
                                    <option value="pabrik">Pabrik</option>
                                    <option value="pbf">Pedagang Besar Farmasi</option>
                                </select>
                                <p id="_msg_jenis_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Email Suplier <span style="color: red;">*</span></label>
                                <input type="email" class="form-control" id="_email_suplier" name="email_suplier" required="">
                                <p id="_msg_email_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Telp Suplier <span style="color: red;">*</span></label>
                                <input type="number" class="form-control" id="_tlp_suplier" name="tlp_suplier" required="">
                                <p id="_msg_tlp_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kantor Suplier<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_ktr_suplier" name="alamat_ktr_suplier" required="">
                                <p id="_msg_alamat_ktr_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat Kirim Suplier <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_alamat_krm_suplier" name="alamat_krm_suplier" required="">
                                <p id="_msg_alamat_krm_suplier" style="color: red;"></p>
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">Website<span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_website" name="website" required="">
                                <p id="_msg_website" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                

            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_suplier" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_suplier------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert_suplier--------------------------//
    //=========================================================================//
        $("#add_suplier").click(function() {
            var data_main = new FormData();
            data_main.append('nama_suplier'           , $("#nama_suplier").val());
            data_main.append('jenis_suplier'           , $("#jenis_suplier").val());
            data_main.append('email_suplier'          , $("#email_suplier").val());
            data_main.append('tlp_suplier'            , $("#tlp_suplier").val());
            data_main.append('alamat_ktr_suplier'     , $("#alamat_ktr_suplier").val());
            data_main.append('alamat_krm_suplier'     , $("#alamat_krm_suplier").val());
            data_main.append('website'                , $("#website").val());
           
            $.ajax({
                url: "<?php echo base_url()."admin/supliermain/insert_suplier";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_suplier').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_insert();
            } else {
                $("#msg_nama_suplier").html(detail_msg.nama_suplier);
                $("#msg_jenis_suplier").html(detail_msg.jenis_suplier);
                $("#msg_email_suplier").html(detail_msg.email_suplier);
                $("#msg_tlp_suplier").html(detail_msg.tlp_suplier);
                $("#msg_alamat_krm_suplier").html(detail_msg.alamat_krm_suplier);
                $("#msg_alamat_ktr_suplier").html(detail_msg.alamat_ktr_suplier);
                $("#msg_website").html(detail_msg.website);
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }

        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function clear_form_insert(){
            // $("#id_tipe_admin").val("0");
            $("#nama_suplier").val("");
            $("#jenis_suplier").val("");
            $("#email_suplier").val("");
            $("#tlp_suplier").val("");
            $("#alamat_ktr_suplier").val("");
            $("#alamat_krm_suplier").val("");
            $("#website").val("");

            $("#msg_nama_suplier").html("");
            $("#msg_jenis_suplier").html("");
            $("#msg_email_suplier").html("");
            $("#msg_tlp_suplier").html("");
            $("#msg_alamat_ktr_suplier").html("");
            $("#msg_alamat_krm_suplier").html("");
            $("#msg_website").html("");
            
        }

        function render_data(data){
            var str_table = "";
            var no = 1;
            for (let i in data) {

                var str_jn_sup = "Pedagang Besar Farmasi";
                switch (data[i].jenis_suplier) {
                    case 'pbf':
                        str_jn_sup = "Pedagang Besar Farmasi";
                        break;

                    case 'pabrik':
                        str_jn_sup = "Pabrik";
                        break;

                    default:
                        str_jn_sup = "";
                        break;
                }
                
                str_table += "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td>"+data[i].id_suplier+"</td>"+
                    "<td>"+data[i].nama_suplier+"</td>"+
                    "<td>"+str_jn_sup+"</td>"+
                    "<td>"+data[i].email_suplier+"</td>"+
                    "<td>"+data[i].tlp_suplier+"</td>"+
                    "<td>"+
                        "<center>"+
                        "<button class=\"btn btn-info\" id=\"up_suplier\" onclick=\"update_suplier('"+data[i].id_suplier+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                        "<button class=\"btn btn-danger\" id=\"del_suplier\" onclick=\"delete_suplier('"+data[i].id_suplier+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                        "</center>"+
                    "</td>"+
                    "</tr>";
                    no++;
                // str_table += "";
            }
            $("#main_table_content").html(str_table);
        }

        function alert_confirm(title, msg, status, txt_cencel_btn, id_suplier){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: title,
                        text: msg,
                        type: status,
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: txt_cencel_btn,
                        closeOnConfirm: false
                    }, function() {
                        method_delete(id_suplier);
                        swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
    //=========================================================================//
    //-----------------------------------insert_suplier--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_suplier_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nama_suplier").val("");
            $("#_jenis_suplier").val("");
            $("#_email_suplier").val("");
            $("#_tlp_suplier").val("");
            $("#_alamat_ktr_suplier").val("");
            $("#_alamat_krm_suplier").val("");
            $("#_website").val("");

            $("#_msg_nama_suplier").html("");
            $("#_msg_jenis_suplier").html("");
            $("#_msg_email_suplier").html("");
            $("#_msg_tlp_suplier").html("");
            $("#_msg_alamat_ktr_suplier").html("");
            $("#_msg_alamat_krm_suplier").html("");
            $("#_msg_website").html("");
        }

        function update_suplier(id_suplier) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_suplier', id_suplier);

            $.ajax({
                url: "<?php echo base_url()."admin/supliermain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_suplier);
                    $("#update_suplier").modal('show');
                }
            });
        }

        function set_val_update(res, id_suplier) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_suplier;

                $("#_nama_suplier").val(list_data.nama_suplier);
                $("#_jenis_suplier").val(list_data.jenis_suplier);
                $("#_email_suplier").val(list_data.email_suplier);
                $("#_tlp_suplier").val(list_data.tlp_suplier);
                $("#_alamat_ktr_suplier").val(list_data.alamat_ktr_suplier);
                $("#_alamat_krm_suplier").val(list_data.alamat_krm_suplier);
                $("#_website").val(list_data.website);
                
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_suplier_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_suplier").click(function() {
            var data_main = new FormData();
            data_main.append('id_suplier', id_cache);

            data_main.append('nama_suplier'         , $("#_nama_suplier").val());
            data_main.append('jenis_suplier'         , $("#_jenis_suplier").val());
            data_main.append('email_suplier'        , $("#_email_suplier").val());
            data_main.append('tlp_suplier'          , $("#_tlp_suplier").val());
            data_main.append('alamat_ktr_suplier'   , $("#_alamat_ktr_suplier").val());
            data_main.append('alamat_krm_suplier'   , $("#_alamat_krm_suplier").val());
            data_main.append('website'              , $("#_website").val());
            

            $.ajax({
                url: "<?php echo base_url()."admin/supliermain/update_suplier";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_suplier').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_nama_suplier").html(detail_msg.nama_suplier);
                $("#_msg_jenis_suplier").html(detail_msg.jenis_suplier);
                $("#_msg_email_suplier").html(detail_msg.email_suplier);
                $("#_msg_tlp_suplier").html(detail_msg.tlp_suplier);
                $("#_msg_alamat_ktr_suplier").html(detail_msg.alamat_ktr_suplier);
                $("#_msg_alamat_krm_suplier").html(detail_msg.alamat_krm_suplier);
                $("#_msg_website").html(detail_msg.website);
                

                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------update_suplier--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------suplier_delete--------------------------//
    //=========================================================================//

        function method_delete(id_suplier){
            var data_main = new FormData();
            data_main.append('id_suplier', id_suplier);

            $.ajax({
                url: "<?php echo base_url()."admin/supliermain/delete_suplier";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_suplier(id_suplier) {
            alert_confirm("Pesan Konfirmasi.!!", "Hapus ?", "warning", "Hapus", id_suplier);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------delete suplier-------------------------//
    //=========================================================================//

    
</script>