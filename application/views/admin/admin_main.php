<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Data Admin</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data Admin</h4>

                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data Admin</button>

                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="15%">Username</th>
                                            <th width="15%">Nama Admin</th>
                                            <th width="15%">Tipe Admin</th>
                                            <th width="10%">Password Admin</th>
                                            <th width="10%">Status Admin</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r_admin => $v_admin) {
                                                    $str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                                                        if($v_admin->status_active == "1"){
                                                            $str_active = "<span class=\"label label-info\">aktif</span>";
                                                        }elseif ($v_admin->status_active == "2") {
                                                            $str_active = "<span class=\"label label-danger\">di blockir</span>";
                                                        }

                                                    $str_lv = "Super Admin";
                                                        if($v_admin->id_tipe_admin == "1"){
                                                            $str_lv = "Admin Frontliner";
                                                        }

                                                    $str_btn_active = "<button class=\"btn btn-primary\" id=\"un_ac_admin\" onclick=\"disabled_admin('".$v_admin->id_admin."')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                        if($v_admin->status_active != "1"){
                                                            $str_btn_active = "<button class=\"btn btn-success\" id=\"ac_admin\" onclick=\"active_admin('".$v_admin->id_admin."')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                        }

                                                    $str_btn_ch_pass = "<button class=\"btn btn-info\" id=\"btn_ch_pass\" onclick=\"ch_pass('".$v_admin->id_admin."')\">Ubah Password</button>";

                                                    echo "<tr>
                                                        <td>".($r_admin+1)."</td>
                                                        <td>".$v_admin->username."</td>
                                                        <td>".$v_admin->nama_admin."</td>
                                                        <td>".$str_lv."</td>
                                                        <td>".$str_btn_ch_pass."</td>
                                                        <td>".$str_active."</td>
                                                        
                                                        
                                                        <td>
                                                            <center>
                                                            ".$str_btn_active."
                                                            <button class=\"btn btn-info\" id=\"up_admin\" onclick=\"update_admin('".$v_admin->id_admin."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                            <button class=\"btn btn-danger\" id=\"del_admin\" onclick=\"delete_admin('".$v_admin->id_admin."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                            </center>
                                                        </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="ch_pass" name="pass" required="">
                                <p id="_msg_ch_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="ch_repass" name="repass" required="">
                                <p id="_msg_ch_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tipe Admin <span style="color: red;">*</span></label>
                                <select class="form-control" id="id_tipe_admin" name="id_tipe_admin">
                                    <option value="0">Admin</option>
                                    <option value="1">Frontliner</option>
                                </select>
                                <p id="msg_id_tipe_admin" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="email" name="email" required="">
                                <p id="msg_email" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Admin <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_admin" name="nama_admin" required="">
                                <p id="msg_nama_admin" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6" hidden="">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nomor Induk Pegawai <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nip_admin" name="nip_admin" value="a" readonly="" required="">
                                <p id="msg_nip_admin" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="username" name="username" required="">
                                <p id="msg_username" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Admin</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Tipe Admin <span style="color: red;">*</span></label>
                                <select class="form-control" id="_id_tipe_admin" name="id_tipe_admin">
                                    <option value="0">Admin</option>
                                    <option value="1">Frontliner</option>
                                </select>
                                <p id="_msg_id_tipe_admin" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_email" name="email" required="">
                                <p id="_msg_email" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama Admin <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_admin" name="nama_admin" required="">
                                <p id="_msg_nama_admin" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6" hidden="">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nomor Induk Pegawai <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nip_admin" name="nip_admin" value="a" readonly="" required="">
                                <p id="_msg_nip_admin" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_username" name="username" required="">
                                <p id="_msg_username" style="color: red;"></p>
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="_pass" name="pass" required="">
                                <p id="_msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="_repass" name="repass" required="">
                                <p id="_msg_repass" style="color: red;"></p>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_admin" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            data_main.append('id_tipe_admin', $("#id_tipe_admin").val());
            data_main.append('email'        , $("#email").val());
            data_main.append('username'     , $("#username").val());
            data_main.append('password'     , $("#pass").val());
            data_main.append('repassword'   , $("#repass").val());

            data_main.append('nama_admin'   , $("#nama_admin").val());
            data_main.append('nip_admin'    , $("#nip_admin").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/insert_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_insert();
            } else {
                $("#msg_id_tipe_admin").html(detail_msg.id_tipe_admin);
                $("#msg_email").html(detail_msg.email);
                $("#msg_username").html(detail_msg.username);
                $("#msg_pass").html(detail_msg.pass);
                $("#msg_repass").html(detail_msg.repass);
                $("#msg_nama_admin").html(detail_msg.nama_admin);
                $("#msg_nip_admin").html(detail_msg.nip_admin);

                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }

        function create_alert(title, msg, status){
            $(function() {
                "use strict";                      
               $.toast({
                heading: title,
                text: msg,
                position: 'top-right',
                loaderBg:'#ff6849',
                icon: status,
                hideAfter: 3500, 
                stack: 6
              });
            });
        }

        function clear_form_insert(){
            // $("#id_tipe_admin").val("0");
            $("#email").val("");
            $("#username").val("");
            $("#pass").val("");
            $("#repass").val("");
            $("#nama_admin").val("");
            $("#nip_admin").val("");

            $("#msg_id_tipe_admin").html("");
            $("#msg_email").html("");
            $("#msg_username").html("");
            $("#msg_pass").html("");
            $("#msg_repass").html("");
            $("#msg_nama_admin").html("");
            $("#msg_nip_admin").html("");
        }

        function render_data(data){
            var str_table = "";
            var no = 1;
            for (let i in data) {
                var str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                    if(data[i].status_active == "1"){
                        str_active = "<span class=\"label label-info\">aktif</span>";
                    }else if (data[i].status_active == "2") {
                        str_active = "<span class=\"label label-danger\">di blockir</span>";
                    }

                var str_lv = "Super Admin";
                    if(data[i].id_tipe_admin == "1"){
                        str_lv = "Admin Frontliner";
                    }

                var str_btn_active = "<button class=\"btn btn-primary\" id=\"un_ac_admin\" onclick=\"disabled_admin('"+data[i].id_admin+"')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                    if(data[i].status_active != "1"){
                        str_btn_active = "<button class=\"btn btn-success\" id=\"ac_admin\" onclick=\"active_admin('"+data[i].id_admin+"')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                    }

                 var str_btn_ch_pass = "<button class=\"btn btn-info\" id=\"btn_ch_pass\" onclick=\"ch_pass('"+data[i].id_admin+"')\">Ubah Password</button>";

                str_table += "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td>"+data[i].username+"</td>"+
                    "<td>"+data[i].nama_admin+"</td>"+
                    "<td>"+str_lv+"</td>"+
                    "<td>"+str_btn_ch_pass+"</td>"+
                    "<td>"+str_active+"</td>"+
                    "<td>"+
                        "<center>"+
                        str_btn_active+
                        "<button class=\"btn btn-info\" id=\"up_admin\" onclick=\"update_admin('"+data[i].id_admin+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                        "<button class=\"btn btn-danger\" id=\"del_admin\" onclick=\"delete_admin('"+data[i].id_admin+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                        "</center>"+
                    "</td>"+
                    "</tr>";
                    no++;
                // str_table += "";
            }
            $("#main_table_content").html(str_table);
        }

        function alert_confirm(title, msg, status, txt_cencel_btn, id_admin){
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: title,
                        text: msg,
                        type: status,
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: txt_cencel_btn,
                        closeOnConfirm: false
                    }, function() {
                        method_delete(id_admin);
                        swal.close();
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_email").val("");
            $("#_username").val("");
            // $("#_pass").val("");
            // $("#_repass").val("");
            $("#_nama_admin").val("");
            $("#_nip_admin").val("");

            $("#_msg_id_tipe_admin").html("");
            $("#_msg_email").html("");
            $("#_msg_username").html("");
            // $("#_msg_pass").html("");
            // $("#_msg_repass").html("");
            $("#_msg_nama_admin").html("");
            $("#_msg_nip_admin").html("");
        }

        function update_admin(id_admin) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_admin);
                    $("#update_admin").modal('show');
                }
            });
        }

        function set_val_update(res, id_admin) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_admin;

                $("#_id_tipe_admin").val(list_data.id_tipe_admin);
                $("#_email").val(list_data.email);
                $("#_username").val(list_data.username);
                // $("#_pass").val(detail_msg.pas);
                // $("#_repass").val(detail_msg.id_admin);
                $("#_nama_admin").val(list_data.nama_admin);
                $("#_nip_admin").val(list_data.nip_admin);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_admin").click(function() {
            var data_main = new FormData();
            data_main.append('id_admin', id_cache);

            data_main.append('id_tipe_admin', $("#_id_tipe_admin").val());
            data_main.append('email'        , $("#_email").val());
            data_main.append('username'     , $("#_username").val());
            data_main.append('password'     , $("#_pass").val());
            data_main.append('repassword'   , $("#_repass").val());

            data_main.append('nama_admin'   , $("#_nama_admin").val());
            data_main.append('nip_admin'    , $("#_nip_admin").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/update_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_admin').modal('toggle');
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_form_update();
            } else {
                $("#_msg_id_tipe_admin").html(detail_msg.id_tipe_admin);
                $("#_msg_email").html(detail_msg.email);
                $("#_msg_username").html(detail_msg.username);
                $("#_msg_pass").html(detail_msg.pass);
                $("#_msg_repass").html(detail_msg.repass);
                $("#_msg_nama_admin").html(detail_msg.nama_admin);
                $("#_msg_nip_admin").html(detail_msg.nip_admin);

                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_admin){
            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/delete_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_admin(id_admin) {
            alert_confirm("Pesan Konfirmasi.!!", "Hapus ?", "warning", "Hapus", id_admin);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_admin', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/change_pass_admin";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_admin_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
                $("#_msg_ch_pass").val(detail_msg.ch_pass);
                $("#_msg_ch_repass").val(detail_msg.ch_repass);
            }
        }

        function ch_pass(id_admin) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_admin;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------dasabled_admin------------------------//
    //=========================================================================//
        function disabled_admin(id_admin) {
            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/disabled_admin/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_disabled(res);
                }
            });
        }

        function response_disabled(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }   
    //=========================================================================//
    //-----------------------------------dasabled_admin------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------activate_admin------------------------//
    //=========================================================================//

        function active_admin(id_admin) {
            
            var data_main = new FormData();
            data_main.append('id_admin', id_admin);

            $.ajax({
                url: "<?php echo base_url()."admin/adminmain/activate_admin/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_active_admin(res);
                }
            });
                       
        }

        function response_active_admin(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_alert('Proses Berhasil', main_msg.msg, 'success');
                render_data(detail_msg.list_data);
            } else {
                create_alert('Proses Gagal', main_msg.msg, 'error');
            }
        }
    //=========================================================================//
    //-----------------------------------activate_admin------------------------//
    //=========================================================================//
</script>