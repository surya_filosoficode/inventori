<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Format BPOM</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card-outline-default">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="m-b-0">Filter Laporan Format BPOM</h4>
                        </div>
                        <div class="col-md-4 text-right">
                            <select class="form-control custom-select">
                                <option value="Per Tanggal">Per Tanggal</option>
                                <option value="Per Bulan">Per Bulan</option>
                                <option value="Per 3 Bulan">Per 3 Bulan</option>
                                <option value="Per Tahun">Per Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" class="form-horizontal">
                            <div class="col-12">
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tanggal Mulai</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="tgl_awal" name="tgl_awal" />
                                        <p id="msg_tgl_awal" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tanggal Akhir</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="tgl_akhir" name="tgl_akhir" />
                                        <p id="msg_tgl_akhir" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Bulan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="bulan" name="bulan" >
                                            <option value="Januari">Januari</option>
                                            <option value="Februari">Februari</option>
                                            <option value="Maret">Maret</option>
                                            <option value="April">April</option>
                                            <option value="Mei">Mei</option>
                                            <option value="Juni">Juni</option>
                                            <option value="Juli">Juli</option>
                                            <option value="Agustus">Agustus</option>
                                            <option value="September">September</option>
                                            <option value="Oktober">Oktober</option>
                                            <option value="November">November</option>
                                            <option value="Desember">Desember</option>
                                        </select>
                                        <p id="msg_bulan" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tahun" name="tahun" />
                                        <p id="msg_tahun" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Periode</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="periode" name="periode">
                                            <option value="triwulan 1">Triwulan 1</option>
                                        </select>
                                        <p id="msg_periode" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun" name="tahun" />
                                        <p id="msg_tahun" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun Mulai</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun_awal" name="tahun_awal" />
                                        <p id="msg_tahun_awal" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun Akhir</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun_akhir" name="tahun_akhir" />
                                        <p id="msg_tahun_akhir" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="modal-footer">
                                <div class="form-group" style="padding-top:23px;">
                                    <button type="button" id="proses" name="proses" class="btn waves-effect btn-rounded waves-light btn-info">Proses</button>
                                </div>
                              </div>
                            </div>
                        </form>
                        <div class="row" id="report">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <hr />
                                    <a href="#" class="btn btn-info"><i class="fa fa-print"></i> PRINT</a> 
                                    <a href="#" class="btn btn-info"><i class="fa fa-file-excel-o"></i> EXCEL</a>
                                    <hr />
                                </div>
                                <div class="col-12 text-center" style="padding-bottom: 25px;">
                                    <h3>REKAPITULASI LAPORAN OBAT PERIODE TRIWULA 1 2019</h3>

                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-sm-1">Nama</div>
                                        <div class="col-sm-11">: BLESSINDO FARMA (Pusat)</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1">No. Izin</div>
                                        <div class="col-sm-11">: HK.07.01/V/453/14</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-1">Alamat</div>
                                        <div class="col-sm-11">: JL. MAYJEN SUNGKONO KOMPLEKS WONOKRITI INDAH S-33 SURABAYA, Kota Surabaya - Jawa Timur </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="col-md-12" style="font-size: 8px; font-weight: 300;">
                                    <table border="1" class="table table-bordered table-striped" style="width:100%;"  >
                                        <thead>
                                            <tr>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">NO.</th>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">PERIODE</th>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">KODE OBAT(NIE)</th>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">NAMA OBAT</th>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">STOK AWAL</th>
                                                <th style="font-weight: bold; text-align: center;" colspan="4">PEMASUKAN</th>
                                                <th style="font-weight: bold; text-align: center;" colspan="9">PENGELUARAN</th>
                                                <th valign="top" style="font-weight: bold;" rowspan="2">STOK AKHIR</th>
                                            </tr>
                                            <tr>
                                                <th>PABRIK</th>
                                                <th>PBF</th>
                                                <th>RETUR</th>
                                                <th>LAINNYA</th>
                                                <th>RUMAH SAKIT</th>
                                                <th>APOTIK</th>
                                                <th>PBF</th>
                                                <th>SARANA PEMERINTAH</th>
                                                <th>PUSKESMAS</th>
                                                <th>KLINIK</th>
                                                <th>TOKO OBAT</th>
                                                <th>RETUR</th>
                                                <th>LAINNYA</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1.</td>
                                                <td>2019-01</td>
                                                <td>DKL.8907435678</td>
                                                <td>Albuvit 10%, tetes mata botol plastik @5ml</td>
                                                <td>19.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>0.00</td>
                                                <td>19.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";
    $(document).ready(function() {
        $("#report").hide();
        
    });
    $("#proses").click(function() {
        $("#report").show();
    });
</script>
