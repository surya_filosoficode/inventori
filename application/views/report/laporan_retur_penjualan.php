<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Retur Penjualan</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card-outline-default">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-8">
                            <h4 class="m-b-0">Filter Laporan Retur Penjualan</h4>
                        </div>
                        <div class="col-md-4 text-right">
                            <select class="form-control custom-select">
                                <option value="Per Tanggal">Per Tanggal</option>
                                <option value="Per Bulan">Per Bulan</option>
                                <option value="Per 3 Bulan">Per 3 Bulan</option>
                                <option value="Per Tahun">Per Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <form method="post" class="form-horizontal">
                            <div class="col-12">
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tanggal Mulai</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="tgl_awal" name="tgl_awal" />
                                        <p id="msg_tgl_awal" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tanggal Akhir</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="date" class="form-control" id="tgl_akhir" name="tgl_akhir" />
                                        <p id="msg_tgl_akhir" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Bulan</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="bulan" name="bulan" >
                                            <option value="Januari">Januari</option>
                                            <option value="Februari">Februari</option>
                                            <option value="Maret">Maret</option>
                                            <option value="April">April</option>
                                            <option value="Mei">Mei</option>
                                            <option value="Juni">Juni</option>
                                            <option value="Juli">Juli</option>
                                            <option value="Agustus">Agustus</option>
                                            <option value="September">September</option>
                                            <option value="Oktober">Oktober</option>
                                            <option value="November">November</option>
                                            <option value="Desember">Desember</option>
                                        </select>
                                        <p id="msg_bulan" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="tahun" name="tahun" />
                                        <p id="msg_tahun" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Periode</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" id="periode" name="periode">
                                            <option value="triwulan 1">Triwulan 1</option>
                                        </select>
                                        <p id="msg_periode" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun" name="tahun" />
                                        <p id="msg_tahun" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun Mulai</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun_awal" name="tahun_awal" />
                                        <p id="msg_tahun_awal" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Tahun Akhir</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="input" class="form-control" id="tahun_akhir" name="tahun_akhir" />
                                        <p id="msg_tahun_akhir" style="color: red;"></p>
                                    </div>
                                </div> 
                              </div>
                              <div class="modal-footer">
                                <div class="form-group" style="padding-top:23px;">
                                    <button type="button" id="proses" name="proses" class="btn waves-effect btn-rounded waves-light btn-info">Proses</button>
                                </div>
                              </div>
                            </div>
                        </form>
                        <div class="row" id="report">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <hr />
                                    <a href="#" class="btn btn-info"><i class="fa fa-print"></i> PRINT</a> 
                                    <a href="#" class="btn btn-info"><i class="fa fa-file-excel-o"></i> EXCEL</a>
                                    <hr />
                                </div>
                                <div class="col-12 text-center">
                                    <h3>LAPORAN RETUR PENJUALAN</h3>
                                    <h4>Periode .....</h4>
                                     <span>Per-Tanggal ......</span>
                                </div>
                                <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                    <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                        <thead>
                                            <tr>
                                                <th style="font-weight: bold;">Tanggal</th>
                                                <th style="font-weight: bold;">No Faktur</th>
                                                <th style="font-weight: bold;">Pelanggan</th>
                                                <th style="font-weight: bold;">Kode Produksi</th>
                                                <th style="font-weight: bold;">Jenis Obat</th>
                                                <th style="font-weight: bold;">Nama Obat</th>
                                                <th style="font-weight: bold;">Jumlah</th>
                                                <th style="font-weight: bold;">Total Retur</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>2019-07-19</td>
                                                <td>1ecfb3b22c098a26</td>
                                                <td>Apotek ABC</td>
                                                <td>001</td>
                                                <td>Omega</td>
                                                <td>Vit B Compex</td>
                                                <td>xxx</td>
                                                <td align="right">Rp. xxx</td>
                                            </tr>
                                            <!-- <tr v-if="!haveData"><td colspan="12" align="center"><i>* Tidak ada data transaksi untuk periode tersebut *</i></td></tr> -->
                                            <tr v-if="haveData" style="font-weight:bold;">
                                              <td colspan="7">Grand Total</td> 
                                              <td align="right">Rp. xxx</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";
    $(document).ready(function() {
        $("#report").hide();
        
    });
    $("#proses").click(function() {
        $("#report").show();
    });
</script>
