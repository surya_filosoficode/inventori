<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Laporan Penjualan Obat Berdasarkan Pelanggan</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<?php
    $str_jenis = "";
    switch ($this->uri->segment(3)) {
        case 'get_penjualan_customer_tgl':
            $str_jenis = "per_tgl";
            break;
        
        case 'get_penjualan_customer_bulan':
            $str_jenis = "per_bulan";
            break;

        case 'get_penjualan_customer_triwulan':
            $str_jenis = "tiga_bulan";
            break;

        case 'get_penjualan_customer_th':
            $str_jenis = "pertahun";
            break;

        default:
            $str_jenis = "per_tgl";
            break;
    }

    $str_param1 = $this->uri->segment(4);
    $str_param2 = $this->uri->segment(5);
    $str_param3 = $this->uri->segment(6);

    if($str_param1 == ""){
        $str_param1 = date("Y-m-d");
    }

    if($str_param2 == ""){
        $str_param2 = date("Y-m-d");
    }
?>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Pelanggan</h4> -->
                    <div class="row">
                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Filter Laporan Penjualan Obat Berdasarkan Pelanggan</h4>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select col-12" id="jenis_filter" name="jenis_filter">
                                <option value="per_tgl">Per Tanggal</option>
                                <option value="per_bulan">Per Bulan</option>
                                <option value="tiga_bulan">Tiga Bulanan</option>
                                <option value="pertahun">Per Tahun</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="out_param_filter">
                        <div class="col-md-12" id="row_per_tgl">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tanggal Mulai</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="date" id="tgl_start" name="tgl_start">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tanggal Akhir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="date" id="tgl_finish" name="tgl_finish">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_per_bulan">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Bulan</label>
                                        <div class="col-md-9">
                                            <select class="custom-select col-12" id="bulan" name="bulan">
                                                <option value="1">Januari</option>
                                                <option value="2">Februari</option>
                                                <option value="3">Maret</option>
                                                <option value="4">April</option>
                                                <option value="5">Mei</option>
                                                <option value="6">Juni</option>
                                                <option value="7">Juli</option>
                                                <option value="8">Agustus</option>
                                                <option value="9">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th" name="th">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_tiga_bulan">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Periode</label>
                                        <div class="col-md-9">
                                            <select class="custom-select col-12" id="triwulan" name="triwulan">
                                                <option value="1-3">Triwulan I</option>
                                                <option value="4-6">Triwulan II</option>
                                                <option value="7-9">Triwulan III</option>
                                                <option value="10-12">Triwulan IV</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_triwulan" name="th_triwulan">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>

                        <div class="col-md-12" id="row_pertahun">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun Awal</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_start" name="th_start">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Tahun Akhir</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="number" id="th_finish" name="th_finish">
                                        </div>
                                    </div>
                                </div>
                            </div>       
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="example-search-input" class="col-md-3 col-form-label">Nama Pelanggan</label>
                                        <div class="col-md-9">
                                            <select class="select2 form-control custom-select" id="customer" name="customer">
                                                <?php
                                                if(isset($customer)){
                                                    foreach ($customer as $key => $value) {
                                                        print_r("<option value=\"".$value->id_rekanan."\">".$value->nama_rekanan."</option>");
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="button" id="lanjut" name="lanjut" class="btn waves-effect waves-light btn-rounded btn-info">Terapkan</button>
                                </div>
                            </div>       
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->

    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card">
                <div class="card-header">
                    <!-- <h4 class="m-b-0 text-white">Laporan Penjualan Obat Berdasarkan Pelanggan</h4> -->
                    <div class="row">

                        <div class="col-md-9">
                            <h4 class="m-b-0 text-black">Tabel Laporan Penjualan Obat Berdasarkan Pelanggan</h4>
                        </div>
                        <div class="col-md-3 text-right">
                            <button class="btn btn-info" id="btn_print" name="btn_print"><i class="fa fa-print"></i> PRINT</button> 
                            <button class="btn btn-info" id="btn_excel" name="btn_excel"><i class="fa fa-file-excel-o"></i> EXCEL</button>
                        </div>                        
                    </div>
                </div>
                <div class="card-body">
                    <div class="row" id="report">
                        <div class="col-md-12">
                            <div class="col-12 text-center">
                                <h3>LAPORAN TRANSAKSI PENJUALAN OBAT BERDASARKAN PELANGGAN</h3>
                                <h4><?php print_r($str_periode);?></h4>
                                 <!-- <span>Per-Tanggal ......</span> -->
                            </div>
                            <div class="col-md-12" style="font-size: 11px; font-weight: 300;">
                                <table id="myTable" class="table table-bordered table-striped" style="width:100%" >
                                    <thead>
                                        <tr>
                                            <th style="font-weight: bold;">Tanggal</th>
                                            <th style="font-weight: bold;">No Faktur</th>
                                            <th style="font-weight: bold;">Pelanggan</th>
                                            <th style="font-weight: bold;">Id Produk</th>
                                            <th style="font-weight: bold;">Nama Produk</th>
                                            <th style="font-weight: bold;">Kode Produksi</th>
                                            <th style="font-weight: bold;">Jumlah Produk</th>
                                            <th style="font-weight: bold;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                        if(isset($list_data)){
                                            foreach ($list_data as $key => $value) {
                                                print_r("
                                                        <tr>
                                                            <td>".$value->tgl_transaksi_tr_header."</td>
                                                            <td>".$value->id_tr_header."</td>
                                                            <td>".$value->nama_rekanan."</td>
                                                            <td>".$value->id_item."</td>
                                                            <td>".$value->nama_item."</td>
                                                            <td>".$value->kode_produksi_item."</td>
                                                            <td align=\"right\">".number_format($value->jml_item_tr_detail, 2, ',', '.')."</td>
                                                            <td align=\"right\">Rp. ".number_format($value->total_pembayaran_pnn_tr_header, 2, ',', '.')."</td>
                                                        </tr>
                                                    ");
                                            }
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script type="text/javascript">
    $(document).ready(function(){
        set_default_value();
        set_default();
        filter_show();

        console.log("");
    });

    function set_default(){
        $("#jenis_filter").val("<?php print_r($str_jenis);?>");
        $("#customer").val("<?php print_r($str_param3);?>");

        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                $("#tgl_start").val("<?php print_r($str_param1);?>");
                $("#tgl_finish").val("<?php print_r($str_param2);?>");
            break;
          case "per_bulan":
                $("#bulan").val("<?php print_r($str_param1);?>");
                $("#th").val("<?php print_r($str_param2);?>"); 
            break;
          case "tiga_bulan":
                $("#triwulan").val("<?php print_r($str_param1);?>");
                $("#th_triwulan").val("<?php print_r($str_param2);?>"); 
            break;
          case "pertahun":
                $("#th_start").val("<?php print_r($str_param1);?>");
                $("#th_finish").val("<?php print_r($str_param2);?>"); 
            break;
          default:
            // code block
        }      
    }

    function set_default_value(){
        $("#tgl_start").val("<?php print_r(date("Y-m-d"))?>");
        $("#tgl_finish").val("<?php print_r(date("Y-m-d"))?>");

        $("#triwulan").val("1-3");
        $("#th_triwulan").val("<?php print_r(date("Y"))?>");

        $("#bulan").val("1");
        $("#th").val("<?php print_r(date("Y"))?>");

        $("#th_start").val("<?php print_r(date("Y"))?>");
        $("#th_finish").val("<?php print_r(date("Y"))?>");
    }

    $("#jenis_filter").change(function(){
        var jenis_filter = $(this).val();
        filter_show();        
    });

    function filter_show(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                $("#row_per_tgl").removeAttr("hidden", true);

                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "per_bulan":
                $("#row_per_tgl").attr("hidden", true);

                $("#row_per_bulan").removeAttr("hidden", true);
                
                $("#row_tiga_bulan").attr("hidden", true);
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "tiga_bulan":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);

                $("#row_tiga_bulan").removeAttr("hidden", true);
                
                $("#row_pertahun").attr("hidden", true);  
            break;
          case "pertahun":
                $("#row_per_tgl").attr("hidden", true);
                $("#row_per_bulan").attr("hidden", true);
                $("#row_tiga_bulan").attr("hidden", true);

                $("#row_pertahun").removeAttr("hidden", true);  
            break;
          default:
            // code block
        }
    }

    $("#lanjut").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                send_per_tgl();
            break;
          case "per_bulan":
                send_per_bulan(); 
            break;
          case "tiga_bulan":
                send_tiga_bulan();  
            break;
          case "pertahun":
                send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var customer = $("#customer").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualancustomer/get_penjualan_customer_tgl/"+tgl_start+"/"+tgl_finish+"/"+customer;
    }

    function send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var customer = $("#customer").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualancustomer/get_penjualan_customer_triwulan/"+triwulan+"/"+th_triwulan+"/"+customer;
    }

    function send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var customer = $("#customer").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualancustomer/get_penjualan_customer_bulan/"+bulan+"/"+th+"/"+customer;
    }

    function send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var customer = $("#customer").val();

        window.location.href = "<?php print_r(base_url())?>report/reportpenjualancustomer/get_penjualan_customer_th/"+th_start+"/"+th_finish+"/"+customer;
    }




    $("#btn_print").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                print_send_per_tgl();
            break;
          case "per_bulan":
                print_send_per_bulan(); 
            break;
          case "tiga_bulan":
                print_send_tiga_bulan();  
            break;
          case "pertahun":
                print_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function print_send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var customer = $("#customer").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/print_get_penjualan_customer_tgl/"+tgl_start+"/"+tgl_finish+"/"+customer, "_blank");
    }

    function print_send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/print_get_penjualan_customer_triwulan/"+triwulan+"/"+th_triwulan+"/"+customer, "_blank");
    }

    function print_send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/print_get_penjualan_customer_bulan/"+bulan+"/"+th+"/"+customer, "_blank");
    }

    function print_send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/print_get_penjualan_customer_th/"+th_start+"/"+th_finish+"/"+customer, "_blank");
    }




    $("#btn_excel").click(function(){
        var jenis_filter = $("#jenis_filter").val();
        switch(jenis_filter) {
          case "per_tgl":
                excel_send_per_tgl();
            break;
          case "per_bulan":
                excel_send_per_bulan(); 
            break;
          case "tiga_bulan":
                excel_send_tiga_bulan();  
            break;
          case "pertahun":
                excel_send_pertahun();  
            break;
          default:
            // code block
        }
    });

    function excel_send_per_tgl(){
        var tgl_start = $("#tgl_start").val();
        var tgl_finish = $("#tgl_finish").val();
        var customer = $("#customer").val();

        var str_tgl_start = new Date();
        var str_tgl_finish = new Date();


        // console.log(str_tgl_finish);

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/excel_get_penjualan_customer_tgl/"+tgl_start+"/"+tgl_finish+"/"+customer, "_blank");
    }

    function excel_send_tiga_bulan(){
        var triwulan = $("#triwulan").val();
        var th_triwulan = $("#th_triwulan").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/excel_get_penjualan_customer_triwulan/"+triwulan+"/"+th_triwulan+"/"+customer, "_blank");
    }

    function excel_send_per_bulan(){
        var bulan = $("#bulan").val();
        var th = $("#th").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/excel_get_penjualan_customer_bulan/"+bulan+"/"+th+"/"+customer, "_blank");
    }

    function excel_send_pertahun(){
        var th_start = $("#th_start").val();
        var th_finish = $("#th_finish").val();
        var customer = $("#customer").val();

        window.open("<?php print_r(base_url())?>report/reportpenjualancustomer/excel_get_penjualan_customer_th/"+th_start+"/"+th_finish+"/"+customer, "_blank");
    }
    
</script>