<!DOCTYPE html>
<html>
<head>
    <title>Laporan Arus Kas PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN TRANSAKSI ARUS KAS PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
        <br>

        <table width="90%" border="0" align="center">
            <tr>
                <td width="15%" align="left"></td>
                <td width="40%"><b></b></td>
                <td width="3%"></td>
                <td width="25%"></td>
                <td width="3%"></td>
                <td></td>
                <td width="3%"></td>
            </tr>
        <!-- Penjualan -->
            <tr>
                <td colspan="2"><b>1. Penjualan</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                $t_list_data_penjualan = 0;
                if(isset($list_data_penjualan)){
                    foreach ($list_data_penjualan as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Penjualan Kepada ".$value->nama_rekanan."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format($value->total_pembayaran_pnn_tr_header,2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_penjualan += $value->total_pembayaran_pnn_tr_header;          
                    }
                }
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_penjualan,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_penjualan,2,',','.'));?></td>
                <td></td>
            </tr>

        <!-- Penjualan -->

            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>

        <!-- Piutang -->
            <tr>
                <td colspan="2"><b>2. Penerimaan Piutang</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                $t_list_data_piutang = 0;
                if(isset($list_data_piutang)){
                    foreach ($list_data_piutang as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Penerimaan Piutang Dari ".$value->nama_rekanan."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format($value->total_pembayaran_pnn_tr_header,2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_piutang += $value->total_pembayaran_pnn_tr_header;          
                    }
                }
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_piutang,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_piutang,2,',','.'));?></td>
                <td></td>
            </tr>
        <!-- Piutang -->

            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>

        <!-- Pembelian -->
            <tr>
                <td colspan="2"><b>3. Pembelian</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                $t_list_data_pembelian = 0;
                if(isset($list_data_pembelian)){
                    foreach ($list_data_pembelian as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Pembelian Kepada ".$value->nama_suplier."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format((-1*$value->total_pembayaran_pnn_tr_header),2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_pembelian += -1*$value->total_pembayaran_pnn_tr_header;          
                    }
                }
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_pembelian,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_pembelian,2,',','.'));?></td>
                <td></td>
            </tr>
        <!-- Pembelian -->

            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>

        <!-- Hutang -->
            <tr>
                <td colspan="2"><b>4. Pembeyaran Hutang</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php
                $t_all = 0;
                $t_list_data_hutang = 0;
                if(isset($list_data_hutang)){
                    foreach ($list_data_hutang as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Pelunasan Hutang Kepada ".$value->nama_suplier."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format((-1*$value->total_pembayaran_pnn_tr_header),2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_hutang += $value->total_pembayaran_pnn_tr_header;          
                    }
                }

                $t_list_data_hutang = -1*$t_list_data_hutang;
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_hutang,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_hutang,2,',','.'));?></td>
                <td></td>
            </tr>
        <!-- Hutang -->

            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>

        <!-- Retur Penjualan -->
            <tr>
                <td colspan="2"><b>5. Retur Penjualan</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                $t_list_data_retur_penjualan = 0;
                if(isset($list_data_retur_penjualan)){
                    foreach ($list_data_retur_penjualan as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Pendapatan Tambahan Retur Oleh ".$value->nama_rekanan."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format($value->selisih,2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_retur_penjualan += $value->selisih;          
                    }
                }
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_retur_penjualan,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_retur_penjualan,2,',','.'));?></td>
                <td></td>
            </tr>
        <!-- Retur Penjualan -->

            
            <tr>
                <td colspan="7">&nbsp;</td>
            </tr>

        <!-- Retur Pembelian -->
            <tr>
                <td colspan="2"><b>6. Retur Pembelian</b></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <?php
                $t_list_data_retur_pembelian = 0;
                if(isset($list_data_retur_pembelian)){
                    foreach ($list_data_retur_pembelian as $key => $value) {
                        print_r("<tr>
                                    <td align=\"center\">".$value->id_tr_header."</td>
                                    <td>Pembayaran Tambahan Retur Kepada ".$value->nama_suplier."</td>
                                    <td align=\"center\">:</td>
                                    <td align=\"right\">Rp. ".number_format((-1*$value->selisih),2,',','.')."</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>");

                        $t_list_data_retur_pembelian += -1*$value->selisih;          
                    }
                }
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"><hr></td>
                <td align="center">+</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td align="center"></td>
                <td><b>Jumlah</b></td>
                <td align="center">:</td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_retur_pembelian,2,',','.'));?></td>
                <td></td>
                <td align="right">Rp. <?php print_r(number_format($t_list_data_retur_pembelian,2,',','.'));?></td>
                <td></td>
            </tr>
        <!-- Retur Pembelian -->

            <tr>
                <td></td>
                <td align="center" colspan="3"></td>
                <td align="center"></td>
                <td><hr></td>
                <td align="center">+</td>
            </tr>
            <?php
                $t_all = $t_list_data_penjualan + $t_list_data_piutang + $t_list_data_pembelian + $t_list_data_hutang + $t_list_data_retur_pembelian + $t_list_data_retur_penjualan;
            ?>
            <tr>
                <td></td>
                <td align="center" colspan="3"></td>
                <td align="center"></td>
                <td align="right"><b>Rp. <?php print_r(number_format($t_all,2,',','.'));?></b></td>
                <td align="center"></td>
            </tr>
        </table>


        <br><br>


        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>
    </center>
</body>

<script type="text/javascript">window.print();</script>
</html>

