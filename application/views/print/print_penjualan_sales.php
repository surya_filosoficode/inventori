<!DOCTYPE html>
<html>
<head>
    <title>Laporan Penjualan Sales PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN TRANSAKSI PENJUALAN SALES PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
        <table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">Tanggal</th>
                    <th style="font-weight: bold;">No Faktur</th>
                    <th style="font-weight: bold;">Sales</th>
                    <th style="font-weight: bold;">Pelanggan</th>
                    <th style="font-weight: bold;">Subtotal</th>
                    <th style="font-weight: bold;">Diskon</th>
                    <th style="font-weight: bold;">Pajak</th>
                    <th style="font-weight: bold;">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_data)){
                        $t_bayar = 0;
                        print_r("
                                <tr v-if=\"haveData\" style=\"font-weight:bold;\">
                                  <td colspan=\"8\"><hr></td>
                                </tr>");

                        foreach ($list_data as $key => $value) {
                            print_r("
                                    <tr>
                                        <td>".$value->tgl_transaksi_tr_header."</td>
                                        <td>".$value->id_tr_header."</td>
                                        <td>".$value->nama_sales."</td>
                                        <td>".$value->nama_rekanan."</td>
                                        <td align=\"right\">Rp. ".number_format($value->total_pembayaran_tr_header, 2, ',', '.')."</td>
                                        <td align=\"right\">".$value->disc_all_tr_header." %</td>
                                        <td align=\"right\">".$value->ppn_tr_header." %</td>
                                        <td align=\"right\">Rp. ".number_format($value->total_pembayaran_pnn_tr_header, 2, ',', '.')."</td>
                                    </tr>
                                ");

                            $t_bayar += $value->total_pembayaran_pnn_tr_header;
                        }

                        print_r("
                                <tr v-if=\"haveData\" style=\"font-weight:bold;\">
                                  <td colspan=\"8\"><hr></td>
                                </tr>
                                <tr v-if=\"haveData\" style=\"font-weight:bold;\">
                                  <td colspan=\"7\">Grand Total</td> 
                                  <td align=\"right\">Rp. ".number_format($t_bayar, 2, ',', '.')."</td>
                                </tr>"); 
                    }
                ?>
            </tbody>
        </table>
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>
    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

