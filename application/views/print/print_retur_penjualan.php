<!DOCTYPE html>
<html>
<head>
    <title>Laporan Retur Penjualan PT. Blesindo Farma</title>
</head>
<body>
    <center>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <td align='center' style='vertical-align:top'>  
                <span style='font-size:24pt'><b>LAPORAN TRANSAKSI RETUR PENJUALAN PT. BLESSINDO FARMA</b></span>
                <h4><?php print_r($str_periode);?></h4>  
            </td>
        </table>
        <br>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Produk Retur Penjualan</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No</th>
                    <th style="font-weight: bold;">Nama Obat</th>
                    <th style="font-weight: bold;">Exp Date</th>
                    <th style="font-weight: bold;">Kode Produksi</th>
                    <th style="font-weight: bold;">Retur</th>
                    <th style="font-weight: bold;">Retur (Rusak)</th>
                    <th style="font-weight: bold;">Total</th>
                    <th style="font-weight: bold;">Harga Satuan</th>
                    <th style="font-weight: bold;">Jumlah Uang</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $t_harga = 0;
                    $t_harga_p = 0;
                    $t_harga_all = 0;
                    if(isset($list_sample)){
                        $no = 1;

                        $t_harga = 0;
                        foreach ($list_sample as $key => $value) {
                            print_r("
                                <tr>
                                    <td>".$no."</td>
                                    <td>".$value["detail"]["nama_item"]."</td>
                                    <td>".$value["detail"]["tgl_kadaluarsa_item"]."</td>
                                    <td>".$value["detail"]["kode_produksi_item"]."</td>
                                    <td align=\"right\">".$value["t_item"]."</td>
                                    <td align=\"right\">".$value["t_item_rusak"]."</td>
                                    <td align=\"right\">".((int)$value["t_item"] + (int)$value["t_item_rusak"])."</td>
                                    <td align=\"right\">Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.')."</td>
                                    <td align=\"right\">Rp. ".number_format($value["t_harga"], 2, ',', '.')."</td>
                                </tr>");

                            $t_harga += $value["t_harga"];

                            $no++;
                        }
                    }
                ?>
                <tr style="font-weight:bold;">
                    <td colspan="8">Grand Total</td> 
                    <td align="right">Rp. <?php print_r(number_format($t_harga, 2, ',', '.'));?></td>
                </tr>
            </tbody>
        </table>

        <br>
        <table style='font-family:calibri; border-collapse: collapse; margin-bottom: 20px' border = '0' >
            <tr>
                <td>&nbsp;</td>    
            </tr>
            <tr>
                <td align="left"><label>Daftar Produk Pengganti</label></td>
                <!-- <hr> -->
            </tr>
        </table>
        <table cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="1">
            <thead>
                <tr>
                    <th style="font-weight: bold;">No</th>
                    <th style="font-weight: bold;">Nama Obat</th>
                    <th style="font-weight: bold;">Exp Date</th>
                    <th style="font-weight: bold;">Kode Produksi</th>
                    <th style="font-weight: bold;">Pengembalian</th>
                    <th style="font-weight: bold;">Total</th>
                    <th style="font-weight: bold;">Harga Satuan</th>
                    <th style="font-weight: bold;">Jumlah Uang</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if(isset($list_pengganti)){
                        $no = 1;

                        $t_harga_p = 0;
                        foreach ($list_pengganti as $key => $value) {
                            print_r("
                                <tr>
                                    <td>".$no."</td>
                                    <td>".$value["detail"]["nama_item"]."</td>
                                    <td>".$value["detail"]["tgl_kadaluarsa_item"]."</td>
                                    <td>".$value["detail"]["kode_produksi_item"]."</td>
                                    <td align=\"right\">".$value["t_item"]."</td>
                                    <td align=\"right\">".$value["t_item"]."</td>
                                    <td align=\"right\">Rp. ".number_format($value["detail"]["harga_bruto"], 2, ',', '.')."</td>
                                    <td align=\"right\">Rp. ".number_format($value["t_harga"], 2, ',', '.')."</td>
                                </tr>");

                            $t_harga_p += $value["t_harga"];

                            $no++;
                        }

                        $t_harga_all = $t_harga_p - $t_harga;
                    }
                ?>
                <tr style="font-weight:bold;">
                  <td colspan="7">Grand Total</td> 
                  <td align="right">Rp. <?php print_r(number_format($t_harga_p, 2, ',', '.'));?></td>
                </tr>
                <tr style="font-weight:bold;">
                  <td colspan="7">Selisih Harga Sample dan Pengganti</td> 
                  <td align="right">Rp. <?php print_r(number_format($t_harga_all, 2, ',', '.'));?></td>
                </tr>
            </tbody>
        </table>
        <table align="right" cellspacing='0' style='width:60%; font-size:10pt; font-family:calibri;  border-collapse: collapse; margin-top: 20px;' border="0">
            <tr >
                <td style='padding-right:30px; text-align: center;'>Surabaya, 15 Oktober 2019</td>
            </tr>
            <tr >
                <td style='padding-bottom: 65px; padding-right:30px; text-align: center;'>PT. BLESSINDO FARMA</td>
            </tr>
            <tr>
                <td style="text-decoration: underline; padding-right:30px; text-align: center;">Yuliani Lemantara, Ssi, Apt.</td>
            </tr>
            <tr>
                <td style='padding-right:30px; text-align: center;'>19760310/SIKA-35.78/2016/2219</td>
            </tr>
        </table>
    </center>
</body>
<script type="text/javascript">window.print();</script>
</html>

