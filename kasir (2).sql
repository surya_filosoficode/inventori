-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2019 at 03:40 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasir`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`id_tipe_admin` VARCHAR(3), `email` TEXT, `username` VARCHAR(15), `password` TEXT, `status_active` ENUM('0','1','2'), `nama_admin` VARCHAR(64), `nip_admin` VARCHAR(64)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, id_tipe_admin, email, username, password, status_active, nama_admin, nip_admin, '0');
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_item` (`id_brand_in` INT, `nama_item` TEXT, `satuan` VARCHAR(32), `kode_produksi_item` TEXT, `tgl_kadaluarsa_item` DATE, `harga_bruto` VARCHAR(64), `harga_netto` VARCHAR(64), `harga_jual` VARCHAR(64), `admin_create_item` VARCHAR(12), `time_up_item` DATETIME, `is_del_item` ENUM('0','1')) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from item WHERE id_brand = id_brand_in;
        
  select id_item into last_key_user from item
  	WHERE id_brand = id_brand_in
    order by id_item desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat(LPAD(id_brand_in, 3, '0'), "00001");
  else
    set fix_key_user = concat(LPAD(id_brand_in, 3, '0'), LPAD(RIGHT(last_key_user,5)+1, 5, '0'));
      
  END IF;
  
  
  insert into item values(fix_key_user, id_brand_in, nama_item, kode_produksi_item, tgl_kadaluarsa_item, '0', '0', satuan, harga_bruto, harga_netto, harga_jual, admin_create_item, time_up_item, is_del_item);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pb_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `disc_item_tr_detail` VARCHAR(8), `harga_total_fix_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_pb_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_pb_detail values(fix_key_user, id_tr_header_in, id_item, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, disc_item_tr_detail, harga_total_fix_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pb_tr_header` (`id_suplier` VARCHAR(13), `tgl_transaksi_tr_header` DATE, `cara_pembayaran_tr_header` ENUM('0','1'), `tempo_tr_header` DATE, `status_hutang` ENUM('0','1'), `total_pembayaran_tr_header` VARCHAR(32), `disc_all_tr_header` VARCHAR(8), `total_pembayaran_disc_tr_header` VARCHAR(32), `ppn_tr_header` VARCHAR(8), `total_pembayaran_pnn_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_pb_header where substr(id_tr_header,4,8) = left(NOW()+0, 8);
        
  select id_tr_header into last_key_user from tr_pb_header where 	substr(id_tr_header,4,8) = left(NOW()+0, 8) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TBH", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("TBH", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into tr_pb_header values(fix_key_user, id_suplier,  tgl_transaksi_tr_header, cara_pembayaran_tr_header, tempo_tr_header, status_hutang, total_pembayaran_tr_header, disc_all_tr_header, total_pembayaran_disc_tr_header, ppn_tr_header, total_pembayaran_pnn_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_record_stok` (`id_tr_detail` VARCHAR(19), `id_item` VARCHAR(10), `tgl_insert` DATE, `keterangan_record_stok` TEXT, `jenis_record_stok` VARCHAR(64), `status_record_stok` VARCHAR(64), `stok_awal_record_stok` INT(11), `stok_tr_record_stok_before` INT(11), `stok_tr_record_stok` INT(11), `stok_akhir_record_stok` INT(11), `admin_create_record_stok` VARCHAR(12), `time_up_record_stok` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from record_stok where substr(id_record,4,8) = left(NOW()+0, 8);
        
  select id_record into last_key_user from record_stok where 	substr(id_record,4,8) = left(NOW()+0, 8) order by id_record desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("RCI", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("RCI", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into record_stok values(fix_key_user, id_tr_detail, id_item, tgl_insert, keterangan_record_stok, jenis_record_stok, status_record_stok, stok_awal_record_stok, stok_tr_record_stok_before, stok_tr_record_stok, stok_akhir_record_stok, admin_create_record_stok, time_up_record_stok, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rekanan` (`nama_rekanan` TEXT, `email_rekanan` TEXT, `tlp_rekanan` VARCHAR(13), `alamat_ktr_rekanan` TEXT, `alamat_krm_rekanan` TEXT, `website_rekanan` TEXT, `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from rekanan where substr(id_rekanan,4,6) = left(NOW()+0, 6);
        
  select id_rekanan into last_key_user from rekanan
  	where substr(id_rekanan,4,6) = left(NOW()+0, 6)
  	order by id_rekanan desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("RKA", left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("RKA", left(NOW()+0, 6), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into rekanan values(fix_key_user, nama_rekanan, email_rekanan,  tlp_rekanan, alamat_ktr_rekanan, alamat_krm_rekanan, website_rekanan, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sales` (`nama_sales` VARCHAR(64), `jk_sales` ENUM('0','1'), `alamat_sales` TEXT, `tlp_sales` VARCHAR(13), `nik_sales` VARCHAR(16), `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(11) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from sales where substr(id_sales,3,4) = left(NOW()+0, 4);
        
  select id_sales into last_key_user from sales
  	where substr(id_sales,3,4) = left(NOW()+0, 4)
  	order by id_sales desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("SL", left(NOW()+0, 4),"0001");
  else
      	set fix_key_user = concat("SL", left(NOW()+0, 4), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into sales values(fix_key_user, nama_sales, jk_sales,  alamat_sales, tlp_sales, nik_sales, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_sm_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_sm_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_sm_detail values(fix_key_user, id_tr_header_in, id_item, kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_sm_tr_header` (`id_customer` VARCHAR(13), `atas_nama` TEXT, `tgl_transaksi_tr_header` DATE, `total_pembayaran_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_sm_header where substr(id_tr_header,4,8) = left(NOW()+0, 8);
        
  select id_tr_header into last_key_user from tr_sm_header where 	substr(id_tr_header,4,8) = left(NOW()+0, 8) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TSH", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("TSH", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into tr_sm_header values(fix_key_user, id_customer, atas_nama, tgl_transaksi_tr_header, total_pembayaran_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_suplier` (`nama_suplier` TEXT, `email_suplier` TEXT, `tlp_suplier` VARCHAR(13), `alamat_ktr_suplier` TEXT, `alamat_krm_suplier` TEXT, `website` TEXT, `is_delete` ENUM('0','1'), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(14) CHARSET latin1 BEGIN
  declare last_key_user varchar(14);
  declare count_row_user int;
  declare fix_key_user varchar(14);
   
  select count(*) into count_row_user from suplier where substr(id_suplier,4,6) = left(NOW()+0, 6);
        
  select id_suplier into last_key_user from suplier
  	where substr(id_suplier,4,6) = left(NOW()+0, 6)
  	order by id_suplier desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("SUP", left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("SUP", left(NOW()+0, 6), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));
      
  END IF;
  
  
  insert into suplier values(fix_key_user, nama_suplier, email_suplier,  tlp_suplier, alamat_ktr_suplier, alamat_krm_suplier, website, is_delete, time_update, id_admin);
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_tr_detail` (`id_tr_header_in` VARCHAR(15), `id_item` VARCHAR(10), `kode_produksi_tr_detail` TEXT, `tgl_kadaluarsa_tr_detail` DATE, `harga_satuan_tr_detail` VARCHAR(32), `jml_item_tr_detail` VARCHAR(8), `harga_total_tr_detail` VARCHAR(32), `disc_item_tr_detail` VARCHAR(8), `harga_total_fix_tr_detail` VARCHAR(32), `admin_create_tr_detail` VARCHAR(12), `time_up_tr_detail` DATETIME) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(19);
  declare count_row_user int;
  declare fix_key_user varchar(19);
   
  select count(*) into count_row_user from tr_detail where substr(id_tr_detail,1,15) = id_tr_header_in;
        
  select id_tr_detail into last_key_user from tr_detail where substr(id_tr_detail,1,15) = id_tr_header_in order by id_tr_detail desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat(id_tr_header_in, "-001");
  else
    set fix_key_user = concat(id_tr_header_in, "-", LPAD(RIGHT(last_key_user, 3)+1, 3, '0'));  
  END IF;
  
  insert into tr_detail values(fix_key_user, id_tr_header_in, id_item,  kode_produksi_tr_detail, tgl_kadaluarsa_tr_detail, harga_satuan_tr_detail, jml_item_tr_detail, harga_total_tr_detail, disc_item_tr_detail, harga_total_fix_tr_detail, admin_create_tr_detail, time_up_tr_detail, "0");
    
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_tr_header` (`id_customer` VARCHAR(13), `id_sales` VARCHAR(10), `tgl_transaksi_tr_header` DATE, `cara_pembayaran_tr_header` ENUM('0','1'), `tempo_tr_header` DATE, `status_hutang` ENUM('0','1'), `total_pembayaran_tr_header` VARCHAR(32), `disc_all_tr_header` VARCHAR(8), `total_pembayaran_disc_tr_header` VARCHAR(32), `ppn_tr_header` VARCHAR(8), `total_pembayaran_pnn_tr_header` VARCHAR(32), `admin_create_tr_header` VARCHAR(12), `time_up_tr_header` DATETIME) RETURNS VARCHAR(15) CHARSET latin1 BEGIN
  declare last_key_user varchar(15);
  declare count_row_user int;
  declare fix_key_user varchar(15);
   
  select count(*) into count_row_user from tr_header where substr(id_tr_header,4,8) = left(NOW()+0, 8);
        
  select id_tr_header into last_key_user from tr_header where 	substr(id_tr_header,4,8) = left(NOW()+0, 8) order by id_tr_header desc limit 1;
  if(count_row_user <1) then
  	set fix_key_user = concat("TRH", left(NOW()+0, 8), "0001");
  else
    set fix_key_user = concat("TRH", left(NOW()+0, 8), LPAD(RIGHT(last_key_user,4)+1, 4, '0'));  
  END IF;
  
  insert into tr_header values(fix_key_user, id_customer, id_sales,  tgl_transaksi_tr_header, cara_pembayaran_tr_header, tempo_tr_header, status_hutang, total_pembayaran_tr_header, disc_all_tr_header, total_pembayaran_disc_tr_header, ppn_tr_header, total_pembayaran_pnn_tr_header, admin_create_tr_header, time_up_tr_header, "0");
    
  
  return fix_key_user;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AD2019110001', '0', 'suryahanggara@gmail.com', 'surya', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya', '20190001', '0'),
('AD2019110002', '0', 'sudirman@gmail.com', 'sudirman', '21232f297a57a5a743894a0e4a801fc3', '1', 'sudirman', '7838372', '0');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id_brand` int(11) NOT NULL,
  `nama_brand` text NOT NULL,
  `keterangan_brand` text NOT NULL,
  `admin_create_brand` varchar(12) NOT NULL,
  `time_up_brand` datetime NOT NULL,
  `is_del_brand` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id_brand`, `nama_brand`, `keterangan_brand`, `admin_create_brand`, `time_up_brand`, `is_del_brand`) VALUES
(1, 'Omega', '-', 'AD2019110001', '2019-11-13 14:05:14', '0'),
(2, 'Probosx', 'Nordikx', 'AD2019110001', '2019-11-13 14:05:47', '0');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id_item` varchar(10) NOT NULL,
  `id_brand` int(11) NOT NULL,
  `nama_item` text NOT NULL,
  `kode_produksi_item` text NOT NULL,
  `tgl_kadaluarsa_item` date NOT NULL,
  `stok` int(11) NOT NULL,
  `stok_opnam` int(11) NOT NULL,
  `satuan` varchar(32) NOT NULL,
  `harga_bruto` varchar(64) NOT NULL,
  `harga_netto` varchar(64) NOT NULL,
  `harga_jual` varchar(64) NOT NULL,
  `admin_create_item` varchar(12) NOT NULL,
  `time_up_item` datetime NOT NULL,
  `is_del_item` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id_item`, `id_brand`, `nama_item`, `kode_produksi_item`, `tgl_kadaluarsa_item`, `stok`, `stok_opnam`, `satuan`, `harga_bruto`, `harga_netto`, `harga_jual`, `admin_create_item`, `time_up_item`, `is_del_item`) VALUES
('00100001', 1, 'omega am 1', '', '0000-00-00', 0, 0, 'pcs', '13000', '13000', '20000', 'AD2019110001', '2019-11-13 14:16:32', '0'),
('00100002', 1, 'titan', '', '0000-00-00', 0, 0, 'pcs', '12000', '12000', '15000', 'AD2019110001', '2019-11-13 14:17:08', '0'),
('00100004', 1, 'titan', '', '0000-00-00', 1980, 0, 'pcs', '15000', '15000', '25000', 'AD2019110001', '2019-11-13 14:23:45', '0'),
('00100005', 1, 'predux', '', '0000-00-00', 0, 0, 'pcs', '11000', '11000', '15000', 'AD2019110001', '2019-11-13 14:27:42', '0'),
('00100006', 1, 'donx', '', '0000-00-00', 0, 0, 'pcs', '5000', '5000', '10000', 'AD2019110001', '2019-11-13 14:33:11', '0'),
('00100007', 1, 'mega man', '', '0000-00-00', 0, 0, 'pcs', '10000', '10000', '15000', 'AD2019110001', '2019-11-18 14:51:25', '0'),
('00100008', 1, 'dsad', '9u98hj9f', '2020-12-12', 0, 0, 'dsad', '10000', '10000', '12000', 'AD2019110001', '2019-11-20 12:29:44', '0'),
('00100009', 1, 'titan', '9u98hj9', '2020-12-12', 90, 0, 'pcs', '10000', '10000', '10000', 'AD2019110001', '2019-11-20 12:30:12', '0'),
('00200003', 2, 'pre under', '', '0000-00-00', 990, 0, 'pcs', '10000', '10000', '15000', 'AD2019110001', '2019-11-13 14:17:43', '0'),
('00200004', 2, 'tooner', '', '0000-00-00', 3000, 0, 'pcs', '15000', '15000', '25000', 'AD2019110001', '2019-11-13 14:18:15', '0'),
('00200005', 2, 'test', '', '0000-00-00', 0, 0, 'pcsx', '10000', '10000', '10000', 'AD2019110001', '0000-00-00 00:00:00', '0'),
('00200006', 2, 'untuk', '', '0000-00-00', 0, 0, 'pcs', '2000', '2000', '5000', 'AD2019110001', '2019-11-13 14:33:36', '0'),
('00200007', 2, 'titan', 'pcs', '0000-00-00', 0, 0, '2020-12-12', '10000', '10000', '15000', 'AD2019110001', '2019-11-20 12:09:54', '0'),
('00200008', 2, 'titan', 'pcs', '0000-00-00', 0, 0, '2020-12-12', '10000', '10000', '15000', 'AD2019110001', '2019-11-20 12:10:19', '0');

-- --------------------------------------------------------

--
-- Table structure for table `record_stok`
--

CREATE TABLE `record_stok` (
  `id_record` varchar(15) NOT NULL,
  `id_tr_detail` varchar(19) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `tgl_insert` date NOT NULL,
  `keterangan_record_stok` text NOT NULL,
  `jenis_record_stok` varchar(64) NOT NULL,
  `status_record_stok` varchar(64) NOT NULL,
  `stok_awal_record_stok` int(11) NOT NULL,
  `stok_tr_record_stok_before` int(11) NOT NULL,
  `stok_tr_record_stok` int(11) NOT NULL,
  `stok_akhir_record_stok` int(11) NOT NULL,
  `admin_create_record_stok` varchar(12) NOT NULL,
  `time_up_record_stok` datetime NOT NULL,
  `is_del_record_stok` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record_stok`
--

INSERT INTO `record_stok` (`id_record`, `id_tr_detail`, `id_item`, `tgl_insert`, `keterangan_record_stok`, `jenis_record_stok`, `status_record_stok`, `stok_awal_record_stok`, `stok_tr_record_stok_before`, `stok_tr_record_stok`, `stok_akhir_record_stok`, `admin_create_record_stok`, `time_up_record_stok`, `is_del_record_stok`) VALUES
('RCI201911180001', 'TSH201911180001-001', '00100004', '2019-11-18', 'sample detail', 'insert_sample', 'insert', 2000, 0, 20, 1980, '', '2019-11-18 12:55:45', '0'),
('RCI201911180002', 'TSH201911180001-002', '00200003', '2019-11-18', 'sample detail', 'insert_sample', 'insert', 1000, 0, 10, 990, '', '2019-11-18 12:55:45', '0'),
('RCI201911180003', 'TSH201911180001-001', '00100004', '2019-11-18', 'sample detail', 'update_sample', 'update', 1980, 20, 20, 1980, '', '2019-11-18 12:57:09', '0'),
('RCI201911180004', 'TSH201911180001-002', '00200003', '2019-11-18', 'sample detail', 'update_sample', 'delete', 990, 0, 10, 1000, '', '2019-11-18 12:57:09', '0'),
('RCI201911180005', 'TSH201911180001-001', '00100004', '2019-11-18', 'sample detail', 'delete_sample', 'delete', 1980, 0, 20, 2000, '', '2019-11-18 12:57:50', '0'),
('RCI201911180006', 'TSH201911180002-001', '00100004', '2019-11-18', 'sample detail', 'update_sample', 'insert', 2000, 0, 20, 1980, '', '2019-11-18 02:41:31', '0'),
('RCI201911180007', 'TSH201911180002-002', '00200003', '2019-11-18', 'sample detail', 'update_sample', 'insert', 1000, 0, 10, 990, '', '2019-11-18 02:41:31', '0'),
('RCI201911200001', 'TBH201911200002-001', '00100009', '2019-11-20', 'pembelian detail', 'insert_pembelian', 'insert', 0, 0, 100, 100, '', '2019-11-20 09:41:55', '0'),
('RCI201911200002', 'TSH201911200001-001', '00100009', '2019-11-20', 'sample detail', 'insert_sample', 'insert', 100, 0, 10, 90, '', '2019-11-20 10:42:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `rekanan`
--

CREATE TABLE `rekanan` (
  `id_rekanan` varchar(13) NOT NULL,
  `nama_rekanan` text NOT NULL,
  `email_rekanan` text NOT NULL,
  `tlp_rekanan` varchar(13) NOT NULL,
  `alamat_ktr_rekanan` text NOT NULL,
  `alamat_krm_rekanan` text NOT NULL,
  `website_rekanan` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rekanan`
--

INSERT INTO `rekanan` (`id_rekanan`, `nama_rekanan`, `email_rekanan`, `tlp_rekanan`, `alamat_ktr_rekanan`, `alamat_krm_rekanan`, `website_rekanan`, `is_delete`, `time_update`, `id_admin`) VALUES
('RKA2019110001', 'RS Wahidin Husodo', 'rs.wahidin@gmail.com', '081354354354', 'malang', 'malang', 'rs.wahidin.com', '0', '2019-11-13 02:01:48', ''),
('RKA2019110002', 'dsad', 'sadsa@gmail.com', 'dsad', 'sads', 'adsadsa', 'dsdsad', '1', '2019-11-13 02:02:12', '');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id_sales` varchar(10) NOT NULL,
  `nama_sales` varchar(64) NOT NULL,
  `jk_sales` enum('0','1') NOT NULL,
  `alamat_sales` text NOT NULL,
  `tlp_sales` varchar(13) NOT NULL,
  `nik_sales` varchar(16) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id_sales`, `nama_sales`, `jk_sales`, `alamat_sales`, `tlp_sales`, `nik_sales`, `is_delete`, `time_update`, `id_admin`) VALUES
('SL20190001', 'dimas haryo', '0', 'malang', '087235124214', 'et7621te', '0', '2019-11-13 02:02:51', ''),
('SL20190002', 'Donaldx', '1', 'malangx', '081065845781x', '87y87yy8x', '0', '2019-11-13 02:03:42', '');

-- --------------------------------------------------------

--
-- Table structure for table `suplier`
--

CREATE TABLE `suplier` (
  `id_suplier` varchar(13) NOT NULL,
  `nama_suplier` text NOT NULL,
  `email_suplier` text NOT NULL,
  `tlp_suplier` varchar(13) NOT NULL,
  `alamat_ktr_suplier` text NOT NULL,
  `alamat_krm_suplier` text NOT NULL,
  `website` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suplier`
--

INSERT INTO `suplier` (`id_suplier`, `nama_suplier`, `email_suplier`, `tlp_suplier`, `alamat_ktr_suplier`, `alamat_krm_suplier`, `website`, `is_delete`, `time_update`, `id_admin`) VALUES
('SUP2019110001', 'PT Akiro Data Perkasa', 'akiro@gmail.com', '034103234', 'surabaya', 'surabaya', 'www.akiro.com', '0', '2019-11-13 01:58:07', ''),
('SUP2019110002', 'PT KAI Indonesia Tbk', 'kai@gmail.com', '0341031031', 'malang', 'malang', 'kai.com', '1', '2019-11-13 01:59:52', '');

-- --------------------------------------------------------

--
-- Table structure for table `tr_detail`
--

CREATE TABLE `tr_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `disc_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_fix_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_header`
--

CREATE TABLE `tr_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `id_sales` varchar(10) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `cara_pembayaran_tr_header` enum('0','1') NOT NULL,
  `tempo_tr_header` date NOT NULL,
  `status_hutang` enum('0','1') NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `disc_all_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_disc_tr_header` varchar(32) NOT NULL,
  `ppn_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_pnn_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tr_pb_detail`
--

CREATE TABLE `tr_pb_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `disc_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_fix_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pb_detail`
--

INSERT INTO `tr_pb_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `disc_item_tr_detail`, `harga_total_fix_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('TBH201911160001-001', 'TBH201911160001', '00200003', '10000', '1000', '10000000', '0', '10000000', '', '2019-11-16 09:59:07', '0'),
('TBH201911160001-003', 'TBH201911160001', '00100004', '15000', '2000', '30000000', '2', '29400000', '', '2019-11-16 09:59:07', '0'),
('TBH201911160001-004', 'TBH201911160001', '00200004', '15000', '3000', '45000000', '10', '40500000', '', '2019-11-16 09:59:07', '0'),
('TBH201911200001-001', 'TBH201911200001', '', '9000', '10', '90000', '0', '90000', '', '2019-11-20 09:09:04', '0'),
('TBH201911200002-001', 'TBH201911200002', '00100009', '9000', '100', '900000', '10', '810000', '', '2019-11-20 09:41:55', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_pb_header`
--

CREATE TABLE `tr_pb_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_suplier` varchar(13) NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `cara_pembayaran_tr_header` enum('0','1') NOT NULL,
  `tempo_tr_header` date NOT NULL,
  `status_hutang` enum('0','1') NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `disc_all_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_disc_tr_header` varchar(32) NOT NULL,
  `ppn_tr_header` varchar(8) NOT NULL,
  `total_pembayaran_pnn_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_pb_header`
--

INSERT INTO `tr_pb_header` (`id_tr_header`, `id_suplier`, `tgl_transaksi_tr_header`, `cara_pembayaran_tr_header`, `tempo_tr_header`, `status_hutang`, `total_pembayaran_tr_header`, `disc_all_tr_header`, `total_pembayaran_disc_tr_header`, `ppn_tr_header`, `total_pembayaran_pnn_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('TBH201911160001', 'SUP2019110001', '2019-11-16', '0', '2019-12-26', '0', '79900000', '5', '75905000', '10', '83495500', '', '2019-11-16 09:59:07', '0'),
('TBH201911200001', 'SUP2019110001', '2019-11-20', '0', '2019-11-21', '0', '90000', '19', '72900', '10', '80190', '', '2019-11-20 09:09:04', '0'),
('TBH201911200002', 'SUP2019110001', '2019-11-20', '0', '2019-02-12', '0', '810000', '10', '729000', '10', '801900', '', '2019-11-20 09:41:55', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_detail`
--

CREATE TABLE `tr_sm_detail` (
  `id_tr_detail` varchar(19) NOT NULL,
  `id_tr_header` varchar(15) NOT NULL,
  `id_item` varchar(10) NOT NULL,
  `kode_produksi_tr_detail` text NOT NULL,
  `tgl_kadaluarsa_tr_detail` date NOT NULL,
  `harga_satuan_tr_detail` varchar(32) NOT NULL,
  `jml_item_tr_detail` varchar(8) NOT NULL,
  `harga_total_tr_detail` varchar(32) NOT NULL,
  `admin_create_tr_detail` varchar(12) NOT NULL,
  `time_up_tr_detail` datetime NOT NULL,
  `is_del_tr_detail` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_detail`
--

INSERT INTO `tr_sm_detail` (`id_tr_detail`, `id_tr_header`, `id_item`, `kode_produksi_tr_detail`, `tgl_kadaluarsa_tr_detail`, `harga_satuan_tr_detail`, `jml_item_tr_detail`, `harga_total_tr_detail`, `admin_create_tr_detail`, `time_up_tr_detail`, `is_del_tr_detail`) VALUES
('TSH201911180002-001', 'TSH201911180002', '00100004', '8gh7', '2019-11-30', '25000', '20', '500000', '', '2019-11-18 02:41:31', '0'),
('TSH201911180002-002', 'TSH201911180002', '00200003', '32dsf', '2019-11-30', '15000', '10', '150000', '', '2019-11-18 02:41:31', '0'),
('TSH201911200001-001', 'TSH201911200001', '00100009', '9u98hj9', '2020-12-12', '10000', '10', '100000', '', '2019-11-20 10:42:50', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tr_sm_header`
--

CREATE TABLE `tr_sm_header` (
  `id_tr_header` varchar(15) NOT NULL,
  `id_customer` varchar(13) NOT NULL,
  `atas_nama` text NOT NULL,
  `tgl_transaksi_tr_header` date NOT NULL,
  `total_pembayaran_tr_header` varchar(32) NOT NULL,
  `admin_create_tr_header` varchar(12) NOT NULL,
  `time_up_tr_header` datetime NOT NULL,
  `is_del_tr_header` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tr_sm_header`
--

INSERT INTO `tr_sm_header` (`id_tr_header`, `id_customer`, `atas_nama`, `tgl_transaksi_tr_header`, `total_pembayaran_tr_header`, `admin_create_tr_header`, `time_up_tr_header`, `is_del_tr_header`) VALUES
('TSH201911200001', 'RKA2019110001', 'dimas', '2019-11-20', '100000', '', '2019-11-20 10:42:50', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id_brand`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `record_stok`
--
ALTER TABLE `record_stok`
  ADD PRIMARY KEY (`id_record`);

--
-- Indexes for table `rekanan`
--
ALTER TABLE `rekanan`
  ADD PRIMARY KEY (`id_rekanan`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `suplier`
--
ALTER TABLE `suplier`
  ADD PRIMARY KEY (`id_suplier`);

--
-- Indexes for table `tr_detail`
--
ALTER TABLE `tr_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_header`
--
ALTER TABLE `tr_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_pb_detail`
--
ALTER TABLE `tr_pb_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_pb_header`
--
ALTER TABLE `tr_pb_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- Indexes for table `tr_sm_detail`
--
ALTER TABLE `tr_sm_detail`
  ADD PRIMARY KEY (`id_tr_detail`);

--
-- Indexes for table `tr_sm_header`
--
ALTER TABLE `tr_sm_header`
  ADD PRIMARY KEY (`id_tr_header`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id_brand` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
